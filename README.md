# TempCDSeqEval

Framework to replicate the experiments published in:

* *On the effects of aggregation strategies for different groups of users in venue recommendation* under revision for Information Processing and Management journal (2021)

* *Challenges on evaluating venue recommendation approaches* published in Workshop on Recommenders in Tourism at RecSys2018 (RecTour2018)

* *A novel approach for venue recommendation using cross-domain techniques* published in Workshop on Intelligent Recommender Systems by Knowledge Transfer & Learning at RecSys2018 (KTL2018)

## Getting Started

These instructions will get you a copy of the project up for replicating the experiments of the papers listed above.

### Prerequisites

Java 8

MATLAB (executed in Matlab 2015a)

Maven


## Dependencies
[RankSys 0.4.3](https://github.com/RankSys/RankSys/releases/tag/0.4.3)

Rank fusion library, provided as an independent JAR.

## Instructions

* git clone https://bitbucket.org/PabloSanchezP/tempcdseqeval
* cd tempcdseqeval

* Depending on the experiments that you want to replicate, switch between branches.
  * KTLRecTour2018 for *Challenges on evaluating venue recommendation approaches* and *A novel approach for venue recommendation using cross-domain techniques*
  * master for *On the effects of aggregation strategies for different groups of users in venue recommendation*

* If you want to replicate the results from *On the effects of aggregation strategies for different groups of users in venue recommendation*, run the scripts following the order:
  * step1_ScriptGenerateMapsTransformDatasetAndPrepareSplits.sh
  * step2_ScriptRecommendationRanksysSingleDomain.sh
  * step2_ScriptRecommendationRanksysCrossDomainNNCities.sh
  * step2_ScriptRecommendationRanksysCrossDomainCountryCities.sh
  * step2_ScriptRecommendationRanksysCrossDomainIndependentCities.sh
  * step2_ScriptRecommendationIRENMF.sh
  * step3_postEval.sh


* If you want to replicate the results from RecTour2018, run the scripts following the order:
  * step1_ScriptGenerateMapsTransformDatasetAndPrepareSplits.sh
  * step2_ScriptRecommendationRanksysSingleDomain_RecommendedThingsInTrain.sh
  * step2_ScriptRecommendationIRENMFThingsInTrain.sh
  * step3_postEval.sh

* If you want to replicate the results from KTL2018, run the scripts following the order:
  * step1_ScriptGenerateMapsTransformDatasetAndPrepareSplits.sh
  * step2_ScriptRecommendationRanksysSingleDomain.sh
  * step2_ScriptRecommendationRanksysCrossDomainNNCities.sh
  * step2_ScriptRecommendationRanksysCrossDomainIndependentCities.sh
  * step2_ScriptRecommendationIRENMF.sh
  * step3_postEval.sh

* Update:
  * Added Kernel Density Estimation recommender
  * Added Mymedialite for BPR (with a Matrix factorization approach)
  * Added Cross domain for Country (all cities of a country to be used as source domain)
  * Added more POI recommendation baselines

## Additional algorithms/frameworks used in the experiments
* [MyMedialite](http://www.mymedialite.net/) (for BPR)
* We have adapted the following approaches for ranking evaluation:
  * [Exploiting geographical neighborhood characteristics for location recommendation](https://ink.library.smu.edu.sg/cgi/viewcontent.cgi?referer=https://www.google.com/&httpsredir=1&article=4772&context=sis_research) (for IRenMF) [Original source code](http://spatialkeyword.sce.ntu.edu.sg/eval-vldb17/)




## Authors

* **Pablo Sánchez** - [Universidad Autónoma de Madrid](https://uam.es/ss/Satellite/en/home.htm)
* **Alejandro Bellogín** - [Universidad Autónoma de Madrid](https://uam.es/ss/Satellite/en/home.htm)

## Contact

* **Pablo Sánchez** - <pablo.sanchezp@uam.es>

## License

This project is licensed under [the GNU GPLv3 License](LICENSE.txt)

## Acknowledgments

* This work has been funded by the Ministerio de Economía y Competitividad within the 2017 call for predoctoral contracts co-funded by the European Social Fund (ESF) and the project TIN2016-80630-P.

: '
  Script for normal ranksys recommenders with repetitions
  Script for paper "Challenges on evaluating venue recommendation approaches"

'

# !/bin/bash
# Necessary to execute with ./



# Global variables for JVM
JAR=target/CrossDomainPOIRecommendation-0.1.1-SNAPSHOT.jar
jvmMemory=-Xmx14G
ttpathfiles=TrainTest
similaritiesFolder=Sim
javaCommand=java

recPrefix=rec
simPrefix=sim

# Nomenclature for Datasets
FoursqrName=Foursqr


# Extension of Files
extensionMap=_Mapping.txt
extensionCoords=_Coords.txt
extensionFeature=_Features.txt
extensionAggregated=_Aggregated.txt

# Routes of Original Datasets
FoursqrCheckingsOriginal=$ttpathfiles/dataset_TIST2015_Checkins.txt
FoursqrCitiesOriginal=$ttpathfiles/dataset_TIST2015_Cities.txt
FoursqrPOISOriginal=$ttpathfiles/dataset_TIST2015_POIs.txt
simFolder=Sim

# cities selected to perform the experiments
cities="MX_MexicoCity BR_SaoPaulo RU_Moscow MY_KualaLumpur CL_Santiago JP_Tokyo ID_Jakarta TR_Istanbul"
PathOfSplits=$ttpathfiles/CitiesSplit
aggregateStrategy=AVERAGE
aggregateStrategy2=SUM
aggregateStrategyTime=LAST
trainingStrats="REPETITIONS Last"

recommendedFolder=RecommendationFolder
allneighbours="5 10 20 30 40 50 60 70 80 90 100"
itemsRecommended=100


# Variables for recommendation
itemsRecommended=100
featureFile=$ttpathfiles/NewItemIdsCountryCityJoin.txt
featureItemCol=0
featureCatCol=1
realFeatureFile=$ttpathfiles/POIS_Features.txt

coordFile=$ttpathfiles/POIS_Coords.txt

allKFactorizerRankSys="10 50 100"
allLambdaFactorizerRankSys="0.1 1 10"
allAlphaFactorizerRankSys="0.1 1 10"

complete_cities=complete_cities

cutoffs="5,10"

# For each city (single domain, each city individually)
for fold in 0 # 1
do
  # For each fold
  for city in $cities
  do
    if [ $city != "complete_cities" ] && [ $city != "complete" ] ; then

      trainfile=$PathOfSplits/Fold$fold/$city/"split_""$city""__trainingAggr""$aggregateStrategy""$fold".dat
      trainfileSUM=$PathOfSplits/Fold$fold/$city/"split_""$city""__trainingAggr""$aggregateStrategy2""$fold".dat
      testfile=$PathOfSplits/Fold$fold/$city/"split_""$city""__testWithTrain""$fold".dat

      recommendationFolder=$PathOfSplits/Fold$fold/$city/RecommendationFolder

      # Train with repetitions
      trainfileWithRep=$PathOfSplits/Fold$fold/$city/"split_""$city""__training""$fold".dat
      testFileOfCD=$testfile
      resultStatistics=$PathOfSplits/Fold$fold/$city/"split_""$city""StatisticsFoursqr""Fold""$fold".dat

      mkdir -p $recommendationFolder

      # Temporal Test Order
      outputRecfile=$recommendationFolder/"$recPrefix"_"$city""$fold"_ranksys_"SkylineTestOrder"TestWithRatingsTrain.txt
      $javaCommand $jvmMemory -jar $JAR -o skylineRecommenders -trf $trainfile -tsf $testfile -cIndex true -rr "SkylineTestOrder" -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol -recStrat ITEMS_IN_TRAIN

      # Temporal Test Order Reverse
      outputRecfile=$recommendationFolder/"$recPrefix"_"$city""$fold"_ranksys_"SkylineTestOrderReverse"TestWithRatingsTrain.txt
      $javaCommand $jvmMemory -jar $JAR -o skylineRecommenders -trf $trainfile -tsf $testfile -cIndex true -rr "SkylineTestOrderReverse" -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol -recStrat ITEMS_IN_TRAIN


      for ranksysRecommender in PopularityRecommender RandomRecommender
      do
        # RankSys -> popularity and random recommender
        outputRecfile=$recommendationFolder/"$recPrefix"_"$city""$fold"_ranksys_"$ranksysRecommender"TestWithRatingsTrain.txt
        $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex true -rr "$ranksysRecommender" -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol -recStrat ITEMS_IN_TRAIN
      done
      wait


      for neighbours in $allneighbours
      do

        # Ranksys with similarities UserBased
        for UBsimilarity in SetJaccardUserSimilarity SetCosineUserSimilarity
        do
          outputRecfile=$recommendationFolder/"$recPrefix"_"$city""$fold"_ranksys_UB_"$UBsimilarity"_k"$neighbours"TestWithRatingsTrain.txt
          $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex false -rr UserNeighborhoodRecommender -rs $UBsimilarity -nI $itemsRecommended -n $neighbours -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol -recStrat ITEMS_IN_TRAIN
        done
        wait # End UB sim

        # Ranksys with similarities ItemBased
        for IBsimilarity in SetJaccardItemSimilarity SetCosineItemSimilarity
        do
          outputRecfile=$recommendationFolder/"$recPrefix"_"$city""$fold"_ranksys_IB_"$IBsimilarity"_k"$neighbours"TestWithRatingsTrain.txt
          $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex false -rr ItemNeighborhoodRecommender -rs $IBsimilarity -nI $itemsRecommended -n $neighbours -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol -recStrat ITEMS_IN_TRAIN

        done
        wait # End IB Sim


      done # End Neighbours
      wait

      # HKV
      for rankRecommenderNoSim in MFRecommenderHKV
      do
        for kFactor in $allKFactorizerRankSys
        do
          for lambdaValue in $allLambdaFactorizerRankSys
          do
            for alphaValue in $allAlphaFactorizerRankSys
            do
              # Neighbours is put to 20 because this recommender does not use it
              outputRecfile=$recommendationFolder/"$recPrefix"_"$city""$fold"_ranksys_"$rankRecommenderNoSim"_kFactor"$kFactor"_aFactorizer"$alphaValue"_lFactorizer"$lambdaValue"TestWithRatingsTrain.txt
              $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex false -rr $rankRecommenderNoSim -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfile -kFactorizer $kFactor -aFactorizer $alphaValue -lFactorizer $lambdaValue -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol -recStrat ITEMS_IN_TRAIN

            done
            wait # End alpha values
          done
          wait # End lambda
        done
        wait # End KFactor
      done
      wait # End Rank Recommender


      for poiRecommender in AverageDistanceUserGEO
      do
        outputRecfile=$recommendationFolder/"$recPrefix"_"$city""$fold"_ranksysPOI_"$poiRecommender"SIMPLETestWithRatingsTrain.txt
        $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex false -rr $poiRecommender -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol -coordFile $coordFile -recStrat ITEMS_IN_TRAIN

      done # End poiRecommender
      wait


      # Using the SUM
      for poiRecommender in AverageDistanceUserGEO
      do
        outputRecfile=$recommendationFolder/"$recPrefix"_"$city""$fold"_ranksysPOI_"$poiRecommender"FREQUENCYTestWithRatingsTrain.txt
        $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfileSUM -tsf $testfile -cIndex false -rr $poiRecommender -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol -coordFile $coordFile -scoreFreq FREQUENCY -recStrat ITEMS_IN_TRAIN

      done #End AvgDis
      wait

      # Second baseline popularity, knn and minimum distance
      for poiRecommender in PopGeoNN
      do
        for UBsimilarity in SetJaccardUserSimilarity SetCosineUserSimilarity
        do
          for neighbours in 100
          do
            outputRecfile=$recommendationFolder/"$recPrefix"_"$city""$fold"_ranksysPOI_"$poiRecommender"_UBSim_"$UBsimilarity"_k"$neighbours"TestWithRatingsTrain.txt
            $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex true -rr $poiRecommender -rs $UBsimilarity -nI $itemsRecommended -n $neighbours -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol -coordFile $coordFile -recStrat ITEMS_IN_TRAIN

          done # End neighbours
          wait
        done # End UB similarity
        wait
      done # En popGeoNN
      wait


      outputRecfile=$recommendationFolder/"$recPrefix"_"$city""$fold"_ranksys_"TrainRecommender"TestWithRatingsTrain.txt
      $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex false -rr "TrainRecommender" -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol -coordFile $coordFile -recStrat ITEMS_IN_TRAIN

    fi
  done # End Cities
  wait
done # End Folds
wait


: '
Evaluation part
'

for city in $cities
do
  nonaccresultsPrefix=naeval
  if [ $city != "complete_cities" ] && [ $city != "complete" ] ; then
    # For each fold
    for fold in 0 # 1
    do

      trainfile=$PathOfSplits/Fold$fold/$city/"split_""$city""__trainingAggr""$aggregateStrategy""$fold".dat
      testfile=$PathOfSplits/Fold$fold/$city/"split_""$city""__testWithTrain""$fold".dat

      resultsFolder=$PathOfSplits/Fold$fold/$city/ResultRankSysFolder

      mkdir -p $resultsFolder

      # Find all recommenders and compute the ranksysEvaluation

      recommendedFolderReal=$PathOfSplits/Fold$fold/$city/$recommendedFolder

      find $recommendedFolderReal/ -name "$recPrefix"* | while read recFile; do
        recFileName=$(basename "$recFile" .txt) #extension removed
        if [[ $recFileName == *FREQUENCY* ]]; then
          trainfile=$PathOfSplits/Fold$fold/$city/"split_""$city""__trainingAggr""SUM""$fold".dat
        fi

        if [[ $recFileName == *TestWithRatingsTrain* ]]; then
          for evthreshold in 1 # We work with implicit data
          do
            outputResultfile=$resultsFolder/"$nonaccresultsPrefix"_EvTh"$evthreshold"_"$recFileName"_testWithTrainLCSEVCutOffs"$cutoffs".txt
            if [ ! -f "$outputResultfile" ]; then
              echo 'train file is ' $trainfile
              $javaCommand $jvmMemory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $trainfile -tsf $testfile -rf $recFile -ff $realFeatureFile -thr $evthreshold -rc $cutoffs -orf $outputResultfile -lcsEv true -onlyAcc false
            fi
          done # End evTh
          wait
        fi
      done # End find
      wait
    done # End Folds
    wait
  fi
done # End Cities
wait

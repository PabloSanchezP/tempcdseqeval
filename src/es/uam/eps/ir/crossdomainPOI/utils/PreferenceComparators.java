/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.crossdomainPOI.utils;

import java.util.Comparator;

import org.ranksys.core.util.tuples.Tuple2id;
import org.ranksys.core.util.tuples.Tuple2od;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.preferences.IdTimePref;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.preferences.IdxTimePref;
import es.uam.eps.ir.ranksys.core.preference.IdPref;
import es.uam.eps.ir.ranksys.fast.preference.IdxPref;

/***
 * Class that contain the preference comparators
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public final class PreferenceComparators {
	//All of the comparators are from lower to up
	
	//Comparator of preferences (IdxPref)
	public static Comparator<IdxPref> preferenceComparatorIdxPref = (o1, o2)-> o1.v2 != o2.v2 ? Double.compare(o1.v2, o2.v2): Integer.compare(o1.v1, o2.v1);
	
	//Comparator of indexes (IdxPref)
	public static Comparator<IdxPref> indexComparatorIdxPref = (o1, o2)-> Integer.compare(o1.v1, o2.v1);
	
	///
	//Comparator of preferences (IdxTimePref)
	public static Comparator<IdxTimePref> preferenceComparatorIdxTimePref = (o1, o2)->  o1.v2 != o2.v2 ? Double.compare(o1.v2, o2.v2): Integer.compare(o1.v1, o2.v1);

		
	//Comparator of indexes (IdxTimePref)
	public static Comparator<IdxTimePref> indexComparatorIdxTimePref = (o1, o2)-> Integer.compare(o1.v1, o2.v1);
	
	//Comparator of time (IdxTimePref)
	public static Comparator<IdxTimePref> timeComparatorIdxTimePref = (o1, o2)-> o1.v3 != o2.v3 ? Long.compare(o1.v3, o2.v3): Integer.compare(o1.v1, o2.v1);
	
	
	//Id and IdTime pref	
	
	//Comparator of preferences (IdxPref)
	public static Comparator<IdPref<Long>> preferenceComparatorIdPref = (o1, o2)-> o1.v2 != o2.v2 ?  Double.compare(o1.v2, o2.v2): (o1.v1).compareTo(o2.v1);
	
	//Comparator of indexes (IdxPref)
	public static Comparator<IdPref<Long>> idComparatorIdPref = (o1, o2)-> (o1.v1).compareTo(o2.v1);
	
	///
	//Comparator of preferences (IdTimePref)
	public static Comparator<IdTimePref<Long>> preferenceComparatorIdTimePref = (o1, o2)-> o1.v2 != o2.v2 ?  Double.compare(o1.v2, o2.v2): (o1.v1).compareTo(o2.v1);
	public static<T extends Comparable<T>> Comparator<IdTimePref<T>> preferenceComparatorIdTimePref () {
		return new Comparator<IdTimePref<T>>() {
			
			@Override
			public int compare(IdTimePref<T> o1, IdTimePref<T> o2) {
				return o1.v2 != o2.v2 ?  Double.compare(o1.v2, o2.v2): (o1.v1).compareTo(o2.v1);
			}
		};
	}
	
	public static<T extends Comparable<T>> Comparator<IdPref<T>> preferenceComparatorIdPref () {
		return new Comparator<IdPref<T>>() {
			
			@Override
			public int compare(IdPref<T> o1, IdPref<T> o2) {
				return o1.v2 != o2.v2 ?  Double.compare(o1.v2, o2.v2): (o1.v1).compareTo(o2.v1);
			}
		};
	}
	
	
	
	
	//Comparator of indexes (IdxTimePref)
	public static Comparator<IdTimePref<Long>> idComparatorIdTimePref = (o1, o2) -> (o1.v1).compareTo(o2.v1);
	public static<T extends Comparable<T>> Comparator<IdTimePref<T>> idComparatorIdTimePref () {
		return new Comparator<IdTimePref<T>>() {		
			@Override
			public int compare(IdTimePref<T> o1, IdTimePref<T> o2) {
				return o1.v1.compareTo(o2.v1);
			}
		};
	}

	//Comparator of time (IdxTimePref)
	public static Comparator<IdTimePref<Long>> timeComparatorIdTimePref = (o1, o2) -> o1.v3 != o2.v3 ? Long.compare(o1.v3, o2.v3): (o1.v1).compareTo(o2.v1);
	public static<T extends Comparable<T>> Comparator<IdTimePref<T>> timeComparatorIdTimePref () {
		return new Comparator<IdTimePref<T>>() {
			
			@Override
			public int compare(IdTimePref<T> o1, IdTimePref<T> o2) {
				return o1.v3 != o2.v3 ? Long.compare(o1.v3, o2.v3): (o1.v1).compareTo(o2.v1);
			}
		};
	}
	

	
	//////////////////////////////////
	//Other comparators
	/////////////////////////////////
	public static Comparator<Tuple2od<Comparable<Object>>> recommendationComparatorTuple2od = (o1,o2) -> o1.v2 != o2.v2 ? Double.compare(o1.v2, o2.v2): (o1.v1).compareTo(o2.v1);;
	
	public static Comparator<Tuple2id> recommendationComparatorTuple2id = (o1,o2) -> o1.v2 != o2.v2 ? Double.compare(o1.v2, o2.v2): Integer.compare(o1.v1,o2.v1);
	
	
	public static Comparator<Object> comparatorStringLong = (l1, l2) -> l1.toString().compareTo(l2.toString());

	
	
	
	
}

/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.crossdomainPOI.utils;

import es.uam.eps.ir.crossdomainPOI.datamodel.SimpleFastTemporalPreferenceData;
import es.uam.eps.ir.crossdomainPOI.mains.ExperimentUtils;
import es.uam.eps.ir.ranksys.fast.index.FastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.FastUserIndex;
import es.uam.eps.ir.ranksys.fast.index.SimpleFastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.SimpleFastUserIndex;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.fast.preference.SimpleFastPreferenceData;


import static org.ranksys.formats.parsing.Parsers.lp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.jooq.lambda.tuple.Tuple2;
import org.jooq.lambda.tuple.Tuple3;
import org.jooq.lambda.tuple.Tuple4;
import org.ranksys.formats.preference.SimpleRatingPreferencesReader;

import com.google.common.util.concurrent.AtomicDouble;

/**
 * *
 * Static class to process the data of Foursqr. Contains different methods to work with the
 * Global checkin dataset of https://sites.google.com/site/yangdingqi/home/foursquare-dataset
 * 
 *
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public final class FoursqrProcessData {

    private final static double EARTH_RADIUS = 6371.0; // Approx Earth radius in KM

    /***
     * Method to obtain a map of categories. This categories are hand-made
     * @param fileCategories the file of categories
     * @return a map of grouped categories
     */
    public static Map<String, List<String>> groupingCategories(String fileCategories) {
        Map<String, List<String>> result = new TreeMap<String, List<String>>();

        try {
            Stream<String> stream = Files.lines(Paths.get(fileCategories));
            stream.forEach(line -> {
                String key = getKeyAssociated(line);
                if (result.get(key) == null) {
                    result.put(key, new ArrayList<String>());
                }
                result.get(key).add(line);
            });
            stream.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return result;
    }

    private static String getKeyAssociated(String line) {
        String line2 = line.toLowerCase();
        if (line2.contains("Restaurant".toLowerCase()) || line2.contains("Bar".toLowerCase())) {
            return "Restaurants and Bars";
        } else if (line2.contains("School".toLowerCase()) || line2.contains("College".toLowerCase()) || line2.contains("University".toLowerCase())) {
            return "Education";
        } else if (line2.contains("Airport".toLowerCase())) {
            return "Airport";
        } else if (line2.contains("Museum".toLowerCase()) || line2.contains("Art".toLowerCase()) || line2.contains("Movie".toLowerCase())) {
            return "Art & Museums";
        } else if (line2.contains("Soccer".toLowerCase()) || line2.contains("Football".toLowerCase())
                || line2.contains("Basketball".toLowerCase()) || line2.contains("Baseball".toLowerCase())
                || line2.contains("Tennis".toLowerCase()) || line2.contains("Track".toLowerCase()) || line2.contains("Volleyball".toLowerCase())) {
            return "Sports";
        } else if (line2.contains("Temple".toLowerCase()) || line2.contains("Church".toLowerCase())
                || line2.contains("Synagogue".toLowerCase()) || line2.contains("Mosque".toLowerCase())) {
            return "Temples";
        } else if (line2.contains("Store".toLowerCase()) || line2.contains("Shop".toLowerCase()) || line2.contains("Market".toLowerCase())) {
            return "Stores";
        }

        return "Other";
    }
    
    /***
     * Method to compute the statistics of the train files of the foursqr dataset
     * @param checkingTrainFileRep
     * @param checkingTrainFileNoRep
     * @param poiCitiesFile
     * @param resultStats
     */
    public static void statisticsFoursqr(String checkingTrainFileRep, String checkingTrainFileNoRep, String testFile, String poiCitiesFile, String resultStats) {
		String characterSplit="\t";
    	try {
    		Map<String, String> mapPoisCities= new HashMap<>();
    		Stream<String> streamCitiesFile = Files.lines(Paths.get(poiCitiesFile));
    		streamCitiesFile.forEach(line -> {
    			String [] poiCity = line.split(characterSplit);
    			mapPoisCities.put(poiCity[0], poiCity[1]);
    		});
    		streamCitiesFile.close();
    		
            PrintStream resultFile = new PrintStream(resultStats);

            if (checkingTrainFileRep != null) {
            	processDataForStatistics(checkingTrainFileRep,mapPoisCities,resultFile);
            }
            
            if (checkingTrainFileRep != null) {
            	processDataForStatistics(checkingTrainFileNoRep,mapPoisCities,resultFile);
            }
            
            if (testFile != null) {
            	processDataForStatistics(testFile,mapPoisCities,resultFile);
            }
            
			resultFile.close();
			
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
    
    
    private static void processDataForStatistics(String file,Map<String, String> mapPoisCities,PrintStream resultFile) {
    	int itemIndexRating=1;
		int userIndexRating=0;
		int timesStampIndex=3;
		String characterSplit="\t";
		try {
	    	Stream<String> lines = Files.lines(Paths.get(file));
			Map<String, Integer> users = new HashMap<>();
			Map<String, Integer> items = new HashMap<>();
			HashSet<String> mapRepetitions= new HashSet<>();
			Set<String> cities= new HashSet<>();
			AtomicInteger ratings = new AtomicInteger(0);
			AtomicInteger repetitions = new AtomicInteger(0);
			
			AtomicLong maxTimestamp= new AtomicLong(0L);
			AtomicLong minTimestamp= new AtomicLong(Long.MAX_VALUE);
			lines.forEach(line -> {
				String [] data = line.split(characterSplit);
				if (users.get(data[userIndexRating]) == null)
					users.put(data[userIndexRating], 0);
				
				if (items.get(data[itemIndexRating]) == null)
					items.put(data[itemIndexRating], 0);
				users.put(data[userIndexRating], users.get(data[userIndexRating]) + 1);
				items.put(data[itemIndexRating], items.get(data[itemIndexRating]) + 1);
				cities.add(mapPoisCities.get(data[itemIndexRating]));
				ratings.getAndIncrement();
				String key = data[userIndexRating] + "_"+data[itemIndexRating];
				if (mapRepetitions.contains(key))
					repetitions.incrementAndGet();
				else
					mapRepetitions.add(key);
				if (data.length > timesStampIndex) {
					Long timeParsed = Long.parseLong(data[timesStampIndex]);
					if (maxTimestamp.get() < timeParsed)
						maxTimestamp.set(timeParsed);
					
					if (minTimestamp.get() > timeParsed)
						minTimestamp.set(timeParsed);
				}
				
				
			});
			lines.close();
			File f = new File(file);
			String filename= f.getName();
			SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy",  Locale.ENGLISH);		

			resultFile.println("File: "+filename);
			resultFile.println("Number of ratings: " + ratings.get());
			resultFile.println("Number of users: " + users.keySet().size());
			resultFile.println("Number of items: " + items.keySet().size());
			resultFile.println("Average ratings per user " + ( (double) ratings.get()) / ( (double) users.keySet().size()));
			resultFile.println("Average ratings per items " + ( (double) ratings.get()) / ( (double) items.keySet().size()));
			resultFile.println("Total repetitions " + repetitions.get());
			resultFile.println("Different cities: ");
			if (maxTimestamp.get() != 0) {
				resultFile.println("Max timestamp: " + maxTimestamp.get() + "Date: " + formatter.format(new Date(maxTimestamp.get()*1000)));
				resultFile.println("Min timestamp: " + minTimestamp.get() + "Date: " + formatter.format(new Date(minTimestamp.get()*1000)));
			}
			for (String city: cities)
				resultFile.print(city+" ");
			resultFile.println("--------------------------");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    
    
    /***
     * Method to generate a file of the center of each city by reading all its Pois associated
     * @param poiCoordsFile the coordinates file of the pois
     * @param poiCityFile the city file of the pois
     * @param resultfilepath the result file
     */
    public static void generateCityCenter(String poiCoordsFile, String poiCityFile, String resultfilepath) {
		try {
			String characterSplit="\t";
			Map<String,String> poiCityMap= new HashMap<>();
			Map<String, List<Tuple2<Double, Double>>> mapCity = new HashMap<String, List<Tuple2<Double, Double>>>();
			Stream<String> streamPoiCityFile = Files.lines(Paths.get(poiCityFile));
			streamPoiCityFile.forEach(line -> {
				String [] poiCity= line.split(characterSplit);
				poiCityMap.put(poiCity[0], poiCity[1]); // map POI -> city
			});
			streamPoiCityFile.close();
			
			//We have read all the POIs 
			Stream<String> streamPoiCoords = Files.lines(Paths.get(poiCoordsFile));
			streamPoiCoords.forEach(line -> {
				String [] poiCoor = line.split(characterSplit);
				String idPoi = poiCoor[0];
				String cityOfPoi = poiCityMap.get(idPoi);
				if (mapCity.get(cityOfPoi) == null)
					mapCity.put(cityOfPoi, new ArrayList<Tuple2<Double,Double>>());
				mapCity.get(cityOfPoi).add(new Tuple2<>(Double.parseDouble(poiCoor[1]), Double.parseDouble(poiCoor[2])));
			});
			streamPoiCoords.close();
			
            PrintStream resultFile = new PrintStream(resultfilepath);
			//Now we have for each city, a list of coordinates
			for (String city: mapCity.keySet()) {
				Tuple2<Double, Double> midPointCity = SequentialRecommendersUtils.midPointCoordinates(mapCity.get(city));
				resultFile.println(city+ characterSplit+midPointCity.v1+characterSplit+midPointCity.v2);				
			}
			resultFile.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    /**
     * Same method of get nnPois but with cities
     * @param coorCities
     * @param nn
     * @param result
     */
    public static void getNNCities(String coorCities, int nn, String fileResult) {
    	List<Tuple3<String,Double,Double>> lstCities = new ArrayList<>();
		String characterSplit="\t";

		try {
			Stream<String> streamPoiCoords = Files.lines(Paths.get(coorCities));
			streamPoiCoords.forEach(line -> {
				String [] tokens = line.split(characterSplit);
				lstCities.add(new Tuple3<>(tokens[0],Double.parseDouble(tokens[1]),Double.parseDouble(tokens[2])));
			});
			streamPoiCoords.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
        getClosestPOIs(fileResult, lstCities, nn);
    }
    
    /**
     * Method to filter the checkins file by a country code
     * @param checkingFile the checking file
     * @param countryCodeFilter the country code 
     * @param filePoiCountryCode the file of the poi and the city
     * @param resultfilepath the result file
     */
    public static void filterCheckingFileByCountryCode(String checkingFile, String countryCodeFilter, String filePoiCountryCode, String resultfilepath) {
		try {
			String characterSplit="\t";
			String characterSplit2="_";			
			Set<String> validPois = new HashSet<String>(); //only pois matching that country code
			Stream<String> stream = Files.lines(Paths.get(filePoiCountryCode));
			stream.forEach(string ->{
				String [] data = string.split(characterSplit);
				String [] cCode = data[1].split(characterSplit2);
				if (cCode[0].equals(countryCodeFilter))
					validPois.add(data[0]);
			});
			stream.close();
			//Now filter the original checkin file
			
            PrintStream resultFile = new PrintStream(resultfilepath);
            Stream<String> stream2 = Files.lines(Paths.get(checkingFile));
			stream2.forEach(fullData -> {
				String idPoi = fullData.split(characterSplit)[1];
				if (validPois.contains(idPoi))
					resultFile.println(fullData);
			});
			stream2.close();
			resultFile.close();
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
    
    public static void filterByNNCities(String checkingFile, String city, String nnFile,int nnCitiesToConsider, String filePoiCountryCode, String resultfilepath) {
    	String characterSplit="\t";
		Set<String> validPois = new HashSet<String>(); //only pois matching the nn cities
		Set<String> validCities = new HashSet<String>(); //set to store the cities
    	try {
			Stream<String> stream = Files.lines(Paths.get(nnFile));
			Optional<String> matched = stream.filter(line -> line.split(characterSplit)[0].equals(city)).findAny();
			if (!matched.isPresent()) {
				System.out.println("No city in the file matchin "+ city);
				stream.close();
				return;
			}
			String [] tokens= matched.get().split(characterSplit);
			//The limit will be the lower in the nn cities to consider and the length of columns 
			int limit = (nnCitiesToConsider > tokens.length - 1) ? tokens.length - 1 : nnCitiesToConsider;
			System.out.println("NN cities of "+ city + " are:");
			for (int i = 0; i < limit+1; i++) {
				validCities.add(tokens[i]);
				System.out.println(tokens[i]);
			}
			stream.close();
			
			//Now we have all the valid cities. We must read the filePoiCountryCode and store only the pois of the cities we want
			
			Stream<String> stream2 = Files.lines(Paths.get(filePoiCountryCode));
			stream2.forEach(string ->{
				String [] data = string.split(characterSplit);
				if (validCities.contains(data[1]))
					validPois.add(data[0]);
			});
			stream2.close();
			
			PrintStream resultFile = new PrintStream(resultfilepath);
            Stream<String> stream3 = Files.lines(Paths.get(checkingFile));
			stream3.forEach(fullData -> {
				String idPoi = fullData.split(characterSplit)[1];
				if (validPois.contains(idPoi))
					resultFile.println(fullData);
			});
			stream3.close();
			resultFile.close();
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
		
	}
    
    
    
    /**
     * Method to analyze the distances between the most popular items of a dataset
     * @param trainFile the training file
     * @param poiCoordsFile the 3 column file cotaining the identifiers of the items and the latitudes and longitudes
     * @param nMostPopular the n items most popular we want to compute
     */
    public static void analyzeDistanceMostPopular(String trainFile, String poiCoordsFile, int nMostPopular) {
    	try {
    		String characterSplit="\t";
    		int indexPoi=1;
    		Map<String, Tuple2<Double,Double>> mapCoordinates = new HashMap<>();
			Stream<String> stream = Files.lines(Paths.get(poiCoordsFile));
			stream.forEach(line ->{
				String [] split = line.split(characterSplit);
				mapCoordinates.put(split[0], new Tuple2<>(Double.parseDouble(split[1]),Double.parseDouble(split[2])));
			});
			stream.close();
			//We have now stored all the coordinates, now we compute the mos popular
			Map<String, Integer> mapPreferences = new HashMap<>();
			Stream<String> stream2 = Files.lines(Paths.get(trainFile));
			stream2.forEach(line -> {
				String [] split = line.split(characterSplit);
				String poi = split[indexPoi];
				if (mapPreferences.get(poi) == null)
					mapPreferences.put(poi, 0);
				mapPreferences.put(poi, mapPreferences.get(split[indexPoi]) + 1);
			});			
			stream2.close();
			List<Tuple2<String, Integer>> lstOfPreferences = new ArrayList<>();
			for (String poi: mapPreferences.keySet())
				lstOfPreferences.add(new Tuple2<>(poi, mapPreferences.get(poi)));
			Comparator<Tuple2<String,Integer>> comparatorTuples= (o1,o2) ->  Double.compare(o1.v2, o2.v2);
			lstOfPreferences.sort(comparatorTuples.reversed());
			//Now we have the list of most popular computed, we take the nn sublist
			List<Tuple2<String, Integer>> lstOfPreferences2 = lstOfPreferences.subList(0, nMostPopular);
			AtomicDouble total = new AtomicDouble(0);
			//This could be optimized to only compute half of the matrix
			lstOfPreferences2.stream().forEach(pref -> {
				lstOfPreferences2.stream().forEach(pref2 -> {
					if (!pref.v1.equals(pref2.v1)) {
						double distance = haversine(mapCoordinates.get(pref.v1).v1, mapCoordinates.get(pref.v1).v2, mapCoordinates.get(pref2.v1).v1, mapCoordinates.get(pref2.v1).v2);
						System.out.println(distance);
						total.set(total.get() + distance);
					}
						
				});
			});
			System.out.println("Average distance of top most popular "+ total.get()/(lstOfPreferences2.size()* lstOfPreferences2.size() - lstOfPreferences2.size()));
	
			AtomicDouble total2 = new AtomicDouble(0);
			AtomicInteger count = new AtomicInteger(0);
			lstOfPreferences.stream().forEach(pref -> {
				count.getAndIncrement();
				if ((double) count.get() / lstOfPreferences.size() > 0.1) {
					System.out.println("10%");
					count.set(0);
				}
				lstOfPreferences.stream().forEach(pref2 -> {
					if (!pref.v1.equals(pref2.v1)) {
						double distance = haversine(mapCoordinates.get(pref.v1).v1, mapCoordinates.get(pref.v1).v2, mapCoordinates.get(pref2.v1).v1, mapCoordinates.get(pref2.v1).v2);
						total2.set(total2.get() + distance);
					}
						
				});
			});
			System.out.println("Average distance of full dataset "+ total2.get()/(lstOfPreferences.size()* lstOfPreferences.size() - lstOfPreferences.size()));

			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
    }

    public static Map<String, String[]> readFoursquareCategories(String inputFile) throws IOException {
        Map<String, String[]> categoriesWithLevels = new HashMap<>();
        Stream<String> stream = Files.lines(Paths.get(inputFile), StandardCharsets.ISO_8859_1);
        // this file has a header, so we can skip it
        stream.skip(1).forEach(line -> {
            // level1_name,level1_id,level2_name,level2_id,level3_name,level3_id,level4_name,level4_id
            String[] data = line.split(",", -1);
            //System.out.println(data.length + "<->" + line);
            String level1 = data[0];
            String level2 = data[2];
            String level3 = data[4];
            String level4 = data[6];
            List<String> values = new ArrayList<>();
            String key = level4;
            if (key.isEmpty()) {
                key = level3;
                if (key.isEmpty()) {
                    key = level2;
                    if (key.isEmpty()) {
                        key = level1;
                        // this level should never be empty!
                        values.add(level1);
                    } else {
                        values.add(level1);
                        values.add(level2);
                    }
                } else {
                    values.add(level1);
                    values.add(level2);
                    values.add(level3);
                }
            } else {
                values.add(level1);
                values.add(level2);
                values.add(level3);
                values.add(level4);
            }
            String[] valuesAsArray = new String[values.size()];
            values.toArray(valuesAsArray);
            categoriesWithLevels.put(key, valuesAsArray);
        });
        stream.close();
        return categoriesWithLevels;
    }

    private static String parseFoursquareCheckinCategory(String category) {
        switch (category) {
            case "Light Rail":
                return "Light Rail Stations";
            case "College & University":
                return "Colleges & Universities";
            case "Yogurt":
                return "Frozen Yogurt";
            case "Car Dealership":
                return "Auto Dealerships";
            case "Deli / Bodega":
                return "Delis / Bodegas";
            case "Athletic & Sport":
                return "Athletics & Sports";
            case "Subway":
                return "Metro Stations";
            case "Mall":
                return "Shopping Malls";
            case "Spa / Massage":
                return "Spas";
            case "Home (private)":
                return "Homes (private)";
            case "Gym / Fitness Center":
                return "Gyms or Fitness Centers";
            case "Gas Station / Garage":
                return "Gas Stations";
            case "Shop & Service":
                return "Shops & Services";
            case "Tennis":
                return "Tennis Stadiums";
            case "Residential Building (Apartment / Condo)":
                return "Residential Buildings (Apartments / Condos)";
            case "Hiking Trail":
                return "Trails";
            case "Boat or Ferry":
                return "Boats or Ferries";
            case "Embassy / Consulate":
                return "Embassies / Consulates";
            case "Bike Rental / Bike Share":
                return "Bike Rentals / Bike Shares";
            case "General College & University":
                return "General Colleges & Universities";
            case "Drugstore / Pharmacy":
                return "Pharmacies";
            case "Ferry":
                return "Boats or Ferries";
            case "Salon / Barbershop":
                return "Salons / Barbershops";
            case "Malaysian Restaurant":
                return "Malay Restaurants";
            case "Harbor / Marina":
                return "Harbors / Marinas";
            case "Theme Park Ride / Attraction":
                return "Theme Park Rides/Attractions";
            case "Ramen /  Noodle House":
                // this could also be "Noodle Houses"
                return "Ramen Restaurants";
            case "Monument / Landmark":
                return "Monuments / Landmarks";
        }
        if (category.startsWith("Caf") && category.length() < 6) {
            return "Caf�s";
        }
        return category;
    }

    public static String matchingFoursquareCheckinCategory(String category, int level, Map<String, String[]> categoriesWithLevels) {
        // some plurals are not in the file
        String[] categoriesToMatch = new String[]{category, category + "s", category + "es", category.substring(0, category.length() - 1) + "ies", parseFoursquareCheckinCategory(category)};
        for (String c : categoriesToMatch) {
            String[] categories = categoriesWithLevels.get(c);
            if (categories != null) {
                // there is a matching: let's find the closest level
                while (level > categories.length) {
                    level--;
                }
                String matchedCategory = categories[level - 1];
                return matchedCategory;
            }
        }
        return null;
    }

    /**
     * *
     * Method to print the nn closest POIS to each POI of the dataset
     * The result file will contain the nn closest pois of each poi in the training dataset
     * @param trainingFile the file of total POIs (used to filter)
     * @param poisCoordsFile the poisCoordsFile
     * @param resultFileNN the result file
     * @param nn the number of neighbours to print
     */
    public static void printClosestPOIs(String trainingFile, String poisCoordsFile, String resultFileNN, String resultFileCoordsOfTrain, int nn) {

        String charactersSplit = "\t";
        int indexPoiTrainFile = 1;

        int indexPoiCoordsFile = 0;
        int indexLat = 1;
        int indexLong = 2;
        Set<String> trainingPOIs = new HashSet<String>();
        try {
            PrintStream resultFileCoordsFiltered = new PrintStream(resultFileCoordsOfTrain);
            //Get all training POIs. We will not compute distances of more POIs
            Stream<String> stream = Files.lines(Paths.get(trainingFile));
            stream.forEach(line -> {
                String[] data = line.split(charactersSplit);
                trainingPOIs.add(data[indexPoiTrainFile]);
            });
            stream.close();

            //Now we read the POIs file
            List<Tuple3<String, Double, Double>> poisTuple = new ArrayList<>();
            Stream<String> stream2 = Files.lines(Paths.get(poisCoordsFile));
            stream2.forEach(line -> {
                String[] data = line.split(charactersSplit);
                String poiIdentifier = data[indexPoiCoordsFile];
                if (trainingPOIs.contains(poiIdentifier)) {
                    poisTuple.add(new Tuple3<>(poiIdentifier, Double.parseDouble(data[indexLat]), Double.parseDouble(data[indexLong])));
                    resultFileCoordsFiltered.println(line);
                }
            });
            stream2.close();
            resultFileCoordsFiltered.close();
            //We have the list, we retrieve the map
            getClosestPOIs(resultFileNN, poisTuple, nn);

        } catch (IOException e) {

        }
    }

    /**
     * Receiving a list of POIs, will return a map of the closest nn pois
     *
     * @param lst the list of POIS represented with a tuple (ID, lat and long)
     * @param nn the number of POIS to be returned
     * @return
     */
    public static void getClosestPOIs(String resultFileNN, List<Tuple3<String, Double, Double>> lst, int nn) {
        try {
            PrintStream resultFile = new PrintStream(resultFileNN);
            lst.stream().forEach(tuple3Poi -> {
                List<Tuple2<String, Double>> lstReturn = new ArrayList<>();

                lst.stream().filter(tuple3Poi2 -> tuple3Poi2.v1 != tuple3Poi.v1).forEach(tuple3Poi2 -> {
                    lstReturn.add(new Tuple2<>(tuple3Poi2.v1, haversine(tuple3Poi.v2, tuple3Poi.v3, tuple3Poi2.v2, tuple3Poi2.v3)));
                });
                lstReturn.sort(closerPois); //Each element of the list will contain id poi and the distance. The lower the distance the closest the poi

                List<Tuple2<String, Double>> rest = lstReturn.subList(0, nn);
                resultFile.print(tuple3Poi.v1);
                for (Tuple2<String, Double> neigh : rest) {
                    resultFile.print("\t" + neigh.v1);
                }
                resultFile.println();
            });
            resultFile.close();
        } catch (Exception e) {

        }
    }

    private static Comparator<Tuple2<String, Double>> closerPois = (o1, o2) -> Double.compare(o1.v2, o2.v2);

    /**
     * *
     * Method to parse a file containing 2 columns, first of user and second a
     * list of recommended items to a file where each row is idUser, idItem and
     * rank
     *
     * @param recommendedFile
     * @param newFile
     */
    public static void processRecommendedData(String recommendedFile, String newFile) {
        try {
            PrintStream resultFile = new PrintStream(newFile);

            String characterSplitFst = "\t";
            String characterSplitSnd = ",";

            Stream<String> stream = Files.lines(Paths.get(recommendedFile));

            stream.forEach(line -> {
                String[] data = line.split(characterSplitFst);
                String[] itemsRec = data[1].split(characterSplitSnd);
                for (String item : itemsRec) {
                    resultFile.print(data[0] + "\t" + item);
                }
            });
            stream.close();

            resultFile.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * *
     * Method to generate a file with grouped values (every time an user have
     * rated the same item, we will increment it by one)
     *
     * @param originalTrainFile the original training file from foursqr
     * @param usersMapFile the map of users file
     * @param poisMapFile the map of pois file
     * @param destFile the destination file
     */
    @Deprecated
    public static void generateAggregatedFile(String originalTrainFile, String usersMapFile, String poisMapFile,
            String destFile) {

        Map<String, Long> usersMap = new HashMap<>();
        readMap(usersMapFile, usersMap);
        Map<String, Long> poisMap = new HashMap<>();
        readMap(poisMapFile, poisMap);

        Map<Long, Map<Long, Integer>> mapedUserItem = new HashMap<>();

        try {
            PrintStream resultFile = new PrintStream(destFile);

            String characterSplit = "\t";

            int columnUserId = 0;
            int columnVenueId = 1;

            Stream<String> stream = Files.lines(Paths.get(originalTrainFile));

            stream.forEach(line -> {
                String[] originalData = line.split(characterSplit);
                String oldVenueId = originalData[columnVenueId];
                String oldUserId = originalData[columnUserId];

                Long newUserId = usersMap.get(oldUserId);
                Long newPoiId = poisMap.get(oldVenueId);

                // New user.
                if (mapedUserItem.get(newUserId) == null) {
                    mapedUserItem.put(newUserId, new HashMap<>());
                }

                // New item for the user
                if (mapedUserItem.get(newUserId).get(newPoiId) == null) {
                    mapedUserItem.get(newUserId).put(newPoiId, 1);
                } else {
                    Integer oldValue = mapedUserItem.get(newUserId).get(newPoiId);
                    mapedUserItem.get(newUserId).put(newPoiId, oldValue + 1);
                }
            });
            stream.close();

            // Now write the map in the file
            for (Long newUserId : mapedUserItem.keySet()) {
                for (Long newItemId : mapedUserItem.get(newUserId).keySet()) {
                    resultFile
                            .println(newUserId + "\t" + newItemId + "\t" + mapedUserItem.get(newUserId).get(newItemId));
                }
            }

            resultFile.close();

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    /**
     * *
     * Method to generate a feature file (IdPoi, idFeature, Rating)
     *
     * @param originalPoisFile the original pois file from foursqr
     * @param categoriesMapFile the map of categories
     * @param poisMapFile the map of pois
     * @param featureDestFile the destination path of the file
     */
    public static void generateFeatureFile(String originalPoisFile, String poisMapFile, String categoriesMapFile,
            String featureDestFile) {

        Map<String, Long> categoriesMap = new HashMap<>();
        readMap(categoriesMapFile, categoriesMap);

        Map<String, Long> poisMap = new HashMap<>();
        readMap(poisMapFile, poisMap);

        try {
            PrintStream resultFile = new PrintStream(featureDestFile);

            String characterSplit = "\t";

            int columnVenueId = 0;
            int columnvenueCategory = 3;

            Stream<String> stream = Files.lines(Paths.get(originalPoisFile), StandardCharsets.ISO_8859_1);
            stream.forEach(line -> {
                String[] originalData = line.split(characterSplit);
                String venueCategory = originalData[columnvenueCategory];
                String venueid = originalData[columnVenueId];
                resultFile.println(poisMap.get(venueid) + "\t" + categoriesMap.get(venueCategory) + "\t" + 1.0);

            });
            stream.close();
            resultFile.close();

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    /**
     * *
     * Method to generate a mapping files of categories of POIS
     *
     * @param originalPoisFile the original pois file
     * @param destinationFile the destination file
     */
    public static void generateMapOfVenuesCategories(String originalPoisFile, String destinationFile) {
        Map<String, Long> mapCategoriesVenues = new HashMap<>();
        try {
            PrintStream resultFile = new PrintStream(destinationFile);

            String characterSplit = "\t";

            int columnvenueCategory = 3;

            AtomicLong counterTypesOfCategories = new AtomicLong(1L);
            Stream<String> stream = Files.lines(Paths.get(originalPoisFile), StandardCharsets.ISO_8859_1);
            stream.forEach(line -> {
                String[] originalData = line.split(characterSplit);
                String venueCategory = originalData[columnvenueCategory];
                if (mapCategoriesVenues.get(venueCategory) == null) {
                    mapCategoriesVenues.put(venueCategory, counterTypesOfCategories.getAndIncrement());
                }

            });
            stream.close();
            for (String key : mapCategoriesVenues.keySet()) {
                resultFile.println(key + "\t" + mapCategoriesVenues.get(key));
            }

            resultFile.close();

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    /**
     * *
     * Method to obtain a file of POIs coordinates
     *
     * @param originalPoisFile the file of original pois (information associated
     * of the POIS/VENUES)
     * @param fileMapItems file containing the map of olderId -> New id
     * @param destinationFile the destination file of the latitudes and
     * longitudes of the pois
     */
    public static void generatePoisCoords(String originalPoisFile, String fileMapItems, String destinationFile) {
        final Map<String, Long> mapItems = new HashMap<>();

        readMap(fileMapItems, mapItems);

        BufferedReader br;

        try {
            PrintStream resultFile = new PrintStream(destinationFile);

            br = new BufferedReader(new FileReader(originalPoisFile));

            String characterSplit = "\t";

            int columnVenueId = 0;
            int columnLatitude = 1;
            int columnLongitude = 2;

            String line = br.readLine();
            while (line != null) {
                if (line != null) {
                    String[] originalData = line.split(characterSplit);
                    // New id, latitude and longitude
                    resultFile.println(mapItems.get(originalData[columnVenueId]) + "\t" + originalData[columnLatitude]
                            + "\t" + originalData[columnLongitude]);
                }
                line = br.readLine();

            }
            br.close();
            resultFile.close();

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    /****
     * Method to filter a datamodel by the POIs of a city 
     * @param original the original datamodel
     * @param city the city to filter
     * @param cityMapping the mapping of items-cities
     * @return a new datamodel containing only the pois of a city
     */
    public static <U, I> SimpleFastTemporalPreferenceData<U, I> cityFilter(SimpleFastTemporalPreferenceData<U, I> original, String city, Map<I, String> cityMapping) {

        Stream<Tuple4<U, I, Double, Long>> prev = original.getAsTemporalTuples();
        Stream<Tuple4<U, I, Double, Long>> prevFiltered = prev.filter(t -> cityMapping.get(t.v2).equals(city));
        List<Tuple4<U, I, Double, Long>> lst = prevFiltered.collect(Collectors.toList());
        Set<U> usersFiltered = new HashSet<>();
        Set<I> itemsFiltered = new HashSet<>();
        lst.stream().forEach(t -> {
            usersFiltered.add(t.v1);
            itemsFiltered.add(t.v2);
        });
        FastUserIndex<U> userIndex = SimpleFastUserIndex.load(usersFiltered.stream());
        FastItemIndex<I> itemIndex = SimpleFastItemIndex.load(itemsFiltered.stream());

        SimpleFastTemporalPreferenceData<U, I> result = SimpleFastTemporalPreferenceData.loadTemporal(lst.stream(), userIndex, itemIndex);

        return result;
    }
    /***
     *  Method to filter a datamodel by the POIs blonging of different cities
     * @param original the original datamodel
     * @param cities the cities
     * @param cityMapping the mapping of item-cities
     * @return a new datamodel with POIs belongin to that cities
     */
    public static <U, I> SimpleFastTemporalPreferenceData<U, I> citiesFilter(SimpleFastTemporalPreferenceData<U, I> original, Set<String> cities, Map<I, String> cityMapping) {

        Stream<Tuple4<U, I, Double, Long>> prev = original.getAsTemporalTuples();
        Stream<Tuple4<U, I, Double, Long>> prevFiltered = prev.filter(t -> cities.contains(cityMapping.get(t.v2)));
        List<Tuple4<U, I, Double, Long>> lst = prevFiltered.collect(Collectors.toList());
        Set<U> usersFiltered = new HashSet<>();
        Set<I> itemsFiltered = new HashSet<>();
        lst.stream().forEach(t -> {
            usersFiltered.add(t.v1);
            itemsFiltered.add(t.v2);
        });
        FastUserIndex<U> userIndex = SimpleFastUserIndex.load(usersFiltered.stream());
        FastItemIndex<I> itemIndex = SimpleFastItemIndex.load(itemsFiltered.stream());

        SimpleFastTemporalPreferenceData<U, I> result = SimpleFastTemporalPreferenceData.loadTemporal(lst.stream(), userIndex, itemIndex);

        return result;
    }

    /**
     *
     * Creates different splittings depending on the constraints received as
     * parameters. Repetitions are not allowed except in the first splitting. It
     * will also control for repetitions between the splits.
     *
     * @param constraints array of initial and end timestamps for each split
     * that should be created
     * @param fullData the original check-in file as PreferenceData
     * @param filePoiIdCity file with mapping between POIs and their
     * corresponding cities. It can be null if splitting per city is not needed
     * @param kCore constraint to be applied for the kCore
     * @param acceptableCities a list of the cities to be considered when
     * creating the datamodels, or null (or empty) if all the check-ins should
     * be used
     *
     * @return the different splittings created as temporal datamodels
     */
    public static Map<String, SimpleFastTemporalPreferenceData<Long, Long>[]> createSplitting(final Long[][] constraints, final SimpleFastTemporalPreferenceData<Long, Long> fullData, final int kCore, final String filePoiIdCity, final Set<String> acceptableCities) {
        // read poi (with new id) <-> city mapping
        Map<Long, String> mapping = new HashMap<>();
        try {
            if (filePoiIdCity != null) {
                String characterSplit = "\t";
                Stream<String> streamMapping = Files.lines(Paths.get(filePoiIdCity));
                streamMapping.forEach(line -> {
                    String[] poiCity = line.split(characterSplit);
                    if (poiCity.length == 3) {
                        mapping.put(Long.parseLong(poiCity[0]), poiCity[1] + "_" + poiCity[2].replaceAll("\\s+", ""));
                    } else {
                        mapping.put(Long.parseLong(poiCity[0]), poiCity[1].replaceAll("\\s+", ""));
                    }
                });
                streamMapping.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Map<String, SimpleFastTemporalPreferenceData<Long, Long>[]> cityModels = new HashMap<>();
        // perform temporal filtering
        SimpleFastTemporalPreferenceData<Long, Long> timeFilteredData = SequentialRecommendersUtils.temporalFilter(fullData, constraints[0][0], constraints[constraints.length - 1][constraints[constraints.length - 1].length - 1]);
        // run k-core
        SimpleFastTemporalPreferenceData<Long, Long> kCoreData = SequentialRecommendersUtils.KCore(timeFilteredData, kCore, kCore);
        // split into training and test
        Boolean[] allowedRepetitions = new Boolean[constraints.length];
        Arrays.fill(allowedRepetitions, false);
        allowedRepetitions[0] = true;
        final SimpleFastTemporalPreferenceData<Long, Long>[] splits = SequentialRecommendersUtils.splitData(kCoreData, constraints, allowedRepetitions);
        cityModels.put("complete", splits);
        // for each city, filter training and test
        final Set<String> cities = new HashSet<>();
        if (filePoiIdCity != null) {
            if ((acceptableCities == null) || (acceptableCities.isEmpty())) {
                cities.addAll(mapping.values());
            } else {
                cities.addAll(acceptableCities);
            }
        }
        final SimpleFastTemporalPreferenceData<Long, Long>[] cityCompleteSplits = new SimpleFastTemporalPreferenceData[constraints.length];
        IntStream.range(0, constraints.length).forEach(i -> cityCompleteSplits[i] = citiesFilter(splits[i], cities, mapping));
        cityModels.put("complete_cities", cityCompleteSplits);
        for (String city : cities) {
            final SimpleFastTemporalPreferenceData<Long, Long>[] citySplits = new SimpleFastTemporalPreferenceData[constraints.length];
            IntStream.range(0, constraints.length).forEach(i -> citySplits[i] = cityFilter(splits[i], city, mapping));
            cityModels.put(city, citySplits);
        }
        return cityModels;
    }

    public static Map<String, SimpleFastTemporalPreferenceData<Long, Long>[]> createCityPercentageSplitting(final SimpleFastTemporalPreferenceData<Long, Long> fullData, final int kCore, final String filePoiIdCity, final Set<String> acceptableCities) {
        // read poi (with new id) <-> city mapping
        Map<Long, String> mapping = new HashMap<>();
        try {
            if (filePoiIdCity != null) {
                String characterSplit = "\t";
                Stream<String> streamMapping = Files.lines(Paths.get(filePoiIdCity));
                streamMapping.forEach(line -> {
                    String[] poiCity = line.split(characterSplit);
                    if (poiCity.length == 3) {
                        mapping.put(Long.parseLong(poiCity[0]), poiCity[1] + "_" + poiCity[2].replaceAll("\\s+", ""));
                    } else {
                        mapping.put(Long.parseLong(poiCity[0]), poiCity[1].replaceAll("\\s+", ""));
                    }
                });
                streamMapping.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Map<String, SimpleFastTemporalPreferenceData<Long, Long>[]> cityModels = new HashMap<>();
        // perform city filtering
        final Set<String> cities = new HashSet<>();
        if (filePoiIdCity != null) {
            if ((acceptableCities == null) || (acceptableCities.isEmpty())) {
                cities.addAll(mapping.values());
            } else {
                cities.addAll(acceptableCities);
            }
        }
        SimpleFastTemporalPreferenceData<Long, Long> citiesFilteredData = citiesFilter(fullData, cities, mapping);
        // run k-core
        SimpleFastTemporalPreferenceData<Long, Long> kCoreData = SequentialRecommendersUtils.KCore(citiesFilteredData, kCore, kCore);
        // split into training and test
        Boolean[] allowedRepetitions = new Boolean[2];
        Arrays.fill(allowedRepetitions, false);
        allowedRepetitions[0] = true;
        final SimpleFastTemporalPreferenceData<Long, Long>[] splits = SequentialRecommendersUtils.splitPercentageData(kCoreData, 0.8, allowedRepetitions);
        cityModels.put("complete_cities", splits);
        // for each city, filter training and test
        for (String city : cities) {
            final SimpleFastTemporalPreferenceData<Long, Long>[] citySplits = new SimpleFastTemporalPreferenceData[splits.length];
            IntStream.range(0, splits.length).forEach(i -> citySplits[i] = cityFilter(splits[i], city, mapping));
            cityModels.put(city, citySplits);
        }
        return cityModels;
    }
    
    /***
     * Method that will create a new dataset from the original, following these steps:
     * -Filtering out the ratings that does not belong to the acceptable cities
     * -Removing duplicates.
     * -KCore
     * -Temporal filter
     * @param fullData the full dataset
     * @param kCore minimum number of ratings to filter both users and items
     * @param filePoiIdCity the file of pois IDs and cities
     * @param acceptableCities the cities to filter
     * @return
     */
    public static Map<String, SimpleFastTemporalPreferenceData<Long, Long>[]> createCityPercentageSplittingRemoveDuplicatesAndSplit(final SimpleFastTemporalPreferenceData<Long, Long> fullData, final int kCore, final String filePoiIdCity, final Set<String> acceptableCities) {
        // read poi (with new id) <-> city mapping
        Map<Long, String> mapping = new HashMap<>();
        try {
            if (filePoiIdCity != null) {
                String characterSplit = "\t";
                Stream<String> streamMapping = Files.lines(Paths.get(filePoiIdCity));
                streamMapping.forEach(line -> {
                    String[] poiCity = line.split(characterSplit);
                    if (poiCity.length == 3) {
                        mapping.put(Long.parseLong(poiCity[0]), poiCity[1] + "_" + poiCity[2].replaceAll("\\s+", ""));
                    } else {
                        mapping.put(Long.parseLong(poiCity[0]), poiCity[1].replaceAll("\\s+", ""));
                    }
                });
                streamMapping.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Map<String, SimpleFastTemporalPreferenceData<Long, Long>[]> cityModels = new HashMap<>();
        // perform city filtering
        final Set<String> cities = new HashSet<>();
        if (filePoiIdCity != null) {
            if ((acceptableCities == null) || (acceptableCities.isEmpty())) {
                cities.addAll(mapping.values());
            } else {
                cities.addAll(acceptableCities);
            }
        }
        SimpleFastTemporalPreferenceData<Long, Long> citiesFilteredData = citiesFilter(fullData, cities, mapping);
        Boolean[] allowedRepetitions = new Boolean[2];
        Arrays.fill(allowedRepetitions, false);
        
        final SimpleFastTemporalPreferenceData<Long, Long>[] splits = SequentialRecommendersUtils.splitPercentageRemoveDuplicatesAndSplit(citiesFilteredData, 0.8, allowedRepetitions,kCore, kCore);
        
        // split into training and test
        cityModels.put("complete_cities", splits);
        System.out.println(splits[0].numPreferences());
        System.out.println(splits[1].numPreferences());
        // for each city, filter training and test
        for (String city : cities) {
            final SimpleFastTemporalPreferenceData<Long, Long>[] citySplits = new SimpleFastTemporalPreferenceData[splits.length];
            IntStream.range(0, splits.length).forEach(i -> citySplits[i] = cityFilter(splits[i], city, mapping));
            cityModels.put(city, citySplits);
        }
        return cityModels;
    }

    


    public static void generateSplitting(final String folder, final String[] suffix, final Long[][] constraints, final SimpleFastTemporalPreferenceData<Long, Long> data, final int kCore, final String filePoiCity, final Set<String> acceptableCities) {
        Map<String, SimpleFastTemporalPreferenceData<Long, Long>[]> splits = createSplitting(constraints, data, kCore, filePoiCity, acceptableCities);
        for (int i = 0; i < constraints.length; i++) {
            for (String city : splits.keySet()) {
                SimpleFastTemporalPreferenceData<Long, Long> s = splits.get(city)[i];
                String outfile = folder + "split_" + city + "__" + suffix[i];
                try {
                	File f = new File(outfile);
                	if (!f.exists()) {
                		System.out.println(outfile + " does not exist. Computing");
	                    PrintStream out = new PrintStream(outfile);
	                    s.getAsTemporalTuples().forEach(t -> {
	                        out.println(t.v1 + "\t" + t.v2 + "\t" + t.v3 + "\t" + t.v4);
	                    });
	                    out.close();
                	}
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    public static void generateSplittingTemporalFirstRemovingDuplicates(final String folder, final String[] suffix, final SimpleFastTemporalPreferenceData<Long, Long> data, final int kCore, final String filePoiCity, final Set<String> acceptableCities) {
        Map<String, SimpleFastTemporalPreferenceData<Long, Long>[]> splits = createCityPercentageSplittingRemoveDuplicatesAndSplit(data, kCore, filePoiCity, acceptableCities);
        for (int i = 0; i < suffix.length; i++) {
            for (String city : splits.keySet()) {
                SimpleFastTemporalPreferenceData<Long, Long> s = splits.get(city)[i];
                String outfile = folder + "split_" + city + "__" + suffix[i];
                try {
                    PrintStream out = new PrintStream(outfile);
                    s.getAsTemporalTuples().forEach(t -> {
                        out.println(t.v1 + "\t" + t.v2 + "\t" + t.v3 + "\t" + t.v4);
                    });
                    out.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    
/****
 * Method to check if a recommendation file is correct depending on some constraints:
 * -For every user, there must not be any any item recommended that has been previously seen in train.
 * -Every item recommended must match the matching category.
 * 
 * @param trainFile the train file 
 * @param poiCityFile the poiCityFile
 * @param recommendedFile the recommended file
 * @param matchCategory the matching category
 */
    public static void checkRecommendationCorrection(String trainFile, String poiCityFile, String recommendedFile, String matchCategory, String testFile) {
        AtomicBoolean result = new AtomicBoolean(true);
        AtomicBoolean hasCityFile = new AtomicBoolean(false);
        if (poiCityFile != null) {
        	hasCityFile.set(true);
        }
        try {
        	Tuple2<List<Long>, List<Long>> setIndexUsersItems = ExperimentUtils.getUserItemsFromFile(trainFile,lp,lp);
        	
        	FastUserIndex<Long> userIndex = SimpleFastUserIndex.load(setIndexUsersItems.v1.stream());
            FastItemIndex<Long> itemIndex = SimpleFastItemIndex.load(setIndexUsersItems.v2.stream());

            String characterSplit = "\t";
            FastPreferenceData<Long, Long> ranksysTrainDataOriginal = SimpleFastPreferenceData
                    .load(SimpleRatingPreferencesReader.get().read(trainFile, lp, lp), userIndex, itemIndex);
            
            Tuple2<List<Long>, List<Long>> setIndexUsersItemsTest = ExperimentUtils.getUserItemsFromFile(testFile,lp,lp);
        	
        	FastUserIndex<Long> userIndexTest = SimpleFastUserIndex.load(setIndexUsersItemsTest.v1.stream());
            FastItemIndex<Long> itemIndexTest = SimpleFastItemIndex.load(setIndexUsersItemsTest.v2.stream());

            FastPreferenceData<Long, Long> ranksysTestDataOriginal = SimpleFastPreferenceData
                    .load(SimpleRatingPreferencesReader.get().read(testFile, lp, lp), userIndexTest, itemIndexTest);

            
            Set<Long> poisCities = new HashSet<>();
            if (hasCityFile.get()) {
	            Stream<String> stream = Files.lines(Paths.get(poiCityFile));
	            stream.forEach(line -> {
	                String[] split = line.split(characterSplit);
	                if (split[1].contains(matchCategory)) {
	                    poisCities.add(Long.parseLong(split[0]));
	                }
	            });
	            stream.close();
            }
            //We got now the pois associated with the city. We now check that the recommended items are not in train and are from that city

            Stream<String> stream2 = Files.lines(Paths.get(recommendedFile));
            stream2.forEach(line -> {
                String[] recommended = line.split(characterSplit);
                Long user = Long.parseLong(recommended[0]);
                Long item = Long.parseLong(recommended[1]);
                
                if (!ranksysTestDataOriginal.containsUser(user)) {
                	System.out.println("User "+ user +" is not in the test subset");
                	result.set(false);
                }

                if (ranksysTrainDataOriginal.containsUser(user) && ranksysTrainDataOriginal.getUserPreferences(user).filter(pref -> pref.v1.equals(item)).findAny().isPresent()) {
                    System.out.println("Item " + item + " has been rated in train by user " + user);
                    result.set(false);
                }
                
                if (hasCityFile.get() && !poisCities.contains(item)) {
                    System.out.println("Item " + item + " is not in city");
                    result.set(false);
                }
                if (ranksysTrainDataOriginal.containsItem(item)) {
	                if (!ranksysTrainDataOriginal.getItemPreferences(item).findFirst().isPresent()) {
	                	System.out.println("Item " + item + "is not rated in train");
	                	result.set(false);
	                }
                }else {
                	System.out.println("Item "+ item +" is not in the train subset");
                	result.set(false);
                }

            });
            stream2.close();
            if (result.get()) {
                System.out.println("Recommendations correct");
                System.out.println("No item rated in train by same user. No items not in city " + matchCategory + ". All items are rated in train. No user in recommendation file that it is not in test.");
            }

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    /**
     * Creates a datamodel from the original check-in file.
     *
     * @param originalDataset the original check-in file
     * @param processData flag to indicate if the date is already processed (it
     * is a timestamp) or not, the same for users or items
     * @param userMappingFile file containing the user mapping
     * @param itemMappingFile file containing the item mapping
     *
     * @return the check-in file in memory as a temporal datamodel
     */
    public static SimpleFastTemporalPreferenceData<Long, Long> readFullDataset(final String originalDataset, final boolean processData, final String userMappingFile, final String itemMappingFile) {
        String characterSplit = "\t";
        try {
            Set<Long> users = new HashSet<>();
            AtomicLong userCounter = new AtomicLong(1L);
            AtomicLong itemCounter = new AtomicLong(1L);
            Map<String, Long> userMapping = new HashMap<>();
            if (processData && new File(userMappingFile).exists()) {
                readMap(userMappingFile, userMapping);
            }
            Map<String, Long> itemMapping = new HashMap<>();
            if (processData && new File(itemMappingFile).exists()) {
                readMap(itemMappingFile, itemMapping);
            }
            Set<Long> items = new HashSet<>();
            // check-in
            Map<Long, Map<Long, List<Tuple2<Double, Long>>>> userItemInteractions = new HashMap<>();
            Stream<String> stream = Files.lines(Paths.get(originalDataset));
            stream.forEach(line -> {
                String[] originalData = line.split(characterSplit);
                String user = originalData[0];
                Long u = userMapping.get(user);
                if (!processData)
                	u = Long.parseLong(user);
                else if (u == null) {
	                    u = userCounter.getAndIncrement();
	                    userMapping.put(user, u);
	                }
                
                String poi = originalData[1];
                Long item = itemMapping.get(poi);
                if (!processData)
                	item = Long.parseLong(poi);
                else if (item == null) {
                    item = itemCounter.getAndIncrement();
                    itemMapping.put(poi, item);
                }
                users.add(u);
                items.add(item);
                String date = (processData ? originalData[2] : originalData[3]);
                Double score = (processData ? 1.0 : Double.parseDouble(originalData[2]));
                try {
                    Long processedDate = (processData ? parseFoursqrDateLocalTime(date, originalData[3]) : Long.parseLong(date));
                    if (!userItemInteractions.containsKey(u)) {
                        userItemInteractions.put(u, new HashMap<>());
                    }
                    if (!userItemInteractions.get(u).containsKey(item)) {
                        userItemInteractions.get(u).put(item, new ArrayList<>());
                    }
                    userItemInteractions.get(u).get(item).add(new Tuple2<>(score, processedDate));
                } catch (IllegalArgumentException e) {
                    System.out.println("Ignoring " + line);
                }
            });
            stream.close();
            // create the datamodels
            FastUserIndex<Long> uIndex = SimpleFastUserIndex.load(users.stream());
            FastItemIndex<Long> iIndex = SimpleFastItemIndex.load(items.stream());
            List<Tuple4<Long, Long, Double, Long>> tuples = new ArrayList<>();
            for (Long u : userItemInteractions.keySet()) {
                for (Long item : userItemInteractions.get(u).keySet()) {
                    for (Tuple2<Double, Long> t : userItemInteractions.get(u).get(item)) {
                        tuples.add(new Tuple4<>(u, item, t.v1, t.v2));
                    }
                }
            }
            SimpleFastTemporalPreferenceData<Long, Long> model = SimpleFastTemporalPreferenceData.loadTemporal(tuples.stream(), uIndex, iIndex);
            return model;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * *
     * Method to obtain the minimum and maximum timestamps for each city based
     * on the check-ins
     *
     * @param originalDataset the original check-in file
     * @param filePoiCity file with mapping between POIs and their corresponding
     * cities
     * @param processDate flag to indicate if the date is already processed (it
     * is a timestamp) or not
     */
    public static void computeTimeIntervalsPerCity(final String originalDataset, final String filePoiCity, final boolean processDate) {
        String characterSplit = "\t";
        try {
            Map<String, Long> minDates = new HashMap<>();
            Map<String, Long> maxDates = new HashMap<>();
            // read poi <-> city mapping
            Map<String, String> mapping = new HashMap<>();
            Stream<String> streamMapping = Files.lines(Paths.get(filePoiCity));
            streamMapping.forEach(line -> {
                String[] poiCity = line.split(characterSplit);
                mapping.put(poiCity[0], poiCity[1] + "_" + poiCity[2]);
            });
            streamMapping.close();
            // check-in
            Stream<String> stream = Files.lines(Paths.get(originalDataset));
            stream.forEach(line -> {
                String[] originalData = line.split(characterSplit);
                String city = mapping.get(originalData[1]);
                String date = originalData[2];
                try {
                    Long processedDate = (processDate ? parseFoursqrDateLocalTime(date,originalData[3]) : Long.parseLong(date));
                    minDates.put(city, Long.min(processedDate, minDates.getOrDefault(city, Long.MAX_VALUE)));
                    maxDates.put(city, Long.max(processedDate, maxDates.getOrDefault(city, Long.MIN_VALUE)));
                } catch (IllegalArgumentException e) {
                    System.out.println("Ignoring " + line);
                }
            });
            stream.close();

            minDates.keySet().forEach(k -> System.out.println(k + "\t" + minDates.get(k) + "\t" + maxDates.get(k)));
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /****
     * Method to parse a file containing the old ids of POIS and cities to a file containing the new ids of pois and cities
     * @param originalFile the original pois-cities file
     * @param fileMapItems the map of items
     * @param destFile the destination file
     */
    public static void parsePOISCities(String originalFile, String fileMapItems, String destFile) {
        final Map<String, Long> mapItems = new HashMap<>();
        readMap(fileMapItems, mapItems);
        String characterSplit = "\t";

        try {
            PrintStream resultFile = new PrintStream(destFile);
            Stream<String> stream = Files.lines(Paths.get(originalFile));
            stream.forEach(line -> {
                String[] originalData = line.split(characterSplit);
                try {
                    resultFile.println(mapItems.get(originalData[0]) + "\t" + originalData[1]);
                } catch (IllegalArgumentException e) {
                    System.out.println("Ignoring " + line);
                }
            });
            stream.close();
            resultFile.close();

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * *
     * Method to generate a new file of check-ins from the original dataset but
     * with new ids of users/items and the original time switched to timestamp
     *
     * @param originalDataset the original check-in file
     * @param fileMapUsers map of users (old id of user -> new id of user)
     * @param fileMapItems map of items (old id of item -> new id of item)
     * @param newDataset the dataset with new ids of users/items and timestamps
     */
    public static void generateNewCheckinFileWithTimeStamps(String originalDataset, String fileMapUsers,
            String fileMapItems, String newDataset) {
        // Old id -> new id
        final Map<String, Long> mapUsers = new HashMap<>();
        readMap(fileMapUsers, mapUsers);

        // Old id -> new Id
        final Map<String, Long> mapItems = new HashMap<>();
        readMap(fileMapItems, mapItems);

        String characterSplit = "\t";

        try {
            PrintStream resultFile = new PrintStream(newDataset);
            Stream<String> stream = Files.lines(Paths.get(originalDataset));
            stream.forEach(line -> {
                String[] originalData = line.split(characterSplit);
                try {
                    // to work with ratings we will need to concatenate an 1.0 as a rating
                    String utcDate = originalData[2];
                    String offsetStr = originalData[3];
                    resultFile.println(mapUsers.get(originalData[0]) + "\t" + mapItems.get(originalData[1]) + "\t" + 1.0
                            + "\t" + parseFoursqrDateLocalTime(utcDate, offsetStr)  + "\t" + parseFoursqrDateUTC(utcDate));
                } catch (IllegalArgumentException e) {
                    System.out.println("Ignoring " + line);
                }
            });
            stream.close();
            resultFile.close();

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    /**
     * Method to read map of older ids and new ids
     *
     * @param fileMap
     * @return the map read
     */
    public static Map<String, Long> readMap(String fileMap, Map<String, Long> result) {
        String characterSplit = "\t";
        try {
            Stream<String> stream = Files.lines(Paths.get(fileMap));
            stream.forEach(line -> {
                String[] oldNewId = line.split(characterSplit);
                result.put(oldNewId[0], Long.parseLong(oldNewId[1]));

            });
            stream.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Method to obtain files of maps of items, and users with the old and the
     * new ids
     *
     * @param originalDataset the original dataset (check-ins file)
     * @param destMapUsers the destination file to store the map of users
     * @param destMapItems the destination file to store the map of items
     */
    public static void generateMapOfUsersVenues(String originalDataset, String destMapUsers, String destMapItems) {
        // Old id -> new id
        Map<String, Long> mapUsers = new HashMap<>();

        // Old id -> new Id
        Map<String, Long> mapItems = new HashMap<>();

        AtomicLong counterUsers = new AtomicLong(1L);
        AtomicLong counterItems = new AtomicLong(1L);

        try {
            PrintStream usersStream = new PrintStream(destMapUsers);
            PrintStream itemsStream = new PrintStream(destMapItems);

            String characterSplit = "\t";

            // Parameters to configure
            int columnVenueId = 1;
            int columnUserId = 0;

            // First pass of the file
            System.out.println("Reading CheckingsFile file");
            Stream<String> stream = Files.lines(Paths.get(originalDataset));
            stream.forEach(line -> {
                String[] data = line.split(characterSplit);
                String venueID = data[columnVenueId];
                String userId = data[columnUserId];

                if (mapUsers.get(userId) == null) {
                    mapUsers.put(userId, counterUsers.getAndIncrement());
                }

                if (mapItems.get(venueID) == null) {
                    mapItems.put(venueID, counterItems.getAndIncrement());
                }
            });
            stream.close();

            for (String oldUserId : mapUsers.keySet()) {
                usersStream.println(oldUserId + "\t" + mapUsers.get(oldUserId));
            }

            for (String oldItemId : mapItems.keySet()) {
                itemsStream.println(oldItemId + "\t" + mapItems.get(oldItemId));
            }

            itemsStream.close();
            usersStream.close();

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    /**
     * *
     * Method to generate check-ins files per cites
     *
     * @param venuesFile the file of Pois/venues
     * @param citiesFile the cities file
     * @param checkinsData the original check-ins file (Original)
     * @param destPath the destinationPaths
     */
    public static void generateFilesPerCity(String venuesFile, String citiesFile, String checkinsData, String destPath,
            String pathOfVenuesAndCity) {
        // For venues
        Map<String, String> venuesCountryCode;
        Map<String, FoursqrVenue> venuesIDInformation;
        Tuple2<Map<String, String>, Map<String, FoursqrVenue>> tuplesVenues;

        // For cities
        Map<String, String> countryCodeAndCityToCountryCode = new HashMap<>();
        Map<String, FoursqrCity> countryCodeAndCitiesComplete = new HashMap<>();
        Map<String, Set<String>> countryCodeToCountryCodeAndCities = new HashMap<>();
        Tuple3<Map<String, String>, Map<String, Set<String>>, Map<String, FoursqrCity>> tupleCities;

        // Reading the files
        tuplesVenues = readFoursqrVenues(venuesFile);

        venuesCountryCode = tuplesVenues.v1;
        venuesIDInformation = tuplesVenues.v2;

        tupleCities = readFoursqrCities(citiesFile);
        countryCodeAndCityToCountryCode = tupleCities.v1;
        countryCodeToCountryCodeAndCities = tupleCities.v2;
        countryCodeAndCitiesComplete = tupleCities.v3;

        // Venues to cities
        Map<String, String> venuesToCity = new HashMap<String, String>();

        // All the files associated with the countries
        Map<String, PrintStream> mapPrint = new HashMap<>();
        try {
            if (destPath != null) {
                for (String city : countryCodeAndCityToCountryCode.keySet()) {
                    mapPrint.put(city, new PrintStream(destPath + city + ".txt"));
                }
            }

            // All print streams opened. Now read the check-in file and process it
            String characterSplit = "\t";

            // Parameters to configure
            int columnVenueId = 1;

            // First pass of the file
            System.out.println("Reading CheckingsFile file");

            Map<String, Set<String>> countryCodeToCountryCodeAndCities2 = countryCodeToCountryCodeAndCities;
            Map<String, FoursqrCity> countryCodeAndCitiesComplete2 = countryCodeAndCitiesComplete;
            Stream<String> stream = Files.lines(Paths.get(checkinsData));
            stream.forEach(line -> {
                String[] data = line.split(characterSplit);
                String venueID = data[columnVenueId];
                // Get the venue ID , get the country associated, write in that printstream the
                // checkings
                String countryCode = venuesCountryCode.get(venueID);
                Set<String> citiesOfCountry = countryCodeToCountryCodeAndCities2.get(countryCode);
                if (venuesToCity.get(venueID) == null) {
                    // We have not found the city of this venue
                    String contryCodeAndcity = getCityFromVenue(venuesIDInformation.get(venueID), citiesOfCountry,
                            countryCodeAndCitiesComplete2);
                    venuesToCity.put(venueID, contryCodeAndcity);
                }

                // We have now the city
                if (mapPrint.containsKey(venuesToCity.get(venueID))) {
                    mapPrint.get(venuesToCity.get(venueID)).println(line);
                }

            });
            stream.close();

            // Write venues to cities file
            PrintStream venuetoCity = new PrintStream(pathOfVenuesAndCity);
            for (String venueID : venuesToCity.keySet()) {
                venuetoCity.println(venueID + "\t" + venuesToCity.get(venueID));
            }
            venuetoCity.close();

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            for (String city : mapPrint.keySet()) {
                mapPrint.get(city).close();
            }
        }
    }

    /**
     * *
     * Method that receives a list of cities and a venue and returns the city
     * associated to that venue
     *
     * @param v the venue
     * @param cities the list of cities
     * @param citiesComplete the map of complete information of the city
     * @return the city associated to the venue
     */
    private static String getCityFromVenue(FoursqrVenue v, Set<String> cities,
            Map<String, FoursqrCity> citiesComplete) {
        double minDistance = Double.MAX_VALUE;
        String finalCity = null;
        for (String city : cities) {
            double heaverD = heaversine(v, citiesComplete.get(city));
            if (minDistance > heaverD) {
                minDistance = heaverD;
                finalCity = city;
            }
        }

        return finalCity;
    }

    /**
     * *
     * Method to parse the date of foursqr
     *
     * @param dateNotParsed
     * @return the new date parsed
     */
    @Deprecated
    private static Long parseFoursqrDate(String dateNotParsed) {
        String[] split = dateNotParsed.split(" ");
        if (split.length == 5) {
            System.out.print("changed " + dateNotParsed + " to ");
            // wrong line: Wed Mar 27 17:429793	120
            String changedHour = split[3].split(":")[0] + ":" + split[3].split(":")[1].substring(0, 2) + ":00";
            dateNotParsed = split[0] + " " + split[1] + " " + split[2] + " " + changedHour + " +0000 2013 " + split[4];
            split = dateNotParsed.split(" ");
            System.out.println(dateNotParsed);
        }
        if (split.length != 7) {
            throw new IllegalArgumentException("Incorrect date " + dateNotParsed);
        }
        double offset = Double.parseDouble(split[6]) / 60.0;
        int hour = (int) Math.ceil(offset);
        int minuteHour = (int) ((offset - hour) * 60);

        String hourS = String.format("%02d", Math.abs(hour));
        String minuteS = String.format("%02d", Math.abs(minuteHour));

        String finalString;
        finalString = split[0] + " " + split[1] + " " + split[2] + " " + split[3] + " "
                + (Math.signum(offset) < 0 ? "-" : "+") + hourS + minuteS + " " + split[5];

        SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = formatter.parse(finalString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return date.getTime() / 1000;
    }
    
    /***
     * Method to parse a Foursqr date without offsets
     * @param utcDate
     * @param offsetStr
     * @return
     */
    public static Long parseFoursqrDateUTC(String utcDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy",  Locale.ENGLISH);		
		Date date = null;
        try {
            date = formatter.parse(utcDate);
        } catch (ParseException e) {
            System.out.println("Date "+ date + " not supported");
            throw new IllegalArgumentException("Incorrect date " + date);
        }

        return date.getTime() / 1000;
    }
    
    /**
     * Method to parse the date considering the local time. We need to add the minutes to the date. 
     * @param utcDate
     * @param offseStr
     * @return
     */
    public static Long parseFoursqrDateLocalTime(String utcDate, String offseStr) {
		SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy",  Locale.ENGLISH);		
		Date date = null;
        try {
            date = formatter.parse(utcDate);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.MINUTE, Integer.parseInt(offseStr));
            date = cal.getTime();
        } catch (ParseException e) {
            System.out.println("Date "+ date + " not supported");
            throw new IllegalArgumentException("Incorrect date " + date);
        }

        return date.getTime() / 1000;
    }
    
    

    /**
     * *
     * Method to generate files of checkings per country
     *
     * @param venuesFile
     * @param citiesFile
     * @param checkinsData
     * @param destPath
     */
    public static void generateFilesPerCountry(String venuesFile, String citiesFile, String checkinsData,
            String destPath) {
        // For venues
        Map<String, String> venuesCountryCode;
        Map<String, FoursqrVenue> venuesIDInformation;
        Tuple2<Map<String, String>, Map<String, FoursqrVenue>> tuplesVenues;

        // For cities
        Map<String, String> countryCodeAndCityToCountryCode = new HashMap<>();
        Map<String, FoursqrCity> countryCodeCitiesComplete = new HashMap<>();
        Map<String, Set<String>> countryCodeToCountryCodeCities = new HashMap<>();
        Tuple3<Map<String, String>, Map<String, Set<String>>, Map<String, FoursqrCity>> tupleCities;

        // Reading the files
        tuplesVenues = readFoursqrVenues(venuesFile);

        venuesCountryCode = tuplesVenues.v1;
        venuesIDInformation = tuplesVenues.v2;

        tupleCities = readFoursqrCities(citiesFile);
        countryCodeAndCityToCountryCode = tupleCities.v1;
        countryCodeToCountryCodeCities = tupleCities.v2;
        countryCodeCitiesComplete = tupleCities.v3;

        // All the files associated with the countries
        Map<String, PrintStream> mapPrint = new HashMap<>();
        try {
            for (String countryCode : countryCodeToCountryCodeCities.keySet()) {
                List<String> cities = new ArrayList<>(countryCodeToCountryCodeCities.get(countryCode)); // we are only
                // interested in
                // the full name
                // of the
                // country
                mapPrint.put(countryCode,
                        new PrintStream(destPath + countryCodeCitiesComplete.get(cities.get(0)).country + ".txt"));
            }

            // All print streams opened. Now read the checkin file and process it
            String characterSplit = "\t";

            // Parameters to configure
            int columnVenueId = 1;

            // First pass of the file
            System.out.println("Reading CheckingsFile file");
            Stream<String> stream = Files.lines(Paths.get(checkinsData));
            stream.forEach(line -> {
                String[] data = line.split(characterSplit);
                String venueID = data[columnVenueId];
                // Get the venue ID , get the country associated, write in that printstream the
                // checkins
                String countryCode = venuesCountryCode.get(venueID);
                mapPrint.get(countryCode).println(line);

            });
            stream.close();

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            for (String countryCode : countryCodeToCountryCodeCities.keySet()) {
                mapPrint.get(countryCode).close();
            }
        }

    }

    /**
     * Read foursqr venues
     *
     * @param venuesFile
     * @return
     */
    private static Tuple2<Map<String, String>, Map<String, FoursqrVenue>> readFoursqrVenues(String venuesFile) {
        // Assume structure of the file is: UserdId(String) ItemID(String)
        // rating(double) timestamp(long)

        // Map of venuesID -> country
        Map<String, String> venuesCountryCode = new HashMap<>();

        // Map of venuesID, and information
        Map<String, FoursqrVenue> venuesIDInformation = new HashMap<>();

        String characterSplit = "\t";

        // Parameters to configure
        int columnVenueId = 0;
        int columnLatitude = 1;
        int columnLongitude = 2;
        int columnCountryCode = 4;

        try {

            // First pass of the file
            System.out.println("Reading POIS/Venues file");
            Stream<String> stream = Files.lines(Paths.get(venuesFile));
            stream.forEach(line -> {
                String[] data = line.split(characterSplit);
                String venueID = data[columnVenueId];
                String countryCode = data[columnCountryCode];
                Double longitude = Double.parseDouble(data[columnLongitude]);
                Double latitude = Double.parseDouble(data[columnLatitude]);

                if (venuesCountryCode.get(venueID) == null) {
                    venuesCountryCode.put(venueID, countryCode);
                }

                if (venuesIDInformation.get(venueID) == null) {
                    venuesIDInformation.put(venueID, new FoursqrVenue(countryCode, latitude, longitude));
                }

            });
            stream.close();

            return new Tuple2<>(venuesCountryCode, venuesIDInformation);

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;

    }

    /**
     * Read foursqr cities file
     *
     * @param citiesFile
     * @return
     */
    private static Tuple3<Map<String, String>, Map<String, Set<String>>, Map<String, FoursqrCity>> readFoursqrCities(
            String citiesFile) {
        // Assume structure of the file is: UserdId(String) ItemID(String)
        // rating(double) timestamp(long)
        // For each city there is only one country
        Map<String, String> countryCodeAndCityToCountryCode = new HashMap<>();

        // Each city with its latitude and longitude
        Map<String, FoursqrCity> countryCodeAndCitiesComplete = new HashMap<>();

        // Each country may have multiple cities
        Map<String, Set<String>> countryCodeToCountryCodeAndCities = new HashMap<>();

        String characterSplit = "\t";

        // Parameters to configure
        int columnCityName = 0;
        int columnLatitude = 1;
        int columnLongitude = 2;
        int columnCountryCode = 3;
        int columnCountry = 4;
        int columnCityType = 5;

        try {

            // First pass of the file
            System.out.println("Reading cities file");
            Stream<String> stream = Files.lines(Paths.get(citiesFile));
            stream.forEach(line -> {
                String[] data = line.split(characterSplit);
                String cityName = data[columnCityName];
                String countryCode = data[columnCountryCode];
                cityName = cityName.replaceAll("\\s+", ""); //Remove spaces

                countryCodeAndCityToCountryCode.put(countryCode + "_" + cityName, countryCode);

                countryCodeAndCitiesComplete.put(countryCode + "_" + cityName,
                        new FoursqrCity(data[columnCityType], data[columnCountry],
                                Double.parseDouble(data[columnLatitude]), Double.parseDouble(data[columnLongitude])));

                if (countryCodeToCountryCodeAndCities.get(countryCode) == null) {
                    countryCodeToCountryCodeAndCities.put(countryCode, new TreeSet<>());
                }

                countryCodeToCountryCodeAndCities.get(countryCode).add(countryCode + "_" + cityName);
            });
            stream.close();

            return new Tuple3<>(countryCodeAndCityToCountryCode, countryCodeToCountryCodeAndCities,
                    countryCodeAndCitiesComplete);

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;

    }

    /**
     * Specific class to work with foursqr venues or POIS
     *
     * @author Pablo Sanchez
     *
     */
    public static class FoursqrVenue {

        String countryCode;
        double latitude;
        double longitude;

        public FoursqrVenue(String countryCode, double latitude, double longitude) {
            this.countryCode = countryCode;
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public String getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(String cityType) {
            this.countryCode = cityType;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

    }

    /**
     * Specific class for working with Foursqr data
     *
     * @author Pablo Sanchez
     *
     */
    public static class FoursqrCity {

        String cityType;
        String country;
        double latitude;
        double longitude;

        public FoursqrCity(String cityType, String country, double latitude, double longitude) {
            this.cityType = cityType;
            this.latitude = latitude;
            this.longitude = longitude;
            this.country = country;
        }

        public String getCityType() {
            return cityType;
        }

        public void setCityType(String cityType) {
            this.cityType = cityType;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

    }

    /**
     * *
     * Method to compute the heaversine distance
     *
     * @param v the foursqr venue
     * @param fc the foursqr city
     * @return the distance (in km)
     */
    public static double heaversine(FoursqrVenue v, FoursqrCity fc) {
        double lat1 = v.latitude;
        double long1 = v.longitude;
        double lat2 = fc.latitude;
        double long2 = fc.longitude;

        return haversine(lat1, long1, lat2, long2);

    }

    public static double haversine(double lat1, double long1, double lat2, double long2) {
        double dlat = Math.toRadians(lat2 - lat1);
        double dLong = Math.toRadians(long2 - long1);

        double startLat = Math.toRadians(lat1);
        double endLat = Math.toRadians(lat2);

        double a = haversin(dlat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return EARTH_RADIUS * c;
    }

    private static double haversin(double val) {
        return Math.pow(Math.sin(val / 2), 2);
    }

	/***
	 * Method to obtain a result file for tourists and locals
	 * @param dataTrain
	 * @param outputLocalTourist
	 * @param maxDiffBetweenFirstAndLastDate
	 */
	public static void obtainLocalsAndTouristsByTime(String dataTrain, String outputLocalTourist, long maxDiffBetweenFirstAndLastDate, long minDiffforBots, int minPrefsToBeCOnsideredBot) {
		
		//userid -> list of timestamps
		Map<String, List<Long>> usersTimestamps = new HashMap<>();
		
		//userid -> set of sessions
		Map<String, Set<String>> usersSessions = new HashMap<>();
		
		//userid -> List of POIS (can be repeated)
		Map<String, List<String>> usersPois = new HashMap<>();
		Stream<String> stream;
		try {
			PrintStream out = new PrintStream(outputLocalTourist);
			stream = Files.lines(Paths.get(dataTrain));
			stream.forEach(line -> {
				String data[] = line.split("\t");
				String userId = data[0];
				String itemId = data[1];
				Double score = Double.parseDouble(data[2]);
				Long time = Long.parseLong(data[3]);
				String session = "NA"; 
				if (data.length > 4) {
					session = data[4];
				}

				if (usersTimestamps.get(userId) == null) {
					usersTimestamps.put(userId, new ArrayList<>());
					usersSessions.put(userId, new HashSet<>());
					usersPois.put(userId, new ArrayList<>());
				}
				
				usersTimestamps.get(userId).add(time);
				usersSessions.get(userId).add(session);
				usersPois.get(userId).add(itemId);
			});
			stream.close();
			//All timestamps are stored -> sort and see if it is local (L) or Tourist (T)
			
			out.println("UserId" + "\t" + "L-T-B" + "\t" + "FirstTimestamp" + "\t" + "LastTimestamp" + "\t"+ "HoursDiffFstLstTime" + "\t" + "DaysDiff" + "\t" + "PoisVisited" + "\t" + "UniquePoisVisited" + "\t" + "NumberSessions" + "\t" + "PoisPerSession" + "\t" + "UniquePoisPerSession");
			for (String user: usersTimestamps.keySet()) {
				Collections.sort(usersTimestamps.get(user));
				
				Long fst = usersTimestamps.get(user).get(0);
				Long lst = usersTimestamps.get(user).get(usersTimestamps.get(user).size() - 1);
				double hoursDiff = (lst - fst) / 3600.0;
				double daysDiff = hoursDiff / 24.0;
				String localTouristBot = "";
				Set<String> uniquePoisUser = new HashSet<>(usersPois.get(user));
				boolean isbot = isBot(usersTimestamps.get(user), minDiffforBots, minPrefsToBeCOnsideredBot);
				if (isbot) {
					localTouristBot = "B";
				} else {
				
					if ((lst - fst) > maxDiffBetweenFirstAndLastDate) {
						localTouristBot = "L";
					} else {
						localTouristBot = "T";
					}
				}
				out.println(user + "\t" + localTouristBot + "\t" + fst + "\t" + lst + "\t" + hoursDiff + "\t" + daysDiff + "\t" + usersPois.get(user).size() + 
						"\t" + uniquePoisUser.size() + "\t" + usersSessions.get(user).size() + "\t" + (double)usersPois.get(user).size() / (double) usersSessions.get(user).size() + "\t" + (double)uniquePoisUser.size() / (double) usersSessions.get(user).size() );
			}
			out.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/***
	 * Method to detect if a user is a bot by a list of timestamps
	 * @param timestamps the lsit of timestamps
	 * @param minDiffforBots the difference between 2 timestamps to be considered as bot
	 * @param minPrefsToBeConsideredBot the number of times that the difference need to be lower so it is considered as bot
	 * @return
	 */
	private static boolean isBot(List<Long> timestamps, long minDiffforBots, int minPrefsToBeConsideredBot) {
		Long actualTime = timestamps.get(0);
		int count = 0;
		for (int i = 1; i < timestamps.size(); i++) {
			Long newTime = timestamps.get(i);
			Long diff = Math.abs(actualTime - newTime);
			if (diff <= minDiffforBots) {
				count ++;
			}
			actualTime = newTime;
		}
		return count >= minPrefsToBeConsideredBot;
	}

	

}

/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.crossdomainPOI.utils;

import java.util.List;
import java.util.Map;

import org.jooq.lambda.tuple.Tuple2;

/***
 * Abstract class of Kernel Density Estimation
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 */
public abstract class KernelDensityEstimation <U> {
	
	//User and set of coordinates
	protected Map<U, List<Tuple2<Double, Double>>> usersCoordinates;
		
	public KernelDensityEstimation(Map<U, List<Tuple2<Double, Double>>> userCoordinates){
		this.usersCoordinates = userCoordinates;
	}
	
	
	public abstract double probability(U user, Tuple2<Double, Double> targetCoordinatesItem);

}

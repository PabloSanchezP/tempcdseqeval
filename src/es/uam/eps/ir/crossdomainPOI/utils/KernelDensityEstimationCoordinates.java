/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.crossdomainPOI.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jooq.lambda.tuple.Tuple2;


/***
 * KDE Estimation as shown in:
 * -LORE: Exploiting Sequential Influence for Location Recommendations
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U> the type of users
 */
public class KernelDensityEstimationCoordinates <U> extends KernelDensityEstimation<U> {
		
	private Map<U, Double> sigmaUsers;


	
	public KernelDensityEstimationCoordinates(Map<U, List<Tuple2<Double, Double>>> usersCoordinates){
		super(usersCoordinates);
		this.sigmaUsers = new HashMap<>();
		for (U user : this.usersCoordinates.keySet()) {
			List<Tuple2<Double, Double>> userCoordinates = usersCoordinates.get(user);
			double n = userCoordinates.size();
			double [] mean = mean(user);
			double [] std = std(user, mean);
			this.sigmaUsers.put(user, optimalBandwidth(n, std));
		}
	}
	
	
	/**
	 * Probability of user U to visit the destination item
	 * @param user the user
	 * @param coordinatesItem the coordinates of the destination POI
	 * @return the probability
	 */
	public double probability(U user, Tuple2<Double, Double> coordinatesItem) {
		List<Tuple2<Double, Double>> userCoordinates = usersCoordinates.get(user);
		double n = userCoordinates.size();

		double sigma = this.sigmaUsers.get(user);
		double acc = 0;
		double [] vec = new double[2];
		for (Tuple2<Double, Double> poiCoordinates: userCoordinates) {
			vec[0] = 0;
			vec[1] = 0;
			vec[0] = -(coordinatesItem.v1 - poiCoordinates.v1) / sigma;
			vec[1] = -(coordinatesItem.v2 - poiCoordinates.v2) / sigma;
			acc += twoDimensionalNormalKernel(vec);
		}		
		
		return 1.0/(n * sigma * sigma) * acc;
	}
	
	
	private double twoDimensionalNormalKernel(double [] vec) {
		return (1.0/(2 * Math.PI)) * Math.pow(Math.E, -0.5 * dotProduct(vec, vec));
	}
	
	
	private double optimalBandwidth(double n, double [] std) {
		return Math.pow(n, -1.0/6.0) * Math.sqrt(0.5 * dotProduct(std, std));
	}
	

	private double[] std (U user, double[] mean) {
		List<Tuple2<Double, Double>> userCoordinates = usersCoordinates.get(user);

		double [] std = new double[2];
		double length = userCoordinates.size();
		for (Tuple2<Double, Double> poiCoordinates: userCoordinates) {
			std[0] += (poiCoordinates.v1 - mean[0]) * (poiCoordinates.v1 - mean[0]);
			std[1] += (poiCoordinates.v2 - mean[1]) * (poiCoordinates.v2 - mean[1]);
		}
		
		std[0] /= length;
		std[1] /= length;
		
		std[0] = Math.sqrt(std[0]);
		std[1] = Math.sqrt(std[1]);
		
		
		return std;
	}
	
	private double[] mean (U user) {
		List<Tuple2<Double, Double>> userCoordinates = usersCoordinates.get(user);
		
		double [] mean = new double[2];
		double length = userCoordinates.size();
		for (Tuple2<Double, Double> poiCoordinates: userCoordinates) {
			mean[0] += poiCoordinates.v1;
			mean[1] += poiCoordinates.v2;
		}
		
		mean[0] /= length;
		mean[1] /= length;
		return mean;
	}
	
	/***
	 * Method to compute the dot product between 2 vectors
	 * @param a first vector
	 * @param b second vector
	 * @return
	 */
	private double dotProduct(double []a, double[] b) {
		double sum = 0;
	    for (int i = 0; i < a.length; i++) {
	      sum += a[i] * b[i];
	    }
	    return sum;
	}
}

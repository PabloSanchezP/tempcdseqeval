/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.crossdomainPOI.utils;

import static org.ranksys.formats.parsing.Parsers.lp;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.DoubleUnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.jooq.lambda.tuple.Tuple2;
import org.jooq.lambda.tuple.Tuple3;
import org.jooq.lambda.tuple.Tuple4;
import org.ranksys.formats.parsing.Parser;
import org.ranksys.lda.LDAModelEstimator;
import org.ranksys.lda.LDARecommender;

import com.google.common.util.concurrent.AtomicDouble;

import cc.mallet.topics.ParallelTopicModel;
import es.uam.eps.ir.crossdomainPOI.mains.ExperimentUtils;
import es.uam.eps.ir.attrrec.main.AttributeRecommendationUtils;
import es.uam.eps.ir.crossdomainPOI.datamodel.SimpleFastTemporalFeaturePreferenceData;
import es.uam.eps.ir.crossdomainPOI.datamodel.SimpleFastTemporalPreferenceData;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.preferences.IdTimePref;
import es.uam.eps.ir.crossdomainPOI.recommenders.AverageDistanceToUserGEO;
import es.uam.eps.ir.crossdomainPOI.recommenders.GeoBPRMF;
import es.uam.eps.ir.crossdomainPOI.recommenders.GeoSoca;
import es.uam.eps.ir.crossdomainPOI.recommenders.GeoSoca.GEOSOCA_MODEL;
import es.uam.eps.ir.crossdomainPOI.recommenders.KDERecommender;
import es.uam.eps.ir.crossdomainPOI.recommenders.PopGeoNN;
import es.uam.eps.ir.crossdomainPOI.recommenders.RankGeoFMRecommender;
import es.uam.eps.ir.crossdomainPOI.recommenders.RankGeoFMRecommenderNotDense;
import es.uam.eps.ir.crossdomainPOI.recommenders.SkylineTestOrder;
import es.uam.eps.ir.crossdomainPOI.recommenders.TrainRecommender;
import es.uam.eps.ir.crossdomainPOI.recommenders.fmfmgm.FMFMGM;
import es.uam.eps.ir.ranksys.core.feature.FeatureData;
import es.uam.eps.ir.ranksys.core.preference.PreferenceData;
import es.uam.eps.ir.ranksys.core.preference.SimplePreferenceData;
import es.uam.eps.ir.ranksys.fast.index.FastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.FastUserIndex;
import es.uam.eps.ir.ranksys.fast.index.SimpleFastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.SimpleFastUserIndex;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.metrics.rank.ExponentialDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rank.LogarithmicDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rank.NoDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rank.RankingDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rank.ReciprocalDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rel.BackgroundBinaryRelevanceModel;
import es.uam.eps.ir.ranksys.metrics.rel.BinaryRelevanceModel;
import es.uam.eps.ir.ranksys.metrics.rel.NoRelevanceModel;
import es.uam.eps.ir.ranksys.metrics.rel.RelevanceModel;
import es.uam.eps.ir.ranksys.mf.Factorization;
import es.uam.eps.ir.ranksys.mf.als.HKVFactorizer;
import es.uam.eps.ir.ranksys.mf.als.PZTFactorizer;
import es.uam.eps.ir.ranksys.mf.plsa.PLSAFactorizer;
import es.uam.eps.ir.ranksys.mf.rec.MFRecommender;
import es.uam.eps.ir.ranksys.nn.item.ItemNeighborhoodRecommender;
import es.uam.eps.ir.ranksys.nn.item.neighborhood.CachedItemNeighborhood;
import es.uam.eps.ir.ranksys.nn.item.neighborhood.ItemNeighborhood;
import es.uam.eps.ir.ranksys.nn.item.neighborhood.TopKItemNeighborhood;
import es.uam.eps.ir.ranksys.nn.item.sim.SetCosineItemSimilarity;
import es.uam.eps.ir.ranksys.nn.item.sim.SetJaccardItemSimilarity;
import es.uam.eps.ir.ranksys.nn.item.sim.VectorCosineItemSimilarity;
import es.uam.eps.ir.ranksys.nn.item.sim.VectorJaccardItemSimilarity;
import es.uam.eps.ir.ranksys.nn.user.UserNeighborhoodRecommender;
import es.uam.eps.ir.ranksys.nn.user.neighborhood.TopKUserNeighborhood;
import es.uam.eps.ir.ranksys.nn.user.neighborhood.UserNeighborhood;
import es.uam.eps.ir.ranksys.nn.user.sim.SetCosineUserSimilarity;
import es.uam.eps.ir.ranksys.nn.user.sim.SetJaccardUserSimilarity;
import es.uam.eps.ir.ranksys.nn.user.sim.VectorCosineUserSimilarity;
import es.uam.eps.ir.ranksys.nn.user.sim.VectorJaccardUserSimilarity;
import es.uam.eps.ir.ranksys.rec.Recommender;
import es.uam.eps.ir.ranksys.rec.fast.basic.PopularityRecommender;
import es.uam.eps.ir.ranksys.rec.fast.basic.RandomRecommender;
import es.uam.eps.nets.rankfusion.combination.AnzCombiner;
import es.uam.eps.nets.rankfusion.combination.MNZCombiner;
import es.uam.eps.nets.rankfusion.combination.MaxCombiner;
import es.uam.eps.nets.rankfusion.combination.MedCombiner;
import es.uam.eps.nets.rankfusion.combination.MinCombiner;
import es.uam.eps.nets.rankfusion.combination.SumCombiner;
import es.uam.eps.nets.rankfusion.interfaces.IFCombiner;
import es.uam.eps.nets.rankfusion.interfaces.IFNormalizer;
import es.uam.eps.nets.rankfusion.normalization.DistributionNormalizer;
import es.uam.eps.nets.rankfusion.normalization.DummyNormalizer;
import es.uam.eps.nets.rankfusion.normalization.RankSimNormalizer;
import es.uam.eps.nets.rankfusion.normalization.StdNormalizer;
import es.uam.eps.nets.rankfusion.normalization.SumNormalizer;
import es.uam.eps.nets.rankfusion.normalization.TwoMUVNormalizer;
import es.uam.eps.nets.rankfusion.normalization.ZMUVNormalizer;
import es.uam.eps.nets.util.math.Function.Distribution;
import es.uam.eps.ir.crossdomainPOI.utils.UsersMidPoints.SCORES_FREQUENCY;


/**
 * Final class that have useful methods in order to work in a more general way.
 *
 *
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public final class SequentialRecommendersUtils {


    
    
  
    
    

    

    

 

    
    
    /***
     * Method to obtain the combiner for the Rank Fusion library
     * @param combiner string identifier of the combiner
     * @return the interface of the combiner
     */
    public static IFCombiner getCombiner(String combiner) {
        IFCombiner comb = null;
        if (combiner.compareTo("sumcomb") == 0) {
            comb = new SumCombiner();
        } else if (combiner.compareTo("mincomb") == 0) {
            comb = new MinCombiner();
        } else if (combiner.compareTo("mnzcomb") == 0) {
            comb = new MNZCombiner();
        } else if (combiner.compareTo("anzcomb") == 0) {
            comb = new AnzCombiner();
        } else if (combiner.compareTo("maxcomb") == 0) {
            comb = new MaxCombiner();
        } else if (combiner.compareTo("medcomb") == 0) {
            comb = new MedCombiner();
        } else if (combiner.compareTo("defaultcomb") == 0) {
            // Default combiner
            comb = new SumCombiner();
        } else {
            // Unrecognized combiner
        	System.out.println(combiner +" not recognized");
            comb = null;
        }
        return comb;
    }

    /**
     * Method to obtain the normalizer of the RankFusion algorithm
     * @param normalizer the string containing the normalizer
     * @param idealDist the ideal distribution
     * @param dataDist the data distribution
     * @return the Normalizer selected
     */
    public static IFNormalizer getNormalizer(String normalizer, Distribution idealDist, Distribution dataDist) {
        IFNormalizer norm = null;
        if (normalizer.compareTo("stdnorm") == 0) {
            norm = new StdNormalizer();
        } else if (normalizer.compareTo("zmuvnorm") == 0) {
            norm = new ZMUVNormalizer();
        } else if (normalizer.compareTo("ranksimnorm") == 0) {
            norm = new RankSimNormalizer();
        } else if (normalizer.compareTo("sumnorm") == 0) {
            norm = new SumNormalizer();
        } else if (normalizer.compareTo("2muvnorm") == 0) {
            norm = new TwoMUVNormalizer();
//            norm = new StdNormalizer();
        } else if (normalizer.compareTo("distrnorm") == 0) {
            Map<String, Distribution> mapDist = new HashMap<String, Distribution>();
            mapDist.put("", dataDist);
            norm = new DistributionNormalizer(idealDist, mapDist);
        } else if (normalizer.compareTo("defaultnorm") == 0) {
            // Default normalizer
            norm = new DummyNormalizer();
        } else {
            // Unrecognized normalizer
        	System.out.println(normalizer +" not recognized");
        	norm = null;
        }
        return norm;
    }

    
    /***
     * Method to obtain the RankSys recommender depending on the string. It requires the rest of the variables to build the model.
     * The parameters that are not used by the recommender can be null
     * 
     * @param rec the string denoting the recommender we want to instantiate
     * @param similarity similarity for RankSys
     * @param similarity2 the second similarity for RankSys
     * @param data the preference data to build the recommender
     * @param kNeighbours the k neighbours
     * @param userIndex the user index
     * @param itemIndex the item index
     * @param kFactorizer k factors for the MF recommender
     * @param alphaF the alpha value for the HKV recommender
     * @param lambdaF the lambda value for the HKV recommender
     * @param numInteractions the numInteractions value for a MF recommender
     * @param originalPOICoordsFile the POICoords file for POI recommenders
     * @param originalFeatureFile the Feature file to be exploited by CB recommender
     * @param poiCityFile the file containing all POIs of a city
     * @param citySimilarityFile 
     * @param reg1 regularization for some MF recommenders
     * @param reg2 second regularization for some MF recommenders
     * @param score if we are considering the scores as frequency or not. Only for POI recommender
     * @return a RankSys recommender
     */
    public static Recommender<Long, Long> obtRankSysRecommeder	(String rec, String similarity, String similarity2,
            FastPreferenceData<Long, Long> data, int kNeighbours, FastUserIndex<Long> userIndex,
            FastItemIndex<Long> itemIndex, int kFactorizer, double alphaF, double lambdaF, int numInteractions,
            String originalPOICoordsFile, String originalFeatureFile, String poiCityFile, String citySimilarityFile, double reg1, double reg2, SCORES_FREQUENCY score, 
            double regUser, double regItem, double learnRate, double maxRate, double regBias, double decay, boolean isboldDriver, double regImpItem,  double beta,
            double maxDistance, String mode, double theta, double alphaF2, boolean useSigmoid, FeatureData<Long, String, Double> featureData, double c, double epsilon) {
  
    	
    	
        switch (rec) {
            case "RandomRecommender": {
            	System.out.println("RandomRecommender");
                return new RandomRecommender<>(data, data);
            }
            case "PopularityRecommender":{
            	System.out.println("PopularityRecommender");
                return new PopularityRecommender<>(data);
            }
           

            case "UserNeighborhoodRecommender": { // User based
                es.uam.eps.ir.ranksys.nn.user.sim.UserSimilarity<Long> simUNR = AttributeRecommendationUtils.obtRanksysUserSimilarity(data, similarity);
                if (simUNR == null) {
                    return null;
                } else {
                	System.out.println("UserNeighborhoodRecommender");
                	System.out.println("kNeighs: "+kNeighbours);
                	System.out.println("Sim: "+similarity);
                }
                UserNeighborhood<Long> urneighborhood = new TopKUserNeighborhood<>(simUNR, kNeighbours);
                return new UserNeighborhoodRecommender<>(data, urneighborhood, 1);
            }
            case "ItemNeighborhoodRecommender": {// Item based
                es.uam.eps.ir.ranksys.nn.item.sim.ItemSimilarity<Long> simINR = AttributeRecommendationUtils.obtRanksysItemSimilarity(data, similarity);
                if (simINR == null) {
                	return null;
                }else{
                	System.out.println("ItemNeighborhoodRecommender");
                	System.out.println("kNeighs: "+kNeighbours);
                	System.out.println("Sim: "+similarity);             
                }
                ItemNeighborhood<Long> neighborhood = new TopKItemNeighborhood<>(simINR, kNeighbours);
                neighborhood = new CachedItemNeighborhood<>(neighborhood);
                return new ItemNeighborhoodRecommender<>(data, neighborhood, 1);
            }
           
            case "TrainRecommender": {
            	System.out.println("TrainRecommender");
            	return new TrainRecommender<>(data, false);
            }
            case "MFRecommenderHKV": { // Matrix factorization
                int k = kFactorizer;
                double lambda = lambdaF;
                double alpha = alphaF;
                int numIter = numInteractions;
                System.out.println("MFRecommenderHKV");
                System.out.println("kFactors: "+ k);
                System.out.println("lambda: "+ lambda);
                System.out.println("alpha: "+ alpha);
                System.out.println("numIter: "+ numIter);


                DoubleUnaryOperator confidence = x -> 1 + alpha * x;
                Factorization<Long, Long> factorization = new HKVFactorizer<Long, Long>(lambda, confidence, numIter)
                        .factorize(k, data);
                return new MFRecommender<>(userIndex, itemIndex, factorization);
            }
            case "MFRecommenderPZT": { // Probabilistic latent semantic analysis of
                // Hofmann 2004
                int k = kFactorizer;
                double lambda = lambdaF;
                double alpha = alphaF;
                int numIter = numInteractions;
                System.out.println("MFRecommenderPZT");
                System.out.println("kFactors: "+ k);
                System.out.println("lambda: "+ lambda);
                System.out.println("alpha: "+ alpha);
                System.out.println("numIter: "+ numIter);
                
                DoubleUnaryOperator confidence = x -> 1 + alpha * x;
                Factorization<Long, Long> factorization = new PZTFactorizer<Long, Long>(lambda, confidence, numIter)
                        .factorize(k, data);
                return new MFRecommender<>(userIndex, itemIndex, factorization);
            }
            case "MFRecommenderPLSA": { // Probabilistic latent semantic analysis of
                // Hofmann 2004
                int k = kFactorizer;
                int numIter = numInteractions;
                System.out.println("MFRecommenderPLSA");
                System.out.println("kFactors: "+ k);
                System.out.println("numIter: "+ numIter);
                
                Factorization<Long, Long> factorization = new PLSAFactorizer<Long, Long>(numIter).factorize(k, data);
                return new MFRecommender<>(userIndex, itemIndex, factorization);
            }
            case "LDARecommender": { // Probabilistic latent semantic analysis of
                // Hofmann 2004
                int k = kFactorizer;
                double alpha = alphaF;
                int numIter = numInteractions;
                int burninPeriod = 50;
                System.out.println("LDARecommender");
                System.out.println("kFactors: "+ k);
                System.out.println("numIter: "+ numIter);
                System.out.println("beta: "+ beta);
                System.out.println("burninPeriod: "+ burninPeriod);
                
                
                ParallelTopicModel topicModel = null;
                try {
                    topicModel = LDAModelEstimator.estimate(data, k, alpha, beta, numIter, burninPeriod);
                } catch (IOException e) {
                    return null;
                }
                return new LDARecommender<>(userIndex, itemIndex, topicModel);
            }
            /**
             * POI Recommendation algorithms
             */
            //Basic Geographical recommender
            case "AverageDistanceUserGEO": {
                Map<Long, Tuple2<Double, Double>> mapCoordinates = POICoordinatesMap(originalPOICoordsFile, lp);
                System.out.println("AverageDistanceUserGEO");
                
                UsersMidPoints<Long,Long> usermid = new UsersMidPoints<>(data, mapCoordinates, score);
                return new AverageDistanceToUserGEO<>(data, usermid);
            }

            //Algorithm that combines popularity, knn and geospatial
            case "PopGeoNN": {
                PopularityRecommender<Long, Long> popRec = new PopularityRecommender<Long, Long>(data);
                Map<Long, Tuple2<Double, Double>> mapCoordinates = POICoordinatesMap(originalPOICoordsFile, lp);
                UsersMidPoints<Long,Long> usermid = new UsersMidPoints<>(data, mapCoordinates, score);

                AverageDistanceToUserGEO<Long, Long> distanceRev = new AverageDistanceToUserGEO<Long, Long>(data, usermid);

                es.uam.eps.ir.ranksys.nn.user.sim.UserSimilarity<Long> simUNR = AttributeRecommendationUtils.obtRanksysUserSimilarity(data, similarity);
                if (simUNR == null) {
                    return null;
                }
                UserNeighborhood<Long> urneighborhood = new TopKUserNeighborhood<>(simUNR, kNeighbours);
                UserNeighborhoodRecommender<Long, Long> ubRec = new UserNeighborhoodRecommender<>(data, urneighborhood, 1);
                System.out.println("PopGeoNN");
                System.out.println("kNeighbours: "+kNeighbours);
                System.out.println("Sim: "+similarity);
                
                return new PopGeoNN<>(data, popRec, distanceRev, ubRec);
            }
            
            case "RankGeoFMRec":
    		case "RankGeoFMRecommender": {
    			System.out.println("RankGeoFMRecommender");
    			System.out.println("kFactors: " + kFactorizer);
    			System.out.println("kNeighbours " + kNeighbours);
    			System.out.println("decay: " + decay);
    			System.out.println("isboldDriver: " + isboldDriver);
    			System.out.println("numIter: " + numInteractions);
    			System.out.println("learnRate: " + learnRate);
    			System.out.println("maxRate: " + maxRate);
    			System.out.println("alpha: " + alphaF);
    			System.out.println("c: " + c);
    			System.out.println("epsilon: " + epsilon);

    			Map<Long, Tuple2<Double, Double>> mapCoordinates = SequentialRecommendersUtils
    					.POICoordinatesMap(originalPOICoordsFile, lp);
    			float[][] distanceMatrix = distanceMatrix(data.getAllItems().collect(Collectors.toList()), data, mapCoordinates);

    			// C and epsilon
    			return new RankGeoFMRecommender<>(data, distanceMatrix, kFactorizer, kNeighbours, alphaF, c, epsilon,
    					numInteractions, learnRate, maxRate, isboldDriver, decay);
    		}
    		
    		case "RankGeoFMRecNotDense":
    		case "RankGeoFMRecommenderNotDense": {
    			System.out.println("RankGeoFMRecommender");
    			System.out.println("kFactors: " + kFactorizer);
    			System.out.println("kNeighbours " + kNeighbours);
    			System.out.println("decay: " + decay);
    			System.out.println("isboldDriver: " + isboldDriver);
    			System.out.println("numIter: " + numInteractions);
    			System.out.println("learnRate: " + learnRate);
    			System.out.println("maxRate: " + maxRate);
    			System.out.println("alpha: " + alphaF);
    			System.out.println("c: " + c);
    			System.out.println("epsilon: " + epsilon);

    			Map<Long, Tuple2<Double, Double>> mapCoordinates = SequentialRecommendersUtils
    					.POICoordinatesMap(originalPOICoordsFile, lp);

    			return new RankGeoFMRecommenderNotDense<>(data, mapCoordinates, kFactorizer, kNeighbours, alphaF, c, epsilon,
    					numInteractions, learnRate, maxRate, isboldDriver, decay);
    		}
    		
    		
    		
    		
            
            case "KDEstimatorRecommender": {
            	 Map<Long, Tuple2<Double, Double>> mapCoordinates = POICoordinatesMap(originalPOICoordsFile, lp);
                 Map<Long, List<Tuple2<Double, Double>>> userCoordinates = getCoordinatesUser(data, mapCoordinates);
                 KernelDensityEstimation<Long> kde = new KernelDensityEstimationCoordinates<>(userCoordinates);
                 return new KDERecommender<>(data, kde, mapCoordinates);	    	
            }
            
            case "GeoBPRMF": {
            	int k = kFactorizer;
                int numIter = numInteractions;
                System.out.println("GeoBPRMF");
            	System.out.println("kFactors: "+ k);
            	System.out.println("numIter: "+ numInteractions);
            	System.out.println("regUser: " + regUser);
            	System.out.println("regItem: " + regItem);
            	System.out.println("learnRate: " + learnRate);
            	System.out.println("regBias: " + regBias);
            	
                Map<Long, Tuple2<Double, Double>> mapCoordinates = SequentialRecommendersUtils.POICoordinatesMap(originalPOICoordsFile, lp);

            	GeoBPRMF<Long, Long> geoBPRMF = new GeoBPRMF<>(data, regUser, regItem, regBias, numIter, k, learnRate, mapCoordinates, maxDistance);
            	geoBPRMF.fit();
            	return geoBPRMF;
            	
            }
            

            
            case "FMFMGM": {
        		Map<Long, Tuple2<Double, Double>> mapCoordinates = SequentialRecommendersUtils.POICoordinatesMap(originalPOICoordsFile, lp);
        		System.out.println("Alpha: " + alphaF);
        		System.out.println("Theta: " + theta);
        		System.out.println("Maximum distance" + maxDistance);
        		System.out.println("Iterations " + numInteractions);
        		System.out.println("Factors: " + kFactorizer);
        		System.out.println("Alpha2: " + alphaF2);
        		System.out.println("Beta: " + beta);
        		System.out.println("Lerarning rate: " + learnRate);
        		System.out.println("Sigmoid: " + useSigmoid);
        		
        		FMFMGM<Long, Long> fmfMGMRecommender = new FMFMGM<>(data, mapCoordinates, alphaF, theta, maxDistance, numInteractions, kFactorizer, alphaF2, beta, learnRate, useSigmoid);
        		return fmfMGMRecommender;
            }
            
            //LikeGeoSoca but using KNN instead of social component
            case "GeoKNNca": {
                Map<Long, Tuple2<Double, Double>> mapCoordinates = SequentialRecommendersUtils.POICoordinatesMap(originalPOICoordsFile, lp);
                es.uam.eps.ir.ranksys.nn.user.sim.UserSimilarity<Long> simUNR =  AttributeRecommendationUtils.obtRanksysUserSimilarity(data, similarity);
                if (simUNR == null) {
                    return null;
                } else {
                	System.out.println("GeoKNNca");
                	System.out.println("kNeighs: " + kNeighbours);
                	System.out.println("Sim: " + similarity);
                	System.out.println("Alpha: " + alphaF);
                }
                UserNeighborhood<Long> urneighborhood = new TopKUserNeighborhood<>(simUNR, kNeighbours);
            	Map<Long, Set<Long>> socialInfluence = new HashMap<Long, Set<Long>>();
            	urneighborhood.getAllUsers().forEach(u -> {
            		Set<Long> uKnn = new HashSet<Long>();
            		//Obtain all the neighbours and put in the map
            		uKnn.addAll(urneighborhood.getNeighbors(u).map(t2 -> t2.v1).collect(Collectors.toSet()));
            		socialInfluence.put(u, uKnn);
            	});
            	GEOSOCA_MODEL model = ExperimentUtils.obtGeoSoCaModel(mode);

                GeoSoca<Long, Long, String> geosocaRec = new GeoSoca<Long, Long, String>(data, socialInfluence, featureData, mapCoordinates, alphaF, true, true, true, model);
            	return geosocaRec;
            }
            
            
            


            default:
                System.out.println("Carefull. Recommender is null");
                return null;
        }

    }
    
    /***
	 * Method to compute the matrix distance matrix of a list of pois
	 * 
	 * @param itemStream
	 *            the stream of items
	 * @param itemIndexes
	 *            the indexes
	 * @param coordinates
	 *            the coordinates
	 * @return a matrix with the same indexes as the FastItemIndex
	 */
	public static <I> float[][] distanceMatrix(List<I> itemStream, FastItemIndex<I> itemIndexes,
			Map<I, Tuple2<Double, Double>> coordinates) {
		int size = itemStream.size();

		float[][] result = new float[size][size];

		for (I item1 : itemStream) {
			int indexX = itemIndexes.item2iidx(item1);
			double latx = coordinates.get(item1).v1;
			double longx = coordinates.get(item1).v2;

			for (I item2 : itemStream) {
				if (!item1.equals(item2)) {
					int indexY = itemIndexes.item2iidx(item2);
					if (result[indexX][indexY] == 0.0 || result[indexY][indexX] == 0.0) {
						double laty = coordinates.get(item2).v1;
						double longy = coordinates.get(item2).v2;
						double dist = FoursqrProcessData.haversine(latx, longx, laty, longy);
						result[indexX][indexY] = (float) dist;
						result[indexY][indexX] = (float) dist;
					}
				}
			}
		}

		return result;
	}
    
    /***
     * RankSys skyline recommenders
     * @param rec the recommender identifier
     * @param dataTemporalTest the temporal test data
     * @return
     */
    public static Recommender<Long, Long> obtRankSysSkylineRecommender(String rec, FastTemporalPreferenceDataIF<Long, Long> dataTemporalTest){
    	switch (rec){
    		case "skylineTestOrder": 
    		case "SkylineTestOrder": {
            	System.out.println("SkylineTestOrder");
            	return new SkylineTestOrder<>(dataTemporalTest, false);
            }
            case "skylineTestOrderReverse": 
            case "SkylineTestOrderReverse": {
            	System.out.println("SkylineTestOrderReverse");
            	return new SkylineTestOrder<>(dataTemporalTest, true);

            }
    		default:
    			System.out.println("Recommender not recognized. Recommender is null");
    			return null;
    	}
    }
    
    /***
	 * Method to parse MyMedialLte recommendations
	 * 
	 * @param sourceFile the source file
	 * @param destFile   the new recommedner file
	 */
	public static void parseMyMediaLite(String sourceFile, FastUserIndex<Long> userIndexTest, String destFile) {
		BufferedReader br;
		PrintStream writer = null;
		try {
			br = new BufferedReader(new FileReader(sourceFile));
			String line = br.readLine();
			writer = new PrintStream(destFile);

			while (line != null) {
				if (line != null) {
					String[] fullData = line.split("\t");
					Long user = Long.parseLong(fullData[0]);
					line = fullData[1].replace("[", "");
					line = line.replace("]", "");
					String[] data = line.split(",");
					if (userIndexTest.containsUser(user)) {
						for (String itemAndPref : data) {
							Long item = Long.parseLong(itemAndPref.split(":")[0]);
							Double preference = Double.parseDouble(itemAndPref.split(":")[1]);
							writer.println(user + "\t" + item + "\t" + preference);
						}
					}

					line = br.readLine();
				}
			}
			br.close();
			writer.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    

    
    
    /***
     * Method to compute the midpoint from a list of coordinates
     * @param lstCoordinates
     * @return the midpoint of the coordinates
     */
    public static Tuple2<Double, Double> midPointCoordinates(List<Tuple2<Double, Double>> lstCoordinates){
    	double total = lstCoordinates.size();
    	double x = 0;
    	double y = 0;
    	double z = 0;
    	for (Tuple2<Double,Double> coordinate: lstCoordinates) {
    		double latitudeRad = Math.toRadians(coordinate.v1);
			double longitudeRad = Math.toRadians(coordinate.v2);
			x += Math.cos(latitudeRad) * Math.cos(longitudeRad);
			y += Math.cos(latitudeRad) * Math.sin(longitudeRad);
			z += Math.sin(latitudeRad);
    	}
		x /= total;
		y /= total;
		z /= total;
		
		//Now we have the average in the cartesian map, we must retrieve the original coordinates
		double longitude = Math.atan2(y,x);
		double hyp = Math.sqrt(x * x+ y * y);
		
		double lat2 = Math.atan2(z, hyp);
		
		return new Tuple2<Double,Double>(lat2 * (180.0/Math.PI), longitude * (180.0/Math.PI));
    }
   

    /**
     * *
     * Method to obtain a map of POI- coordinates. It assumes that the format is POI_ID \t latitude \t longitude
     *
     * @param originalPOICoordsFile the original file of POI coordinates
     * @param ip the item parser
     * @return the map
     */
    public static <I> Map<I, Tuple2<Double, Double>> POICoordinatesMap(String originalPOICoordsFile, Parser<I> ip) {
        try {
            Map<I, Tuple2<Double, Double>> result = new HashMap<I, Tuple2<Double, Double>>();
            Stream<String> stream = Files.lines(Paths.get(originalPOICoordsFile));
            stream.forEach(line -> {
                String[] split = line.split("\t");
                I item = ip.parse(split[0]);
                result.put(item, new Tuple2<Double, Double>(Double.parseDouble(split[1]), Double.parseDouble(split[2])));
            });
            stream.close();
            return result;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;

    }

    /**
     * *
     * Method that will read a file with n columns
     *
     * @param <I>
     * @param path the path of the file
     * @param catItem the category of the item (or the city) that the item must
     * match in order to be retrieved
     * @param parserItems the parser of the items
     * @param columnCategory the column category
     * @param columnItem the column of the item
     * @return the stream of the items
     */
    public static <I> Stream<I> obtainCandidatesFile(String path, String catItem, Parser<I> parserItems, int columnCategory, int columnItem) {
        try {
            List<I> result = new ArrayList<>();
            Stream<String> streamRead = Files.lines(Paths.get(path));
            streamRead.forEach(line -> {
                String[] data = line.split("\t");
                String cat = data[columnCategory];
                if (cat.equals(catItem)) {
                    result.add(parserItems.parse(data[columnItem]));
                }
            });
            streamRead.close();
            return result.stream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Method to obtain a k-core from a original datamodel
     *
     * @param original the original dataModel
     * @param minimumNumberRatingsUser the minimum number of ratings per user
     * @param minimumNumberRatingsItem the minimum number of ratings per item
     * @return the temporal data
     */
    public static <U, I> SimpleFastTemporalPreferenceData<U, I> KCore(SimpleFastTemporalPreferenceData<U, I> original, int minimumNumberRatingsUser, int minimumNumberRatingsItem) {
        SimpleFastTemporalPreferenceData<U, I> result = original;

        while (!checkKCore(result, minimumNumberRatingsUser, minimumNumberRatingsItem)) {
            SimpleFastTemporalPreferenceData<U, I> aux = result;
            Set<U> usersFiltered = result.getAllUsers().filter(u -> aux.getUserPreferences(u).count() >= minimumNumberRatingsUser).collect(Collectors.toSet());
            Set<I> itemsFiltered = result.getAllItems().filter(i -> aux.getItemPreferences(i).count() >= minimumNumberRatingsItem).collect(Collectors.toSet());
            if (usersFiltered.isEmpty() || itemsFiltered.isEmpty()) {
                return null;
            }
            FastUserIndex<U> userIndex = SimpleFastUserIndex.load(usersFiltered.stream());
            FastItemIndex<I> itemIndex = SimpleFastItemIndex.load(itemsFiltered.stream());
            Stream<Tuple4<U, I, Double, Long>> prev = original.getAsTemporalTuples();
            Stream<Tuple4<U, I, Double, Long>> prevFiltered = prev.filter(t -> userIndex.containsUser(t.v1) && itemIndex.containsItem(t.v2));

            result = SimpleFastTemporalFeaturePreferenceData.loadTemporalFeature(prevFiltered, userIndex, itemIndex);
        }

        return result;
    }

    /**
     *
     * @param original the original dataset
     * @param from the minimum timestamp
     * @param to the maximum timestamp
     * @return a new datamodel containing the data between that dates
     */
    public static <U, I> SimpleFastTemporalPreferenceData<U, I> temporalFilter(SimpleFastTemporalPreferenceData<U, I> original, Long from, Long to) {

        Stream<Tuple4<U, I, Double, Long>> prev = original.getAsTemporalTuples();
        Stream<Tuple4<U, I, Double, Long>> prevFiltered = prev.filter(t -> (t.v4 >= from) && (t.v4 < to));
        List<Tuple4<U, I, Double, Long>> lst = prevFiltered.collect(Collectors.toList());
        Set<U> usersFiltered = new HashSet<>();
        Set<I> itemsFiltered = new HashSet<>();
        lst.stream().forEach(t -> {
            usersFiltered.add(t.v1);
            itemsFiltered.add(t.v2);
        });
        FastUserIndex<U> userIndex = SimpleFastUserIndex.load(usersFiltered.stream());
        FastItemIndex<I> itemIndex = SimpleFastItemIndex.load(itemsFiltered.stream());

        SimpleFastTemporalPreferenceData<U, I> result = SimpleFastTemporalFeaturePreferenceData.loadTemporalFeature(lst.stream(), userIndex, itemIndex);

        return result;
    }

    /***
     * Method to remove the repeated preferences of a datamodel
     * @param original the original dataseet
     * @return the filtered dataset
     */
    public static <U, I> SimpleFastTemporalPreferenceData<U, I> removeDuplicates(SimpleFastTemporalPreferenceData<U, I> original) {
        List<Tuple4<U, I, Double, Long>> tuples = new ArrayList<>();
        original.getAllUsers().forEach(u -> {
            Set<I> items = new HashSet<>();
            Comparator<IdTimePref<I>> timeComparatorIdTimePref = (o1, o2) -> o1.v3 != o2.v3 ? Long.compare(o1.v3, o2.v3) : ((Comparable) o1.v1).compareTo(o2.v1);
            original.getUserPreferences(u).sorted(timeComparatorIdTimePref).forEach(p -> {
                if (!items.contains(p.v1)) {
                    tuples.add(new Tuple4<>(u, p.v1, p.v2, p.v3));
                    items.add(p.v1);
                }
            });
        });

        Set<U> usersFiltered = new HashSet<>();
        tuples.forEach(t -> usersFiltered.add(t.v1));
        Set<I> itemsFiltered = new HashSet<>();
        tuples.forEach(t -> itemsFiltered.add(t.v2));
        FastUserIndex<U> userIndex = SimpleFastUserIndex.load(usersFiltered.stream());
        FastItemIndex<I> itemIndex = SimpleFastItemIndex.load(itemsFiltered.stream());
        SimpleFastTemporalPreferenceData<U, I> model = SimpleFastTemporalFeaturePreferenceData.loadTemporalFeature(tuples.stream(), userIndex, itemIndex);
        return model;
    }

    /**
     * Method to split the data
     * @param original the original dataset
     * @param constraints the constraints
     * @param allowedRepetitions booleans indicating if we are allowing repetitions
     * @return the new data
     */
    public static <U, I> SimpleFastTemporalPreferenceData<U, I>[] splitData(SimpleFastTemporalPreferenceData<U, I> original, final Long[][] constraints, final Boolean[] allowedRepetitions) {
        SimpleFastTemporalPreferenceData<U, I>[] models = new SimpleFastTemporalPreferenceData[constraints.length];

        for (int i = 0; i < constraints.length; i++) {
            SimpleFastTemporalPreferenceData<U, I> split = temporalFilter(original, constraints[i][0], constraints[i][1]);
            if (!allowedRepetitions[i]) {
                split = removeDuplicates(split);
            }
            models[i] = split;

        }
        return models;
    }

    public static <U, I> SimpleFastTemporalPreferenceData<U, I>[] temporalPartition(SimpleFastTemporalPreferenceData<U, I> original, final double trainingPercentage) {
        SimpleFastTemporalPreferenceData<U, I>[] models = new SimpleFastTemporalPreferenceData[2];

        int nprefs = original.numPreferences();
        int nprefsTraining = (int) Math.floor(trainingPercentage * nprefs);
        System.out.println("Num preferences for train" + nprefsTraining);
        final List<Tuple4<U, I, Double, Long>> lstTraining = new ArrayList<>();
        final List<Tuple4<U, I, Double, Long>> lstTest = new ArrayList<>();

        AtomicInteger counter = new AtomicInteger(0);
        Stream<Tuple4<U, I, Double, Long>> prev = original.getAsTemporalTuples();
        // sort so first tuples are older than following ones
        Comparator<Tuple4<?, I, ?, Long>> timestampComparator = (o1, o2) -> o1.v4().compareTo(o2.v4());
        prev.sorted(timestampComparator).forEach(t -> {
            if (counter.get() <= nprefsTraining) {
                lstTraining.add(t);
            } else {
                lstTest.add(t);
            }
            counter.incrementAndGet();
        });
        System.out.println("Preferences train " + lstTraining.size());
        System.out.println("Preferences test " + lstTest.size());

        IntStream.range(0, 2).forEach(i -> {
            Set<U> usersFiltered = new HashSet<>();
            Set<I> itemsFiltered = new HashSet<>();
            List<Tuple4<U, I, Double, Long>> lst = (i == 0 ? lstTraining : lstTest);
            lst.stream().forEach(t -> {
                usersFiltered.add(t.v1);
                itemsFiltered.add(t.v2);
            });
            FastUserIndex<U> userIndex = SimpleFastUserIndex.load(usersFiltered.stream());
            FastItemIndex<I> itemIndex = SimpleFastItemIndex.load(itemsFiltered.stream());
            models[i] = SimpleFastTemporalFeaturePreferenceData.loadTemporalFeature(lst.stream(), userIndex, itemIndex);
        });
        return models;
    }

    public static <U, I> SimpleFastTemporalPreferenceData<U, I>[] splitPercentageData(SimpleFastTemporalPreferenceData<U, I> original, final double trainingPercentage, final Boolean[] allowedRepetitions) {
        SimpleFastTemporalPreferenceData<U, I>[] models = new SimpleFastTemporalPreferenceData[2];

        SimpleFastTemporalPreferenceData<U, I>[] split = temporalPartition(original, trainingPercentage);
        for (int i = 0; i < 2; i++) {
            if (!allowedRepetitions[i]) {
                split[i] = removeDuplicates(split[i]);
            }
            models[i] = split[i];
            // check if this model contains anything that is already in the first split
            if (i > 0) {
                Stream<Tuple4<U, I, Double, Long>> prev = split[i].getAsTemporalTuples();
                // only add the tuple if it is not present in model 0
                Stream<Tuple4<U, I, Double, Long>> prevFiltered = prev.filter(t -> (models[0].getPreferences(t.v1, t.v2) == null || models[0].getPreferences(t.v1, t.v2).count() == 0));
                List<Tuple4<U, I, Double, Long>> lst = prevFiltered.collect(Collectors.toList());
                Set<U> usersFiltered = new HashSet<>();
                Set<I> itemsFiltered = new HashSet<>();
                lst.stream().forEach(t -> {
                    usersFiltered.add(t.v1);
                    itemsFiltered.add(t.v2);
                });
                FastUserIndex<U> userIndex = SimpleFastUserIndex.load(usersFiltered.stream());
                FastItemIndex<I> itemIndex = SimpleFastItemIndex.load(itemsFiltered.stream());
                models[i] = SimpleFastTemporalFeaturePreferenceData.loadTemporalFeature(lst.stream(), userIndex, itemIndex);
            }
        }
        return models;
    }

    public static <U, I> SimpleFastTemporalPreferenceData<U, I>[] splitPercentageRemoveDuplicatesAndSplit(
            SimpleFastTemporalPreferenceData<U, I> fullDataRepetitions, double trainingPercentage, Boolean[] allowedRepetitions, int kCoreUsers, int kCoreItems) {
        SimpleFastTemporalPreferenceData<U, I> fullDataNoRepetitions = removeDuplicates(fullDataRepetitions);

        SimpleFastTemporalPreferenceData<U, I> fullDataNoRepetitionsKCore = KCore(fullDataNoRepetitions, kCoreUsers, kCoreItems);
        SimpleFastTemporalPreferenceData<U, I>[] models = new SimpleFastTemporalPreferenceData[2];

        System.out.println("Total preferences " + fullDataNoRepetitionsKCore.numPreferences());
        SimpleFastTemporalPreferenceData<U, I>[] split = temporalPartition(fullDataNoRepetitionsKCore, trainingPercentage);

        for (int i = 0; i < 2; i++) {
            models[i] = split[i];
            // check if this model contains anything that is already in the first split
            if (i > 0) {
                Stream<Tuple4<U, I, Double, Long>> prev = split[i].getAsTemporalTuples();
                System.out.println("Prev to filter " + split[i].getAsTemporalTuples().collect(Collectors.toList()).size());
                // only add the tuple if it is not present in model 0
                Stream<Tuple4<U, I, Double, Long>> prevFiltered = prev.filter(t -> (models[0].getPreferences(t.v1, t.v2) == null || models[0].getPreferences(t.v1, t.v2).count() == 0));
                List<Tuple4<U, I, Double, Long>> lst = prevFiltered.collect(Collectors.toList());
                System.out.println("after to filter " + lst.size());

                Set<U> usersFiltered = new HashSet<>();
                Set<I> itemsFiltered = new HashSet<>();
                lst.stream().forEach(t -> {
                    usersFiltered.add(t.v1);
                    itemsFiltered.add(t.v2);
                });
                FastUserIndex<U> userIndex = SimpleFastUserIndex.load(usersFiltered.stream());
                FastItemIndex<I> itemIndex = SimpleFastItemIndex.load(itemsFiltered.stream());
                models[i] = SimpleFastTemporalFeaturePreferenceData.loadTemporalFeature(lst.stream(), userIndex, itemIndex);
            }
        }
        return models;
    }

    /**
     * *
     * Method to check if all items and user of a specific dataModel have a
     * certain number of ratings
     *
     * @param dataModel the datamodel
     * @param minimumNumberRatingsUser the minimum number of ratings per user
     * @param minimumNumberRatingsItem the minimum number of ratings per item
     * @return boolean if it matches the condition, false otherwise
     */
    private static <U, I> boolean checkKCore(SimpleFastTemporalPreferenceData<U, I> dataModel, int minimumNumberRatingsUser, int minimumNumberRatingsItem) {
        Optional<U> opUser = dataModel.getAllUsers().filter(u -> dataModel.getUserPreferences(u).count() < minimumNumberRatingsUser).findAny();
        if (opUser.isPresent()) {
            return false;
        }

        Optional<I> opItem = dataModel.getAllItems().filter(i -> dataModel.getItemPreferences(i).count() < minimumNumberRatingsItem).findAny();
        if (opItem.isPresent()) {
            return false;
        }

        return true;
    }
    /***
     * Method to filter the preference data by providing the set of valid users and valid items 
     * @param original the original preferences
     * @param validUsers the set of valid users
     * @param validItems the set of valid items
     * @return the preference data filtered
     */
    public static <U, I> PreferenceData<U, I> filterPreferenceData(PreferenceData<U, I> original, Set<U> validUsers, Set<I> validItems) {
        final List<Tuple3<U, I, Double>> tuples = new ArrayList<>();
        original.getUsersWithPreferences().filter(u -> validUsers.contains(u)).forEach(u -> {
            if (validItems != null) {
                original.getUserPreferences(u).filter(t -> (validItems.contains(t.v1))).forEach(idPref -> {
                    tuples.add(new Tuple3<>(u, idPref.v1, idPref.v2));
                });
            } else {
                original.getUserPreferences(u).forEach(idPref -> {
                    tuples.add(new Tuple3<>(u, idPref.v1, idPref.v2));
                });
            }
        });
        System.out.println("Tuples original: " + original.numPreferences());
        System.out.println("Tuples original filtered: " + tuples.size());
        Stream<Tuple3<U, I, Double>> prev = tuples.stream();
        PreferenceData<U, I> result = SimplePreferenceData.load(prev);
        return result;
    }
    
    

    /**
     * *
     * Method to obtain a stream of tuples from a preference data
     *
     * @param prefData the preference data
     * 
     * @return a stream of tuples (user, item, rating)
     */
    public static <U, I> Stream<Tuple3<U, I, Double>> preferenceDataToTuples(PreferenceData<U, I> prefData) {
        List<Tuple3<U, I, Double>> lstPref = new ArrayList<>();
        prefData.getUsersWithPreferences().forEach(u -> {
            lstPref.addAll(prefData.getUserPreferences(u).map(pref -> new Tuple3<>(u, pref.v1, pref.v2)).collect(Collectors.toList()));
        });
        return lstPref.stream();
    }
    
 
    
    /***
     * Method to obtain the minimum and the maximum ratings for a RankSysPreferenceData
     * @param data the preference data
     * @return a tuple containing the minimum and the maximum rating of the dataset
     */
    public static <U,I> Tuple2<Double, Double> getMinMaxDataRating(PreferenceData<U, I> data){
    	AtomicDouble minRating = new AtomicDouble(Double.MAX_VALUE);
    	AtomicDouble maxRating = new AtomicDouble(Double.MIN_VALUE);
    	data.getUsersWithPreferences().forEach(u -> {
    		double userMin = data.getUserPreferences(u).mapToDouble(timePref -> timePref.v2).min().getAsDouble();
    		double userMax = data.getUserPreferences(u).mapToDouble(timePref -> timePref.v2).max().getAsDouble();
    		minRating.set(userMin < minRating.get() ? userMin: minRating.get());
    		maxRating.set(userMax > maxRating.get() ? userMax: maxRating.get());
    	});
    	return new Tuple2<>(minRating.get(), maxRating.get());
    }
    
    /***
     * Method to get all the coordinates of the POIs visited by an user
     * @param data the preference data
     * @param mapCoordinates the complete Map of coordinates
     * @return a Map of list containing all the coordinates of the users
     */
    public static Map<Long, List<Tuple2<Double, Double>>> getCoordinatesUser(FastPreferenceData<Long, Long> data,
			Map<Long, Tuple2<Double, Double>> mapCoordinates) {
    	Map<Long, List<Tuple2<Double, Double>>> result = new HashMap<Long, List<Tuple2<Double, Double>>>();
    	
    	data.getUsersWithPreferences().forEach(u -> {
    		result.put(u, new ArrayList<Tuple2<Double, Double>>());
    		List<Tuple2<Double, Double>> coordinatesUser = result.get(u);
    		data.getUserPreferences(u).forEach(pref ->{
    			coordinatesUser.add(mapCoordinates.get(pref.v1));
    		});
    	});
		return result;
	}

}

/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.crossdomainPOI.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.jooq.lambda.tuple.Tuple2;

import com.google.common.util.concurrent.AtomicDouble;

import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;

/***
 * Class to compute the midPoints of the users (the average of the user location)
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 */
public class UsersMidPoints<U, I> {
	
	
	// Enumeration for considering score as frequency or just to ignore
	public enum SCORES_FREQUENCY {
		SIMPLE, FREQUENCY
	};

	protected final Map<U, Tuple2<Double, Double>> usersAvgCoordinates;
	protected final FastPreferenceData<U, I> data;
	protected final Map<I, Tuple2<Double, Double>> itemsCoordinates;
	protected final SCORES_FREQUENCY scoreFreq;

	// http://www.geomidpoint.com/example.html
	public UsersMidPoints(FastPreferenceData<U, I> data, Map<I, Tuple2<Double, Double>> itemsCoordinates) {
		this(data, itemsCoordinates, SCORES_FREQUENCY.SIMPLE);
	}

	public UsersMidPoints(FastPreferenceData<U, I> data, Map<I, Tuple2<Double, Double>> itemsCoordinates, SCORES_FREQUENCY freq) {
		usersAvgCoordinates = new HashMap<U, Tuple2<Double, Double>>();
		scoreFreq = freq;

		data.getUsersWithPreferences().forEach(u -> {
			Tuple2<Double, Double> avgUser = midPoint(data, u, itemsCoordinates);
			usersAvgCoordinates.put(u, avgUser);
		});

		this.data = data;
		this.itemsCoordinates = itemsCoordinates;
	}

	/***
	 * Method to compute the midPoint of the list of preferences of the user 
	 * @param data the preferences of the system
	 * @param u the specific user
	 * @param itemsCoordinates the item coordinates
	 * @return the latitude and the longitude of the mid point of the user
	 */
	private Tuple2<Double, Double> midPoint(FastPreferenceData<U, I> data, U u,
			Map<I, Tuple2<Double, Double>> itemsCoordinates) {
		AtomicDouble x = new AtomicDouble(0);
		AtomicDouble y = new AtomicDouble(0);
		AtomicDouble z = new AtomicDouble(0);

		AtomicInteger totalPref = new AtomicInteger(0);
		data.getUserPreferences(u).forEach(pref -> {
			double latitudeRad = Math.toRadians(itemsCoordinates.get(pref.v1).v1);
			double longitudeRad = Math.toRadians(itemsCoordinates.get(pref.v1).v2);

			if (scoreFreq.equals(SCORES_FREQUENCY.SIMPLE)) {
				x.set(x.get() + Math.cos(latitudeRad) * Math.cos(longitudeRad));
				y.set(y.get() + Math.cos(latitudeRad) * Math.sin(longitudeRad));
				z.set(z.get() + Math.sin(latitudeRad));

				totalPref.incrementAndGet();
			} else if (scoreFreq.equals(SCORES_FREQUENCY.FREQUENCY)) {
				
				//The score is the number of times that the user have visited the POI
				int maxScore = (int) pref.v2;
				for (int i = 0; i < maxScore; i++) {
					x.set(x.get() + Math.cos(latitudeRad) * Math.cos(longitudeRad));
					y.set(y.get() + Math.cos(latitudeRad) * Math.sin(longitudeRad));
					z.set(z.get() + Math.sin(latitudeRad));

					totalPref.incrementAndGet();
				}

			}
		});
		x.set(x.get() / totalPref.get());
		y.set(y.get() / totalPref.get());
		z.set(z.get() / totalPref.get());

		// Now we have the average in the cartesian map, we must retrieve the original
		// coordinates
		double longitude = Math.atan2(y.get(), x.get());
		double hyp = Math.sqrt(x.get() * x.get() + y.get() * y.get());

		double lat2 = Math.atan2(z.get(), hyp);

		return new Tuple2<Double, Double>(lat2 * (180.0 / Math.PI), longitude * (180.0 / Math.PI));

	}

	public Map<U, Tuple2<Double, Double>> getUsersAvgCoordinates() {
		return usersAvgCoordinates;
	}
	
	public Tuple2<Double, Double> getUserAvgCoordinates(U user){
		return usersAvgCoordinates.get(user);
	}


	public Map<I, Tuple2<Double, Double>> getItemsCoordinates() {
		return itemsCoordinates;
	}
	
	public Tuple2<Double, Double> getItemCoordinates(I item){
		return itemsCoordinates.get(item);
	}
	
	public Map<Integer, Tuple2<Double, Double>> toFastUsersAvgCoordinates(){
		Map<Integer, Tuple2<Double, Double>> mapResult = new HashMap<>();
		for (U u: usersAvgCoordinates.keySet()) {
			mapResult.put(this.data.user2uidx(u), usersAvgCoordinates.get(usersAvgCoordinates.get(u)));
		}
		return mapResult;
	}
	
	
}

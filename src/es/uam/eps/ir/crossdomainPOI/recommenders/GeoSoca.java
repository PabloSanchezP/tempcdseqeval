package es.uam.eps.ir.crossdomainPOI.recommenders;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.IntPredicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jooq.lambda.tuple.Tuple2;
import org.ranksys.core.util.tuples.Tuple2id;

import com.google.common.util.concurrent.AtomicDouble;

import es.uam.eps.ir.crossdomainPOI.utils.PreferenceComparators;
import es.uam.eps.ir.crossdomainPOI.utils.UAdaptativeKDE;
import es.uam.eps.ir.ranksys.core.feature.FeatureData;
import es.uam.eps.ir.ranksys.core.preference.IdPref;
import es.uam.eps.ir.ranksys.fast.FastRecommendation;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.rec.fast.AbstractFastRecommender;


/***
 * GeoSoca Recommender from paper:
 * 
 * [1] Jia-Dong Zhang, Chi-Yin Chow:
GeoSoCa: Exploiting Geographical, Social and Categorical Correlations for Point-of-Interest Recommendations. SIGIR 2015: 443-452
 * 
 * Code based on work of:
 * [2] Geographic Diversification of Recommended POIs in Frequently Visited
 * Areas (2019) : https://dlnext.acm.org/doi/abs/10.1145/3362505
 * 
 * and
 * 
 * [3] An Experimental Evaluation of Point-of-interest Recommendation in Location-based Social Networks
 * PVLDB 2017. http://www.vldb.org/pvldb/vol10/p1010-liu.pdf
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 * @param <F>
 */
public class GeoSoca<U, I, F> extends AbstractFastRecommender<U, I>  {
	
	//Generic variables
	private final FastPreferenceData<U, I> fastPreferenceData;
	private final Map<I, Double> aggregatedFreqItems;

	public enum GEOSOCA_MODEL {ORIGINAL, EXPERIMENTAL_SURVEY};
	
	
	
	//Variables for the geographical influence
	private double alpha;
	private Map<I, Tuple2<Double, Double>> coordinatesItems;
	
	//Variables for the social influence aspect
	private Map<U, Set<U>> socialInfluence;
	private double beta;
	
	//Variables for the category influence
	private FeatureData<I, F, Double> featureData;
	private Map<U, Map<F, Double>> userPopCategory;
	private double gamma;
	
	private GEOSOCA_MODEL model = GEOSOCA_MODEL.ORIGINAL;
	
	private boolean useSocial = true;
	private boolean useCategorical = true;
	private boolean useGeographical = true;
	
	public GeoSoca(FastPreferenceData<U, I> fastPreferenceData, Map<U, Set<U>> socialInfluence, FeatureData<I, F, Double> featureData, Map<I, Tuple2<Double, Double>> coordinatesItems,
			double alpha, boolean useSocial, boolean useCategorical, boolean useGeographical, GEOSOCA_MODEL model) {
		super(fastPreferenceData, fastPreferenceData);
		
		this.useCategorical = useCategorical;
		this.useGeographical = useGeographical;
		this.useSocial = useSocial;
		this.model = model;
		
		this.fastPreferenceData = fastPreferenceData;
		this.aggregatedFreqItems = aggregatedFreqItems();

		if (useSocial) {
			System.out.println("Using Social");
			this.socialInfluence = socialInfluence;
			this.beta = estimateBetaSocialInfluence();
		}
		
		if (useCategorical) {
			System.out.println("Using Categorical");
			this.featureData = featureData;
			this.userPopCategory = aggregatedPopUserCategories();
			this.gamma = estimategammaCategoricalInfluence();
		}
		
		if (useGeographical) {
			System.out.println("Using Geographical");
			this.alpha = alpha;
			this.coordinatesItems = coordinatesItems;
		}
	}
	
	public GeoSoca(FastPreferenceData<U, I> fastPreferenceData, Map<U, Set<U>> socialInfluence, FeatureData<I, F, Double> featureData, Map<I, Tuple2<Double, Double>> coordinatesItems,
			double alpha) {
		this(fastPreferenceData, socialInfluence, featureData, coordinatesItems, alpha, true, true, true, GEOSOCA_MODEL.ORIGINAL);
	}


	private Map<I, Double> aggregatedFreqItems(){
		Map<I, Double> itemMap = new HashMap<I, Double>();
		
		this.fastPreferenceData.getItemsWithPreferences().forEach(item -> {
			itemMap.put(item, this.fastPreferenceData.getItemPreferences(item).mapToDouble(t -> t.v2).sum());
		});
		
		return itemMap;
	}
	
	private Map<U, Map<F, Double>> aggregatedPopUserCategories(){
		Map<U, Map<F, Double>> userPopCategory = new HashMap<>();
		
		this.fastPreferenceData.getUsersWithPreferences().forEach(user -> {
			userPopCategory.put(user, new HashMap<>());
			this.fastPreferenceData.getUserPreferences(user).forEach(prefu -> {
				double popAggr = prefu.v2;
				Stream<Tuple2<F, Double>> featuresItem = this.featureData.getItemFeatures(prefu.v1);
				if (featuresItem != null) {
					featuresItem.forEach(feat -> {
						Map<F, Double> userFeatures = userPopCategory.get(user);
						if (!userFeatures.containsKey(feat.v1)) {
							userFeatures.put(feat.v1, 0.0);
						}
						userFeatures.put(feat.v1, userFeatures.get(feat.v1) + popAggr);
					});
				}
			});
			
		});
		return userPopCategory;
		
	}

	
	

	@Override
	public FastRecommendation getRecommendation(int uidx, int maxLength, IntPredicate filter) {
		U user = this.fastPreferenceData.uidx2user(uidx);
		List<IdPref<I>> userPreferences = this.fastPreferenceData.getUserPreferences(this.fastPreferenceData.uidx2user(uidx)).collect(Collectors.toList());
		List<Tuple2id> items = new ArrayList<>(); 
		
		List<Tuple2id> items2 = items;
		
		UAdaptativeKDE<U, I> userKernel = null;
		if (useGeographical) {
			userKernel = new UAdaptativeKDE<>(userPreferences, coordinatesItems, alpha);
		}
		
		UAdaptativeKDE<U, I> userKernel2 = userKernel;
		
		this.fastPreferenceData.getIidxWithPreferences().filter(filter).forEach(iidx -> {
			I item = this.fastPreferenceData.iidx2item(iidx);
			double geoCoef = 1.0;
			double socialCoef = 1.0;
			double cateCoef = 1.0;
			
			if (useGeographical) {
				geoCoef = userKernel2.finalDensity(item);
			}
			
			if (useSocial) {
				socialCoef = cumulativeDistributionSocialxul(xulSocialInfluence(user, item));
			}
			
			if (useCategorical) {
				cateCoef = cumulativeDistributionFeatureyul(yulFeatureInfluence(user, item));
			}
			
			items2.add(new Tuple2id(iidx, geoCoef * socialCoef * cateCoef));
		});
		
		items = items2;
		
		Collections.sort(items, PreferenceComparators.recommendationComparatorTuple2id.reversed() );
        items = items.subList(0, (maxLength > items.size()) ? items.size() : maxLength);
		
		
		return new FastRecommendation(uidx, items);
	}
	
	/*
	 * Category influence methods
	 */
	

	/***
	 * Compute the aggregation of the checkin frequency of the friends of user (eq 14)
	 * @param user the user
	 * @param item the item
	 * @return the aggregation of checkin
	 */
	private double yulFeatureInfluence(U user, I item) {
		AtomicDouble acc = new AtomicDouble(0.0);
		this.featureData.getItemFeatures(item).forEach(feat -> {
			if (this.userPopCategory.get(user) != null &&  this.userPopCategory.get(user).get(feat.v1) != null) {
				
				//(Original paper implementation)
				if (this.model == GEOSOCA_MODEL.ORIGINAL) {
					acc.addAndGet(this.aggregatedFreqItems.get(item) * this.userPopCategory.get(user).get(feat.v1));
				} else {
					//Based on paper [3]
					acc.addAndGet(this.userPopCategory.get(user).get(feat.v1));
				}
			}
		});
		return acc.get();
	}
	
	
	/**
	 * Method to estimate the gamma (eq 16 GeoSoca)
	 * @return
	 */
	private double estimategammaCategoricalInfluence() {
		double acc = 0;
		AtomicDouble accthirdSum = new AtomicDouble(0.0);
		AtomicDouble divideMean = new AtomicDouble(0.0);
		
		this.fastPreferenceData.getUsersWithPreferences().forEach(u -> {
			this.fastPreferenceData.getItemsWithPreferences().forEach(i -> {
				AtomicDouble in = new AtomicDouble(1.0);
				
				//Version of the experimental survey
				double popItem = 1;
				
				//Previous (Version original paper GeoSoca)
				if (this.model == GEOSOCA_MODEL.ORIGINAL) {
					popItem = this.aggregatedFreqItems.get(i);
				}
				
				double finalPopItem = popItem;
				this.featureData.getItemFeatures(i).forEach(feat -> {
					Double val = userPopCategory.get(u).get(feat.v1);
					if (val != null) {
						in.addAndGet(val * finalPopItem);
						divideMean.addAndGet(1.0);
					}
				});
				accthirdSum.addAndGet(Math.log(in.get()));
			});
		});
		
		//Previous (Version original paper GeoSoca)
		if (this.model == GEOSOCA_MODEL.ORIGINAL) {
			double userAndItems = (double) (this.fastPreferenceData.getUsersWithPreferences().count() * this.fastPreferenceData.getItemsWithPreferences().count());
			acc = userAndItems / accthirdSum.get();
		}
		else {
			//Version of the experimental survey
			acc = divideMean.get() / accthirdSum.get();
		}
		return 1.0 + acc;
	}
	
	/**
	 * Estimation of the category distribution (eq 17 GeoSoca)
	 * @param xul
	 * @return
	 */
	private double cumulativeDistributionFeatureyul(double yul) {
		return 1.0 - Math.pow(1 + yul, 1 - this.gamma);
	}
	
	
	
	/*
	 * Social influence methods
	 * */
	
	/***
	 * Compute the aggregation of the checkin frequency of the friends of user (eq 10 GeoSoca)
	 * @param user the user
	 * @param item the item
	 * @return the aggregation of checkin
	 */
	private double xulSocialInfluence(U user, I item) {
		Set<U> userFriends = this.socialInfluence.get(user);
		double acc = 0;
		for (U friend: userFriends) {
			Optional<? extends IdPref<I>> pref = this.fastPreferenceData.getUserPreferences(friend).filter(t -> t.v1.equals(item)).findFirst();
			if (pref.isPresent()) {
				acc += pref.get().v2; 
			}
		}
		return acc;
	}
	
	/**
	 * Method to estimate the beta (eq 12 GeoSoca)
	 * @return
	 */
	private double estimateBetaSocialInfluence() {
		double acc = 0;
		AtomicDouble accthirdSum = new AtomicDouble(0.0);
		AtomicDouble divideMean = new AtomicDouble(0.0);

		this.fastPreferenceData.getUsersWithPreferences().forEach(u -> {
			this.fastPreferenceData.getItemsWithPreferences().forEach(i -> {
				double in = 1.0;
				boolean seen = false;
				for (U friend: this.socialInfluence.get(u)) {
					Optional<? extends IdPref<I>> prefFriend = this.fastPreferenceData.getUserPreferences(friend).filter(p -> p.v1.equals(i)).findFirst();
					if (prefFriend.isPresent()) {
						in+= prefFriend.get().v2;
						
						if (!seen) {
							seen = true;
							divideMean.addAndGet(1.0);
						}
					}
				}
				accthirdSum.addAndGet(Math.log(in));
			});
		});
		
		//Previous (Version original paper GeoSoca)		
		if (this.model == GEOSOCA_MODEL.ORIGINAL) {
			double userAndItems = (double) (this.fastPreferenceData.getUsersWithPreferences().count() * this.fastPreferenceData.getItemsWithPreferences().count());
			acc = userAndItems / accthirdSum.get();
		} else {
			//Version of the experimental survey
			acc = divideMean.get() / accthirdSum.get();
		}
		return 1.0 + acc;
		
	}
	
	/**
	 * Estimation of the social distribution (eq 13 GeoSoca)
	 * @param xul
	 * @return
	 */
	private double cumulativeDistributionSocialxul(double xul) {
		return 1.0 - Math.pow(1 + xul, 1 - this.beta);
	}
	


	
	

}

/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.crossdomainPOI.recommenders;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.IntPredicate;

import org.ranksys.core.util.tuples.Tuple2id;

import es.uam.eps.ir.crossdomainPOI.utils.PreferenceComparators;
import es.uam.eps.ir.crossdomainPOI.utils.SequentialRecommendersUtils;
import es.uam.eps.ir.ranksys.fast.FastRecommendation;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.nn.user.UserNeighborhoodRecommender;
import es.uam.eps.ir.ranksys.rec.fast.AbstractFastRecommender;
import es.uam.eps.ir.ranksys.rec.fast.basic.PopularityRecommender;
import es.uam.eps.nets.rankfusion.GenericRankAggregator;
import es.uam.eps.nets.rankfusion.GenericResource;
import es.uam.eps.nets.rankfusion.GenericSearchResults;
import es.uam.eps.nets.rankfusion.interfaces.IFCombiner;
import es.uam.eps.nets.rankfusion.interfaces.IFNormalizer;
import es.uam.eps.nets.rankfusion.interfaces.IFRankAggregator;
import es.uam.eps.nets.rankfusion.interfaces.IFResource;
import es.uam.eps.nets.rankfusion.interfaces.IFSearchResults;

/***
 * Method that combines popularity, distance to a POI and a UB recommender in order to be used as a POI recommender.
 * The default weights of each recommender is the same although they can be modified
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 */
public class PopGeoNN<U, I> extends AbstractFastRecommender<U, I> {
	
	private final PopularityRecommender<U, I> popRec;
	
	private final AverageDistanceToUserGEO<U, I> distUser;
	
	private final UserNeighborhoodRecommender<U, I> ubRec;
	
    private final IFRankAggregator rankAggregator;
    
    //Each of the recommenders can have a different weight
    private final double popWeight;
    
    private final double knnWeight;
	
    private final double avgDisWeight;
    
	public PopGeoNN(FastPreferenceData<U, I> data, PopularityRecommender<U, I> popRec, AverageDistanceToUserGEO<U, I> distUser, UserNeighborhoodRecommender<U, I> ubRec, String normalizer, String combiner) {
		this(data, popRec, distUser, ubRec,normalizer,combiner,1.0,1.0,1.0);
	}
	
	public PopGeoNN(FastPreferenceData<U, I> data, PopularityRecommender<U, I> popRec, AverageDistanceToUserGEO<U, I> distUser, UserNeighborhoodRecommender<U, I> ubRec, String normalizer, String combiner, double popWeight, double knnWeight, double avgDisWeight) {
		super(data, data);
		this.popRec = popRec;
		this.distUser = distUser;
		this.ubRec = ubRec;
		
		IFNormalizer norm = SequentialRecommendersUtils.getNormalizer(normalizer, null, null);
        IFCombiner comb = SequentialRecommendersUtils.getCombiner(combiner);
        rankAggregator = new GenericRankAggregator(norm, comb);
        this.popWeight = popWeight;
        this.knnWeight = knnWeight;
        this.avgDisWeight = avgDisWeight;
	}

	
	
	
	
	public PopGeoNN(FastPreferenceData<U, I> data, PopularityRecommender<U, I> popRec, AverageDistanceToUserGEO<U, I> distUser, UserNeighborhoodRecommender<U, I> ubRec) {
		this(data,popRec,distUser,ubRec,"defaultnorm","defaultcomb");
	}
	

	@Override
	public FastRecommendation getRecommendation(int uidx, int maxLength, IntPredicate filter) {		
		List<Tuple2id> items = new ArrayList<>(); 

		FastRecommendation popularityRec = popRec.getRecommendation(uidx, maxLength, filter);
		FastRecommendation nnRec = ubRec.getRecommendation(uidx, maxLength, filter);
		FastRecommendation distanceRecommendation = distUser.getRecommendation(uidx, maxLength, filter);
		List<IFSearchResults> recommendersList = new ArrayList<IFSearchResults>();
		
		
		//Generate the popularity Ranking
		Map<Long, IFResource> popRanking = new HashMap<>();
		int rankCount = 1;
		for (Tuple2id t: popularityRec.getIidxs()) {
			String idItemParsed = this.iidx2item(t.v1).toString();
			double score = t.v2 / popularityRec.getIidxs().get(0).v2;
			popRanking.put(new Long(idItemParsed.hashCode()), new GenericResource(idItemParsed, score, rankCount));
			rankCount++;
		}
		if (popRanking.size() != 0) {
			IFSearchResults popResults = new GenericSearchResults(popRanking, popWeight);
			recommendersList.add(popResults);
		}
		
		//Generate the nearest neighbour Ranking
        Map<Long, IFResource> nnRanking = new HashMap<>();
		rankCount = 1;
		for (Tuple2id t: nnRec.getIidxs()) {
			String idItemParsed = this.iidx2item(t.v1).toString();
			double score = t.v2 / nnRec.getIidxs().get(0).v2;
			nnRanking.put(new Long(idItemParsed.hashCode()), new GenericResource(idItemParsed, score, rankCount));
			rankCount++;
		}
		if (nnRanking.size() != 0) {
			IFSearchResults nnResults = new GenericSearchResults(nnRanking, knnWeight);
			recommendersList.add(nnResults);
		}
		
		//Generate the avg dis Ranking
        Map<Long, IFResource> distanceRanking = new HashMap<>();
		rankCount = 1;
		for (Tuple2id t: distanceRecommendation.getIidxs()) {
			//System.out.println(this.iidx2item(t.v1) +" " + t.v2);
			String idItemParsed = this.iidx2item(t.v1).toString();
			double score = t.v2 / distanceRecommendation.getIidxs().get(0).v2;
			distanceRanking.put(new Long(idItemParsed.hashCode()), new GenericResource(idItemParsed, score, rankCount));
			rankCount++;
		}
		if (distanceRanking.size() != 0) {
			IFSearchResults distanceResults = new GenericSearchResults(distanceRanking, avgDisWeight);
			recommendersList.add(distanceResults);
		}
		
		//Final aggregation of the values of the three recommenders
        List<IFResource> aggResults = rankAggregator.aggregate(recommendersList).getSortedResourceList(GenericSearchResults.I_RESOURCE_ORDER_COMBINED_VALUE);
        for (IFResource aggResult : aggResults) {
        	// System.out.println(aggResult.getId() + " - " + aggResult.getCombinedValue());
	        Long i = Long.parseLong(aggResult.getId());
			if (filter.test(this.item2iidx((I) i))) {
				items.add(new Tuple2id(this.item2iidx((I) i), aggResult.getCombinedValue()));
			}
	    }        
        
		Collections.sort(items, PreferenceComparators.recommendationComparatorTuple2id.reversed() );
        items = items.subList(0, (maxLength > items.size()) ? items.size() : maxLength);
		
        // It should be ordered
		return new FastRecommendation(uidx, items);		
	}

}

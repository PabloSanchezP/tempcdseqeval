/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.crossdomainPOI.recommenders;

import org.jooq.lambda.tuple.Tuple2;

import es.uam.eps.ir.crossdomainPOI.utils.FoursqrProcessData;
import es.uam.eps.ir.crossdomainPOI.utils.UsersMidPoints;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.rec.fast.FastRankingRecommender;
import it.unimi.dsi.fastutil.ints.Int2DoubleMap;
import it.unimi.dsi.fastutil.ints.Int2DoubleOpenHashMap;

/***
 * Recommender based only in distance of the coordinates (the closer the POI to the average of the user, the better)
 * 
 *
 * @param <U>
 * @param <I>
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 */
public class AverageDistanceToUserGEO<U, I> extends FastRankingRecommender<U, I> {
	UsersMidPoints<U,I> usermid;
	
	public AverageDistanceToUserGEO(FastPreferenceData<U, I> data, UsersMidPoints<U,I> usermid) {
		super(data, data);		
		this.usermid = usermid;
	}
	




	@Override
	public Int2DoubleMap getScoresMap(int uidx) {
		Int2DoubleOpenHashMap scoresMap = new Int2DoubleOpenHashMap();
		scoresMap.defaultReturnValue(0.0);
		if (uidx == -1) {
			return scoresMap;
		}
		
		U user = this.uidx2user(uidx);

		Tuple2<Double, Double> avgDisUser = usermid.getUserAvgCoordinates(user);
		if (avgDisUser == null) {
			return scoresMap;
		}
		
		
		this.iIndex.getAllIidx().forEach(itemIndex -> {
			I item = this.iidx2item(itemIndex);
			Tuple2<Double, Double> itemCoordinates = usermid.getItemCoordinates(item);
			scoresMap.put(itemIndex, 1.0 / FoursqrProcessData.haversine(avgDisUser.v1, avgDisUser.v2, itemCoordinates.v1, itemCoordinates.v2));
		});
		return scoresMap;
	}
}

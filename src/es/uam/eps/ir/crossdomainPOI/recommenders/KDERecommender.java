/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.crossdomainPOI.recommenders;

import java.util.Map;

import org.jooq.lambda.tuple.Tuple2;

import es.uam.eps.ir.crossdomainPOI.utils.KernelDensityEstimation;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.rec.fast.FastRankingRecommender;
import it.unimi.dsi.fastutil.ints.Int2DoubleMap;
import it.unimi.dsi.fastutil.ints.Int2DoubleOpenHashMap;

/***
 * Kernel Density Estimation from paper 
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 */
public class KDERecommender<U, I> extends FastRankingRecommender<U, I> {

	private KernelDensityEstimation<U> kde;
	private Map<I, Tuple2<Double, Double>> coordinatesItem;
	
	public KDERecommender(FastPreferenceData<U, I> data, KernelDensityEstimation<U> kdeStimation, Map<I, Tuple2<Double, Double>> coordinatesItem) {
		super(data, data);
		this.coordinatesItem = coordinatesItem;
		this.kde = kdeStimation;
	}
	

	@Override
	public Int2DoubleMap getScoresMap(int uidx) {
		Int2DoubleOpenHashMap scoresMap = new Int2DoubleOpenHashMap();
		scoresMap.defaultReturnValue(0.0);
		if (uidx == -1) {
			return scoresMap;
		}
		
		U user = this.uidx2user(uidx);
		
		this.iIndex.getAllIidx().forEach(itemIndex -> {
			I item = this.iidx2item(itemIndex);
			double prob = kde.probability(user, coordinatesItem.get(item));
			if (!Double.isNaN(prob)) {
				scoresMap.put(itemIndex, prob);
			}
		});
		return scoresMap;
		
	}

}

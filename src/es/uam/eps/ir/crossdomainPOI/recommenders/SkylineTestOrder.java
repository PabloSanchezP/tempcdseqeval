/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.crossdomainPOI.recommenders;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.crossdomainPOI.utils.PreferenceComparators;
import es.uam.eps.ir.ranksys.rec.fast.FastRankingRecommender;
import it.unimi.dsi.fastutil.ints.Int2DoubleMap;
import it.unimi.dsi.fastutil.ints.Int2DoubleOpenHashMap;

/***
 * Skyline that return the test set ordered by timestamp (useful for routes)
 * 
 * @param <U>
 * @param <I>
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 */
public class SkylineTestOrder<U, I> extends FastRankingRecommender<U, I> {

	private Map<Integer, Integer[]> sequenceOfItemsAt;

	public SkylineTestOrder(FastTemporalPreferenceDataIF<U, I> dataTest, boolean newerItemsFirst) {
		super(dataTest, dataTest);
		obtainMapTestUsers(dataTest, newerItemsFirst);
	}

	private void obtainMapTestUsers(final FastTemporalPreferenceDataIF<U, I> testData, final boolean newerItemsFirst) {
		sequenceOfItemsAt = new HashMap<>();
		testData.getUidxWithPreferences().forEach(uidx -> {
			List<Integer> lstIndexUser = testData.getUidxTimePreferences(uidx)
					.sorted(PreferenceComparators.timeComparatorIdxTimePref).mapToInt(pref -> pref.v1).boxed()
					.collect(Collectors.toList());

			if (newerItemsFirst) {
				Collections.reverse(lstIndexUser);
			}

			sequenceOfItemsAt.put(uidx, lstIndexUser.toArray(new Integer[lstIndexUser.size()]));
		});
	}

	@Override
	public Int2DoubleMap getScoresMap(int uidx) {
		Int2DoubleOpenHashMap scoresMap = new Int2DoubleOpenHashMap();
		scoresMap.defaultReturnValue(0.0);

		Integer[] userItems = sequenceOfItemsAt.get(uidx);
		if (userItems != null) {
			int score = 1;
			for (Integer item: userItems) {
				// the items are already sorted, so we simply return the inverse so that items first in the array get larger scores
				if (scoresMap.get(item.intValue()) == 0.0) {
					scoresMap.put(item.intValue(), 1.0 / score);
					score++;
				}
			}
		}

		return scoresMap;
	}

}

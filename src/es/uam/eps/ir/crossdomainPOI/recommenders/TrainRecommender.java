/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.crossdomainPOI.recommenders;


import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.rec.fast.FastRankingRecommender;
import it.unimi.dsi.fastutil.ints.Int2DoubleMap;
import it.unimi.dsi.fastutil.ints.Int2DoubleOpenHashMap;

/***
 * Recommender that will return the train of the user by the score. In case that there is a TIE in the score, 
 * the items that have higher popularity will have a higher score
 *
 * @param <U>
 * @param <I>
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 */
public class TrainRecommender <U, I> extends FastRankingRecommender<U, I> {
	  protected final FastPreferenceData<U, I> dataTrain;
	    private final boolean reverse;

		public TrainRecommender(FastPreferenceData<U, I> dataTrain, boolean reverse) {
			super(dataTrain,dataTrain);
			this.dataTrain = dataTrain;
			this.reverse = reverse;
		}

		@Override
		public Int2DoubleMap getScoresMap(int uidx) {
			Int2DoubleOpenHashMap scoresMap = new Int2DoubleOpenHashMap();
		    scoresMap.defaultReturnValue(0.0);
			
		    List<TrainItem> consumedByUser = dataTrain.getUidxPreferences(uidx).map(pref -> new TrainItem(pref.v1,pref.v2,dataTrain.getIidxPreferences(pref.v1).count())).collect(Collectors.toList());
	    	Collections.sort(consumedByUser);

		    if (reverse) {
		    	Collections.reverse(consumedByUser);	
		    }
		    
		    AtomicInteger integerScore = new AtomicInteger(1);
		    consumedByUser.stream().forEach(trainitem -> {
		    	if (scoresMap.get(trainitem.iidx) == 0) {
		    		scoresMap.put(trainitem.iidx, integerScore.getAndIncrement());
		    	}
		    });
		    
		    return scoresMap;
		}
		
		class TrainItem implements Comparable<TrainItem>{
			int iidx;
			double score;
			long totalPreferences;
			
			public TrainItem(int iidex, double score, long totalPreferences) {
				this.iidx = iidex;
				this.score = score;
				this.totalPreferences = totalPreferences;
			}

			@Override
			public int compareTo(TrainItem o) {
				if (this.score > o.score) {
					return 1;
				}else if (this.score < o.score) {
					return -1;
				}else {
					return Long.compare(this.totalPreferences, o.totalPreferences);
				}
			}
			
		}

}

/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.crossdomainPOI.mains;

import static org.ranksys.formats.parsing.Parsers.lp;
import static org.ranksys.formats.parsing.Parsers.sp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.jooq.lambda.tuple.Tuple4;
import org.ranksys.formats.feature.SimpleFeaturesReader;
import org.ranksys.formats.preference.SimpleRatingPreferencesReader;
import org.ranksys.formats.rec.RecommendationFormat;
import org.ranksys.formats.rec.SimpleRecommendationFormat;

import es.uam.eps.ir.antimetrics.antirel.BinaryAntiRelevanceModel;
import es.uam.eps.ir.antimetrics.utils.AverageRecommendationMetricIgnoreNoRelevantUsersAndNaNs;
import es.uam.eps.ir.attrrec.datamodel.feature.SimpleUserFeatureData;
import es.uam.eps.ir.attrrec.datamodel.feature.SimpleUserFeaturesReader;
import es.uam.eps.ir.attrrec.datamodel.feature.UserFeatureData;
import es.uam.eps.ir.attrrec.main.AttributeRecommendationUtils;
import es.uam.eps.ir.attrrec.metrics.recommendation.averages.WeightedAverageRecommendationMetricIgnoreNoRelevantUsersAndNaNs;
import es.uam.eps.ir.attrrec.metrics.recommendation.averages.WeightedModelUser;
import es.uam.eps.ir.attrrec.metrics.recommendation.averages.WeightedModelUser.UserMetricWeight;
import es.uam.eps.ir.attrrec.metrics.system.RealAggregateDiversity;
import es.uam.eps.ir.attrrec.metrics.system.UserCoverage;
import es.uam.eps.ir.attrrec.utils.PredicatesStrategies;
import es.uam.eps.ir.crossdomainPOI.datamodel.SimpleFastTemporalFeaturePreferenceData;
import es.uam.eps.ir.crossdomainPOI.datamodel.SimpleFastTemporalPreferenceData;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.utils.ExtendedPreferenceReader;
import es.uam.eps.ir.crossdomainPOI.datamodel.wrappers.SimpleFastTemporalFeaturePreferenceDataWrapper;
import es.uam.eps.ir.crossdomainPOI.datamodel.wrappers.SimpleFastTemporalFeaturePreferenceDataWrapper.RepetitionsStrategyPreference;
import es.uam.eps.ir.crossdomainPOI.utils.FoursqrProcessData;
import es.uam.eps.ir.crossdomainPOI.utils.SequentialRecommendersUtils;
import es.uam.eps.ir.crossdomainPOI.utils.UsersMidPoints.SCORES_FREQUENCY;
import es.uam.eps.ir.ranksys.core.feature.FeatureData;
import es.uam.eps.ir.ranksys.core.feature.SimpleFeatureData;
import es.uam.eps.ir.ranksys.core.preference.ConcatPreferenceData;
import es.uam.eps.ir.ranksys.core.preference.PreferenceData;
import es.uam.eps.ir.ranksys.core.preference.SimplePreferenceData;
import es.uam.eps.ir.ranksys.diversity.intentaware.FeatureIntentModel;
import es.uam.eps.ir.ranksys.diversity.intentaware.IntentModel;

import es.uam.eps.ir.ranksys.diversity.sales.metrics.AggregateDiversityMetric;
import es.uam.eps.ir.ranksys.diversity.sales.metrics.GiniIndex;
import es.uam.eps.ir.ranksys.fast.index.FastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.FastUserIndex;
import es.uam.eps.ir.ranksys.fast.index.SimpleFastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.SimpleFastUserIndex;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.fast.preference.SimpleFastPreferenceData;
import es.uam.eps.ir.ranksys.metrics.RecommendationMetric;
import es.uam.eps.ir.ranksys.metrics.SystemMetric;
import es.uam.eps.ir.ranksys.metrics.basic.AverageRecommendationMetric;
import es.uam.eps.ir.ranksys.metrics.rank.NoDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rank.RankingDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rel.BinaryRelevanceModel;
import es.uam.eps.ir.ranksys.metrics.rel.NoRelevanceModel;
import es.uam.eps.ir.ranksys.metrics.rel.RelevanceModel;

import es.uam.eps.ir.ranksys.novdiv.distance.CosineFeatureItemDistanceModel;
import es.uam.eps.ir.ranksys.novdiv.distance.ItemDistanceModel;
import es.uam.eps.ir.ranksys.novelty.temporal.TimestampCalculator;
import es.uam.eps.ir.ranksys.rec.Recommender;


import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.stream.IntStream;
import net.recommenders.rival.core.DataModel;
import net.recommenders.rival.core.SimpleParser;
import net.recommenders.rival.evaluation.metric.MultipleEvaluationMetricRunner;
import net.recommenders.rival.evaluation.statistics.StatisticalSignificance;
import net.recommenders.rival.evaluation.statistics.StatisticsRunner;

/**
 * Main of the framework.
 * 
 * Depending of the option selected, the main will proceed in different ways. Most important functionalities are:
 * -Splitting the dataset
 * -Generate Recommendations
 * -Evaluating the Recommendations
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public class Experiment {

	// All options of the arguments
	// Option of the case
	private static final String OPT_CASE = "option";

	// Train and test files
	private static final String OPT_TRAINFILE = "trainFile";
	private static final String OPT_TESTFILE = "testFile";
	private static final String OPT_TESTFILE2 = "testFile2";

	// Number of items to recommend in the recommenders	
	private static final String OPT_ITEMSRECOMMENDED = "itemsRecommended";
	
	// RankSys use the complete indexes of the files or not (users of both training and test or not)
	private static final String OPT_COMPLETEINDEXES = "completeIndexes";

	// Feature variables
	private static final String OPT_FEATURESITEMCOL = "featuresItemCol";
	private static final String OPT_FEATURESFEATCOL = "featuresFeatCol";
	private static final String OPT_USER_FEATURE_FILE = "user feature file";
	private static final String OPT_USER_FEATURE_SELECTED = "user feature selected";
	private static final String OPT_COMPUTE_USER_FILTER = "compute user filter";

	
	//Output files
	private static final String OPT_OUT_RESULT_FILE = "outResultfile";
	
	// RankSys similarities and recommender
	private static final String OPT_RANKSYS_SIM = "ranksysSimilarity";
	private static final String OPT_RANKSYS_SIM2 = "ranksysSimilarity 2";
	private static final String OPT_RANKSYS_REC = "ranksysRecommender";

	// Saving feature information
	private static final String OPT_SAVINGFEATURESCHEME = "savingFeaturesScheme";
	private static final String OPT_FEATUREREADINGFILE = "featureFile";
	private static final String OPT_FEATUREREADINGFILE2 = "featureFile2";

	// Overwrite output file or not. Default value is false.
	private static final String OPT_OVERWRITE = "outResultFileOverwrite";

	// Variables only used in rankSysEvaluation
	private static final String OPT_RECOMMENDEDFILE = "recommendedFile";
	private static final String OPT_RECOMMENDEDFILES = "recommendedFiles";
	private static final String OPT_CUTOFF = "ranksysCutoff";

	// Matching threshold to consider when are 2 ratings equal
	private static final String OPT_MATCHINGTHRESHOLD = "matching threshold";

	//Working with tourists and locals and bots
	private static final String OPT_MIN_DIFF_TIME = "min diff time";
	private static final String OPT_MAX_DIFF_TIME = "Max difference between timestamps";
	private static final String OPT_MIN_CLOSE_PREF_BOT = "min close pref bot";

	
	//Knn recommenders
	private static final String OPT_NEIGH = "neighbours";
	
	// Parameters for RankGeoFM
	private static final String OPT_EPSILON = "epsilon";
	private static final String OPT_C = "c";
	
	// Parameters for Ranksys Recommender MatrixFactorization
	private static final String OPT_KFACTORIZER = "k factorizer value";
	private static final String OPT_ALPHAFACTORIZER = "alpha factorizer value";
	private static final String OPT_LAMBDAFACTORIZER = "lambda factorizer value";
	private static final String OPT_NUMINTERACTIONSFACTORIZER = "num interactions factorizer value";

	// Parameters for ranksys non accuracy evaluation
	private static final String OPT_RANKSYSRELEVANCEMODEL = "ranksys relevance model";
	private static final String OPT_RANKSYSDISCOUNTMODEL = "ranksys discount model";
	private static final String OPT_RANKSYSBACKGROUND = "ranksys background for relevance model";
	private static final String OPT_RANKSYSBASE = "ranksys base for discount model";

	// Files of items and times for our freshness/novelty metrics
	private static final String OPT_TIMESAVERAGEITEMS = "file of average time items";
	private static final String OPT_TIMESFIRSTITEMS = "file of first time items";
	private static final String OPT_TIMESLASTITEMS = "file of last time items";
	private static final String OPT_TIMESMEDIANITEMS = "file of median time items";
	private static final String OPT_RELEASEDATEITEMS = "file of release dates of items";

	// Mappings
	private static final String OPT_MAPPINGITEMS = "file of mapping items";
	private static final String OPT_MAPPINGUSERS = "file of mapping users";
	private static final String OPT_MAPPINGCATEGORIES = "file of mapping categories";

	private static final String OPT_NEWDATASET = "file for the new dataset";
	private static final String OPT_COORDFILE = "poi coordinate file";
	private static final String OPT_POICITYFILE = "poi city mapping file";
	private static final String OPT_CITYSIMFILE = "city similarity file";
	private static final String OPT_WRAPPERSTRATEGY = "wrapper strategy for ranksys (removing timestamps)";
	private static final String OPT_WRAPPERSTRATEGYTIMESTAMPS = "wrapper strategy for ranksys (strategy for the timestamps)";
	private static final String OPT_MATCHINGCATEGORY = "matching category for recommendation";
	
	//Parameters
	private static final String OPT_MAX_DISTANCE = "maximum distance";
	private static final String OPT_USE_SIGMOID = "useSigmoid";
	private static final String OPT_THETA_FACTORIZER = "theta factorizer value";
	private static final String OPT_ALPHA_FACTORIZER2 = "alpha factorizer (2) value";
	private static final String OPT_SVD_BETA = "Beta value for SVD";
	private static final String OPT_USINGWRAPPER = "using wrapper in ranksys recommenders";

	private static final String OPT_RECOMMENDATIONSTRATEGY = "recommendation strategy";

	// Regularization for SLIM recommender
	private static final String OPT_REGULARIZATIONL1 = "Regularization 1";
	private static final String OPT_REGULARIZATIONL2 = "Regularization 2";

	//Parameters for SVD
	private static final String OPT_SVDNUMBERBINS = "Bins for SVD recommender";
	private static final String OPT_SVDBETA = "Beta value for SVD";
	private static final String OPT_SVDTIMESTRATEGY = "Time strategy for SVD";
	private static final String OPT_SVDREGUSER = "regularization for users for SVD";
	private static final String OPT_SVDREGITEM = "regularization for items for SVD";
	private static final String OPT_SVDREGBIAS = "regularization for biases for SVD";
	private static final String OPT_SVDLEARNRATE = "learning rate for SVD";
	private static final String OPT_SVDMAXLEARNRATE = "maximum learning rate for SVD";
	private static final String OPT_SVDDECAY = "decay for SVD";
	private static final String OPT_SVDISBOLDDRIVER = "bold river for SVD";
	private static final String OPT_SVDREGIMPITEM = "regularization for implicit items for SVD";

	private static final String OPT_MODE = "specific mode of an algorithm";

	private static final String OPT_USER_MODEL_WGT = "user weight model weight";
	private static final String OPT_COMPUTEANTIMETRICS = "Compute anti-metrics";
	private static final String OPT_ANTIRELEVANCETHRESHOLD = "Anti relevance threshold";
	private static final String OPT_COMPUTEONLYACC = "compute also dividing by the number of users in the test set";
	private static final String OPT_SCOREFREQ = "use simple score or the frequency for the AvgDis";
	private static final String OPT_LCS_EVALUATION = "LCS evaluation for taking into ccount the temporal order of the items";
	

	
	
	// Some default values for recommenders as strings

	//For evaluation
	private static final String DEFAULT_ITEMS_RECOMMENDED = "100";
	private static final String DEFAULT_RECOMMENDATION_STRATEGY = "TRAIN_ITEMS";
	private static final String DEFAULT_OVERWRITE = "false";
	private static final String DEFAULT_OPT_COMPUTEONLYACC = "true";
	private static final String DEFAULT_LCS_EVALUTATION = "false";
	private static final String DEFAULT_COMPUTEANTIMETRICS = "false";
	private static final String DEFAULT_COMPUTE_USER_FILTER = "false";
	private static final String DEFAULT_USER_MODEL_WGT = "NORMAL";


	
	//SVD parameters from librec: https://github.com/guoguibing/librec/blob/3ab54c4432ad573678e7b082f05785ed1ff31c5c/core/src/main/java/net/librec/recommender/MatrixFactorizationRecommender.java
	private static final String DEFAULT_SVDLEARNINGRATE = "0.01f";
	private static final String DEFAULT_SVDMAXLEARNINGRATE = "1000f";
	private static final String DEFAULT_SVDREGUSER = "0.01f";
	private static final String DEFAULT_SVDREGITEM = "0.01f";
	private static final String DEFAULT_SVDREGBIAS = "0.01f";
	private static final String DEFAULT_ISBOLDDRIVER = "false";
	private static final String DEFAULT_DECAY = "1f";
	private static final String DEFAULT_REGIMPITEM = "0.025f";
	
	//Other default values for recommenders
	private static final String DEFAULT_COMPLETE_INDEXES = "false";
	private static final String DEFAULT_MAX_DISTANCE = "0.1";
	private static final String DEFAULT_ALPHA_HKV = "1"; 
	private static final String DEFAULT_SVDBETA = "1";
	private static final String DEFAULT_SCORE = "SIMPLE";
	private static final String DEFAULT_REG1 = "0.01"; // from mymedialite
	private static final String DEFAULT_REG2 = "0.001"; // from mymedialite
	
	// Some values for rank geoFM from Librec
	private static final String DEFAULT_EPSILON = "0.3";
	private static final String DEFAULT_C = "1.0";
		
		
	public static void main(String[] args) throws Exception {

		String step = "";
		CommandLine cl = getCommandLine(args);
		if (cl == null) {
			System.out.println("Error in arguments");
			return;
		}

		// Obtain the arguments these 2 are obligatory
		step = cl.getOptionValue(OPT_CASE);
		String trainFile = cl.getOptionValue(OPT_TRAINFILE);

		System.out.println(step);
		switch (step) {

		//Skyline recommenders are the ones that recommend the test set
		case "skylineRecommenders":	
		case "testRecommenders": {
			//They need to receive the test set. We do not work with the train file	
			System.out.println(Arrays.toString(args));
			String outputFile = cl.getOptionValue(OPT_OUT_RESULT_FILE);
			String testFile = cl.getOptionValue(OPT_TESTFILE);	
			boolean completeOrNot = Boolean.parseBoolean(cl.getOptionValue(OPT_COMPLETEINDEXES, DEFAULT_COMPLETE_INDEXES));
			String recommendationStrategy = cl.getOptionValue(OPT_RECOMMENDATIONSTRATEGY,
					DEFAULT_RECOMMENDATION_STRATEGY);
			final int numberItemsRecommend = Integer
					.parseInt(cl.getOptionValue(OPT_ITEMSRECOMMENDED, DEFAULT_ITEMS_RECOMMENDED));

			String rankSysRecommender = cl.getOptionValue(OPT_RANKSYS_REC);
			String overwrite = cl.getOptionValue(OPT_OVERWRITE, DEFAULT_OVERWRITE);
			
			File f = new File(outputFile);
			if (f.exists() && !f.isDirectory() && Boolean.parseBoolean(overwrite) == false) {
				System.out.println("Ignoring " + f + " because it already exists");
				return;
			}

			Tuple4<List<Long>, List<Long>, List<Long>, List<Long>> indexes = ExperimentUtils
					.retrieveTrainTestIndexes(completeOrNot, trainFile, testFile, lp, lp);
			List<Long> usersTrain = indexes.v1;
			List<Long> itemsTrain = indexes.v2;
			List<Long> usersTest = indexes.v3;
			List<Long> itemsTest = indexes.v4;

			FastUserIndex<Long> userIndexTrain = SimpleFastUserIndex.load(usersTrain.stream());
			FastItemIndex<Long> itemIndexTrain = SimpleFastItemIndex.load(itemsTrain.stream());

			FastUserIndex<Long> userIndexTest = SimpleFastUserIndex.load(usersTest.stream());
			FastItemIndex<Long> itemIndexTest = SimpleFastItemIndex.load(itemsTest.stream());
			
			FastPreferenceData<Long, Long> ranksysTrainDataOriginal = SimpleFastPreferenceData
					.load(SimpleRatingPreferencesReader.get().read(trainFile, lp, lp), userIndexTrain, itemIndexTrain);
			
			FastPreferenceData<Long, Long> ranksysTestDataOriginal = SimpleFastPreferenceData
					.load(SimpleRatingPreferencesReader.get().read(testFile, lp, lp), userIndexTest, itemIndexTest);

			Stream<Tuple4<Long, Long, Double, Long>> tuplesStreamTest = ExtendedPreferenceReader.get()
					.readPreferencesTimeStamp(testFile, lp, lp);
			SimpleFastTemporalFeaturePreferenceData<Long, Long, Long, Double> ranksysTestTemporal = SimpleFastTemporalFeaturePreferenceData
					.loadTemporalFeature(tuplesStreamTest, userIndexTest, itemIndexTest);
			
			
			Recommender<Long, Long> rec = SequentialRecommendersUtils.obtRankSysSkylineRecommender(rankSysRecommender, ranksysTestTemporal);

			PredicatesStrategies.ranksysWriteRanking(ranksysTrainDataOriginal, ranksysTestDataOriginal, rec, outputFile, numberItemsRecommend, ExperimentUtils.obtRecommendationStrategy(recommendationStrategy));
			
		}	
		break;
		
		case "ParseMyMediaLite": {
			System.out.println("-o ParseMyMediaLite -trf myMediaLiteRecommendation testFile newRecommendation");
			System.out.println(Arrays.toString(args));
			Tuple4<List<Long>, List<Long>, List<Long>, List<Long>> indexes = ExperimentUtils
					.retrieveTrainTestIndexes(false, args[4], args[4], lp, lp);

			List<Long> usersTest = indexes.v1;
			FastUserIndex<Long> userIndexTest = SimpleFastUserIndex.load(usersTest.stream());

			SequentialRecommendersUtils.parseMyMediaLite(trainFile, userIndexTest, args[5]);
		}
			break;	
		
		
			

		

		// RankSys only (not reading similarities). Not working with time datasets
		// (working with FastPreferenceData only)
		case "ranksysOnlyComplete": {
			/*
			 * Necessary arguments: -Option of the case. -Training file. -OutputFile -Users
			 * file. -Items file. -testFile -ranksysimilarity the string of the similarity
			 * that ranksys framework will use -ranksysRecommender the string of the
			 * recommeder that ranksys framework will use -numberItemsRecommend in the
			 * recommendation -neighbours to use in the recommendations
			 * 
			 */
			String outputFile = cl.getOptionValue(OPT_OUT_RESULT_FILE);
			String ranksysSimilarity = cl.getOptionValue(OPT_RANKSYS_SIM);
			String ranksysSimilarity2 = cl.getOptionValue(OPT_RANKSYS_SIM2);

			String ranksysRecommender = cl.getOptionValue(OPT_RANKSYS_REC);
			String testFile = cl.getOptionValue(OPT_TESTFILE);
			String testFile2 = cl.getOptionValue(OPT_TESTFILE2); // this is the test file for foursqr with the cross
																	// domain approach

			// Check if we include factorizers
			String kFactorizer = cl.getOptionValue(OPT_KFACTORIZER);
			String alphaFactorizer = cl.getOptionValue(OPT_ALPHAFACTORIZER);
			String lambdaFactorizer = cl.getOptionValue(OPT_LAMBDAFACTORIZER);
			String numInteractions = cl.getOptionValue(OPT_NUMINTERACTIONSFACTORIZER);
			
			//SVD Parameters
			Double regUser = Double.parseDouble(cl.getOptionValue(OPT_SVDREGUSER, DEFAULT_SVDREGUSER));
			Double regItem = Double.parseDouble(cl.getOptionValue(OPT_SVDREGITEM, DEFAULT_SVDREGITEM));
			Double regBias = Double.parseDouble(cl.getOptionValue(OPT_SVDREGBIAS, DEFAULT_SVDREGBIAS));
			Double learnRate = Double.parseDouble(cl.getOptionValue(OPT_SVDLEARNRATE, DEFAULT_SVDLEARNINGRATE));
			Double maxRate = Double.parseDouble(cl.getOptionValue(OPT_SVDMAXLEARNRATE, DEFAULT_SVDMAXLEARNINGRATE));
			Boolean isboldDriver = Boolean.parseBoolean(cl.getOptionValue(OPT_SVDISBOLDDRIVER, DEFAULT_ISBOLDDRIVER));
			Double regImpItem = Double.parseDouble(cl.getOptionValue(OPT_SVDREGIMPITEM, DEFAULT_REGIMPITEM));
			Double decay = Double.parseDouble(cl.getOptionValue(OPT_SVDDECAY, DEFAULT_DECAY));
			
			
			//
			
			Double maxDistance = Double.parseDouble(cl.getOptionValue(OPT_MAX_DISTANCE, DEFAULT_MAX_DISTANCE));
			String mode = cl.getOptionValue(OPT_MODE);
			Double theta = Double.parseDouble(cl.getOptionValue(OPT_THETA_FACTORIZER, DEFAULT_ALPHA_HKV));
			Double alphaFactorizer2 = Double.parseDouble(cl.getOptionValue(OPT_ALPHA_FACTORIZER2, DEFAULT_ALPHA_HKV));
			boolean useSigmoid = Boolean.parseBoolean(cl.getOptionValue(cl.getOptionValue(OPT_USE_SIGMOID, "false")));
			Double beta = Double.parseDouble(cl.getOptionValue(OPT_SVD_BETA, DEFAULT_SVDBETA));


			
			// For candidates items matching a category
			String featureFile = cl.getOptionValue(OPT_FEATUREREADINGFILE);
			String realFeatureFile = cl.getOptionValue(OPT_FEATUREREADINGFILE2);
			System.out.println("real feature file " + realFeatureFile);
			String itemFeatureColumn = cl.getOptionValue(OPT_FEATURESITEMCOL);
			String featureColumn = cl.getOptionValue(OPT_FEATURESFEATCOL);
			String categoryMatch = cl.getOptionValue(OPT_MATCHINGCATEGORY);

			String poiCoordsFile = cl.getOptionValue(OPT_COORDFILE);
			String poiCityFile = cl.getOptionValue(OPT_POICITYFILE);
			String citySimFile = cl.getOptionValue(OPT_CITYSIMFILE);

			String regularization1S = cl.getOptionValue(OPT_REGULARIZATIONL1, DEFAULT_REG1);
			String regularization2S = cl.getOptionValue(OPT_REGULARIZATIONL2, DEFAULT_REG2);
			
			String scoreFreq = cl.getOptionValue(OPT_SCOREFREQ, DEFAULT_SCORE);

			Double regularization1 = Double.parseDouble(regularization1S);
			Double regularization2 = Double.parseDouble(regularization2S);

			String recommendationStrategy = cl.getOptionValue(OPT_RECOMMENDATIONSTRATEGY,
					DEFAULT_RECOMMENDATION_STRATEGY);

			String usingwrapper = cl.getOptionValue(OPT_USINGWRAPPER, "false");
			boolean usingwrapperP = Boolean.parseBoolean(usingwrapper);
			int kFactorizeri = 50;
			double alphaFactorizeri = 1;
			double lambdaFactorizeri = 0.1;
			int numInteractioni = 20;
			
			// Rank Geo
			Double epsilon = Double.parseDouble(cl.getOptionValue(OPT_EPSILON, DEFAULT_EPSILON));
			Double c = Double.parseDouble(cl.getOptionValue(OPT_C, DEFAULT_C));
			
			
			SCORES_FREQUENCY scoref = ExperimentUtils.obtScoreFreq(scoreFreq);

			if (ranksysSimilarity == null && ranksysSimilarity2 == null) {
				System.out.println("Not working with any recommender using similarities");
			}

			if (ranksysSimilarity2 == null) {
				ranksysSimilarity2 = ranksysSimilarity;
			}

			if (kFactorizer != null) {
				kFactorizeri = Integer.parseInt(kFactorizer);
				System.out.println("Working with kFactorizeri=" + kFactorizeri);
			}

			if (alphaFactorizer != null) {
				alphaFactorizeri = Double.parseDouble(alphaFactorizer);
				System.out.println("Working with alphaFactorizer=" + alphaFactorizeri);
			}
			if (lambdaFactorizer != null) {
				lambdaFactorizeri = Double.parseDouble(lambdaFactorizer);
				System.out.println("Working with lambdaFactorizer=" + lambdaFactorizer);
			}
			if (numInteractions != null) {
				numInteractioni = Integer.parseInt(numInteractions);
				System.out.println("Working with numInteractions=" + numInteractions);
			}

			final int numberItemsRecommend = Integer.parseInt(cl.getOptionValue(OPT_ITEMSRECOMMENDED));
			int neighbours = Integer.parseInt(cl.getOptionValue(OPT_NEIGH));

			System.out.println(Arrays.toString(args));

			String[] outputsFiles = outputFile.split(",");
			String overwrite = cl.getOptionValue(OPT_OVERWRITE, DEFAULT_OVERWRITE);

			boolean allExist = true;
			for (String outputSingleFile : outputsFiles) {
				File f = new File(outputSingleFile);
				if (f.exists() && !f.isDirectory() && Boolean.parseBoolean(overwrite) == false) {
					System.out.println("Ignoring " + f + " because it already exists");
				} else {
					System.out.println(f + " does not exist. Computing.");
					allExist = false;
				}
			}
			if (allExist && Boolean.parseBoolean(overwrite) == false) // If all the outputFiles exist and the overwrite is in false, we end
			{
				return;
			}
			boolean completeOrNot = Boolean.parseBoolean(cl.getOptionValue(OPT_COMPLETEINDEXES, DEFAULT_COMPLETE_INDEXES));
			Tuple4<List<Long>, List<Long>, List<Long>, List<Long>> indexes = null;

			String[] differentTests = testFile.split(",");

			// Testfile2 should be a concatenation of all test files that we are using
			if (differentTests.length == 1) {
				indexes = ExperimentUtils.retrieveTrainTestIndexes(completeOrNot, trainFile, testFile, lp, lp);
			}
			else {
				indexes = ExperimentUtils.retrieveTrainTestIndexes(completeOrNot, trainFile, testFile2, lp, lp);
			}
			List<Long> usersTrain = indexes.v1;
			List<Long> itemsTrain = indexes.v2;
			List<Long> usersTest = indexes.v3;
			List<Long> itemsTest = indexes.v4;

			FastUserIndex<Long> userIndexTrain = SimpleFastUserIndex.load(usersTrain.stream());
			FastItemIndex<Long> itemIndexTrain = SimpleFastItemIndex.load(itemsTrain.stream());

			FastUserIndex<Long> userIndexTest = SimpleFastUserIndex.load(usersTest.stream());
			FastItemIndex<Long> itemIndexTest = SimpleFastItemIndex.load(itemsTest.stream());

			FastPreferenceData<Long, Long> trainPrefData = null;
			FastPreferenceData<Long, Long> testPrefData = null;
			
			//
			
			

			if (usingwrapperP) {
				System.out.println("Using wrapper (working with timestamps)");
				Stream<Tuple4<Long, Long, Double, Long>> tuplesStreamTrain = ExtendedPreferenceReader.get()
						.readPreferencesTimeStamp(trainFile, lp, lp);
				SimpleFastTemporalFeaturePreferenceData<Long, Long, Long, Double> ranksysTrainTemporal = SimpleFastTemporalFeaturePreferenceData
						.loadTemporalFeature(tuplesStreamTrain, userIndexTrain, itemIndexTrain);

				// Create the wrapper to be able to work with the ranksys datamodel
				trainPrefData = new SimpleFastTemporalFeaturePreferenceDataWrapper<>(ranksysTrainTemporal,
						RepetitionsStrategyPreference.FIRST);

			} else {
				System.out.println("Not using wrapper (working with no timestamps)");
				trainPrefData = SimpleFastPreferenceData.load(
						SimpleRatingPreferencesReader.get().read(trainFile, lp, lp), userIndexTrain, itemIndexTrain);

			}
			
			FeatureData<Long, String, Double> featureData = null;
			if (realFeatureFile != null) {
				featureData = SimpleFeatureData.load(SimpleFeaturesReader.get().read(realFeatureFile, lp, sp));
			}
			
			System.out.println("Some stats: ");
			System.out.println("Users: " + trainPrefData.numUsers());
			System.out.println("Items: " + trainPrefData.numItems());
			System.out.println("Users with preferences: " + trainPrefData.numUsersWithPreferences());
			System.out.println("Items with preferences: " + trainPrefData.numItemsWithPreferences());
			System.out.println("Num preferences: " + trainPrefData.numPreferences());
			

			Recommender<Long, Long> rankSysrec = SequentialRecommendersUtils.obtRankSysRecommeder(ranksysRecommender,
					ranksysSimilarity, ranksysSimilarity2, trainPrefData, neighbours, userIndexTrain, itemIndexTrain,
					kFactorizeri, alphaFactorizeri, lambdaFactorizeri, numInteractioni, poiCoordsFile,
					realFeatureFile, poiCityFile, citySimFile, regularization1, regularization2, scoref, regUser, regItem, learnRate, maxRate, regBias, decay, isboldDriver, regImpItem, 
					beta, maxDistance, mode, theta, alphaFactorizer2, useSigmoid, featureData, c, epsilon);

			String[] candidatesFile = null;
			String[] categoriesMatch = null;
			if (featureFile != null) {
				candidatesFile = featureFile.split(",");
			}
			if (categoryMatch != null) {
				categoriesMatch = categoryMatch.split(",");
			}
			
			

			for (int i = 0; i < differentTests.length; i++) {
				System.out.println("Analyzing " + differentTests[i]);
				System.out.println("Recommender file " + outputsFiles[i]);
				File f = new File(outputsFiles[i]);
				if (f.exists() && !f.isDirectory() && Boolean.parseBoolean(overwrite) == false) {
					System.out.println(outputsFiles[i] + " exist, ignoring that recommender file");
					continue;
				}

				if (usingwrapperP) {
					Stream<Tuple4<Long, Long, Double, Long>> tuplesStreamTest = ExtendedPreferenceReader.get()
							.readPreferencesTimeStamp(differentTests[i], lp, lp);
					SimpleFastTemporalFeaturePreferenceData<Long, Long, Long, Double> ranksysTestTemporal = SimpleFastTemporalFeaturePreferenceData
							.loadTemporalFeature(tuplesStreamTest, userIndexTest, itemIndexTest);
					testPrefData = new SimpleFastTemporalFeaturePreferenceDataWrapper<>(ranksysTestTemporal,
							RepetitionsStrategyPreference.FIRST);
				} else {
					testPrefData = SimpleFastPreferenceData.load(
							SimpleRatingPreferencesReader.get().read(differentTests[i], lp, lp), userIndexTest,
							itemIndexTest);
				}

				// Normal recommendation. Only recommend for test users items that have not been
				// seen in train
				if (featureFile == null || itemFeatureColumn == null || featureColumn == null
						|| categoryMatch == null) {
					System.out.println(
							"Writing recommended file. Not items candidates file provided. All candidates are the items not seen by that user in train.");
					PredicatesStrategies.ranksysWriteRanking(trainPrefData, testPrefData, rankSysrec,
							outputsFiles[i], numberItemsRecommend,
							ExperimentUtils.obtRecommendationStrategy(recommendationStrategy));
				} else {
					System.out.println("Matching category " + categoriesMatch[i]);
					System.out.println("candidates " + candidatesFile[i]);
					System.out.println(
							"Writing recommended file. Item candidates file provided. Candidates are items from that file that have not been rated by that user in train.");
					Stream<Long> candidates = SequentialRecommendersUtils.obtainCandidatesFile(candidatesFile[i],
							categoriesMatch[i], lp, Integer.parseInt(featureColumn),
							Integer.parseInt(itemFeatureColumn));
					PredicatesStrategies.ranksysWriteRankingFromCandidatesList(trainPrefData, testPrefData,
							rankSysrec, outputsFiles[i], numberItemsRecommend, candidates.collect(Collectors.toSet()), ExperimentUtils.obtRecommendationStrategy(recommendationStrategy) );
				}
			}

		}
			break;
			
		case "StatisticsFoursqr": {
			System.out.println(
					"-o StatisticsFoursqr -trf trainFileRep trainFileNoRep testFile poiCitiesFile resultStatistics");
			System.out.println(Arrays.toString(args));
			FoursqrProcessData.statisticsFoursqr(trainFile, args[4], args[5], args[6], args[7]);
		}
			break;

		case "CheckRecommendationsFile": {
			System.out.println(Arrays.toString(args));
			String featureFile = cl.getOptionValue(OPT_FEATUREREADINGFILE);
			String recFile = cl.getOptionValue(OPT_RECOMMENDEDFILE);
			String matchCat = cl.getOptionValue(OPT_MATCHINGCATEGORY);
			String testFile = cl.getOptionValue(OPT_TESTFILE);
			FoursqrProcessData.checkRecommendationCorrection(trainFile, featureFile, recFile, matchCat, testFile);
		}
			break;
		// Foursqr mains
		case "FoursqrCheckinsPerCountry": {
			System.out.println("-o FoursqrCheckinsPerCountry -trf checkins venues cities destinationPath");
			System.out.println(Arrays.toString(args));
			FoursqrProcessData.generateFilesPerCountry(args[4], args[5], trainFile, args[6]);
		}
			break;

		case "FoursqrCheckinsPerCity": {
			System.out.println("-o FoursqrCheckinsPerCity -trf checkins venues cities destinationPath MappingFile");
			System.out.println(Arrays.toString(args));
			FoursqrProcessData.generateFilesPerCity(args[4], args[5], trainFile, args[6], args[7]);
		}
			break;

		case "FoursqrMappingPoiCity": {
			System.out.println("-o FoursqrCheckinsPerCity -trf checkins venues cities MappingFile");
			System.out.println(Arrays.toString(args));
			FoursqrProcessData.generateFilesPerCity(args[4], args[5], trainFile, null, args[6]);
		}
			break;

		case "FoursqrDatesPerCity": {
			System.out.println("-o FoursqrDatesPerCity -trf checkins cities processDates?");
			System.out.println(Arrays.toString(args));
			FoursqrProcessData.computeTimeIntervalsPerCity(trainFile, args[4], Boolean.parseBoolean(args[5]));
		}
			break;

		case "FoursqrNewPoisIdsPerCity": {
			String itemMap = cl.getOptionValue(OPT_MAPPINGITEMS);
			System.out.println(
					"-o FoursqrNewPoisIdsPerCity -trf OriginalPoisPerCity -IMapping itemMap destinationPoisPerCity");
			System.out.println(Arrays.toString(args));
			FoursqrProcessData.parsePOISCities(trainFile, itemMap, args[6]);
		}
			break;

		case "FoursqrFilterFileByCountryCode": {
			System.out
					.println("-o FoursqrFilterFileByCountryCode -trf checkins countryCode fileCountryCodes resultFile");
			System.out.println(Arrays.toString(args));
			FoursqrProcessData.filterCheckingFileByCountryCode(trainFile, args[4], args[5], args[6]);
		}
			break;

		case "FoursqrFilterFileByNCitiesOfCity": {
			System.out.println(
					"-o FoursqrFilterFileByNNCities -trf checkins city NNCities numberCitiesToConsider fileCountryCodes  resultFile");
			System.out.println(Arrays.toString(args));
			FoursqrProcessData.filterByNNCities(trainFile, args[4], args[5], Integer.parseInt(args[6]), args[7],
					args[8]);
		}
			break;

		//Train with repetitions and test with no repetitions AND with no preferences of the train set
		case "FoursqrGenerateSplits": {
			System.out.println(
					"-o FoursqrGenerateSplits -trf checkins cities processData? UserMapping ItemMapping cities_to_be_filtered K-core outfolder");
			System.out.println(Arrays.toString(args));

			//
			Boolean processData = Boolean.parseBoolean(args[5]);
			String userMappingFile = args[6];
			String itemMappingFile = args[7];
			SimpleFastTemporalPreferenceData<Long, Long> fullData = FoursqrProcessData.readFullDataset(trainFile,
					processData, userMappingFile, itemMappingFile);
			//
			Integer kCore = Integer.parseInt(args[9]);
			String filePoiCity = args[4];
			//
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssZ");
			String outfolder = args[10];
			Set<String> filterCities = new HashSet<>();
			String citiesToBeFiltered = args[8];
			if (new File(citiesToBeFiltered).exists()) {
				Stream<String> stream = Files.lines(Paths.get(citiesToBeFiltered));
				stream.forEach(line -> {
					filterCities.add(line.replaceAll("\\s+", ""));
				});
				stream.close();
			}
			// May-Oct, Nov 2012
			String train0 = "20120501000000+0000";
			String test0 = "20121101000000+0000";
			String end0 = "20121201000000+0000";
			Long[][] constraints0 = new Long[][] {
					{ sdf.parse(train0).getTime() / 1000, sdf.parse(test0).getTime() / 1000 },
					{ sdf.parse(test0).getTime() / 1000, sdf.parse(end0).getTime() / 1000 } };
			String[] suffix0 = new String[] { "training0.dat", "testWithTrain0.dat"};
			FoursqrProcessData.generateSplitting(outfolder, suffix0, constraints0, fullData, kCore, filePoiCity, filterCities);
			// Split between 1335823200 and 1351724400: 10,715,130
			// Split between 1351724400 and 1354316400: 90,705
			// Nov-Apr, May 2013
			String train1 = "20121101000000+0000";
			String test1 = "20130501000000+0000";
			String end1 = "20130601000000+0000";
			Long[][] constraints1 = new Long[][] {
					{ sdf.parse(train1).getTime() / 1000, sdf.parse(test1).getTime() / 1000 },
					{ sdf.parse(test1).getTime() / 1000, sdf.parse(end1).getTime() / 1000 } };
			String[] suffix1 = new String[] { "training1.dat", "testWithTrain1.dat"};
			FoursqrProcessData.generateSplitting(outfolder, suffix1, constraints1, fullData, kCore, filePoiCity, filterCities);
			// Split between 1351724400 and 1364767200: 11,652,730
			// Split between 1364767200 and 1367359200: 49,032
		}
			break;
			
		case "GenerateTestWithoutTrain" : {
			System.out.println(
					"-o GenerateTestWithoutTrain -trf originalTrain testWithTrain testWithoutTrain");
			
			String newTest = args[5];
			String previousTest = args[4];

			boolean completeOrNot = Boolean.parseBoolean(cl.getOptionValue(OPT_COMPLETEINDEXES, DEFAULT_COMPLETE_INDEXES));
			Tuple4<List<Long>, List<Long>, List<Long>, List<Long>> indexes = ExperimentUtils
					.retrieveTrainTestIndexes(completeOrNot, trainFile, previousTest, lp, lp);
			List<Long> usersTrain = indexes.v1;
			List<Long> itemsTrain = indexes.v2;
			
			List<Long> usersTest = indexes.v3;
			List<Long> itemsTest = indexes.v4;

			FastUserIndex<Long> userIndexTrain = SimpleFastUserIndex.load(usersTrain.stream());
			FastItemIndex<Long> itemIndexTrain = SimpleFastItemIndex.load(itemsTrain.stream());
			
			FastUserIndex<Long> userIndexTest = SimpleFastUserIndex.load(usersTest.stream());
			FastItemIndex<Long> itemIndexTest = SimpleFastItemIndex.load(itemsTest.stream());

			Stream<Tuple4<Long, Long, Double, Long>> tuplesStreamTrain = ExtendedPreferenceReader.get()
					.readPreferencesTimeStamp(trainFile, lp, lp);
			SimpleFastTemporalFeaturePreferenceData<Long, Long, Long, Double> ranksysTrainTemporal = SimpleFastTemporalFeaturePreferenceData
					.loadTemporalFeature(tuplesStreamTrain, userIndexTrain, itemIndexTrain);
			
			Stream<Tuple4<Long, Long, Double, Long>> tuplesStreamTest = ExtendedPreferenceReader.get()
					.readPreferencesTimeStamp(previousTest, lp, lp);
			
			SimpleFastTemporalFeaturePreferenceData<Long, Long, Long, Double> ranksysTestTemporal = SimpleFastTemporalFeaturePreferenceData
					.loadTemporalFeature(tuplesStreamTest, userIndexTest, itemIndexTest);
			
			ExperimentUtils.generateNewFile(ranksysTrainTemporal, ranksysTestTemporal, newTest);
			
			
			
		}	
		break;

		// Obtain files of new Indexes for users and items
		case "FoursqrObtainMapUsersItems": {
			System.out.println(
					"-o FoursqrObtainMapUsersItems -trf checkins -UMapping destinationUsers -IMapping destinationItems");
			System.out.println(Arrays.toString(args));

			String resultMappingItems = cl.getOptionValue(OPT_MAPPINGITEMS);
			String resultMappingUsers = cl.getOptionValue(OPT_MAPPINGUSERS);

			FoursqrProcessData.generateMapOfUsersVenues(trainFile, resultMappingUsers, resultMappingItems);
		}
			break;

		case "FoursqrObtainMapCategories": {
			String resultMappingCategories = cl.getOptionValue(OPT_MAPPINGCATEGORIES);
			System.out.println(
					"-o FoursqrObtainMapItemsCategories -trf originalPOISFile -CMapping resultMappingCategories");
			System.out.println(Arrays.toString(args));
			FoursqrProcessData.generateMapOfVenuesCategories(trainFile, resultMappingCategories);
		}
			break;

		case "FoursqrObtainPoisCoords": {
			System.out.println(
					"-o FoursqrObtainPoisCoords -trf originalPOISFile -IMapping fileMapItems -coordFile destinationCoords");
			System.out.println(Arrays.toString(args));

			String poisCoordsFile = cl.getOptionValue(OPT_COORDFILE);
			String mappingItems = cl.getOptionValue(OPT_MAPPINGITEMS);
			FoursqrProcessData.generatePoisCoords(trainFile, mappingItems, poisCoordsFile);
		}
			break;

		case "FoursqrObtainPoisFeatures": {
			System.out.println(
					"-o FoursqrObtainPoisFeatures -trf originalPOISFile -IMapping fileMapItems -CMapping fileMapCategories -sfs destinationFile");
			System.out.println(Arrays.toString(args));

			String featureFile = cl.getOptionValue(OPT_SAVINGFEATURESCHEME);
			String mappingItems = cl.getOptionValue(OPT_MAPPINGITEMS);
			String resultMappingCategories = cl.getOptionValue(OPT_MAPPINGCATEGORIES);

			FoursqrProcessData.generateFeatureFile(trainFile, mappingItems, resultMappingCategories, featureFile);
		}
			break;

		case "generateNewCheckingFileWithTimeStamps": {
			System.out.println(Arrays.toString(args));

			String userMapFile = cl.getOptionValue(OPT_MAPPINGUSERS);
			String itemsMapFile = cl.getOptionValue(OPT_MAPPINGITEMS);
			String newDataset = cl.getOptionValue(OPT_NEWDATASET);

			FoursqrProcessData.generateNewCheckinFileWithTimeStamps(trainFile, userMapFile, itemsMapFile, newDataset);
		}
			break;
			
		case "generateTouristLocalBotFile": {
			System.out.println(Arrays.toString(args));
			String outputResultFile = cl.getOptionValue(OPT_OUT_RESULT_FILE);
			Long maxDiff = Long.parseLong(cl.getOptionValue(OPT_MAX_DIFF_TIME));
			Long minDiff = Long.parseLong(cl.getOptionValue(OPT_MIN_DIFF_TIME));
			int minclosePrefForBots = Integer.parseInt(cl.getOptionValue(OPT_MIN_CLOSE_PREF_BOT));
			FoursqrProcessData.obtainLocalsAndTouristsByTime(trainFile, outputResultFile, maxDiff, minDiff, minclosePrefForBots);
		}
		break;

		

		case "AggregateWithWrapper": {
			System.out.println(Arrays.toString(args));
			String newDataset = cl.getOptionValue(OPT_NEWDATASET);
			String wrapperStrategy = cl.getOptionValue(OPT_WRAPPERSTRATEGY);

			boolean completeOrNot = Boolean.parseBoolean(cl.getOptionValue(OPT_COMPLETEINDEXES, DEFAULT_COMPLETE_INDEXES));
			Tuple4<List<Long>, List<Long>, List<Long>, List<Long>> indexes = ExperimentUtils
					.retrieveTrainTestIndexes(completeOrNot, trainFile, trainFile, lp, lp);
			List<Long> usersTrain = indexes.v1;
			List<Long> itemsTrain = indexes.v2;

			FastUserIndex<Long> userIndexTrain = SimpleFastUserIndex.load(usersTrain.stream());
			FastItemIndex<Long> itemIndexTrain = SimpleFastItemIndex.load(itemsTrain.stream());

			Stream<Tuple4<Long, Long, Double, Long>> tuplesStreamTrain = ExtendedPreferenceReader.get()
					.readPreferencesTimeStamp(trainFile, lp, lp);
			SimpleFastTemporalFeaturePreferenceData<Long, Long, Long, Double> ranksysTrainTemporal = SimpleFastTemporalFeaturePreferenceData
					.loadTemporalFeature(tuplesStreamTrain, userIndexTrain, itemIndexTrain);
			// Create the wrapper to be able to work with the ranksys datamodel
			SimpleFastTemporalFeaturePreferenceDataWrapper<Long, Long> wrpTrain = new SimpleFastTemporalFeaturePreferenceDataWrapper<>(
					ranksysTrainTemporal, ExperimentUtils.obtRepetitionsStrategy(wrapperStrategy));

			wrpTrain.writePreferences(newDataset);
		}
			break;

		case "AggregateWithWrapperTimeStamps": {
			System.out.println(Arrays.toString(args));
			String newDataset = cl.getOptionValue(OPT_NEWDATASET);
			String wrapperStrategy = cl.getOptionValue(OPT_WRAPPERSTRATEGYTIMESTAMPS);

			boolean completeOrNot = Boolean.parseBoolean(cl.getOptionValue(OPT_COMPLETEINDEXES, DEFAULT_COMPLETE_INDEXES));
			Tuple4<List<Long>, List<Long>, List<Long>, List<Long>> indexes = ExperimentUtils
					.retrieveTrainTestIndexes(completeOrNot, trainFile, trainFile, lp, lp);
			List<Long> usersTrain = indexes.v1;
			List<Long> itemsTrain = indexes.v2;

			FastUserIndex<Long> userIndexTrain = SimpleFastUserIndex.load(usersTrain.stream());
			FastItemIndex<Long> itemIndexTrain = SimpleFastItemIndex.load(itemsTrain.stream());

			Stream<Tuple4<Long, Long, Double, Long>> tuplesStreamTrain = ExtendedPreferenceReader.get()
					.readPreferencesTimeStamp(trainFile, lp, lp);
			SimpleFastTemporalFeaturePreferenceData<Long, Long, Long, Double> ranksysTrainTemporal = SimpleFastTemporalFeaturePreferenceData
					.loadTemporalFeature(tuplesStreamTrain, userIndexTrain, itemIndexTrain);
			// Create the wrapper to be able to work with the ranksys datamodel
			SimpleFastTemporalFeaturePreferenceDataWrapper<Long, Long> wrpTrain = new SimpleFastTemporalFeaturePreferenceDataWrapper<Long, Long>(
					ranksysTrainTemporal, ExperimentUtils.obtRepetitionsStrategyTime(wrapperStrategy));

			wrpTrain.writePreferencesTimeStamps(newDataset);
		}
			break;


		case "printClosestPOIs": {
			System.out.println(Arrays.toString(args));

			// String trainingFile, String poisCoordsFile, String resultFileNN, String
			// resultFileCoordsOfTrain, int nn
			FoursqrProcessData.printClosestPOIs(trainFile, args[4], args[5], args[6], Integer.parseInt(args[7]));
		}
			break;

		// evaluate using RiVal
		case "rivalEvaluation": {

			System.out.println(
					"-o rivalEvaluation -trf testFile resultsOfPredictionsFile rankingMetric Predictionthreshold Cutoffs outResulFile");
			System.out.println(Arrays.toString(args));

			Properties evalProps = new Properties();

			String predictionPath = args[4];
			evalProps.setProperty(MultipleEvaluationMetricRunner.METRICS, args[5]);
			evalProps.setProperty(MultipleEvaluationMetricRunner.RELEVANCE_THRESHOLD, args[6]);
			evalProps.setProperty(MultipleEvaluationMetricRunner.RANKING_CUTOFFS, args[7]);
			evalProps.setProperty(MultipleEvaluationMetricRunner.ERROR_STRATEGY, args[8]);
			String outputFile = args[9];

			File f = new File(outputFile);

			// If file of ranksys evaluation already exist then nothing
			if (f.exists() && !f.isDirectory()) {
				System.out.println("Ignoring " + f + " because it already exists");
				return;
			}

			DataModel<Long, Long> predictions = null;
			predictions = new SimpleParser().parseData(new File(predictionPath));

			DataModel<Long, Long> test = null;

			test = new SimpleParser().parseData(new File(trainFile));

			// MultipleEvaluationMetricRunner.run(evalProps);
			Map<String, Map<String, Double>> mapResults = ExperimentUtils.evaluateStrategy(evalProps, test,
					predictions);
			PrintStream out = new PrintStream(new File(outputFile));
			for (String metric : new TreeSet<String>(mapResults.keySet())) {
				out.println(metric + "\t" + mapResults.get(metric).get("all"));
			}
			out.close();
		}
			break;

		case "rivalEvaluation2": {

			System.out.println(
					"-o rivalEvaluation2 -trf testFile resultsOfPredictionsFile rankingMetric Predictionthreshold Cutoffs outResulFile -tsf trainingFile");
			System.out.println(Arrays.toString(args));

			Properties evalProps = new Properties();

			String predictionPath = args[4];
			evalProps.setProperty(MultipleEvaluationMetricRunner.METRICS, args[5]);
			evalProps.setProperty(MultipleEvaluationMetricRunner.RELEVANCE_THRESHOLD, args[6]);
			evalProps.setProperty(MultipleEvaluationMetricRunner.RANKING_CUTOFFS, args[7]);
			evalProps.setProperty(MultipleEvaluationMetricRunner.ERROR_STRATEGY, args[8]);
			String outputFile = args[9];

			// TREC_EVAL format: user 'Q0' item ranking score 'r'
			DataModel<String, Long> predictions = ExperimentUtils.parseModel(0, 2, 4, predictionPath);

			DataModel<String, Long> test = ExperimentUtils.parseModel(SimpleParser.USER_TOK, SimpleParser.ITEM_TOK,
					SimpleParser.RATING_TOK, trainFile);

			String trainingFileFromTestFlag = cl.getOptionValue(OPT_TESTFILE);
			DataModel<String, Long> training = ExperimentUtils.parseModel(SimpleParser.USER_TOK, SimpleParser.ITEM_TOK,
					SimpleParser.RATING_TOK, trainingFileFromTestFlag);

			Map<Long, Integer> observedItemRelevance = new HashMap<>();
			training.getItemUserPreferences().entrySet()
					.forEach(e -> observedItemRelevance.put(e.getKey(), e.getValue().size()));

			File f = new File(outputFile);
			// If file of ranksys evaluation already exist then nothing
			if (f.exists() && !f.isDirectory()) {
				System.out.println("Ignoring " + f + " because it already exists");
				return;
			}

			Map<String, Map<String, Double>> mapResults = ExperimentUtils.evaluateStrategy(evalProps, test, predictions,
					observedItemRelevance);
			PrintStream out = new PrintStream(new File(outputFile));
			for (String metric : new TreeSet<String>(mapResults.keySet())) {
				out.println(metric + "\t" + mapResults.get(metric).get("all"));
			}
			out.close();
		}
			break;

		case "rivalEvaluation3": {

			System.out.println(
					"-o rivalEvaluation3 -trf testFile resultsOfPredictionsFile rankingMetric Predictionthreshold Cutoffs outResulFile -tsf popFile");
			System.out.println(Arrays.toString(args));

			Properties evalProps = new Properties();

			String predictionPath = args[4];
			evalProps.setProperty(MultipleEvaluationMetricRunner.METRICS, args[5]);
			evalProps.setProperty(MultipleEvaluationMetricRunner.RELEVANCE_THRESHOLD, args[6]);
			evalProps.setProperty(MultipleEvaluationMetricRunner.RANKING_CUTOFFS, args[7]);
			evalProps.setProperty(MultipleEvaluationMetricRunner.ERROR_STRATEGY, args[8]);
			String outputFile = args[9];

			// TREC_EVAL format: user 'Q0' item ranking score 'r'
			DataModel<String, Long> predictions = ExperimentUtils.parseModel(0, 2, 4, predictionPath);

			DataModel<String, Long> test = ExperimentUtils.parseModel(SimpleParser.USER_TOK, SimpleParser.ITEM_TOK,
					SimpleParser.RATING_TOK, trainFile);

			File f = new File(outputFile);

			String popFileFromTestFlag = cl.getOptionValue(OPT_TESTFILE);
			Map<Long, Integer> popMap = new HashMap<>();
			BufferedReader popReader = new BufferedReader(new FileReader(popFileFromTestFlag));
			String line = null;
			while ((line = popReader.readLine()) != null) {
				String[] toks = line.split("\t");
				Long i = Long.parseLong(toks[0]);
				Integer c = Math.round(Float.parseFloat(toks[1]));
				popMap.put(i, c);
			}
			popReader.close();

			// If file of ranksys evaluation already exist then nothing
			if (f.exists() && !f.isDirectory()) {
				System.out.println("Ignoring " + f + " because it already exists");
				return;
			}

			Map<String, Map<String, Double>> mapResults = ExperimentUtils.evaluateStrategy(evalProps, test, predictions,
					popMap);
			PrintStream out = new PrintStream(new File(outputFile));
			for (String metric : new TreeSet<String>(mapResults.keySet())) {
				out.println(metric + "\t" + mapResults.get(metric).get("all"));
			}
			out.close();
		}
			break;

		case "rivalEvaluation4": {

			System.out.println(
					"-o rivalEvaluation3 -trf testFile resultsOfPredictionsFile rankingMetric Predictionthreshold Cutoffs outResulFile -tsf popFile");
			System.out.println(Arrays.toString(args));

			Properties evalProps = new Properties();

			String predictionPath = args[4];
			evalProps.setProperty(MultipleEvaluationMetricRunner.METRICS, args[5]);
			evalProps.setProperty(MultipleEvaluationMetricRunner.RELEVANCE_THRESHOLD, args[6]);
			evalProps.setProperty(MultipleEvaluationMetricRunner.RANKING_CUTOFFS, args[7]);
			evalProps.setProperty(MultipleEvaluationMetricRunner.ERROR_STRATEGY, args[8]);
			String outputFile = args[9];

			// TREC_EVAL format: user 'Q0' item ranking score 'r'
			DataModel<String, Long> predictions = ExperimentUtils.parseModel(0, 2, 4, predictionPath);

			// TREC_EVAL QRELS format: user '0' item score
			DataModel<String, Long> test = ExperimentUtils.parseModel(0, 2, 3, trainFile);

			File f = new File(outputFile);

			String popFileFromTestFlag = cl.getOptionValue(OPT_TESTFILE);
			Map<Long, Integer> popMap = new HashMap<>();
			BufferedReader popReader = new BufferedReader(new FileReader(popFileFromTestFlag));
			String line = null;
			while ((line = popReader.readLine()) != null) {
				String[] toks = line.split("\t");
				Long i = Long.parseLong(toks[0]);
				Integer c = Math.round(Float.parseFloat(toks[1]));
				popMap.put(i, c);
			}
			popReader.close();

			// If file of ranksys evaluation already exist then nothing
			if (f.exists() && !f.isDirectory()) {
				System.out.println("Ignoring " + f + " because it already exists");
				return;
			}

			Map<String, Map<String, Double>> mapResults = ExperimentUtils.evaluateStrategy(evalProps, test, predictions,
					popMap);
			PrintStream out = new PrintStream(new File(outputFile));
			for (String metric : new TreeSet<String>(mapResults.keySet())) {
				out.println(metric + "\t" + mapResults.get(metric).get("all"));
			}
			out.close();
		}
			break;

		case "statisticTestForUsersInFolds": // n files for each recommender, considering values for each user
		case "statisticTestForAllInFolds": // n files for each recommender, considering values denotes as 'all'
		case "statisticTestPerFold": { // one file for each recommender, considering values for each user
			System.out.println(Arrays.toString(args));
			System.out.println("-o " + step + " -trf recfiles");

			final String statisticType = step;
			final String[] recFiles = trainFile.split(",");
			final String[] statMethods = new String[] { "pairedT", "wilcoxon" };
			final PrintStream out = System.out;
			final Set<String> recMethods = new HashSet<>();
			final Map<String, Map<String, Map<String, Double>>> methodMetricUserValues = new HashMap<>();
			final Set<String> metrics = new HashSet<>();

			IntStream.range(0, recFiles.length).filter(i -> !recFiles[i].isEmpty()).forEach(i -> {
				String[] files = recFiles[i].split(";");
				IntStream.range(0, files.length).filter(j -> !files[j].isEmpty()).forEach(j -> {
					String file = files[j];
					String methodName = new File(file).getName();
					if (!statisticType.equals("statisticTestPerFold")) {
						methodName = recFiles[i];
					}
					recMethods.add(methodName);
					Map<String, Map<String, Double>> metricUserMap = methodMetricUserValues.getOrDefault(methodName,
							new HashMap<>());
					methodMetricUserValues.put(methodName, metricUserMap);
					try {
						Stream<String> stream = Files.lines(Paths.get(file));
						stream.forEach(line -> {
							// metric \t user \t value
							String[] toks = line.split("\t");
							String metric = toks[0];
							String user = toks[1];
							if (!statisticType.equals("statisticTestPerFold")) {
								// we combine the user id with the fold
								user += "_" + j;
							}
							Double value = Double.parseDouble(toks[2]);
							if ((!statisticType.equals("statisticTestForAllInFolds") && !user.startsWith("all"))
									|| (statisticType.equals("statisticTestForAllInFolds") && user.startsWith("all"))) {
								metricUserMap.put(metric, metricUserMap.getOrDefault(metric, new HashMap<>()));
								metricUserMap.get(metric).put(user, value);
								metrics.add(metric);
							}
						});
						stream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				});
			});

			recMethods.forEach(method1 -> {
				recMethods.forEach(method2 -> {
					if (method1.compareTo(method2) > 0) {
						metrics.forEach(metric -> {
							// keep only users in the intersection (because the tests are paired)
							Map<String, Double> map1tmp = methodMetricUserValues.get(method1).get(metric);
							Map<String, Double> map2tmp = methodMetricUserValues.get(method2).get(metric);
							if (map1tmp != null && map2tmp != null) {
								Set<String> commonKeys = new HashSet<>(map1tmp.keySet());
								commonKeys.retainAll(map2tmp.keySet());
								final Map<String, Double> map1 = new HashMap<>();
								final Map<String, Double> map2 = new HashMap<>();
								commonKeys.forEach(k -> {
									map1.put(k, map1tmp.get(k));
									map2.put(k, map2tmp.get(k));
								});
								final double avg1 = map1.values().stream().collect(Collectors.averagingDouble(d -> d));
								final double avg2 = map2.values().stream().collect(Collectors.averagingDouble(d -> d));
								IntStream.range(0, statMethods.length).forEach(i -> {
									String statMethod = statMethods[i];
									double p = new StatisticalSignificance(map1, map2).getPValue(statMethod);
									out.println(method1 + "\t" + method2 + "\t" + metric + "\t" + statMethod + "\t" + p
											+ "\t" + avg1 + "\t" + avg2);
								});
							}
						});
					}
				});
			});
			out.close();
		}
			break;

		case "distancesMostPopular": {
			System.out.println(Arrays.toString(args));
			System.out.println("-o distancesMostPopular -trf trainFile poisCoords nnMostPopular");
			FoursqrProcessData.analyzeDistanceMostPopular(trainFile, args[4], Integer.parseInt(args[5]));

		}
			break;

		case "FoursqrObtainCityCenterByPOIs": {
			System.out.println(Arrays.toString(args));
			System.out.println("-o FoursqrObtainCityCenterByPOIs -trf poiCoordsFile poiCityFile resultfilepath");
			FoursqrProcessData.generateCityCenter(trainFile, args[4], args[5]);
		}
			break;

		case "NearestCities": {
			System.out.println(Arrays.toString(args));
			System.out.println("-o NearestCities -trf coordCitiesFile numberNN fileResult");
			Integer nn = Integer.parseInt(args[4]);
			FoursqrProcessData.getNNCities(trainFile, nn, args[5]);
		}
			break;
			
		case "StatisticsTestRival": {
			System.out.println(
					"-o StatisticsTestRival -trf baselineFile testMethodFiles formatStatistics overwrite? outputFile Functions alpha usersToAvoid");
			System.out.println(Arrays.toString(args));
			Properties statisticsProps = new Properties();
			statisticsProps.setProperty(StatisticsRunner.BASELINE_FILE, trainFile);
			statisticsProps.setProperty(StatisticsRunner.TEST_METHODS_FILES, args[4]);
			statisticsProps.setProperty(StatisticsRunner.INPUT_FORMAT, args[5]);
			statisticsProps.setProperty(StatisticsRunner.OUTPUT_OVERWRITE, args[6]);
			statisticsProps.setProperty(StatisticsRunner.STATISTICS, args[7]);
			statisticsProps.setProperty(StatisticsRunner.ALPHA, args[8]);
			statisticsProps.setProperty(StatisticsRunner.AVOID_USERS, args[9]);

			StatisticsRunner.run(statisticsProps);

		}
			break;
		// Ranksys with non accuracy metrics
		case "ranksysNonAccuracyWithoutFeatureMetricsEvaluation":
		case "ranksysNonAccuracyMetricsEvaluation":
		case "ranksysNonAccuracyMetricsEvaluationPerUser":
		case "ranksysNonAccuracyWithoutFeatureMetricsEvaluationPerUser": {
			/*
			 * -Train file -Test file -Recommended file -Item feature file -Ranksys Metric
			 * -Output file -Threshold -Cutoff
			 */
			System.out.println(Arrays.toString(args));
			String outputFile = cl.getOptionValue(OPT_OUT_RESULT_FILE);
			String testFile = cl.getOptionValue(OPT_TESTFILE);
			String userFeaturFile = cl.getOptionValue(OPT_USER_FEATURE_FILE);

			String recommendedFile = cl.getOptionValue(OPT_RECOMMENDEDFILE);
			String itemFeatureFile = cl.getOptionValue(OPT_FEATUREREADINGFILE);
			int threshold = Integer.parseInt(cl.getOptionValue(OPT_MATCHINGTHRESHOLD));
			int antiRelevanceThreshold = Integer.parseInt(cl.getOptionValue(OPT_ANTIRELEVANCETHRESHOLD, "0"));
			String cutoffs = cl.getOptionValue(OPT_CUTOFF);
			String overwrite = cl.getOptionValue(OPT_OVERWRITE, DEFAULT_OVERWRITE);

			Boolean computeAntiMetrics = Boolean
					.parseBoolean(cl.getOptionValue(OPT_COMPUTEANTIMETRICS, DEFAULT_COMPUTEANTIMETRICS));
			Boolean computeOnlyAcc = Boolean.parseBoolean(cl.getOptionValue(OPT_COMPUTEONLYACC,DEFAULT_OPT_COMPUTEONLYACC));
			
			String ranksysRelevanceModel = cl.getOptionValue(OPT_RANKSYSRELEVANCEMODEL);
			String ranksysDiscountModel = cl.getOptionValue(OPT_RANKSYSDISCOUNTMODEL);
			String ranksysBackground = cl.getOptionValue(OPT_RANKSYSBACKGROUND);
			String ranksysBase = cl.getOptionValue(OPT_RANKSYSBASE);

			String itemsTimeAvgFile = cl.getOptionValue(OPT_TIMESAVERAGEITEMS);
			String itemsTimeFirstFile = cl.getOptionValue(OPT_TIMESFIRSTITEMS);
			String itemsTimeMedianFile = cl.getOptionValue(OPT_TIMESMEDIANITEMS);
			String itemsTimeLastFile = cl.getOptionValue(OPT_TIMESLASTITEMS);
			String itemsReleaseDate = cl.getOptionValue(OPT_RELEASEDATEITEMS);
			Boolean computeUserFilter = Boolean
					.parseBoolean(cl.getOptionValue(OPT_COMPUTE_USER_FILTER, DEFAULT_COMPUTE_USER_FILTER));
			String userSelFeature = cl.getOptionValue(OPT_USER_FEATURE_SELECTED);
			
			String userWeightModel = cl.getOptionValue(OPT_USER_MODEL_WGT, DEFAULT_USER_MODEL_WGT);


			
			Boolean lcsEvaluation = Boolean.parseBoolean(cl.getOptionValue(OPT_LCS_EVALUATION, DEFAULT_LCS_EVALUTATION));


			Boolean isPerUser = step.contains("PerUser");

			File f = new File(outputFile);
			// If file of ranksys evaluation already exist then nothing
			if (f.exists() && !f.isDirectory() && Boolean.parseBoolean(overwrite) == false) {
				System.out.println("Ignoring " + f + " because it already exists");
				return;
			}

			if (isPerUser) {
				System.out.println("Per user evaluation. We will compute the metrics for every user indvidually.");
			} else {
				System.out.println("Normal evaluation, computing the average of the results in the metrics.");
			}

			
			//This section is to use a temporal preference data in the test set, for a time aware metric
			SimpleFastTemporalFeaturePreferenceData<Long, Long, Long, Double> rankSysTemporalTestData = null;
			if (lcsEvaluation) {
				System.out.println("Reading test set with timestamps for LCS evaluation");
				Tuple4<List<Long>, List<Long>, List<Long>, List<Long>> indexes = ExperimentUtils
						.retrieveTrainTestIndexes(true, trainFile, testFile, lp, lp);
				
				List<Long> usersTest = indexes.v3;
				List<Long> itemsTest = indexes.v4;
	
				FastUserIndex<Long> userIndexTest = SimpleFastUserIndex.load(usersTest.stream());
				FastItemIndex<Long> itemIndexTest = SimpleFastItemIndex.load(itemsTest.stream());
	
				Stream<Tuple4<Long, Long, Double, Long>> tuplesStream = ExtendedPreferenceReader.get()
						.readPreferencesTimeStamp(testFile, lp, lp);
				rankSysTemporalTestData = SimpleFastTemporalFeaturePreferenceData
						.loadTemporalFeature(tuplesStream, userIndexTest, itemIndexTest);
			}
			//End of test Temporal data
			
			final PreferenceData<Long, Long> trainData = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(trainFile, lp, lp));
			final PreferenceData<Long, Long> testData = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(testFile, lp, lp));
			
			
			
			final PreferenceData<Long, Long> totalData = new ConcatPreferenceData<>(trainData, testData);

			final Set<Long> testUsers = testData.getUsersWithPreferences().collect(Collectors.toSet());

			final PreferenceData<Long, Long> originalRecommendedData = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(recommendedFile, lp, lp));
			// recommended data has to be filtered to avoid evaluating users not in test
			final PreferenceData<Long, Long> recommendedData = SequentialRecommendersUtils
					.filterPreferenceData(originalRecommendedData, testUsers, null);

			////////////////////////
			// INDIVIDUAL METRICS //
			////////////////////////
			TimestampCalculator<Long, Long> its = ExperimentUtils.createTimeStampCalculator(trainFile, itemsTimeAvgFile,
					itemsTimeFirstFile, itemsTimeMedianFile, itemsTimeLastFile);

			FeatureData<Long, String, Double> featureData = null;
			ItemDistanceModel<Long> dist = null;
			IntentModel<Long, Long, String> intentModel = null;
			if (!step.contains("WithoutFeature")) {
				featureData = SimpleFeatureData.load(SimpleFeaturesReader.get().read(itemFeatureFile, lp, sp));
				// COSINE DISTANCE
				dist = new CosineFeatureItemDistanceModel<>(featureData);
				// INTENT MODEL
				intentModel = new FeatureIntentModel<>(totalData, featureData);
			}

			// Binary relevance and anti relevance model for ranking metrics
			BinaryRelevanceModel<Long, Long> binRel = new BinaryRelevanceModel<>(false, testData, threshold);
			
			//Anti relevance model
			BinaryAntiRelevanceModel<Long, Long> selectedAntiRelevance = new BinaryAntiRelevanceModel<>(false, testData, antiRelevanceThreshold);
			// Relevance model for novelty/diversity (can be with or without relevance)
			RelevanceModel<Long, Long> selectedRelevance = null;

			double ranksysBackgroundD = 0.0;
			double ranksysBaseD = 0.0;

			if (ranksysBackground != null) {
				ranksysBackgroundD = Double.parseDouble(ranksysBackground);
			}

			if (ranksysBase != null) {
				ranksysBaseD = Double.parseDouble(ranksysBase);
			}

			if (ranksysRelevanceModel == null) {
				selectedRelevance = new NoRelevanceModel<>();
			} else {
				selectedRelevance = AttributeRecommendationUtils.obtRelevanceModelRanksys(ranksysRelevanceModel,
						testData, threshold, ranksysBackgroundD);
			}

			RankingDiscountModel discModel = null;
			if (ranksysDiscountModel == null) {
				discModel = new NoDiscountModel();
			} else {
				discModel = AttributeRecommendationUtils.obtRankingDiscountModel(ranksysDiscountModel, ranksysBaseD);
			}

			int numUsersTest = testData.numUsersWithPreferences();
			int numUsersRecommended = recommendedData.numUsersWithPreferences();
			int numItems = totalData.numItemsWithPreferences(); // Num items with preferences in the data
			System.out.println("\n\nNum users in the test set " + numUsersTest);
			System.out.println("\n\nNum users to whom we have made recommendations " + numUsersRecommended);

			Map<String, SystemMetric<Long, Long>> sysMetrics = new HashMap<>();
			// Ranking metrics (avg for recommendation)
			Map<String, RecommendationMetric<Long, Long>> recMetricsAvgRelUsers = new HashMap<>();
			Map<String, RecommendationMetric<Long, Long>> recMetricsAvgAntiRelUsers = new HashMap<>();
			Map<String, RecommendationMetric<Long, Long>> recMetricsAllRecUsers = new HashMap<>();

			String[] differentCutoffs = cutoffs.split(",");

			for (String cutoffS : differentCutoffs) {
				int cutoff = Integer.parseInt(cutoffS);
				ExperimentUtils.addMetrics(recMetricsAvgRelUsers, recMetricsAvgAntiRelUsers, recMetricsAllRecUsers, threshold, antiRelevanceThreshold, cutoff, trainData, testData,
						its, itemsTimeAvgFile, itemsTimeFirstFile, itemsTimeMedianFile, itemsTimeLastFile,
						selectedRelevance, binRel, selectedAntiRelevance, discModel, featureData, dist, intentModel,
						itemsReleaseDate, step, computeAntiMetrics, computeOnlyAcc, rankSysTemporalTestData, lcsEvaluation);

				// SYSTEM METRICS. Only for normal evaluation, not per user
				if (!isPerUser) {
					sysMetrics.put("aggrdiv@" + cutoff, new AggregateDiversityMetric<>(cutoff, selectedRelevance));
					sysMetrics.put("gini@" + cutoff, new GiniIndex<>(cutoff, numItems));
					sysMetrics.put("RealAD@" + cutoff, new RealAggregateDiversity<Long, Long>(cutoff));
					sysMetrics.put("usercov", new UserCoverage<Long, Long>());
				}
			}
			UserFeatureData<Long, String, Double> ufD = null;
			if (userFeaturFile != null) {
				ufD = SimpleUserFeatureData.load(SimpleUserFeaturesReader.get().read(userFeaturFile, lp, sp));
			}
			UserFeatureData<Long, String, Double> ufD2 = ufD;
			
			UserMetricWeight weight = ExperimentUtils.obtUserMetricWeight(userWeightModel);
			System.out.println("Working with user weight " + weight.toString());
			
			WeightedModelUser<Long, Long, String, Double> wmu = new WeightedModelUser<>(trainData, weight,
					userSelFeature, ufD);

			// Average of all only for normal evaluation, not per user
			recMetricsAvgRelUsers.forEach((name, metric) -> sysMetrics.put(name + "_rec",
						new WeightedAverageRecommendationMetricIgnoreNoRelevantUsersAndNaNs<>(metric, binRel, wmu)));
			recMetricsAvgAntiRelUsers.forEach((name, metric) -> sysMetrics.put(name + "_rec",
						new WeightedAverageRecommendationMetricIgnoreNoRelevantUsersAndNaNs<>(metric, selectedAntiRelevance, wmu)));
			recMetricsAllRecUsers.forEach((name, metric) -> sysMetrics.put(name + "_rec",
						new WeightedAverageRecommendationMetricIgnoreNoRelevantUsersAndNaNs<>(metric, binRel, wmu)));


			RecommendationFormat<Long, Long> format = new SimpleRecommendationFormat<>(lp, lp);

			System.out.println("Computing user filter: " + computeUserFilter);

			format.getReader(recommendedFile).readAll().filter(
					rec -> ExperimentUtils.isValidUser(rec.getUser(), computeUserFilter, ufD2, userSelFeature))
					.forEach(rec -> sysMetrics.values().forEach(metric -> metric.add(rec)));

			PrintStream out = new PrintStream(new File(outputFile));
			sysMetrics.forEach((name, metric) -> out.println(name + "\t" + metric.evaluate()));
			out.close();
			
		}
			break;

		

		case "processPostEval": {
			System.out.println(Arrays.toString(args));
			String postEval = trainFile;
			String[] indexesStr = args[4].split(",");
			String[] filters = args[5].split(",");
			String[] families = args[6].split(",");
			String metric = args[7];
			String outputFile = args[8];
			int[] arrIndex = new int[indexesStr.length];
			for (int i = 0; i < indexesStr.length; i++) {
				arrIndex[i] = Integer.parseInt(indexesStr[i]);
			}

			ExperimentUtils.parseResultsFile(postEval, arrIndex, filters, families, metric, outputFile);
		}
			break;

		// For some recommenders that do not converge, if we want to take the average of
		// the results
		case "AverageSetOfResults": {
			System.out.println(Arrays.toString(args));
			ExperimentUtils.averageResults(trainFile, args[4], args[5]);
		}
			break;

		default:
			System.out.println("Option " + step + " not recognized");
			break;
		}
	}

	/**
	 * Method that will obtain a command line using the available options of the
	 * arguments
	 *
	 * @param args
	 *            the arguments that the program will receive
	 * @return a command line if the arguments are correct or null if an error
	 *         occurred
	 */
	private static CommandLine getCommandLine(String[] args) {
		Options options = new Options();

		// Number of the case
		Option caseIdentifier = new Option("o", OPT_CASE, true, "option of the case");
		caseIdentifier.setRequired(true);
		options.addOption(caseIdentifier);

		// Train file
		Option trainFile = new Option("trf", OPT_TRAINFILE, true, "input file train path");
		trainFile.setRequired(true);
		options.addOption(trainFile);

		// Here not required
		// TestFile file
		Option testFile = new Option("tsf", OPT_TESTFILE, true, "input file test path");
		testFile.setRequired(false);
		options.addOption(testFile);

		// TestFile file (test file full, only for Foursqr when using cross domain)
		Option testFile2 = new Option("tsfFull", OPT_TESTFILE2, true, "input file test path containing all test Files");
		testFile2.setRequired(false);
		options.addOption(testFile2);

		// Neighbours
		Option neighbours = new Option("n", OPT_NEIGH, true, "neighbours");
		neighbours.setRequired(false);
		options.addOption(neighbours);

		// ThresholdSimilarity
		Option thresholdSim = new Option("thr", OPT_MATCHINGTHRESHOLD, true, "Matching threshold");
		thresholdSim.setRequired(false);
		options.addOption(thresholdSim);

		// NumberItemsRecommended
		Option numberItemsRecommended = new Option("nI", OPT_ITEMSRECOMMENDED, true, "Number of items recommended");
		numberItemsRecommended.setRequired(false);
		options.addOption(numberItemsRecommended);

		// OutResultfile
		Option outfile = new Option("orf", OPT_OUT_RESULT_FILE, true, "output result file");
		outfile.setRequired(false);
		options.addOption(outfile);

		// Ranksys similarity
		Option rankSysSim = new Option("rs", OPT_RANKSYS_SIM, true, "ranksys similarity");
		rankSysSim.setRequired(false);
		options.addOption(rankSysSim);

		// Ranksys similarity 2
		Option rankSysSim2 = new Option("rs2", OPT_RANKSYS_SIM2, true, "ranksys similarity 2");
		rankSysSim2.setRequired(false);
		options.addOption(rankSysSim2);

		// Ranksys recommender
		Option rankSysRecommender = new Option("rr", OPT_RANKSYS_REC, true, "ranksys recommeder");
		rankSysRecommender.setRequired(false);
		options.addOption(rankSysRecommender);

		// Overwrite result
		Option outputOverwrite = new Option("ovw", OPT_OVERWRITE, true, "overwrite");
		outputOverwrite.setRequired(false);
		options.addOption(outputOverwrite);

		// ItemColFeature
		Option itemColFeature = new Option("fic", OPT_FEATURESITEMCOL, true, "item column of feature file");
		itemColFeature.setRequired(false);
		options.addOption(itemColFeature);

		// FeatColFeature
		Option featColFeature = new Option("ffc", OPT_FEATURESFEATCOL, true, "feature column of feature file");
		featColFeature.setRequired(false);
		options.addOption(featColFeature);

		// SavingFeature
		Option savingFeature = new Option("sfs", OPT_SAVINGFEATURESCHEME, true, "saving feature scheme");
		savingFeature.setRequired(false);
		options.addOption(savingFeature);

		// Feature reading file
		Option featureReadingFile = new Option("ff", OPT_FEATUREREADINGFILE, true, "features file");
		featureReadingFile.setRequired(false);
		options.addOption(featureReadingFile);

		// Feature reading file2
		Option featureReadingFile2 = new Option("ff2", OPT_FEATUREREADINGFILE2, true, "features file (2)");
		featureReadingFile2.setRequired(false);
		options.addOption(featureReadingFile2);

		// Feature reading file
		Option recommendedFile = new Option("rf", OPT_RECOMMENDEDFILE, true, "recommended file");
		recommendedFile.setRequired(false);
		options.addOption(recommendedFile);

		// RankSysMetric
		Option ranksysCutoff = new Option("rc", OPT_CUTOFF, true, "ranksyscutoff");
		ranksysCutoff.setRequired(false);
		options.addOption(ranksysCutoff);

		// Recommenders files
		Option recommendedFiles = new Option("rfs", OPT_RECOMMENDEDFILES, true, "recommendenders files");
		recommendedFiles.setRequired(false);
		options.addOption(recommendedFiles);

		// RankSys factorizers (k)
		Option kFactorizer = new Option("kFactorizer", OPT_KFACTORIZER, true, "k factorizer");
		kFactorizer.setRequired(false);
		options.addOption(kFactorizer);

		// RankSys factorizers (alpha)
		Option alhpaFactorizer = new Option("aFactorizer", OPT_ALPHAFACTORIZER, true, "alpha factorizer");
		alhpaFactorizer.setRequired(false);
		options.addOption(alhpaFactorizer);

		// RankSys factorizers (lambda)
		Option lambdaFactorizer = new Option("lFactorizer", OPT_LAMBDAFACTORIZER, true, "lambda factorizer");
		lambdaFactorizer.setRequired(false);
		options.addOption(lambdaFactorizer);

		// RankSys factorizers (numInteractions)
		Option numInteractionsFact = new Option("nIFactorizer", OPT_NUMINTERACTIONSFACTORIZER, true,
				"numInteractions factorizer");
		numInteractionsFact.setRequired(false);
		options.addOption(numInteractionsFact);
	
		// Factorizers (theta)
		Option thetaFactorizer = new Option("thetaFactorizer", OPT_THETA_FACTORIZER, true, "theta factorizer");
		thetaFactorizer.setRequired(false);
		options.addOption(thetaFactorizer);
		
		// factorizers (alpha2)
		Option alphaFactorizer2 = new Option("aFactorizer2", OPT_ALPHA_FACTORIZER2, true, "alpha factorizer (2)");
		alphaFactorizer2.setRequired(false);
		options.addOption(alphaFactorizer2);

		// RansksyLibrary NonAccuracyEvaluationParameters
		Option ranksysRelevanceModel = new Option("ranksysRelModel", OPT_RANKSYSRELEVANCEMODEL, true,
				"ranksys relevance model");
		ranksysRelevanceModel.setRequired(false);
		options.addOption(ranksysRelevanceModel);

		Option ranksysDiscountModel = new Option("ranksysDiscModel", OPT_RANKSYSDISCOUNTMODEL, true,
				"ranksys discount model");
		ranksysDiscountModel.setRequired(false);
		options.addOption(ranksysDiscountModel);

		Option ranksysBackground = new Option("ranksysBackRel", OPT_RANKSYSBACKGROUND, true,
				"ranksys background for relevance model");
		ranksysBackground.setRequired(false);
		options.addOption(ranksysBackground);

		Option ranksysBase = new Option("ranksysBaseDisc", OPT_RANKSYSBASE, true, "ranksys base for discount model");
		ranksysBase.setRequired(false);
		options.addOption(ranksysBase);

		// Freshness/novelty metrics
		Option itemsTimeAverage = new Option("ItimeAvgFile", OPT_TIMESAVERAGEITEMS, true, "average item time file");
		itemsTimeAverage.setRequired(false);
		options.addOption(itemsTimeAverage);

		Option itemsTimeFirst = new Option("ItimeFstFile", OPT_TIMESFIRSTITEMS, true, "first item time file");
		itemsTimeFirst.setRequired(false);
		options.addOption(itemsTimeFirst);

		Option itemsTimeLast = new Option("ItimeLastFile", OPT_TIMESLASTITEMS, true, "last item time file");
		itemsTimeLast.setRequired(false);
		options.addOption(itemsTimeLast);

		Option itemsTimeMedian = new Option("ItimeMedFile", OPT_TIMESMEDIANITEMS, true,
				"median base for discount model");
		itemsTimeMedian.setRequired(false);
		options.addOption(itemsTimeMedian);

		Option itemsReleaseDate = new Option("IReleaseDate", OPT_RELEASEDATEITEMS, true,
				"release dates file for items");
		itemsReleaseDate.setRequired(false);
		options.addOption(itemsReleaseDate);

		// File containing 2 columns, old id and new id, for datasets that we have
		// changed the ids (for items)
		Option itemsMapping = new Option("IMapping", OPT_MAPPINGITEMS, true, "mapping file of items");
		itemsMapping.setRequired(false);
		options.addOption(itemsMapping);

		// File containing 2 columns, old id and new id, for datasets that we have
		// changed the ids
		Option usersMapping = new Option("UMapping", OPT_MAPPINGUSERS, true, "mapping file of users");
		usersMapping.setRequired(false);
		options.addOption(usersMapping);

		// File containing 2 columns, old id and new id, for datasets that we have
		// changed the ids
		Option mapCategories = new Option("CMapping", OPT_MAPPINGCATEGORIES, true, "mapping file of categories");
		mapCategories.setRequired(false);
		options.addOption(mapCategories);

		// Option for creating a new dataset when transforming the users and items
		Option newDatasetFile = new Option("newDataset", OPT_NEWDATASET, true, "new dataset destination");
		newDatasetFile.setRequired(false);
		options.addOption(newDatasetFile);

		// Option for creating a new dataset when transforming the users and items
		Option poiCoordFile = new Option("coordFile", OPT_COORDFILE, true, "poi coordinates file");
		poiCoordFile.setRequired(false);
		options.addOption(poiCoordFile);

		Option poiCityFile = new Option("poiCityFile", OPT_POICITYFILE, true, "poi city mapping file");
		poiCityFile.setRequired(false);
		options.addOption(poiCityFile);

		Option citySimFile = new Option("citySimFile", OPT_CITYSIMFILE, true, "city similarity file");
		citySimFile.setRequired(false);
		options.addOption(citySimFile);

		// Option of the wrapper strategy to parse datasets or to use it in ranksys
		Option wrapperStrategyPreferences = new Option("wStrat", OPT_WRAPPERSTRATEGY, true,
				"strategy to use in the wrapper");
		wrapperStrategyPreferences.setRequired(false);
		options.addOption(wrapperStrategyPreferences);

		// Option of the wrapper strategy to parse datasets or to use it in ranksys
		Option wrapperStrategyTimeStamps = new Option("wStratTime", OPT_WRAPPERSTRATEGYTIMESTAMPS, true,
				"strategy to use in the wrapper (for timestamps)");
		wrapperStrategyTimeStamps.setRequired(false);
		options.addOption(wrapperStrategyTimeStamps);

		Option matchingCategory = new Option("matchCat", OPT_MATCHINGCATEGORY, true,
				"matching category for candidate items");
		matchingCategory.setRequired(false);
		options.addOption(matchingCategory);

		Option usingWrapper = new Option("usingWrapper", OPT_USINGWRAPPER, true, "using wrapper in ranksys");
		usingWrapper.setRequired(false);
		options.addOption(usingWrapper);

		Option useCompleteIndex = new Option("cIndex", OPT_COMPLETEINDEXES, true,
				"use the complete indexes (train + test)");
		useCompleteIndex.setRequired(false);
		options.addOption(useCompleteIndex);

		Option recommendationStrategy = new Option("recStrat", OPT_RECOMMENDATIONSTRATEGY, true,
				"recommendation strategy");
		recommendationStrategy.setRequired(false);
		options.addOption(recommendationStrategy);

		Option regularizationl1 = new Option("reg1", OPT_REGULARIZATIONL1, true, "regularization L1");
		regularizationl1.setRequired(false);
		options.addOption(regularizationl1);

		Option regularizationl2 = new Option("reg2", OPT_REGULARIZATIONL2, true, "regularization L2");
		regularizationl2.setRequired(false);
		options.addOption(regularizationl2);

		Option svdBins = new Option("svdBins", OPT_SVDNUMBERBINS, true, "bins for svd++ recommender");
		svdBins.setRequired(false);
		options.addOption(svdBins);

		Option svdBeta = new Option("svdBeta", OPT_SVDBETA, true, "beta for svd++ recommender");
		svdBeta.setRequired(false);
		options.addOption(svdBeta);

		Option svdTimeStrategy = new Option("svdTimeStrat", OPT_SVDTIMESTRATEGY, true, "svd time strategy");
		svdTimeStrategy.setRequired(false);
		options.addOption(svdTimeStrategy);
		
		Option svdRegUser = new Option("svdRegUser", OPT_SVDREGUSER, true, "svd regularization for users");
		svdRegUser.setRequired(false);
		options.addOption(svdRegUser);
		
		Option svdRegItem = new Option("svdRegItem", OPT_SVDREGITEM, true, "svd regularization for items");
		svdRegItem.setRequired(false);
		options.addOption(svdRegItem);
		
		Option svdRegBias = new Option("svdRegBias", OPT_SVDREGBIAS, true, "svd regularization for biases");
		svdRegBias.setRequired(false);
		options.addOption(svdRegBias);

		Option svdLearnRate = new Option("svdLearnRate", OPT_SVDLEARNRATE, true, "svd learning rate");
		svdLearnRate.setRequired(false);
		options.addOption(svdLearnRate);
		
		Option svdMaxLearnRate = new Option("svdMaxLearnRate", OPT_SVDMAXLEARNRATE, true, "svd maximum learning rate");
		svdMaxLearnRate.setRequired(false);
		options.addOption(svdMaxLearnRate);
		
		Option svdDecay = new Option("svdDecay", OPT_SVDDECAY, true, "svd decay");
		svdDecay.setRequired(false);
		options.addOption(svdDecay);
		
		Option svdIsboldDriver = new Option("svdIsboldDriver", OPT_SVDISBOLDDRIVER, true, "svd bold driver");
		svdIsboldDriver.setRequired(false);
		options.addOption(svdIsboldDriver);
		
		Option svdRegImpItem = new Option("svdRegImpItem", OPT_SVDREGIMPITEM, true, "reg imp item");
		svdRegImpItem.setRequired(false);
		options.addOption(svdRegImpItem);

		Option antiMetrics = new Option("computeAntiMetrics", OPT_COMPUTEANTIMETRICS, true,
				"boolean in order to compute anti metrics");
		antiMetrics.setRequired(false);
		options.addOption(antiMetrics);

		Option antiRelevanceThreshold = new Option("antiRelTh", OPT_ANTIRELEVANCETHRESHOLD, true,
				"integer in order to indicate the antiRelevanceTh");
		antiRelevanceThreshold.setRequired(false);
		options.addOption(antiRelevanceThreshold);
		
		Option computeOnlyAccuracy = new Option("onlyAcc", OPT_COMPUTEONLYACC, true, "if we are computing just accuracy metrics or also novelty-diversity");
		computeOnlyAccuracy.setRequired(false);
		options.addOption(computeOnlyAccuracy);
		
		Option scoreFreq = new Option("scoreFreq", OPT_SCOREFREQ, true, "simple or frequency");
		scoreFreq.setRequired(false);
		options.addOption(scoreFreq);
		
		Option lcsEv = new Option("lcsEv", OPT_LCS_EVALUATION, true, "LCS evaluation for taking into ccount the temporal order of the items");
		lcsEv.setRequired(false);
		options.addOption(lcsEv);
		
		Option maxDist = new Option("maxDist", OPT_MAX_DISTANCE, true, "maximum distance");
		maxDist.setRequired(false);
		options.addOption(maxDist);
		
		Option mode = new Option("mode", OPT_MODE, true, "mode of an algorithm");
		mode.setRequired(false);
		options.addOption(mode);
		
		Option useSigmoid = new Option("useSigmoid", OPT_USE_SIGMOID, true, "use sigmoid");
		useSigmoid.setRequired(false);
		options.addOption(useSigmoid);
		
		Option maxDiffBetweenTimestamps = new Option("maxDiffTime", OPT_MAX_DIFF_TIME, true, "maxDifftime");
		maxDiffBetweenTimestamps.setRequired(false);
		options.addOption(maxDiffBetweenTimestamps);
		
		Option minDiffBetweenTimestamps = new Option("minDiffTime", OPT_MIN_DIFF_TIME, true, "minDifftime");
		minDiffBetweenTimestamps.setRequired(false);
		options.addOption(minDiffBetweenTimestamps);
		
		Option minClosePrefBot = new Option("minClosePrefBot", OPT_MIN_CLOSE_PREF_BOT, true, "minDifftime");
		minClosePrefBot.setRequired(false);
		options.addOption(minClosePrefBot);
		
		Option userFeatureFile = new Option("uff", OPT_USER_FEATURE_FILE, true, "user feature file");
		userFeatureFile.setRequired(false);
		options.addOption(userFeatureFile);
		
		Option userFeatureSelected = new Option("ufs", OPT_USER_FEATURE_SELECTED, true, "user feature selected");
		userFeatureSelected.setRequired(false);
		options.addOption(userFeatureSelected);
		
		Option computeUserFilter = new Option("compUF", OPT_COMPUTE_USER_FILTER, true, "compute user filter");
		computeUserFilter.setRequired(false);
		options.addOption(computeUserFilter);
		
		Option epsilon = new Option("epsilon", OPT_EPSILON, true, "epsilon");
		epsilon.setRequired(false);
		options.addOption(epsilon);
		
		Option c = new Option("c", OPT_C, true, "c");
		c.setRequired(false);
		options.addOption(c);
		
		Option userModelWeight = new Option("userModelW", OPT_USER_MODEL_WGT, true, "user model weight");
		userModelWeight.setRequired(false);
		options.addOption(userModelWeight);
		
		CommandLineParser parser = new DefaultParser();
		HelpFormatter formatter = new HelpFormatter();
		CommandLine cmd;

		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			formatter.printHelp("utility-name", options);

			return null;
		}
		return cmd;

	}

}

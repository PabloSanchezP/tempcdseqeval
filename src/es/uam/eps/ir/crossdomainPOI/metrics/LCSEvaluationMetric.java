/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.crossdomainPOI.metrics;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.ranksys.core.util.tuples.Tuple2od;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.crossdomainPOI.utils.PreferenceComparators;
import es.uam.eps.ir.ranksys.core.Recommendation;
import es.uam.eps.ir.ranksys.metrics.AbstractRecommendationMetric;
import es.uam.eps.ir.ranksys.metrics.rel.RelevanceModel;
import es.uam.eps.ir.ranksys.metrics.rel.RelevanceModel.UserRelevanceModel;

/**
 * Metric based on the longest common subsequence algorithm. We will order the test set by timestamp and see the order of our recommendations.
 * The better the subsequence matching the better
 *  
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 */
public class LCSEvaluationMetric <U, I> extends AbstractRecommendationMetric<U, I>{
	
	public enum LCSEvaluationNormalization {DIVIDE_CUTOFF, TEST_LENGTH, POWDIVMULT};
	
    private final RelevanceModel<U, I> relModel;
	private final int cutOff;
	private final FastTemporalPreferenceDataIF<U, I> testData;
	private final LCSEvaluationNormalization norm;
	private Map<U, Integer[]> sequenceOfItemsAt;
	
	
	public LCSEvaluationMetric(int cutOff, RelevanceModel<U, I> relModel, FastTemporalPreferenceDataIF<U, I> testData, LCSEvaluationNormalization norm) {
		this.cutOff = cutOff;
		this.relModel = relModel;
		this.testData = testData;
		this.norm = norm;
		obtainMapTestUsers();
	}
	
	/***
	 * Method to obtain the sequences of the users in the test set
	 */
	private void obtainMapTestUsers() {
		sequenceOfItemsAt = new HashMap<>();
		testData.getUidxWithPreferences().forEach(uidx -> {
	        UserRelevanceModel<U, I> userRelModel = relModel.getModel(testData.uidx2user(uidx));

			List<Integer> lstIndexUser = testData.getUidxTimePreferences(uidx).sorted(PreferenceComparators.timeComparatorIdxTimePref).filter(pref -> userRelModel.isRelevant(testData.iidx2item(pref.v1))).mapToInt(pref -> pref.v1).boxed().collect(Collectors.toList());
			
			sequenceOfItemsAt.put(testData.uidx2user(uidx), lstIndexUser.toArray(new Integer[lstIndexUser.size()]));
		});
	}
	

	@Override
	public double evaluate(Recommendation<U, I> recommendation) {
		List<Integer> lstRec = recommendation.getItems().stream().limit(cutOff).map(Tuple2od::v1).mapToInt(v1 -> testData.item2iidx(v1)).boxed().collect(Collectors.toList());
		Integer lcs = longestCommonSubsequence(sequenceOfItemsAt.get(recommendation.getUser()), lstRec.toArray(new Integer[lstRec.size()]));
		
		return normalize(lcs, sequenceOfItemsAt.get(recommendation.getUser()).length, lstRec.size());
	}
	
	
	
	
	private static Integer longestCommonSubsequence(Integer[] x, Integer[] y) {
        int m = x.length;
        int n = y.length;
        int[][] c = new int[m + 1][n + 1];

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (x[i].equals(y[j])) {
                    c[i + 1][j + 1] = c[i][j] + 1;
                } else {
                    c[i + 1][j + 1] = Math.max(c[i + 1][j], c[i][j + 1]);
                }
            }
        }

        return c[m][n];
    }
	
	
	private double normalize(double numerator, double testLength, double recLength) {
		switch (norm){
			case DIVIDE_CUTOFF:
				return numerator / this.cutOff;
			case TEST_LENGTH:
				return numerator / testLength;
			case POWDIVMULT:
				return (numerator * numerator)/ (testLength * recLength);
			default: 
				return numerator / this.cutOff;
		}
	}

}

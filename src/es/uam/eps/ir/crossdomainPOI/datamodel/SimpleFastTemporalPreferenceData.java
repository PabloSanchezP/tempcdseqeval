/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.crossdomainPOI.datamodel;

import static java.util.Comparator.comparingInt;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.jooq.lambda.tuple.Tuple4;
import org.ranksys.core.util.iterators.StreamDoubleIterator;
import org.ranksys.core.util.iterators.StreamIntIterator;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastPointWiseTemporalPreferenceDataIF;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.preferences.AbstractFastTemporalPreferenceData;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.preferences.IdxTimePref;
import es.uam.eps.ir.ranksys.fast.index.FastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.FastUserIndex;
import it.unimi.dsi.fastutil.doubles.DoubleIterator;
import it.unimi.dsi.fastutil.ints.IntIterator;

/**
 * DataModel extended from ranksys that allo us to work with repetitions and timestamps
 * 
 * It should admits: 
 * -Repetitions 
 * -Timestamps 
 * 
 * It does not admit:
 * -Features
 *
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U> Type of users
 * @param <I> Type of items
 */
public class SimpleFastTemporalPreferenceData<U, I> extends AbstractFastTemporalPreferenceData<U, I> implements FastPointWiseTemporalPreferenceDataIF<U, I>, Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -9077662265625949067L;
	protected final int numPreferences;
    protected final List<List<IdxTimePref>> uidxList;
    protected final List<List<IdxTimePref>> iidxList;

    /**
     * Constructor.
     *
     * @param numPreferences number of total preferences
     * @param uidxList list of lists of preferences by user index
     * @param iidxList list of lists of preferences by item index
     * @param uIndex user index
     * @param iIndex item index
     */
    protected SimpleFastTemporalPreferenceData(int numPreferences, List<List<IdxTimePref>> uidxList, List<List<IdxTimePref>> iidxList, FastUserIndex<U> uIndex, FastItemIndex<I> iIndex) {
        super(uIndex, iIndex);
        this.numPreferences = numPreferences;
        this.uidxList = uidxList;
        this.iidxList = iidxList;

        uidxList.parallelStream()
                .filter(l -> l != null)
                .forEach(l -> l.sort(comparingInt(IdxTimePref::v1)));
        iidxList.parallelStream()
                .filter(l -> l != null)
                .forEach(l -> l.sort(comparingInt(IdxTimePref::v1)));
    }

    @Override
    public int numUsers(int iidx) {
        if (iidxList.get(iidx) == null) {
            return 0;
        }
        return iidxList.get(iidx).size();
    }

    @Override
    public int numItems(int uidx) {
        if (uidxList.get(uidx) == null) {
            return 0;
        }
        return uidxList.get(uidx).size();
    }

    @Override
    public Stream<IdxTimePref> getUidxTimePreferences(int uidx) {
        if (uidxList.get(uidx) == null) {
            return Stream.empty();
        } else {
            return uidxList.get(uidx).stream();
        }
    }

    @Override
    public Stream<IdxTimePref> getIidxTimePreferences(int iidx) {
        if (iidxList.get(iidx) == null) {
            return Stream.empty();
        } else {
            return iidxList.get(iidx).stream();
        }
    }

    @Override
    public int numPreferences() {
        return numPreferences;
    }

    @Override
    public IntStream getUidxWithPreferences() {
        return IntStream.range(0, numUsers())
                .filter(uidx -> uidxList.get(uidx) != null);
    }

    @Override
    public IntStream getIidxWithPreferences() {
        return IntStream.range(0, numItems())
                .filter(iidx -> iidxList.get(iidx) != null);
    }

    @Override
    public int numUsersWithPreferences() {
        return (int) uidxList.stream()
                .filter(iv -> iv != null).count();
    }

    @Override
    public int numItemsWithPreferences() {
        return (int) iidxList.stream()
                .filter(iv -> iv != null).count();
    }

    @Override
    public Stream<IdxTimePref> getPreferences(int uidx, int iidx) {
        if (uidx > uidxList.size() - 1 || uidx < 0 || iidx > iidxList.size() - 1 || iidx < 0 || uidxList.get(uidx) == null) {
            return null;
        }

        List<IdxTimePref> uList = uidxList.get(uidx);
        List<IdxTimePref> listTimePref = new ArrayList<>();
        int low = 0;
        int high = uList.size() - 1;

        while (low <= high) {
            int mid = (low + high) >>> 1;
            IdxTimePref p = uList.get(mid);
            int cmp = Integer.compare(p.v1, iidx);
            if (cmp < 0) {
                low = mid + 1;
            } else if (cmp > 0) {
                high = mid - 1;
            } else {
                listTimePref.add(p);
                //Now check how many preferences are before and after
                //before
                int aux = mid;
                while (aux > 0 && aux < uList.size()) {
                    aux--;
                    IdxTimePref p2 = uList.get(aux);
                    if (Integer.compare(p2.v1, iidx) != 0) {
                        break;
                    }
                    listTimePref.add(p2);
                }
                //after
                aux = mid;
                while (aux > -1 && aux < uList.size() - 1) {
                    aux++;
                    IdxTimePref p2 = uList.get(aux);
                    if (Integer.compare(p2.v1, iidx) != 0) {
                        break;
                    }
                    listTimePref.add(p2);
                }
                break;
            }
        }
        return listTimePref.stream();
    }

    @Override
    public IntIterator getUidxIidxs(final int uidx) {
        return new StreamIntIterator(getUidxTimePreferences(uidx).mapToInt(IdxTimePref::v1));
    }

    @Override
    public DoubleIterator getUidxVs(final int uidx) {
        return new StreamDoubleIterator(getUidxTimePreferences(uidx).mapToDouble(IdxTimePref::v2));
    }

    @Override
    public IntIterator getIidxUidxs(final int iidx) {
        return new StreamIntIterator(getIidxTimePreferences(iidx).mapToInt(IdxTimePref::v1));
    }

    @Override
    public DoubleIterator getIidxVs(final int iidx) {
        return new StreamDoubleIterator(getIidxTimePreferences(iidx).mapToDouble(IdxTimePref::v2));
    }

    @Override
    public boolean useIteratorsPreferentially() {
        return false;
    }

    public static <U, I> SimpleFastTemporalPreferenceData<U, I> loadTemporal(Stream<Tuple4<U, I, Double, Long>> tuples, FastUserIndex<U> uIndex, FastItemIndex<I> iIndex) {
        AtomicInteger numPreferences = new AtomicInteger();

        List<List<IdxTimePref>> uidxList = new ArrayList<>();
        for (int uidx = 0; uidx < uIndex.numUsers(); uidx++) {
            uidxList.add(null);
        }

        List<List<IdxTimePref>> iidxList = new ArrayList<>();
        for (int iidx = 0; iidx < iIndex.numItems(); iidx++) {
            iidxList.add(null);
        }

        tuples.forEach(t -> {
            int uidx = uIndex.user2uidx(t.v1);
            int iidx = iIndex.item2iidx(t.v2);

            numPreferences.incrementAndGet();

            List<IdxTimePref> uList = uidxList.get(uidx);
            if (uList == null) {
                uList = new ArrayList<>();
                uidxList.set(uidx, uList);
            }
            uList.add(new IdxTimePref(iidx, t.v3, t.v4));

            List<IdxTimePref> iList = iidxList.get(iidx);
            if (iList == null) {
                iList = new ArrayList<>();
                iidxList.set(iidx, iList);
            }
            iList.add(new IdxTimePref(uidx, t.v3, t.v4));
        });

        return new SimpleFastTemporalPreferenceData<>(numPreferences.intValue(), uidxList, iidxList, uIndex, iIndex);
    }
    
    public void writeDataModel(String destPath) {
    	try {
			PrintStream resultFile = new PrintStream(destPath);
			this.getUsersWithPreferences().forEach(u -> {
	    		this.getUserPreferences(u).forEach(pref -> {
	    			resultFile.println(u + "\t" + pref.v1 +"\t" + pref.v2 + "\t" + pref.v3);
	    		});
	    	});
			resultFile.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
}

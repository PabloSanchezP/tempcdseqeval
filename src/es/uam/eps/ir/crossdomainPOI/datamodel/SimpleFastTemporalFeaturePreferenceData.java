/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.crossdomainPOI.datamodel;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Stream;

import org.jooq.lambda.tuple.Tuple4;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.preferences.IdxTimePref;
import es.uam.eps.ir.ranksys.core.feature.FeatureData;
import es.uam.eps.ir.ranksys.fast.index.FastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.FastUserIndex;

/**
 * DataModel extended that allow us to work with features, repetitions and timestamps
 * 
 * It admits: 
 * -Repetitions 
 * -Timestamps 
 * -Features
 *
 * This is the most complete Datamodel so far
 *
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 *
 * @param <U> type of users
 * @param <I> type of items
 * @param <F> type of features
 * @param <V> type of features values
 */
public class SimpleFastTemporalFeaturePreferenceData<U, I, F, V> extends SimpleFastTemporalPreferenceData<U, I> {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1401443119451152871L;
	private Map<String, FeatureData<I, F, V>> featuresData;

    protected SimpleFastTemporalFeaturePreferenceData(int numPreferences, List<List<IdxTimePref>> uidxList,
            List<List<IdxTimePref>> iidxList, FastUserIndex<U> uIndex, FastItemIndex<I> iIndex, Map<String, FeatureData<I, F, V>> featuresData) {
        super(numPreferences, uidxList, iidxList, uIndex, iIndex);
        this.featuresData = featuresData;
    }
    
    protected SimpleFastTemporalFeaturePreferenceData(int numPreferences, List<List<IdxTimePref>> uidxList,
            List<List<IdxTimePref>> iidxList, FastUserIndex<U> uIndex, FastItemIndex<I> iIndex) {
    	this(numPreferences, uidxList, iidxList, uIndex, iIndex, new TreeMap<>());
    }

    public void loadFeature(String feature, FeatureData<I, F, V> featureData) {
        if (featuresData == null) {
            featuresData = new TreeMap<>();
        }
        featuresData.put(feature, featureData);
    }

    public Map<String, FeatureData<I, F, V>> getFeaturesData() {
        return featuresData;
    }

    public void setFeaturesData(Map<String, FeatureData<I, F, V>> featuresData) {
        this.featuresData = featuresData;
    }

    public static <U, I, F, V> SimpleFastTemporalFeaturePreferenceData<U, I, F, V> loadTemporalFeature(Stream<Tuple4<U, I, Double, Long>> tuples, FastUserIndex<U> uIndex, FastItemIndex<I> iIndex) {
    	SimpleFastTemporalPreferenceData<U, I> tempData = SimpleFastTemporalPreferenceData.loadTemporal(tuples, uIndex, iIndex);
        return new SimpleFastTemporalFeaturePreferenceData<>(tempData.numPreferences, tempData.uidxList, tempData.iidxList, uIndex, iIndex);
    }

    public static <U, I, F, V> SimpleFastTemporalFeaturePreferenceData<U, I, F, V> loadTemporalFeature(Stream<Tuple4<U, I, Double, Long>> tuples, FastUserIndex<U> uIndex, FastItemIndex<I> iIndex, Map<String, FeatureData<I, F, V>> featuresData) {
    	SimpleFastTemporalPreferenceData<U, I> tempData = loadTemporal(tuples, uIndex, iIndex);
        return new SimpleFastTemporalFeaturePreferenceData<>(tempData.numPreferences, tempData.uidxList, tempData.iidxList, uIndex, iIndex, featuresData);
    }
}

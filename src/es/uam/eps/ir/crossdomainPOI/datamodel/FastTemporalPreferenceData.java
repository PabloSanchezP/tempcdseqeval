/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.crossdomainPOI.datamodel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import org.jooq.lambda.tuple.Tuple2;
import org.jooq.lambda.tuple.Tuple4;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.preferences.IdTimePref;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.preferences.IdxTimePref;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.utils.Tuple3id;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.utils.Tuple3od;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.utils.TuplesExtended;
import es.uam.eps.ir.ranksys.fast.index.FastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.FastUserIndex;
import es.uam.eps.ir.ranksys.fast.preference.IdxPref;
import es.uam.eps.ir.ranksys.fast.preference.SimpleFastPreferenceData;

/**
 * DataModel extended from RankSys datamodels that also work with timestamps
 * It admits: 
 * -Timestamps 
 * 
 * It does not admits: 
 * -Repetitions 
 * -Features
 *
 * It works with Time preferences (need tuples of 3)
 *
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public class FastTemporalPreferenceData<U, I> extends SimpleFastPreferenceData<U, I> {

    /**
	 * 
	 */
	private static final long serialVersionUID = -7775384342689185276L;
	List<List<Tuple2<IdxPref, Long>>> usersTimestamps;
    List<List<Tuple2<IdxPref, Long>>> itemsTimestamps;

    protected FastTemporalPreferenceData(int numPreferences, List<List<IdxPref>> uidxList,
            List<List<IdxPref>> iidxList, List<List<Tuple2<IdxPref, Long>>> uTimeStamps, List<List<Tuple2<IdxPref, Long>>> iTimeStamps, FastUserIndex<U> uIndex, FastItemIndex<I> iIndex) {
        super(numPreferences, uidxList, iidxList, uIndex, iIndex);
        this.itemsTimestamps = iTimeStamps;
        this.usersTimestamps = uTimeStamps;

        uTimeStamps.parallelStream()
                .filter(l -> l != null)
                .forEach(l -> Collections.sort(l, new Comparator<Tuple2<IdxPref, Long>>() {
                    @Override
                    public int compare(Tuple2<IdxPref, Long> o1, Tuple2<IdxPref, Long> o2) {
                        return o1.v1.compareTo(o2.v1);
                    }
                }));

        iTimeStamps.parallelStream()
                .filter(l -> l != null)
                .forEach(l -> Collections.sort(l, new Comparator<Tuple2<IdxPref, Long>>() {
                    @Override
                    public int compare(Tuple2<IdxPref, Long> o1, Tuple2<IdxPref, Long> o2) {
                        return o1.v1.compareTo(o2.v1);
                    }
                }));
    }

    public static <U, I> FastTemporalPreferenceData<U, I> loadTimeStamps(Stream<Tuple4<U, I, Double, Long>> tuples, FastUserIndex<U> uIndex, FastItemIndex<I> iIndex) {
        AtomicInteger numPreferences = new AtomicInteger();

        List<List<IdxPref>> uidxList = new ArrayList<>();
        for (int uidx = 0; uidx < uIndex.numUsers(); uidx++) {
            uidxList.add(null);
        }

        List<List<IdxPref>> iidxList = new ArrayList<>();
        for (int iidx = 0; iidx < iIndex.numItems(); iidx++) {
            iidxList.add(null);
        }

        List<List<Tuple2<IdxPref, Long>>> usersTimestamps = new ArrayList<>();
        for (int uidx = 0; uidx < uIndex.numUsers(); uidx++) {
            usersTimestamps.add(null);
        }

        List<List<Tuple2<IdxPref, Long>>> itemsTimestamps = new ArrayList<>();
        for (int iidx = 0; iidx < iIndex.numItems(); iidx++) {
            itemsTimestamps.add(null);
        }

        tuples.forEach(t -> {
            int uidx = uIndex.user2uidx(t.v1);
            int iidx = iIndex.item2iidx(t.v2);
            long timeStamp = t.v4;

            numPreferences.incrementAndGet();

            List<IdxPref> uList = uidxList.get(uidx);
            if (uList == null) {
                uList = new ArrayList<>();
                uidxList.set(uidx, uList);
            }
            IdxPref user = new IdxPref(iidx, t.v3);
            uList.add(user);

            List<IdxPref> iList = iidxList.get(iidx);
            if (iList == null) {
                iList = new ArrayList<>();
                iidxList.set(iidx, iList);
            }
            IdxPref item = new IdxPref(uidx, t.v3);
            iList.add(item);
            //Add now for the timeStamps
            List<Tuple2<IdxPref, Long>> uListT = usersTimestamps.get(uidx);
            if (uListT == null) {
                uListT = new ArrayList<>();
                usersTimestamps.set(uidx, uListT);
            }
            uListT.add(new Tuple2<IdxPref, Long>(user, timeStamp));

            List<Tuple2<IdxPref, Long>> iListT = itemsTimestamps.get(iidx);
            if (iListT == null) {
                iListT = new ArrayList<>();
                itemsTimestamps.set(iidx, iListT);
            }
            iListT.add(new Tuple2<IdxPref, Long>(item, timeStamp));

        });
        return new FastTemporalPreferenceData<>(numPreferences.intValue(), uidxList, iidxList, usersTimestamps, itemsTimestamps, uIndex, iIndex);
    }

    public Stream<? extends IdxTimePref> getUidxPreferencesTimeStamp(int uidx) {
        Stream<Tuple2<IdxPref, Long>> preferences = usersTimestamps.get(uidx).stream();
        return preferences.map(tuple -> new IdxTimePref(tuple.v1.v1, tuple.v1.v2, tuple.v2));
    }

    public Stream<? extends IdxTimePref> getIidxPreferencesTimeStamp(int iidx) {
        Stream<Tuple2<IdxPref, Long>> preferences = itemsTimestamps.get(iidx).stream();
        return preferences.map(tuple -> new IdxTimePref(tuple.v1.v1, tuple.v1.v2, tuple.v2));
    }

    public Stream<? extends IdTimePref<I>> getUserPreferencesTimeStamp(final U u) {
        return getUidxPreferencesTimeStamp(user2uidx(u))
                .map(this::iidx2itemExtended)
                .map(IdTimePref::new);
    }

    public Stream<? extends IdTimePref<U>> getItemPreferencesTimeStamp(final I i) {
        return getIidxPreferencesTimeStamp(item2iidx(i))
                .map(this::uidx2userExtended)
                .map(IdTimePref::new);
    }

    public Optional<IdxTimePref> getPreferenceTimeStamp(int uidx, int iidx) {
        List<Tuple2<IdxPref, Long>> uList = usersTimestamps.get(uidx);

        int low = 0;
        int high = uList.size() - 1;

        while (low <= high) {
            int mid = (low + high) >>> 1;
            Tuple2<IdxPref, Long> tuple = uList.get(mid);
            int cmp = Integer.compare(tuple.v1.v1, iidx);
            if (cmp < 0) {
                low = mid + 1;
            } else if (cmp > 0) {
                high = mid - 1;
            } else {
                return Optional.of(new IdxTimePref(tuple.v1.v1, tuple.v1.v2, tuple.v2));
            }
        }

        return Optional.empty();
    }

    public Optional<? extends IdTimePref<I>> getPreferenceTimeStamp(U u, I i) {
        Optional<? extends IdxTimePref> pref = getPreferenceTimeStamp(user2uidx(u), item2iidx(i));

        if (!pref.isPresent()) {
            return Optional.empty();
        } else {
            return Optional.of(new IdTimePref<>(i, pref.get().v2, pref.get().v3));
        }
    }

    private Tuple3od<U> uidx2userExtended(Tuple3id tuple) {
        return TuplesExtended.tuple(uidx2user(tuple.v1), tuple.v2, tuple.v3);
    }

    private Tuple3od<I> iidx2itemExtended(Tuple3id tuple) {
        return TuplesExtended.tuple(iidx2item(tuple.v1), tuple.v2, tuple.v3);
    }

}

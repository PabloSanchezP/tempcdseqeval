/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.crossdomainPOI.datamodel.temporal.utils;

import static es.uam.eps.ir.ranksys.core.util.FastStringSplitter.split;
import static java.lang.Double.parseDouble;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Stream;

import org.jooq.lambda.tuple.Tuple;
import org.jooq.lambda.tuple.Tuple3;
import org.jooq.lambda.tuple.Tuple4;
import org.ranksys.formats.parsing.Parser;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.TemporalPreferencesReaderIF;

/**
 * *
 *
 * Class to read the preferences that have a timeStamp. The process is the same
 * as the preference reader but working with time preferences. It can read data
 * with timeStamps and files of features
 *
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 * @param <F>
 */
public class ExtendedPreferenceReader<U, I, F> implements TemporalPreferencesReaderIF{

    public static <U, I, F> ExtendedPreferenceReader get() {
        return new ExtendedPreferenceReader<>();
    }

    private ExtendedPreferenceReader() {
    }

    public Stream<Tuple4<U, I, Double, Long>> readPreferencesTimeStamp(String in, Parser<U> up, Parser<I> ip) throws IOException {
        return read(new FileInputStream(in), up, ip);
    }

    public Stream<Tuple3<I, F, Double>> readFeature(String in, Parser<I> ip, Parser<F> fp) throws IOException {
        return readFeature(new FileInputStream(in), ip, fp);
    }

    public Stream<Tuple3<I, F, Double>> readFeature(InputStream in, Parser<I> ip, Parser<F> fp) throws IOException {
        return new BufferedReader(new InputStreamReader(in)).lines().map(line -> {
            CharSequence[] tokens = split(line, '\t', 3);
            I item = ip.parse(tokens[0]);
            F feature = fp.parse(tokens[1]);
            Double featureValue = parseDouble(tokens[2].toString());
            return Tuple.tuple(item, feature, featureValue);
        });
    }

	@Override
	public <U, I> Stream<Tuple4<U, I, Double, Long>> read(InputStream in, Parser<U> up, Parser<I> ip) throws IOException {
        return new BufferedReader(new InputStreamReader(in)).lines().map(line -> {
        	 CharSequence[] tokens = line.split("\t");
             U user = up.parse(tokens[0]);
             I item = ip.parse(tokens[1]);
             double value = parseDouble(tokens[2].toString());
             //These are compulsory. If timeStamp does no exist, we put a -1
             Long timestamp = -1L;
             if (tokens.length >= 4) {
             	timestamp = Long.parseLong(tokens[3].toString());
             }
             return Tuple.tuple(user, item, value, timestamp);
        });
    }

}

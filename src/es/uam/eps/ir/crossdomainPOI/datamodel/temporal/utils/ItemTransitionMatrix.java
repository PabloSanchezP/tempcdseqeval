/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.crossdomainPOI.datamodel.temporal.utils;

import java.util.concurrent.atomic.AtomicInteger;

import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.crossdomainPOI.utils.PreferenceComparators;

/**
 * Class to build a item x item transition matrix (k-order = 1)
 * -Rows represent the current state and columns represent the probability to go to the next item
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public class ItemTransitionMatrix{
	private DenseDoubleMatrix2D itemToitemM;
	
	public ItemTransitionMatrix(FastTemporalPreferenceDataIF<?, ?> temporalPreferenceData) {
		itemToitemM = new DenseDoubleMatrix2D(temporalPreferenceData.numItemsWithPreferences(), temporalPreferenceData.numItemsWithPreferences());
		itemToitemM.assign(x -> x = 0); //Init all the matrix to 0s
		
		//Build the sequence
		temporalPreferenceData.getUidxWithPreferences().forEach(uidx -> {
			AtomicInteger iidxPrev = new AtomicInteger(-1);
			//For every preference of the user (ordered from the most acient to the most recent)
			temporalPreferenceData.getUidxTimePreferences(uidx).sorted(PreferenceComparators.timeComparatorIdxTimePref).forEach(idxtimepref -> {
				if (iidxPrev.get() != -1) {
					itemToitemM.set(iidxPrev.get(), idxtimepref.v1, itemToitemM.get(iidxPrev.get(), idxtimepref.v1) + 1.0);
				}
				iidxPrev.set(idxtimepref.v1);
			});
		});
		
		//Now, we divide all the values in every row by the sum of every row (to compute the probabilities)
		for (int i = 0; i < itemToitemM.rows(); i++) {
			DoubleMatrix1D row = itemToitemM.viewRow(i);
			double rowSum = row.zSum();
			row.assign(row, (x, y) -> x / rowSum);
		}
	}



	public DoubleMatrix1D getItemRow(int iidx) {
		return itemToitemM.viewRow(iidx);
	}

	public double probabilityItemItem(int iidxPrevius, int iidxNext) {
		return itemToitemM.get(iidxPrevius, iidxNext);
	}
	
	
	public DenseDoubleMatrix2D getItemToitemM() {
		return itemToitemM;
	}

	public void setItemToitemM(DenseDoubleMatrix2D itemToitemM) {
		this.itemToitemM = itemToitemM;
	}
	
	
	
	
	

}

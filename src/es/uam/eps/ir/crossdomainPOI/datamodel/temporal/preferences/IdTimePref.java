/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.crossdomainPOI.datamodel.temporal.preferences;

import java.io.Serializable;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.utils.Tuple3od;

/**
 * A user or item time preference (user/item, rating, timestamp).
 *
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <I> type of the user or item
 */
public class IdTimePref<I> extends Tuple3od<I> implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 2267530359077221545L;

	/**
     * Constructor with both values.
     *
     * @param id the ID
     * @param v the double
     */
    public IdTimePref(I id, double v, long t) {
        super(id, v, t);
    }

    /**
     * Constructor from a object-double pair.
     *
     * @param tuple pair of object (id) and double (preference value)
     */
    public IdTimePref(Tuple3od<I> tuple) {
        super(tuple);
    }

}

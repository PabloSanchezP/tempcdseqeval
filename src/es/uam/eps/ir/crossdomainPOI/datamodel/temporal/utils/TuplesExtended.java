/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.crossdomainPOI.datamodel.temporal.utils;


import org.ranksys.core.util.tuples.Tuples;

/**
 * Methods to extend the tuples creation to triplets
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public class TuplesExtended extends Tuples{
	 
	/**
     * Creates a tuple of object-double.
     *
     * @param <T1> type of object
     * @param v1 object value
     * @param v2 double value
     * @return tuple
     */
    public static <T1> Tuple3od<T1> tuple(T1 v1, double v2, long v3) {
        return new Tuple3od<>(v1, v2, v3);
    }

    /**
     * Creates a tuple of integer-double.
     *
     * @param v1 integer value
     * @param v2 double value
     * @return tuple
     */
    public static Tuple3id tuple(int v1, double v2, long v3) {
        return new Tuple3id(v1, v2, v3);
    }

}

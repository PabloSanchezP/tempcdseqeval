/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces;

import java.util.stream.Stream;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.preferences.IdTimePref;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.preferences.IdxTimePref;

/**
 * Access to particular user-item-timestamps triplets
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U> 
 * @param <I>
 */
public interface FastPointWiseTemporalPreferenceDataIF<U,I> extends PointWiseTemporalPreferenceDataIF<U, I>, FastTemporalPreferenceDataIF<U, I>{
	
	@Override
	public default Stream<? extends IdTimePref<I>> getPreferences(U u, I i){
		Stream<? extends IdxTimePref> str = getPreferences(user2uidx(u), item2iidx(i));
		if (str==null) {
			return null;
		}
		else
			return str.map(p -> new IdTimePref<I>(i,p.v2,p.v3));
	}
	
	
	public Stream<? extends IdxTimePref> getPreferences(int uidx, int iidx);
}

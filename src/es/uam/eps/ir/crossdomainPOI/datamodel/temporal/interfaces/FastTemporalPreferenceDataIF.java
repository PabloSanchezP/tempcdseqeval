/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces;

import java.util.stream.IntStream;
import java.util.stream.Stream;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.preferences.IdxTimePref;
import es.uam.eps.ir.ranksys.fast.index.FastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.FastUserIndex;
import it.unimi.dsi.fastutil.doubles.DoubleIterator;
import it.unimi.dsi.fastutil.ints.IntIterator;

/**
 * Extension of the Preference Data making use of the Fast indexes of rankSys.
 * They work with the temporal preference data (preferences with timeStamps)
 *
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 * 
 * @param <U> type of the users
 * @param <I> type of the items
 */
public interface FastTemporalPreferenceDataIF<U, I> extends TemporalPreferenceDataIF<U, I>, FastUserIndex<U>, FastItemIndex<I>{
	/**
     * Returns the number of users who have a preference for the item.
     *
     * @param iidx item index
     * @return number of users who have a preference for the item
     */
    public int numUsers(int iidx);

    /**
     * Returns the number of items for which the user has preference for.
     *
     * @param uidx user index
     * @return number of items for which the user has preference for
     */
    public int numItems(int uidx);
    
    /**
     * Returns a stream of user indexes who have preferences for items.
     *
     * @return a stream of user indexes who have preferences for items
     */
    public IntStream getUidxWithPreferences();
    
    
    /**
     * Returns a stream of item indexes for which users have preferences.
     *
     * @return a stream of item indexes for which users have preferences
     */
    public IntStream getIidxWithPreferences();
    
    /**
     * Gets the preferences of a user.
     *
     * @param uidx user index
     * @return preferences of the user
     */
    public Stream<? extends IdxTimePref> getUidxTimePreferences(int uidx);
    
    /**
     * Gets the preferences of an item.
     *
     * @param iidx item index
     * @return preferences of the item
     */
    public Stream<? extends IdxTimePref> getIidxTimePreferences(int iidx);
    
    
    /**
     * Returns the item idxs of the preferences of a user.
     *
     * @param uidx user index
     * @return iterator of the idxs of the items
     */
    public IntIterator getUidxIidxs(final int uidx);

    /**
     * Returns the item values of the preferences of a user.
     *
     * @param uidx user index
     * @return iterator of the values of the items
     */
    public DoubleIterator getUidxVs(final int uidx);

    /**
     * Returns the user idxs of the preferences for an item.
     *
     * @param iidx item index
     * @return iterator of the idxs of the users.
     */
    public IntIterator getIidxUidxs(final int iidx);

    /**
     * Returns the user values of the preferences for an item.
     *
     * @param iidx item index
     * @return iterator of the values of the users
     */
    public DoubleIterator getIidxVs(final int iidx);

    /**
     * Use methods returning IntIterator or DoubleIterator over streams of
     * IdxPref?
     * 
     * @return yes/no
     */
    public boolean useIteratorsPreferentially();
}





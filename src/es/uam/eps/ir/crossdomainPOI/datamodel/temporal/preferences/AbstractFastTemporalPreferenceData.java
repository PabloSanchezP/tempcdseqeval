/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.crossdomainPOI.datamodel.temporal.preferences;

import java.io.Serializable;
import java.util.stream.Stream;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.utils.Tuple3id;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.utils.Tuple3od;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.utils.TuplesExtended;
import es.uam.eps.ir.ranksys.fast.index.FastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.FastUserIndex;

import java.util.ArrayList;
import java.util.List;

import org.jooq.lambda.tuple.Tuple3;
import org.jooq.lambda.tuple.Tuple4;

/**
 * *
 * Abstract class that implements the FastTemporalPreferenceData interface. Is a
 * copy of FastPreferenceData working with temporal information.
 *
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U> Type of users
 * @param <I> Type of items
 */
public abstract class AbstractFastTemporalPreferenceData<U, I> implements FastTemporalPreferenceDataIF<U, I>, Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -5468959759387219861L;

	/**
     * user index.
     */
    protected final FastUserIndex<U> ui;

    /**
     * item index.
     */
    protected final FastItemIndex<I> ii;

    /**
     * Constructor.
     *
     * @param userIndex user index
     * @param itemIndex item index
     */
    public AbstractFastTemporalPreferenceData(FastUserIndex<U> userIndex, FastItemIndex<I> itemIndex) {
        this.ui = userIndex;
        this.ii = itemIndex;
    }

    @Override
    public int user2uidx(U u) {
        return ui.user2uidx(u);
    }

    @Override
    public U uidx2user(int i) {
        return ui.uidx2user(i);
    }

    @Override
    public boolean containsUser(U u) {
        return ui.containsUser(u);
    }

    @Override
    public int numUsers() {
        return ui.numUsers();
    }

    @Override
    public Stream<U> getAllUsers() {
        return ui.getAllUsers();
    }

    @Override
    public int item2iidx(I i) {
        return ii.item2iidx(i);
    }

    @Override
    public I iidx2item(int i) {
        return ii.iidx2item(i);
    }

    @Override
    public boolean containsItem(I i) {
        return ii.containsItem(i);
    }

    @Override
    public int numItems() {
        return ii.numItems();
    }

    @Override
    public Stream<I> getAllItems() {
        return ii.getAllItems();
    }

    @Override
    public int numUsers(I i) {
        return numUsers(item2iidx(i));
    }

    @Override
    public int numItems(U u) {
        return numItems(user2uidx(u));
    }

    @Override
    public Stream<? extends IdTimePref<I>> getUserPreferences(final U u) {
        return getUidxTimePreferences(user2uidx(u)).
                map(this::iidx2item).
                map(IdTimePref::new);
    }

    @Override
    public Stream<? extends IdTimePref<U>> getItemPreferences(final I i) {
        return getIidxTimePreferences(item2iidx(i))
                .map(this::uidx2user)
                .map(IdTimePref::new);
    }

    @Override
    public double getAverageUser(U u) {
        return getUidxTimePreferences(user2uidx(u)).mapToDouble(IdxTimePref::v2).average().getAsDouble();
    }

    @Override
    public double getAverageItem(I i) {
        return getIidxTimePreferences(item2iidx(i)).mapToDouble(IdxTimePref::v2).average().getAsDouble();

    }

    @Override
    public Stream<U> getUsersWithPreferences() {
        return getUidxWithPreferences().mapToObj(this::uidx2user);
    }

    @Override
    public Stream<I> getItemsWithPreferences() {
        return getIidxWithPreferences().mapToObj(this::iidx2item);
    }

    @Override
    public int numUsersWithPreferences() {
        return (int) getUidxWithPreferences().count();
    }

    @Override
    public int numItemsWithPreferences() {
        return (int) getIidxWithPreferences().count();
    }

    public Tuple3id item2iidx(Tuple3od<I> tuple) {
        return TuplesExtended.tuple(item2iidx(tuple.v1), tuple.v2, tuple.v3);
    }

    public Tuple3od<I> iidx2item(Tuple3id tuple) {
        return TuplesExtended.tuple(iidx2item(tuple.v1), tuple.v2, tuple.v3);
    }

    public Tuple3id user2uidx(Tuple3od<U> tuple) {
        return TuplesExtended.tuple(user2uidx(tuple.v1), tuple.v2, tuple.v3);
    }

    public Tuple3od<U> uidx2user(Tuple3id tuple) {
        return TuplesExtended.tuple(uidx2user(tuple.v1), tuple.v2, tuple.v3);
    }

    /**
     * Method to get the dataModel as (temporal) tuples
     *
     * @return a stream of tuples
     */
    public Stream<Tuple4<U, I, Double, Long>> getAsTemporalTuples() {
        List<Tuple4<U, I, Double, Long>> streamToReturn = new ArrayList<>();
        getUsersWithPreferences().forEach(u -> {
            getUserPreferences(u).forEach(idTimePref -> {
                streamToReturn.add(new Tuple4<>(u, idTimePref.v1, idTimePref.v2, idTimePref.v3));
            });
        });
        return streamToReturn.stream();
    }
    
    /**
     * Method to get the dataModel as tuples ignoring the temporal aspect
     *
     * @return a stream of tuples
     */
    public Stream<Tuple3<U, I, Double>> getAsSimpleTuples() {
        List<Tuple3<U, I, Double>> streamToReturn = new ArrayList<>();
        getUsersWithPreferences().forEach(u -> {
            getUserPreferences(u).forEach(idTimePref -> {
                streamToReturn.add(new Tuple3<>(u, idTimePref.v1, idTimePref.v2));
            });
        });
        return streamToReturn.stream();
    }
    

    
    
    
}

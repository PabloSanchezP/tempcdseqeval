/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces;

import java.util.stream.Stream;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.preferences.IdTimePref;

/**
 * Point-Wise preference data
 * 
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U> type of users
 * @param <I> type of items
 */
public interface PointWiseTemporalPreferenceDataIF<U, I> extends TemporalPreferenceDataIF<U, I>{
	/**
	 * Get preferences of a user for a item
	 * @param u
	 * @param i
	 * @return
	 */
	public Stream<? extends IdTimePref<I>> getPreferences(U u, I i);
	
}

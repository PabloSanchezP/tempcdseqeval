/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.stream.Stream;

import org.jooq.lambda.tuple.Tuple4;
import org.ranksys.formats.parsing.Parser;

/***
 * Interface created to read the preferences of the users 
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public interface TemporalPreferencesReaderIF {

    /**
     * Reads preferences from a file.
     *
     * @param <U> user type
     * @param <I> item type
     * @param in path to file
     * @param up user parser
     * @param ip item parser
     * @return stream of user-item-value quadruples
     * @throws IOException when I/O problems
     */
    public default <U, I> Stream<Tuple4<U, I, Double,Long>> read(String in, Parser<U> up, Parser<I> ip) throws IOException {
        return read(new FileInputStream(in), up, ip);
    }

    /**
     * Reads preferences from an input stream.
     *
     * @param <U> user type
     * @param <I> item tpye
     * @param in input stream to read from
     * @param up user parser
     * @param ip item parser
     * @return stream of user-item-value quadruples
     * @throws IOException when I/O problems
     */
    public <U, I> Stream<Tuple4<U, I, Double,Long>> read(InputStream in, Parser<U> up, Parser<I> ip) throws IOException;

}

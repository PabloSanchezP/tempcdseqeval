/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.crossdomainPOI.datamodel.temporal.utils;

import java.io.Serializable;

import org.jooq.lambda.tuple.Tuple;
import org.jooq.lambda.tuple.Tuple3;

/***
 * Same as Tuple2od with a third argument (timestamp)
 * Object-double-timestamp
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public class Tuple3od<T1> implements Comparable<Tuple3od<T1>>, Serializable, Cloneable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6020519127401694471L;

	/**
     * First value (object).
     */
    public final T1 v1;

    /**
     * Second value (double).
     */
    public final double v2;
    
    /**
     * This value (timestamp)
     */
    public final long v3;

    /**
     * Constructor from a jOOL tuple.
     *
     * @param tuple tuple to be copied.
     */
    public Tuple3od(Tuple3<T1, Double, Long> tuple) {
        this(tuple.v1, tuple.v2, tuple.v3);
    }

    /**
     * Constructor from an object-double tuple.
     *
     * @param tuple tuple to be copied
     */
    public Tuple3od(Tuple3od<T1> tuple) {
        this(tuple.v1, tuple.v2, tuple.v3);
    }

    /**
     * Constructor from separate object and double values.
     *
     * @param v1 object value
     * @param v2 double value
     */
    public Tuple3od(T1 v1, double v2, long v3) {
        this.v1 = v1;
        this.v2 = v2;
        this.v3=v3;
    }

    /**
     * Returns the first element (object).
     *
     * @return first element (object).
     */
    public T1 v1() {
        return v1;
    }

    /**
     * Returns the second element (double).
     *
     * @return second element (double).
     */
    public double v2() {
        return v2;
    }
    
    public long v3(){
    	return v3;
    }

    /**
     * Converts the tuple into a jOOL tuple.
     *
     * @return jOOL tuple
     */
    public Tuple3<T1, Double,Long> asTuple() {
        return Tuple.tuple(v1, v2,v3);
    }

    @Override
    @SuppressWarnings("unchecked")
    public int compareTo(Tuple3od<T1> other) {
        int result;

        result = ((Comparable<T1>) v1).compareTo(other.v1);
        if (result != 0) {
            return result;
        }
        result = Double.compare(v2, other.v2);
        if (result != 0) {
            return result;
        }
        result = Double.compare(v3, other.v3);
        if (result != 0) {
            return result;
        }

        return result;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((v1 == null) ? 0 : v1.hashCode());
		long temp;
		temp = Double.doubleToLongBits(v2);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + (int) (v3 ^ (v3 >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Tuple3od))
			return false;
		Tuple3od other = (Tuple3od) obj;
		if (v1 == null) {
			if (other.v1 != null)
				return false;
		} else if (!v1.equals(other.v1))
			return false;
		if (Double.doubleToLongBits(v2) != Double.doubleToLongBits(other.v2))
			return false;
		if (v3 != other.v3)
			return false;
		return true;
	}

    

}

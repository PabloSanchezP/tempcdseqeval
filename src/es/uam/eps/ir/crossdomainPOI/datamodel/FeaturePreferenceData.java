/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.crossdomainPOI.datamodel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import org.jooq.lambda.tuple.Tuple3;

import es.uam.eps.ir.crossdomainPOI.utils.SequentialRecommendersUtils;
import es.uam.eps.ir.ranksys.core.feature.FeatureData;
import es.uam.eps.ir.ranksys.core.preference.PreferenceData;
import es.uam.eps.ir.ranksys.fast.index.FastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.FastUserIndex;
import es.uam.eps.ir.ranksys.fast.preference.IdxPref;
import es.uam.eps.ir.ranksys.fast.preference.SimpleFastPreferenceData;


/***
 * FeaturePreference data. DataModel extended from ranksys that include features
 * 
 * It admits:
 * -Features
 * It does not admits
 * -Repetitions
 * -Timestamps
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U> Type of the users
 * @param <I> Type of the items
 * @param <F> Type of the features
 * @param <V> Type of the values of features
 */
public class FeaturePreferenceData <U, I, F, V> extends SimpleFastPreferenceData<U, I> {	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2498878188551102471L;
	//All features associated with the items
	private Map<String, FeatureData<I,F,V>> featuresData;


	protected FeaturePreferenceData(int numPreferences, List<List<IdxPref>> uidxList, List<List<IdxPref>> iidxList,
			FastUserIndex<U> uIndex, FastItemIndex<I> iIndex) {
		super(numPreferences, uidxList, iidxList, uIndex, iIndex);
		featuresData = new TreeMap<>();
	}
	
	public void loadFeature(String feature, FeatureData<I,F,V> featureData){
		if (featuresData == null)
			featuresData = new TreeMap<>();
		featuresData.put(feature, featureData);
	}

	public Map<String, FeatureData<I, F, V>> getFeaturesData() {
		return featuresData;
	}

	public void setFeaturesData(Map<String, FeatureData<I, F, V>> featuresData) {
		this.featuresData = featuresData;
	}
	
	public static <U, I,F,V> FeaturePreferenceData<U,I,F,V> loadPreferences(Stream<Tuple3<U, I, Double>> tuples, FastUserIndex<U> uIndex, FastItemIndex<I> iIndex) {
        AtomicInteger numPreferences = new AtomicInteger();

        List<List<IdxPref>> uidxList = new ArrayList<>();
        for (int uidx = 0; uidx < uIndex.numUsers(); uidx++) {
            uidxList.add(null);
        }

        List<List<IdxPref>> iidxList = new ArrayList<>();
        for (int iidx = 0; iidx < iIndex.numItems(); iidx++) {
            iidxList.add(null);
        }

        tuples.forEach(t -> {
            int uidx = uIndex.user2uidx(t.v1);
            int iidx = iIndex.item2iidx(t.v2);

            numPreferences.incrementAndGet();

            List<IdxPref> uList = uidxList.get(uidx);
            if (uList == null) {
                uList = new ArrayList<>();
                uidxList.set(uidx, uList);
            }
            uList.add(new IdxPref(iidx, t.v3));

            List<IdxPref> iList = iidxList.get(iidx);
            if (iList == null) {
                iList = new ArrayList<>();
                iidxList.set(iidx, iList);
            }
            iList.add(new IdxPref(uidx, t.v3));
        });

        return new FeaturePreferenceData<>(numPreferences.intValue(), uidxList, iidxList, uIndex, iIndex);
    }
	
	public static <U, I,F,V> FeaturePreferenceData<U,I,F,V> loadPreferencesfromSimplePreferenceData(PreferenceData<U, I> pref, FastUserIndex<U> uIndex, FastItemIndex<I> iIndex) {
		Stream<Tuple3<U, I, Double>> tuples = SequentialRecommendersUtils.preferenceDataToTuples(pref);
		return loadPreferences(tuples, uIndex, iIndex);
    }
	
	
	
}

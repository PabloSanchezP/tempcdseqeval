#!/bin/bash

: '
  Script for generating the recommendations using the cross domain for the 8 cities selected

  Script used in paper "On the effects of aggregation strategies in different groups of users in venue recommendation"

'

# Necessary to execute with ./

# Global variables for JVM
JAR=target/CrossDomainPOIRecommendation-0.1.1-SNAPSHOT.jar
jvmMemory=-Xmx12G
ttpathfiles=TrainTest
similaritiesFolder=Sim
javaCommand=java

recPrefix=rec

# Nomenclature for Datasets
FoursqrName=Foursqr


# Extension of Files
extensionMap=_Mapping.txt
extensionCoords=_Coords.txt
extensionFeature=_Features.txt
extensionAggregated=_Aggregated.txt

# Routes of Original Datasets
FoursqrCheckingsOriginal=$ttpathfiles/dataset_TIST2015_Checkins.txt
FoursqrCitiesOriginal=$ttpathfiles/dataset_TIST2015_Cities.txt
FoursqrPOISOriginal=$ttpathfiles/dataset_TIST2015_POIs.txt

# cities selected to perform the experiments
cities="MX_MexicoCity CL_Santiago JP_Tokyo BR_SaoPaulo RU_Moscow MY_KualaLumpur CL_Santiago JP_Tokyo ID_Jakarta TR_Istanbul"
PathOfSplits=$ttpathfiles/CitiesSplit
aggregateStrategy=AVERAGE
aggregateStrategySUM=SUM

recommendedFolder=RecommendationFolder

allneighbours="5 10 20 30 40 50 60 70 80 90 100"
itemsRecommended=100


# Variables for recommendation
itemsRecommended=100
featureFile=$ttpathfiles/NewItemIdsCountryCityJoin.txt
featureItemCol=0
featureCatCol=1

realFeatureFile=$ttpathfiles/POIS_Features.txt


allKFactorizerRankSys="10 50 100"
allLambdaFactorizerRankSys="0.1 1 10"
allAlphaFactorizerRankSys="0.1 1 10"

complete_cities=complete_cities

coordFile=$ttpathfiles/POIS_Coords.txt
nonaccresultsPrefix=naeval

cutoffs="5,10"

mymedialitePath=MyMediaLite-3.11/bin
BPRFactors=$allKFactorizerRankSys
BPRBiasReg="0 0.5 1"
BPRLearnRate=0.05
BPRNumIter=50
BPRRegU="0.0025 0.001 0.005 0.01 0.1"
BPRRegJ="0.00025 0.0001 0.0005 0.001 0.01"
extensionMyMediaLite=MyMedLt
geoBPRmaxDists="1 4"



# For rankGeoFM
cs_RANKGEOFM="1"
alphas_RANKGEOFM="0.2"
epsilons_RANKGEOFM="0.3"
kfactors_RANKGEOFM="100"
ns_RANKGEOFM="100"
iters_RANKGEOFM="120"
decays_RANKGEOFM="1"
isboldDriver_RANKGEOFM="true"
learnRates_RANKGEOFM="0.001"
maxRates_RANKGEOFM="0.001"


# For FMFMGM
alphas_FMFMGM="0.4"
thetas_FMFMGM="0.02"
distances_FMFMGM="15"
iters_FMFMGM="30"
kfactors_FMFMGM="100"
alphas2_FMFMGM="20"
betas_FMFMGM="0.2"
learningRates_FMFMGM="0.0001"
sigmoids_FMFMGM="false"

maxDiffTime=1814400
minDiffTime=60
minClosePrefBot=3


# Now, for the cross-domain most popular scenario, we create and make recommendations

for fold in 0 # 1
do
  directory=$PathOfSplits/Fold$fold/$complete_cities
  mkdir -p $directory/RecommendationFolder
  recommendationFolder=$directory/RecommendationFolder
  trainfile=$directory/"split_complete_cities__trainingAggr""$aggregateStrategy""$fold".dat
  trainfileSUM=$directory/"split_complete_cities__trainingAggr""$aggregateStrategySUM""$fold".dat

  # Statistics of Foursqr
  # Training with repetitions
  trainfileWithRep=$directory/"split_complete_cities"__"training"$fold.dat
  testFileOfCD=$directory/"split_complete_cities"__"test"$fold.dat
  resultStatistics=$directory/"split_complete_cities"_"StatisticsFoursqr""Fold""$fold".dat

  # Compute the statistics
  $javaCommand $jvmMemory -jar $JAR -o StatisticsFoursqr -trf $trainfileWithRep $trainfile $testFileOfCD $featureFile $resultStatistics

  concatenationsTest=""
  concatenationsCandidates=""
  concatenationsMatch=""
  for city in $cities
  do
    if [ $city != "complete_cities" ] && [ $city != "complete" ] ; then
      testfile=$PathOfSplits/Fold$fold/$city/"split_""$city""__test""$fold".dat
      concatenationsTest+=$testfile","
      concatenationsMatch+=$city","
      concatenationsCandidates+=$featureFile","
    fi
  done
  concatenationsTest=${concatenationsTest::-1}
  concatenationsMatch=${concatenationsMatch::-1}
  concatenationsCandidates=${concatenationsCandidates::-1}

  # For each fold
  # The training set is always the complete_cities train
  for ranksysRecommender in PopularityRecommender RandomRecommender
  do
    # RankSys -> Popularity
    outputRecfiles=""
    for city in $cities
    do
      if [ $city != "complete_cities" ] && [ $city != "complete" ] ; then
        outputRecfile=$recommendationFolder/"$recPrefix"_"$complete_cities""_""$city"_Fold"$fold"_ranksys_"$ranksysRecommender".txt
        outputRecfiles+=$outputRecfile","
      fi
    done
    outputRecfiles=${outputRecfiles::-1}
    $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $concatenationsTest -cIndex true -rr "$ranksysRecommender" -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfiles -ff $concatenationsCandidates -matchCat $concatenationsMatch -fic $featureItemCol -ffc $featureCatCol -tsfFull $testFileOfCD
  done
  wait

  for neighbours in 40 70 80 90 100
  do

    # Ranksys with similarities UserBased

    for UBsimilarity in SetJaccardUserSimilarity SetCosineUserSimilarity
    do
      outputRecfiles=""
      for city in $cities
      do
        if [ $city != "complete_cities" ] && [ $city != "complete" ] ; then
          outputRecfile=$recommendationFolder/"$recPrefix"_"$complete_cities""_""$city"_Fold"$fold"_ranksys_UB_"$UBsimilarity"_k"$neighbours".txt
          outputRecfiles+=$outputRecfile","
        fi
      done
      outputRecfiles=${outputRecfiles::-1}
      $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $concatenationsTest -cIndex false -rr UserNeighborhoodRecommender -rs $UBsimilarity -nI $itemsRecommended -n $neighbours -orf $outputRecfiles -ff $concatenationsCandidates -matchCat $concatenationsMatch -fic $featureItemCol -ffc $featureCatCol -tsfFull $testFileOfCD

    done
    wait # UB sim

    # Ranksys with similarities ItemBased

    for IBsimilarity in SetJaccardItemSimilarity SetCosineItemSimilarity
    do
      outputRecfiles=""
      for city in $cities
      do
        if [ $city != "complete_cities" ] && [ $city != "complete" ] ; then
          outputRecfile=$recommendationFolder/"$recPrefix"_$complete_cities"_""$city"_Fold"$fold"_ranksys_IB_"$IBsimilarity"_k"$neighbours".txt
          outputRecfiles+=$outputRecfile","
        fi
      done
      outputRecfiles=${outputRecfiles::-1}
      $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $concatenationsTest -cIndex false -rr ItemNeighborhoodRecommender -rs $IBsimilarity -nI $itemsRecommended -n $neighbours -orf $outputRecfiles -ff $concatenationsCandidates -matchCat $concatenationsMatch -fic $featureItemCol -ffc $featureCatCol -tsfFull $testFileOfCD
    done
    wait # IB Sim

  done # End Neighbours	



  # BPRMF recommendation 
  for repetition in 1 #2 3 4
  do
    for factor in 50 100
    do
      for bias_reg in 0
      do
        for regU in $BPRRegU #Regularization for items and users is the same
        do
          # Need to change the recommendation. First, save the model and then for each city, we will load it
          regJ=$(echo "$regU/10" | bc -l)

          saveModel=$recommendationFolder/"$recPrefix"_"$complete_cities"_"$fold"_"$extensionMyMediaLite"_BPRMF_nFact"$factor"_nIter"$BPRNumIter"_LearnR"$BPRLearnRate"_BiasR"$bias_reg"_RegU"$regU"_RegI"$regU"_RegJ"$regJ""Rep$repetition"MODELSAVED.txt
          echo $saveModel

          echo "./$mymedialitePath/item_recommendation --training-file=$trainfile --recommender=BPRMF --save-model=$saveModel --recommender-options="num_factors=$factor bias_reg=$bias_reg reg_u=$regU reg_i=$regU reg_j=$regJ learn_rate=$BPRLearnRate UniformUserSampling=false WithReplacement=false num_iter=$BPRNumIter""
          ./$mymedialitePath/item_recommendation --training-file=$trainfile --recommender=BPRMF --save-model=$saveModel --recommender-options="num_factors=$factor bias_reg=$bias_reg reg_u=$regU reg_i=$regU reg_j=$regJ learn_rate=$BPRLearnRate UniformUserSampling=false WithReplacement=false num_iter=$BPRNumIter"

          for city in $cities
          do
            if [ $city != "complete_cities" ] && [ $city != "complete" ] ; then
              fileCandidatesBPR=$PathOfSplits/Fold$fold/$city/"$city"CANDIDATES"$fold".dat
              trainFileSDAuxiliar=$PathOfSplits/Fold$fold/$city/"split_""$city""__training""$fold".dat
              cut -f2 $trainFileSDAuxiliar | sort | uniq > $fileCandidatesBPR

              outputRecfile=$recommendationFolder/"$recPrefix"_"$complete_cities"_"$city"_Fold"$fold"_"$extensionMyMediaLite"_BPRMF_nFact"$factor"_nIter"$BPRNumIter"_LearnR"$BPRLearnRate"_BiasR"$bias_reg"_RegU"$regU"_RegI"$regU"_RegJ"$regJ""Rep$repetition".txt
              echo $outputRecfile
              outputRecfile2=$outputRecfile"Aux".txt

              echo "./$mymedialitePath/item_recommendation --training-file=$trainfile --recommender=BPRMF --prediction-file=$outputRecfile2 --load-model=$saveModel --candidate-items=$fileCandidatesBPR --predict-items-number=$itemsRecommended"
              ./$mymedialitePath/item_recommendation --training-file=$trainfile --recommender=BPRMF --prediction-file=$outputRecfile2 --load-model=$saveModel --candidate-items=$fileCandidatesBPR --predict-items-number=$itemsRecommended

              testfile=$PathOfSplits/Fold$fold/$city/"split_""$city""__test""$fold".dat
              $javaCommand $jvmMemory -jar $JAR -o ParseMyMediaLite -trf $outputRecfile2 $testfile $outputRecfile
              rm $outputRecfile2
              rm $fileCandidatesBPR
            fi
          done
          wait

        done # Reg U
        wait
      done # Bias reg
      wait
    done # Factors
    wait
  done # Repetition
  wait

    # HKV
	for repetition in 1 #2 3 4 5
    do
	  for rankRecommenderNoSim in MFRecommenderHKV
	  do
		for kFactor in 10 50
		do
		  for lambdaValue in $allLambdaFactorizerRankSys
		  do
		    for alphaValue in $allAlphaFactorizerRankSys
		    do
		      # Neighbours is put to 20 because this recommender does not use it
		      outputRecfiles=""
		      for city in $cities
		      do
		        if [ $city != "complete_cities" ] && [ $city != "complete" ] ; then
		          outputRecfile=$recommendationFolder/"$recPrefix"_"$complete_cities""_""$city"_Fold"$fold"_ranksys_"$rankRecommenderNoSim"_kFactor"$kFactor"_aFactorizer"$alphaValue"_lFactorizer"$lambdaValue"_Rep$repetition.txt
		          outputRecfiles+=$outputRecfile","
		        fi
		      done
		      outputRecfiles=${outputRecfiles::-1}
		      $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $concatenationsTest -cIndex false -rr $rankRecommenderNoSim -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfiles -kFactorizer $kFactor -aFactorizer $alphaValue -lFactorizer $lambdaValue -ff $concatenationsCandidates -matchCat $concatenationsMatch -fic $featureItemCol -ffc $featureCatCol -tsfFull $testFileOfCD
		    done
		    wait # End alpha values
		  done
		  wait # End lambda
		done
		wait # End KFactor
	  done
	  wait # End RankRecommender
	done # repetition
	wait

  for poiRecommender in AverageDistanceUserGEO
  do
    outputRecfiles=""
    outputRecfiles2=""
    for city in $cities
    do
      if [ $city != "complete_cities" ] && [ $city != "complete" ] ; then
		 # Average distance recommender with no frequency
        outputRecfile=$recommendationFolder/"$recPrefix"_$complete_cities"_""$city"_Fold"$fold"_ranksysPOI_"$poiRecommender".txt
        outputRecfiles+=$outputRecfile","

		 # Average distance recommender with frequency
	    outputRecfile2=$recommendationFolder/"$recPrefix"_$complete_cities"_""$city"_Fold"$fold"_ranksysPOI_"$poiRecommender"_FREQUENCY.txt
        outputRecfiles2+=$outputRecfile2","

      fi
    done
    outputRecfiles=${outputRecfiles::-1}
    outputRecfiles2=${outputRecfiles2::-1}
: '
    $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $concatenationsTest -cIndex true -rr $poiRecommender -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfiles -ff $concatenationsCandidates -matchCat $concatenationsMatch -fic $featureItemCol -ffc $featureCatCol -coordFile $coordFile -tsfFull $testFileOfCD
'
    $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfileSUM -tsf $concatenationsTest -cIndex true -rr $poiRecommender -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfiles2 -ff $concatenationsCandidates -matchCat $concatenationsMatch -fic $featureItemCol -ffc $featureCatCol -coordFile $coordFile -tsfFull $testFileOfCD -scoreFreq FREQUENCY

  done
  wait


  # KDE estimator recommender
  for poiRecommender in KDEstimatorRecommender
  do
    outputRecfiles=""
    outputRecfiles2=""
    for city in $cities
    do
      if [ $city != "complete_cities" ] && [ $city != "complete" ] ; then
        outputRecfile=$recommendationFolder/"$recPrefix"_"$complete_cities""_""$city"_Fold"$fold"_ranksys_"$poiRecommender".txt
        outputRecfiles+=$outputRecfile","

        outputRecfile2=$recommendationFolder/"$recPrefix"_"$complete_cities""_""$city"_Fold"$fold"_ranksys_"$poiRecommender"_FREQUENCY.txt
        outputRecfiles2+=$outputRecfile2","

      fi
    done
: '
    outputRecfiles=${outputRecfiles::-1}
    $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $concatenationsTest -cIndex false -rr $poiRecommender -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfiles -ff $concatenationsCandidates -matchCat $concatenationsMatch -fic $featureItemCol -ffc $featureCatCol -coordFile $coordFile -tsfFull $testFileOfCD
'

    outputRecfiles2=${outputRecfiles2::-1}
    $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfileWithRep -tsf $concatenationsTest -cIndex false -rr $poiRecommender -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfiles2 -ff $concatenationsCandidates -matchCat $concatenationsMatch -fic $featureItemCol -ffc $featureCatCol -coordFile $coordFile -tsfFull $testFileOfCD

done
wait

	  # RankGeoFM
      for repetition in 1 #2 3 4 5
    	do
        for c in $cs_RANKGEOFM
        do
          for alphaF in $alphas_RANKGEOFM
          do
            for epsilon in $epsilons_RANKGEOFM
            do
              for kFactorizer in $kfactors_RANKGEOFM
              do
                for kNeighbour in $ns_RANKGEOFM
                do
                  for numIter in $iters_RANKGEOFM
                  do

                    for decay in $decays_RANKGEOFM
                    do

                      for isboldDriver in $isboldDriver_RANKGEOFM
                      do

                        for learnRate in $learnRates_RANKGEOFM
                        do

                          for maxRate in $maxRates_RANKGEOFM
                          do


                            outputRecfiles=""
                            for city in $cities
                            do
                              if [ $city != "complete_cities" ] && [ $city != "complete" ] ; then
                                outputRecfile=$recommendationFolder/"$recPrefix"_"$complete_cities"_"$city"_Fold"$fold"_ranksysPOI_"RankGeoFMRecommenderNotDense""kFac"$kFactorizer"kNgh"$kNeighbour"dec"$decay"boldDriv"$isboldDriver"Iter"$numIter"lRate"$learnRate"mRate"$maxRate"alpha"$alphaF"c"$c"eps"$epsilon""_FREQUENCY_RESET_"Rep$repetition".txt
                                outputRecfiles+=$outputRecfile","
                              fi
                            done
                            outputRecfiles=${outputRecfiles::-1}
                            $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfileSUM -tsf $concatenationsTest -cIndex false -rr "RankGeoFMRecommenderNotDense" -ff $concatenationsCandidates -matchCat $concatenationsMatch -fic $featureItemCol -ffc $featureCatCol -coordFile $coordFile -tsfFull $testFileOfCD -n $kNeighbour -nIFactorizer $numIter -kFactorizer $kFactorizer -aFactorizer $alphaF -epsilon $epsilon -c $c -svdDecay $decay -svdIsboldDriver $isboldDriver -svdLearnRate $learnRate -svdMaxLearnRate $maxRate -nI $itemsRecommended -orf $outputRecfiles

                          done # ENd max learn
                          wait

                        done # Learn rate
                        wait
                      done # End bold
                      wait

                    done # End decay
                    wait
                  done # End iter
                  wait

                done # ENd neigh
                wait

              done # End kFactors
              wait
            done # Epsilon
            wait
          done # Alpha
          wait
        done # End c
        wait

	done # Repetition
      wait

	#Our GeoBPRMF from ranksys
	for repetition in 1 #2 3 4 5
	do
	  for factor in 100 #50 100
	  do
		for bias_reg in 1
		do
		  for regU in 0.001 #Regularization for items and users is the same
		  do
		    for maxDist in 1 #4
		    do

		      outputRecfiles=""
		      for city in $cities
		      do
		        if [ $city != "complete_cities" ] && [ $city != "complete" ] ; then
		          outputRecfile=$recommendationFolder/"$recPrefix"_$complete_cities"_""$city"_Fold"$fold"_RankSys_GeoBPRMF_nFact"$factor"_nIter"$BPRNumIter"_LearnR"$BPRLearnRate"_BiasR"$bias_reg"_RegU"$regU"_RegI"$regU"_MaxDist"$maxDist"_Rep"$repetition".txt
		          outputRecfiles+=$outputRecfile","
		        fi
		      done # End Cities
		      outputRecfiles=${outputRecfiles::-1}
		      $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $concatenationsTest -cIndex false -rr GeoBPRMF -nI $itemsRecommended -n 20 -orf $outputRecfiles -ff $concatenationsCandidates -matchCat $concatenationsMatch -fic $featureItemCol -ffc $featureCatCol -kFactorizer $factor -nIFactorizer $BPRNumIter -svdLearnRate $BPRLearnRate -svdRegBias $bias_reg -svdRegUser $regU -svdRegItem $regU -coordFile $coordFile -maxDist $maxDist -tsfFull $testFileOfCD
		    done
		    wait
		  done # Reg U
		  wait
		done # Bias reg
		wait
	  done # Factors
	  wait
	done # Repetition
	wait








  #PopGeoNN: popularity, knn and minimum distance combined
  for poiRecommender in PopGeoNN
  do
    for UBsimilarity in SetJaccardUserSimilarity
    do
      for neighbours in 100
      do
        outputRecfiles=""
        for city in $cities
        do
          if [ $city != "complete_cities" ] && [ $city != "complete" ] ; then
            outputRecfile=$recommendationFolder/"$recPrefix"_$complete_cities"_""$city"_Fold"$fold"_ranksysPOI_"$poiRecommender"_UBSim_"$UBsimilarity"_k$neighbours.txt
            outputRecfiles+=$outputRecfile","
          fi
        done # End Cities
        outputRecfiles=${outputRecfiles::-1}
        $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $concatenationsTest -cIndex true -rr $poiRecommender -rs $UBsimilarity -nI $itemsRecommended -n $neighbours -orf $outputRecfiles -ff $concatenationsCandidates -matchCat $concatenationsMatch -fic $featureItemCol -ffc $featureCatCol -coordFile $coordFile -tsfFull $testFileOfCD

      done # End Neighs
      wait
    done # End Similarity
    wait
  done # End Recommender
  wait

: '
	UBsimilarity=SetJaccardUserSimilarity
	kNeighbour=100
	alpha=0.5
	modes="ORIGINAL EXPERIMENTAL_SURVEY"

	for mode in $modes
	do
	  outputRecfiles=""
	  for city in $cities
	  do
		if [ $city != "complete_cities" ] && [ $city != "complete" ] ; then
		  outputRecfile=$recommendationFolder/"$recPrefix"_"$complete_cities"_"$city"_Fold"$fold"_ranksysPOI_"GeoKNNCa"_"$mode"_"$UBsimilarity"_"$kNeighbour"_alpha"$alpha"_FREQUENCY_Catlvl3.txt
		  outputRecfiles+=$outputRecfile","
		fi
	  done
	  outputRecfiles=${outputRecfiles::-1}
	  $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $concatenationsTest -cIndex false -rr "GeoKNNca" -coordFile $coordFile -n $kNeighbour -rs $UBsimilarity -aFactorizer $alpha -mode $mode -nI $itemsRecommended -orf $outputRecfiles -tsfFull $testFileOfCD -ff $concatenationsCandidates -matchCat $concatenationsMatch -fic $featureItemCol -ffc $featureCatCol
	done
	wait
'
	#FMFMGM recommender
    for repetition in 1 #2 3 4 5
    do
      for alpha_FMFMGM in $alphas_FMFMGM
      do
        for theta_FMFMGM in $thetas_FMFMGM
        do
          for distance_FMFMGM in $distances_FMFMGM #
          do
            for iter_FMFMGM in $iters_FMFMGM
            do
              for kfactor_FMFMGM in $kfactors_FMFMGM
              do
                for alpha2_FMFMGM in $alphas2_FMFMGM
                do
                  for beta_FMFMGM in $betas_FMFMGM
                  do
                    for learningRate_FMFMGM in $learningRates_FMFMGM
                    do
                      for sigmoid_FMFMGM in $sigmoids_FMFMGM
                      do

                        outputRecfiles=""
                        for city in $cities
                        do
                          if [ $city != "complete_cities" ] && [ $city != "complete" ] ; then
                            outputRecfile=$recommendationFolder/"$recPrefix"_"$complete_cities"_"$city"_Fold"$fold"_ranksysPOI_"FMFMGM"_alpha"$alpha_FMFMGM"_theta"$theta_FMFMGM"_dist"$distance_FMFMGM"_iter"$iter_FMFMGM"_factor"$kfactor_FMFMGM"_alpha2"$alpha2_FMFMGM"_beta"$beta_FMFMGM"_lRate"$learningRate_FMFMGM"_sigmoid"$sigmoid_FMFMGM"_FREQUENCY.txt
                            outputRecfiles+=$outputRecfile","
                          fi
                        done
                        outputRecfiles=${outputRecfiles::-1}
                        $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfileSUM -tsf $concatenationsTest -cIndex false -rr "FMFMGM" -coordFile $coordFile -aFactorizer $alpha_FMFMGM -thetaFactorizer $theta_FMFMGM -maxDist $distance_FMFMGM -nIFactorizer $iter_FMFMGM -kFactorizer $kfactor_FMFMGM -aFactorizer2 $alpha2_FMFMGM -svdBeta $beta_FMFMGM -svdLearnRate $learningRate_FMFMGM -useSigmoid $sigmoid_FMFMGM -n 20 -nI $itemsRecommended -orf $outputRecfiles -tsfFull $testFileOfCD -ff $concatenationsCandidates -matchCat $concatenationsMatch -fic $featureItemCol -ffc $featureCatCol


                      done # sigmoid
                      wait
                    done # learningRate
                    wait
                  done # beta
                  wait
                 done # alphas2
                 wait
                done # kFactor
                wait
              done # iters
              wait
            done # distance
            wait
          done # Theta
          wait
        done # alphas
        wait
      done # Repetition
      wait


    

done # End Folds
wait


: '
  Evaluation part
'

for fold in 0 #1
do

  recommendedFolderReal=$PathOfSplits/Fold$fold/$complete_cities/$recommendedFolder
  trainfile=$PathOfSplits/Fold$fold/$complete_cities/"split_""$complete_cities""__trainingAggr""$aggregateStrategy""$fold".dat
  resultsFolder=$PathOfSplits/Fold$fold/$complete_cities/ResultRankSysFolder




  mkdir -p $resultsFolder
  for city in $cities
  do
    if [ $city != "complete_cities" ] && [ $city != "complete" ] ; then
      testfile=$PathOfSplits/Fold$fold/$city/"split_""$city""__test""$fold".dat
      trainfileSingle=$PathOfSplits/Fold$fold/$city/"split_""$city""__trainingAggr""$aggregateStrategy""$fold".dat

      find $recommendedFolderReal/ -name "$recPrefix"*"$city"* | while read recFile; do
        recFileName=$(basename "$recFile" .txt) # Extension removed
        if [[ $recFileName == *"_"* ]]; then
          for evthreshold in 1 # We work with implicit data
          do
            outputResultfile=$resultsFolder/"$nonaccresultsPrefix"_EvTh"$evthreshold"_"$recFileName"_CutOffs"$cutoffs".txt
            if [ ! -f "$outputResultfile" ]; then
              $javaCommand $jvmMemory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $trainfileSingle -tsf $testfile -rf $recFile -ff $realFeatureFile -thr $evthreshold -rc $cutoffs -orf $outputResultfile -onlyAcc false

			 # Only for Tourists
            user_feature=L
            outputResultfile=$resultsFolder/"$nonaccresultsPrefix"_EvTh"$evthreshold"_"$recFileName"_Cutoff5-10"AggrTrNewPopEvLocals.txt"
            if [ ! -f "$outputResultfile" ]; then
              echo 'train file is ' $train_file
              $javaCommand $jvmMemory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $trainfileSingle -tsf $testfile -rf $recFile -ff $realFeatureFile -thr $evthreshold -rc $cutoffs -orf $outputResultfile -onlyAcc false -lcsEv true -compUF true -uff $PathOfSplits/Fold"$fold"/"$city"/"RepTrainMaxDifTourist"$maxDiffTime"MinDifBot"$minDiffTime"MinNumPrefBot"$minClosePrefBot"LocTouBot"UserAttributeFile.txt -ufs $user_feature
            fi

            # Only for Tourists
            user_feature=T
            outputResultfile=$resultsFolder/"$nonaccresultsPrefix"_EvTh"$evthreshold"_"$recFileName"_Cutoff5-10"AggrTrNewPopEvTourists.txt"
            if [ ! -f "$outputResultfile" ]; then
              echo 'train file is ' $train_file
              $javaCommand $jvmMemory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $trainfileSingle -tsf $testfile -rf $recFile -ff $realFeatureFile -thr $evthreshold -rc $cutoffs -orf $outputResultfile -onlyAcc false -lcsEv true -compUF true -uff $PathOfSplits/Fold"$fold"/"$city"/"RepTrainMaxDifTourist"$maxDiffTime"MinDifBot"$minDiffTime"MinNumPrefBot"$minClosePrefBot"LocTouBot"UserAttributeFile.txt -ufs $user_feature
            fi


            fi
          done # End evThreshold
          wait
        fi
      done # End find
      wait
    fi
  done # End cities
  wait
done # End folds
wait

#!/bin/bash

: '
  Script for normal ranksys recommenders with single domain.

  This should be the second scritpt to execute
  Script used in paper "On the effects of aggregation strategies in different groups of users in venue recommendation"

'


# Necessary to execute with ./


# Global variables for JVM
JAR=target/CrossDomainPOIRecommendation-0.1.1-SNAPSHOT.jar
jvmMemory=-Xmx14G
ttpathfiles=TrainTest
similaritiesFolder=Sim
javaCommand=java

recPrefix=rec
simPrefix=sim

# Nomenclature for datasets
FoursqrName=Foursqr


# extension of files
extensionMap=_Mapping.txt
extensionCoords=_Coords.txt
extensionFeature=_Features.txt
extensionAggregated=_Aggregated.txt

# routes of original datasets
FoursqrCheckingsOriginal=$ttpathfiles/dataset_TIST2015_Checkins.txt
FoursqrCitiesOriginal=$ttpathfiles/dataset_TIST2015_Cities.txt
FoursqrPOISOriginal=$ttpathfiles/dataset_TIST2015_POIs.txt
simFolder=Sim

# cities selected to perform the experiments
cities="MX_MexicoCity BR_SaoPaulo RU_Moscow MY_KualaLumpur CL_Santiago JP_Tokyo ID_Jakarta TR_Istanbul"
PathOfSplits=$ttpathfiles/CitiesSplit
aggregateStrategy=AVERAGE
aggregateStrategy2=SUM
aggregateStrategyTime=LAST
trainingStrats="REPETITIONS Last"

recommendedFolder=RecommendationFolder

#Parameters for neighborhood models
allneighbours="5 10 20 30 40 50 60 70 80 90 100"
itemsRecommended=100


# Generic Variables for recommendation
itemsRecommended=100
featureFile=$ttpathfiles/NewItemIdsCountryCityJoin.txt
featureItemCol=0
featureCatCol=1
realFeatureFile=$ttpathfiles/POIS_Features.txt

coordFile=$ttpathfiles/POIS_Coords.txt

# Parameters for HKV (Matrix factorization) algorithm
allKFactorizerRankSys="10 50 100"
allLambdaFactorizerRankSys="0.1 1 10"
allAlphaFactorizerRankSys="0.1 1 10"

complete_cities=complete_cities

cutoffs="5,10"

# Parameters for BPRMF algorithm
mymedialitePath=MyMediaLite-3.11/bin
BPRFactors=$allKFactorizerRankSys
BPRBiasReg="0 0.5 1"
BPRLearnRate=0.05
BPRNumIter=50
BPRRegU="0.0025 0.001 0.005 0.01 0.1"
BPRRegJ="0.00025 0.0001 0.0005 0.001 0.01"
extensionMyMediaLite=MyMedLt

# Parameters for RankGeoFm
cs_RANKGEOFM="1"
alphas_RANKGEOFM="0.1 0.2"
epsilons_RANKGEOFM="0.3"
kfactors_RANKGEOFM=$allKFactorizerRankSys
ns_RANKGEOFM="10 50 100 200"
iters_RANKGEOFM="120"
decays_RANKGEOFM="1"
isboldDriver_RANKGEOFM="true"
learnRates_RANKGEOFM="0.001"
maxRates_RANKGEOFM="0.001"


# Parameters for FMFMGM
alphas_FMFMGM="0.2 0.4"
thetas_FMFMGM="0.02 0.1"
distances_FMFMGM="15"
iters_FMFMGM="30"
kfactors_FMFMGM="50 100"
alphas2_FMFMGM="20 40"
betas_FMFMGM="0.2"
learningRates_FMFMGM="0.0001"
sigmoids_FMFMGM="false"

# Variables for generating the tourist-local-bot file from training set
maxDiffTime=1814400
minDiffTime=60
minClosePrefBot=3


# For each city (single domain, each city individually)
for fold in 0 #1
do
  # For each fold
  for city in $cities
  do
    if [ $city != "complete_cities" ] && [ $city != "complete" ] ; then

      trainfile=$PathOfSplits/Fold$fold/$city/"split_""$city""__trainingAggr""$aggregateStrategy""$fold".dat
      trainfileSUM=$PathOfSplits/Fold$fold/$city/"split_""$city""__trainingAggr""$aggregateStrategy2""$fold".dat
      testfile=$PathOfSplits/Fold$fold/$city/"split_""$city""__test""$fold".dat

      recommendationFolder=$PathOfSplits/Fold$fold/$city/RecommendationFolder

      # training with repetitions
      trainfileWithRep=$PathOfSplits/Fold$fold/$city/"split_""$city""__training""$fold".dat
      testFileOfCD=$testfile

	  resultUserLocalTouristBotSessions=$PathOfSplits/Fold$fold/$city/"RepTrainMaxDifTourist"$maxDiffTime"MinDifBot"$minDiffTime"MinNumPrefBot"$minClosePrefBot"LocTouBot"
      $javaCommand $jvmMemory -jar $JAR -o generateTouristLocalBotFile -trf $trainfileWithRep -maxDiffTime $maxDiffTime -minDiffTime $minDiffTime -minClosePrefBot $minClosePrefBot -orf $resultUserLocalTouristBotSessions".txt"

      #Now, clean tourist, local bot file
      cut -f1,2 $resultUserLocalTouristBotSessions".txt" > $PathOfSplits/Fold$fold/$city/tempFile.txt
      sed '1d' "$PathOfSplits/Fold$fold/$city/tempFile.txt" > $resultUserLocalTouristBotSessions"UserAttributeFile.txt"
      rm "$PathOfSplits/Fold$fold/$city/tempFile.txt"
		


      mkdir -p $recommendationFolder
: '
      # Temporal Test Order
      outputRecfile=$recommendationFolder/"$recPrefix"_"$city""$fold"_ranksys_"SkylineTestOrder".txt
      $javaCommand $jvmMemory -jar $JAR -o skylineRecommenders -trf $trainfile -tsf $testfile -cIndex true -rr "SkylineTestOrder" -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol

      # Temporal Test Order Reverse
      outputRecfile=$recommendationFolder/"$recPrefix"_"$city""$fold"_ranksys_"SkylineTestOrderReverse".txt
      $javaCommand $jvmMemory -jar $JAR -o skylineRecommenders -trf $trainfile -tsf $testfile -cIndex true -rr "SkylineTestOrderReverse" -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol
'

      # RankSys: popularity and Random recommender
      for ranksysRecommender in PopularityRecommender RandomRecommender
      do
        outputRecfile=$recommendationFolder/"$recPrefix"_"$city""$fold"_ranksys_"$ranksysRecommender".txt
        $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex true -rr "$ranksysRecommender" -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol
      done # End ranksys recommenders
      wait

      for neighbours in $allneighbours
      do

        # ranksys with similarities UserBased
        for UBsimilarity in SetJaccardUserSimilarity SetCosineUserSimilarity
        do
          outputRecfile=$recommendationFolder/"$recPrefix"_"$city""$fold"_ranksys_UB_"$UBsimilarity"_k"$neighbours".txt
          $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex false -rr UserNeighborhoodRecommender -rs $UBsimilarity -nI $itemsRecommended -n $neighbours -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol

        done # UB sim
        wait

        # Ranksys with similarities ItemBased
        for IBsimilarity in SetJaccardItemSimilarity SetCosineItemSimilarity
        do
          outputRecfile=$recommendationFolder/"$recPrefix"_"$city""$fold"_ranksys_IB_"$IBsimilarity"_k"$neighbours".txt
          $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex false -rr ItemNeighborhoodRecommender -rs $IBsimilarity -nI $itemsRecommended -n $neighbours -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol

        done
        wait # End IB Sim


      done # End Neighbours
      wait

	  # BPRMF recommendation 
      for repetition in 1 #2 3 4 5
      do
        for factor in $BPRFactors
        do
          for bias_reg in $BPRBiasReg
          do
            for regU in $BPRRegU #Regularization for items and users is the same
            do
              regJ=$(echo "$regU/10" | bc -l)

              outputRecfile=$recommendationFolder/"$recPrefix"_"$city""$fold"_"$extensionMyMediaLite"_BPRMF_nFact"$factor"_nIter"$BPRNumIter"_LearnR"$BPRLearnRate"_BiasR"$bias_reg"_RegU"$regU"_RegI"$regU"_RegJ"$regJ""Rep$repetition".txt
              if [ ! -f *"$outputRecfile"* ]; then
				  echo $outputRecfile
		          outputRecfile2=$outputRecfile"Aux".txt
		          echo "./$mymedialitePath/item_recommendation --training-file=$trainfile --recommender=BPRMF --prediction-file=$outputRecfile2 --predict-items-number=$itemsRecommended --recommender-options="num_factors=$factor bias_reg=$bias_reg reg_u=$regU reg_i=$regU reg_j=$regJ learn_rate=$BPRLearnRate UniformUserSampling=false WithReplacement=false num_iter=$BPRNumIter""
		          ./$mymedialitePath/item_recommendation --training-file=$trainfile --recommender=BPRMF --prediction-file=$outputRecfile2 --predict-items-number=$itemsRecommended --recommender-options="num_factors=$factor bias_reg=$bias_reg reg_u=$regU reg_i=$regU reg_j=$regJ learn_rate=$BPRLearnRate UniformUserSampling=false WithReplacement=false num_iter=$BPRNumIter"
		          $javaCommand $jvmMemory -jar $JAR -o ParseMyMediaLite -trf $outputRecfile2 $testfile $outputRecfile
		          rm $outputRecfile2
			  fi
            done # Reg U
            wait
          done # Bias reg
          wait
        done # Factors
        wait
      done # Repetition
      wait

	# HKV
    for repetition in 1 #2 3 4 5
    do
      for rankRecommenderNoSim in MFRecommenderHKV
      do
        for kFactor in $allKFactorizerRankSys
        do
          for lambdaValue in $allLambdaFactorizerRankSys
          do
            for alphaValue in $allAlphaFactorizerRankSys
            do
              # Neighbours is put to 20 because this recommender does not use it
              outputRecfile=$recommendationFolder/"$recPrefix"_"$city""$fold"_ranksys_"$rankRecommenderNoSim"_kFactor"$kFactor"_aFactorizer"$alphaValue"_lFactorizer"$lambdaValue"_Rep"$repetition".txt
              $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex false -rr $rankRecommenderNoSim -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfile -kFactorizer $kFactor -aFactorizer $alphaValue -lFactorizer $lambdaValue -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol

            done
            wait # End alpha values
          done
          wait # End lambda
        done
        wait # End KFactor
      done
      wait # End RankRecommender
	done #repetitions
	wait

: '
	  # Average distance recommender with no frequency
      for poiRecommender in AverageDistanceUserGEO
      do
        outputRecfile=$recommendationFolder/"$recPrefix"_"$city""$fold"_ranksysPOI_"$poiRecommender"SIMPLE.txt
        $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex false -rr $poiRecommender -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol -coordFile $coordFile

      done # End poiRecommender
      wait
'
      # Average distance recommender with frequency
      for poiRecommender in AverageDistanceUserGEO
      do
        outputRecfile=$recommendationFolder/"$recPrefix"_"$city""$fold"_ranksysPOI_"$poiRecommender"FREQUENCY.txt
        $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfileSUM -tsf $testfile -cIndex false -rr $poiRecommender -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol -coordFile $coordFile -scoreFreq FREQUENCY

      done
      wait

	  # KDE estimator recommender
      for poiRecommender in KDEstimatorRecommender
      do
		: '
		# KDE estimator recommender with no frequency
        outputRecfile=$recommendationFolder/"$recPrefix"_"$city""$fold"_ranksysPOI_"$poiRecommender".txt
        $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex false -rr $poiRecommender -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol -coordFile $coordFile
		'
		# KDE estimator recommender with frequency
		outputRecfile=$recommendationFolder/"$recPrefix"_"$city""$fold"_ranksysPOI_"$poiRecommender"_FREQUENCY.txt
        $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfileWithRep -tsf $testfile -cIndex false -rr $poiRecommender -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol -coordFile $coordFile

      done # End poiRecommender
      wait


	# RankGeoFM
	for repetition in 1 #2 3 4 5
    do
		for c in $cs_RANKGEOFM
		do
		  for alphaF in $alphas_RANKGEOFM
		  do
			for epsilon in $epsilons_RANKGEOFM
			do
			  for kFactorizer in $kfactors_RANKGEOFM
			  do
				for kNeighbour in $ns_RANKGEOFM
				do
				  for numIter in $iters_RANKGEOFM
				  do

				    for decay in $decays_RANKGEOFM
				    do

				      for isboldDriver in $isboldDriver_RANKGEOFM
				      do

				        for learnRate in $learnRates_RANKGEOFM
				        do
				          for maxRate in $maxRates_RANKGEOFM
				          do

				            outputRecfile=$recommendationFolder/"$recPrefix"_"$city""$fold"_ranksysPOI_"RankGeoFMRecommender""kFac"$kFactorizer"kNgh"$kNeighbour"dec"$decay"boldDriv"$isboldDriver"Iter"$numIter"lRate"$learnRate"mRate"$maxRate"alpha"$alphaF"c"$c"eps"$epsilon""_FREQUENCY_Rep"$repetition".txt
				            $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfileSUM -tsf $testfile -cIndex false -rr "RankGeoFMRecommender" -coordFile $coordFile -n $kNeighbour -nIFactorizer $numIter -kFactorizer $kFactorizer -aFactorizer $alphaF -epsilon $epsilon -c $c -svdDecay $decay -svdIsboldDriver $isboldDriver -svdLearnRate $learnRate -svdMaxLearnRate $maxRate -nI $itemsRecommended -orf $outputRecfile

				          done # ENd max learn
				          wait

				        done # Learn rate
				        wait
				      done # End bold
				      wait

				    done # End decay
				    wait
				  done # End iter
				  wait

				done # ENd neigh
				wait

			  done # End kFactors
			  wait
			done # Epsilon
			wait
		  done # Alpha
		  wait
		done # End c
		wait
	done # Repetition
    wait



	#Our GeoBPRMF from ranksys
    for repetition in 1 #2 3 4 5
    do
      for factor in $BPRFactors
      do
        for bias_reg in $BPRBiasReg
        do
          for regU in $BPRRegU #Regularization for items and users is the same
          do
            for maxDist in $geoBPRmaxDists
            do
              outputRecfile=$recommendationFolder/"$recPrefix"_"$city""$fold"_RankSys_GeoBPRMF_nFact"$factor"_nIter"$BPRNumIter"_LearnR"$BPRLearnRate"_BiasR"$bias_reg"_RegU"$regU"_RegI"$regU"_MaxDist"$maxDist"_Rep"$repetition".txt
              $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex false -rr GeoBPRMF -nI $itemsRecommended -n 20 -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol -kFactorizer $factor -nIFactorizer $BPRNumIter -svdLearnRate $BPRLearnRate -svdRegBias $bias_reg -svdRegUser $regU -svdRegItem $regU -coordFile $coordFile -maxDist $maxDist
            done
            wait
          done # Reg U
          wait
        done # Bias reg
        wait
      done # Factors
      wait
    done # Repetition
    wait

	#PopGeoNN: popularity, knn and minimum distance combined
      for poiRecommender in PopGeoNN
      do
        for UBsimilarity in SetJaccardUserSimilarity
        do
          for neighbours in 100
          do
            outputRecfile=$recommendationFolder/"$recPrefix"_"$city""$fold"_ranksysPOI_"$poiRecommender"_UBSim_"$UBsimilarity"_k"$neighbours".txt
            $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex true -rr $poiRecommender -rs $UBsimilarity -nI $itemsRecommended -n $neighbours -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol -coordFile $coordFile

          done # End neighbours
          wait
        done # End UB similarity
        wait
      done # End poiRecommender
      wait


	#GeoCFCA (based on GeoSoCa)
    UBsimilarity=SetJaccardUserSimilarity
    kNeighbour=100
    alpha=0.5
    modes="ORIGINAL EXPERIMENTAL_SURVEY"

    for mode in $modes
    do
      outputRecfile=$recommendationFolder/"$recPrefix"_"$city""$fold"_ranksysPOI_"GeoKNNCa"_"$mode"_"$UBsimilarity"_"$kNeighbour"_alpha"$alpha"_FREQUENCY_Catlvl3.txt
      $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfileSUM -tsf $testfile -cIndex false -rr "GeoKNNca" -coordFile $coordFile -n $kNeighbour -rs $UBsimilarity -aFactorizer $alpha -ff2 $realFeatureFile -nI $itemsRecommended -orf $outputRecfile -mode $mode
    done
    wait



      # Finally the training recommender
      outputRecfile=$recommendationFolder/"$recPrefix"_"$city""$fold"_ranksys_"TrainRecommender".txt
      $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex false -rr "TrainRecommender" -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol -coordFile $coordFile




    #FMFMGM recommender
    for repetition in 1 #2 3 4 5
    do
      for alpha_FMFMGM in $alphas_FMFMGM
      do
        for theta_FMFMGM in $thetas_FMFMGM
        do
          for distance_FMFMGM in $distances_FMFMGM #
          do
            for iter_FMFMGM in $iters_FMFMGM
            do
              for kfactor_FMFMGM in $kfactors_FMFMGM
              do
                for alpha2_FMFMGM in $alphas2_FMFMGM
                do
                  for beta_FMFMGM in $betas_FMFMGM
                  do
                    for learningRate_FMFMGM in $learningRates_FMFMGM
                    do
                      for sigmoid_FMFMGM in $sigmoids_FMFMGM
                      do

                        outputRecfile=$recommendationFolder/"$recPrefix"_"$city""$fold"_ranksysPOI_"FMFMGM"_alpha"$alpha_FMFMGM"_theta"$theta_FMFMGM"_dist"$distance_FMFMGM"_iter"$iter_FMFMGM"_factor"$kfactor_FMFMGM"_alpha2"$alpha2_FMFMGM"_beta"$beta_FMFMGM"_lRate"$learningRate_FMFMGM"_sigmoid"$sigmoid_FMFMGM"_FREQUENCY_Rep"$repetition".txt
                        $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfileSUM -tsf $testfile -cIndex false -rr "FMFMGM" -coordFile $coordFile -aFactorizer $alpha_FMFMGM -thetaFactorizer $theta_FMFMGM -maxDist $distance_FMFMGM -nIFactorizer $iter_FMFMGM -kFactorizer $kfactor_FMFMGM -aFactorizer2 $alpha2_FMFMGM -svdBeta $beta_FMFMGM -svdLearnRate $learningRate_FMFMGM -useSigmoid $sigmoid_FMFMGM -n 20 -nI $itemsRecommended -orf $outputRecfile
                      done # sigmoid
                      wait
                    done # learningRate
                    wait
                  done # beta
                  wait
                 done # alphas2
                 wait
                done # kFactor
                wait
              done # iters
              wait
            done # distance
            wait
          done # Theta
          wait
        done # alphas
        wait
      done # Repetition
      wait
	
	




    fi
  done # End cities
  wait
done # End Folds
wait


: '
Evaluation part
'

nonaccresultsPrefix=naeval
for city in $cities
do
  if [ $city != "complete_cities" ] && [ $city != "complete" ] ; then

    for fold in 0 #1
    do

      trainfile=$PathOfSplits/Fold$fold/$city/"split_""$city""__trainingAggr""$aggregateStrategy""$fold".dat
      testfile=$PathOfSplits/Fold$fold/$city/"split_""$city""__test""$fold".dat

      resultsFolder=$PathOfSplits/Fold$fold/$city/ResultRankSysFolder

      mkdir -p $resultsFolder

      # Find all recommenders and compute the evaluation

      recommendedFolderReal=$PathOfSplits/Fold$fold/$city/$recommendedFolder

      find $recommendedFolderReal/ -name "$recPrefix"* | while read recFile; do
        recFileName=$(basename "$recFile" .txt) #extension removed


        if [[ $recFileName != *TestWithRatingsTrain* ]]; then
          for evthreshold in 1
          do
            outputResultfile=$resultsFolder/"$nonaccresultsPrefix"_EvTh"$evthreshold"_"$recFileName"_LCSEVCutOffs"$cutoffs".txt
            if [ ! -f "$outputResultfile" ]; then
              echo 'train file is ' $trainfile
              $javaCommand $jvmMemory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $trainfile -tsf $testfile -rf $recFile -ff $realFeatureFile -thr $evthreshold -rc $cutoffs -orf $outputResultfile -lcsEv true -onlyAcc false
            fi
		
			# Now evaluation by user feature. Uncomment the ones we want to evaluate
			# Only for Locals
		: '
            user_feature=L
            outputResultfile=$resultsFolder/"$nonaccresultsPrefix"_EvTh"$evthreshold"_"$recFileName"_Cutoff5-10"AggrTrNewPopEvLocals.txt"
            if [ ! -f "$outputResultfile" ]; then
              echo 'train file is ' $train_file
              $javaCommand $jvmMemory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $trainfile -tsf $testfile -rf $recFile -ff $realFeatureFile -thr $evthreshold -rc $cutoffs -orf $outputResultfile -onlyAcc false -lcsEv true -compUF true -uff $PathOfSplits/Fold"$fold"/"$city"/"RepTrainMaxDifTourist"$maxDiffTime"MinDifBot"$minDiffTime"MinNumPrefBot"$minClosePrefBot"LocTouBot"UserAttributeFile.txt -ufs $user_feature
            fi

            # Only for Tourists
            user_feature=T
            outputResultfile=$resultsFolder/"$nonaccresultsPrefix"_EvTh"$evthreshold"_"$recFileName"_Cutoff5-10"AggrTrNewPopEvTourists.txt"
            if [ ! -f "$outputResultfile" ]; then
              echo 'train file is ' $train_file
              $javaCommand $jvmMemory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $trainfile -tsf $testfile -rf $recFile -ff $realFeatureFile -thr $evthreshold -rc $cutoffs -orf $outputResultfile -onlyAcc false -lcsEv true -compUF true -uff $PathOfSplits/Fold"$fold"/"$city"/"RepTrainMaxDifTourist"$maxDiffTime"MinDifBot"$minDiffTime"MinNumPrefBot"$minClosePrefBot"LocTouBot"UserAttributeFile.txt -ufs $user_feature
            fi
		'



          done # End EvTh
          wait
        fi
      done # End Find
      wait
    done # End Folds
    wait
  fi
done # End Cities
wait

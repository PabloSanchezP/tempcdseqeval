#!/bin/bash
#Necessary to execute with ./

: '
  Script for generating the recommendations using the cross domain for the rest 
  of the cities in the same country with respect to the target one

  Script used in paper "On the effects of aggregation strategies in different groups of users in venue recommendation"

'

#Global variables for jvm
JAR=target/CrossDomainPOIRecommendation-0.1.1-SNAPSHOT.jar
jvmMemory=-Xmx14G
ttpathfiles=TrainTest
similaritiesFolder=Sim
javaCommand=java

recPrefix=rec

#Nomeclature for Datasets
FoursqrName=Foursqr


#Extension of Files
extensionMap=_Mapping.txt
extensionCoords=_Coords.txt
extensionFeature=_Features.txt
extensionAggregated=_Aggregated.txt

#Routes of Original Datasets
FoursqrCheckingsOriginal=$ttpathfiles/dataset_TIST2015_Checkins.txt
FoursqrCitiesOriginal=$ttpathfiles/dataset_TIST2015_Cities.txt
FoursqrPOISOriginal=$ttpathfiles/dataset_TIST2015_POIs.txt

#Cities (obtain names from TrainTest/Cities) This variables should be the same as the previous script
cities="MX_MexicoCity BR_SaoPaulo RU_Moscow MY_KualaLumpur CL_Santiago JP_Tokyo ID_Jakarta TR_Istanbul complete_cities"
PathOfSplits=$ttpathfiles/CitiesSplit
aggregateStrategy=AVERAGE

recommendedFolder=RecommendationFolder
allneighbours="5 10 20 30 40 50 60 70 80 90 100"
itemsRecommended=100


#Variables for recommendation
itemsRecommended=100
featureFile=$ttpathfiles/NewItemIdsCountryCityJoin.txt
featureItemCol=0
featureCatCol=1
realFeatureFile=$ttpathfiles/POIS_Features.txt

coordFile=$ttpathfiles/POIS_Coords.txt

allKFactorizerRankSys="10 50 100"
allLambdaFactorizerRankSys="0.1 1 10"
allAlphaFactorizerRankSys="0.1 1 10"

complete_cities=complete_cities
nonaccresultsPrefix=naeval


lstCountryCodes="MX CL JP" #"" #CL JP "BR MX ID MY RU TR"

mymedialitePath=MyMediaLite-3.11/bin
BPRFactors=$allKFactorizerRankSys
BPRBiasReg="0 0.5 1"
BPRLearnRate=0.05
BPRNumIter=50
BPRRegU="0.0025 0.001 0.005 0.01 0.1"
BPRRegJ="0.00025 0.0001 0.0005 0.001 0.01"
extensionMyMediaLite=MyMedLt

# Variables for generating the tourist-local-bot file from training set
maxDiffTime=1814400
minDiffTime=60
minClosePrefBot=3

cutoffs="5,10"

: '
Recommendation part
'

for fold in 0 #1
do
  directory=$PathOfSplits/Fold$fold/complete_CountryCode
  for countryCode in $lstCountryCodes
  do
    for city in $cities
    do
      if [[ $city == "$countryCode"* ]] ; then
        echo "Working with country code $countryCode"
        echo "Working with city $city"

        trainfile=$directory/complete_"$countryCode"/"split_"complete_"$countryCode""__trainingAggr""$aggregateStrategy""$fold".dat
        trainfile2=$directory/complete_"$countryCode"/"split_"complete_"$countryCode""__trainingAggr"SUM"$fold".dat

        testfile=$PathOfSplits/Fold$fold/$city/"split_""$city""__test""$fold".dat
        trainFileSDAuxiliar=$PathOfSplits/Fold$fold/$city/"split_""$city""__training""$fold".dat
        fileCandidatesBPR=$PathOfSplits/Fold$fold/$city/"$city"CANDIDATES"$fold".dat
        cut -f2 $trainFileSDAuxiliar | sort | uniq > $fileCandidatesBPR


        #Statistics of Foursqr
        #Train with repetitions
        trainfileWithRep=$directory/complete_"$countryCode"/"split_"complete_"$countryCode""__training""$fold".dat
        testFileOfCD=$directory/complete_"$countryCode"/"split_"complete_"$countryCode""__test""$fold".dat
        resultStatistics=$directory/complete_"$countryCode"/"split_"complete_"$countryCode""StatisticsFoursqr""Fold""$fold".dat

        #Compute the statistics

        # $javaCommand $jvmMemory -jar $JAR -o StatisticsFoursqr -trf $trainfileWithRep $trainfile $testFileOfCD $featureFile $resultStatistics



        recommendationFolder=$directory/complete_"$countryCode"/RecommendationFolder
        mkdir -p $recommendationFolder

        for neighbours in 40 80 90 70 100
        do

          #Ranksys with similarities UserBased
          for UBsimilarity in SetJaccardUserSimilarity SetCosineUserSimilarity
          do
            outputRecfile=$recommendationFolder/"$recPrefix"_"complete_"$countryCode""_"$city"_Fold"$fold"_ranksys_UB_"$UBsimilarity"_k"$neighbours".txt
            $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex false -rr UserNeighborhoodRecommender -rs $UBsimilarity -nI $itemsRecommended -n $neighbours -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol


          done
          wait #UB sim

		  # Ranksys with similarities ItemBased
          for IBsimilarity in SetJaccardItemSimilarity SetCosineItemSimilarity
          do
            outputRecfile=$recommendationFolder/"$recPrefix"_"complete_"$countryCode""_"$city"_Fold"$fold"_ranksys_IB_"$IBsimilarity"_k"$neighbours".txt
            $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex false -rr ItemNeighborhoodRecommender -rs $IBsimilarity -nI $itemsRecommended -n $neighbours -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol

          done
          wait #IB Sim

        done
        wait #neighbours

		# BPRMF recommendation 
	    for repetition in 1 #2 3 4 5
        do
          for factor in 50 100
          do
            for bias_reg in 0
            do
              for regU in 0.1 0.01 0.05 0.005 #Regularization for items and users is the same
              do
                regJ=$(echo "$regU/10" | bc -l)

                outputRecfile=$recommendationFolder/"$recPrefix"_"complete_"$countryCode"_""$city"_Fold"$fold"_"$extensionMyMediaLite"_BPRMF_nFact"$factor"_nIter"$BPRNumIter"_LearnR"$BPRLearnRate"_BiasR"$bias_reg"_RegU"$regU"_RegI"$regU"_RegJ"$regJ""Rep$repetition".txt
                if [ ! -f "$outputRecfile" ] ; then
                  echo $outputRecfile
                  outputRecfile2=$outputRecfile"Aux".txt
                  echo "./$mymedialitePath/item_recommendation --training-file=$trainfile --recommender=BPRMF --prediction-file=$outputRecfile2 --candidate-items=$fileCandidatesBPR --predict-items-number=$itemsRecommended --recommender-options="num_factors=$factor bias_reg=$bias_reg reg_u=$regU reg_i=$regU reg_j=$regJ learn_rate=$BPRLearnRate UniformUserSampling=false WithReplacement=false num_iter=$BPRNumIter""
                  ./$mymedialitePath/item_recommendation --training-file=$trainfile --recommender=BPRMF --prediction-file=$outputRecfile2 --candidate-items=$fileCandidatesBPR --predict-items-number=$itemsRecommended --recommender-options="num_factors=$factor bias_reg=$bias_reg reg_u=$regU reg_i=$regU reg_j=$regJ learn_rate=$BPRLearnRate UniformUserSampling=false WithReplacement=false num_iter=$BPRNumIter"
                  $javaCommand $jvmMemory -jar $JAR -o ParseMyMediaLite -trf $outputRecfile2 $testfile $outputRecfile
                  rm $outputRecfile2
                fi
              done # Reg U
              wait
            done # Bias reg
            wait
          done # Factors
          wait
        done # Repetition
        wait


	  # HKV
	  for repetition in 1 #2 3 4 5
        do
        for rankRecommenderNoSim in MFRecommenderHKV
        do
          for kFactor in 10 50
          do
            for lambdaValue in $allLambdaFactorizerRankSys
            do
              for alphaValue in $allAlphaFactorizerRankSys
              do
                #Neighbours is put to 20 because this recommender does not use it
                outputRecfile=$recommendationFolder/"$recPrefix"_"complete_"$countryCode""_"$city"_Fold"$fold"_ranksys_"$rankRecommenderNoSim"_kFactor"$kFactor"_aFactorizer"$alphaValue"_lFactorizer"$lambdaValue"_"Rep$repetition".txt
                $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex false -rr $rankRecommenderNoSim -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfile -kFactorizer $kFactor -aFactorizer $alphaValue -lFactorizer $lambdaValue -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol

              done
              wait #End alpha values
            done
            wait #End lambda
          done
          wait #End KFactor
        done
        wait #End RankRecommender
	  done # repetitions
	  wait




		: '
		# Average distance recommender with no frequency
        for poiRecommender in AverageDistanceUserGEO
        do
          outputRecfile=$recommendationFolder/"$recPrefix"_"complete_"$countryCode"_""$city"_Fold"$fold"_ranksysPOI_"$poiRecommender".txt
          $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex true -rr $poiRecommender -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol -coordFile $coordFile
        done
        wait
		'


        # Average distance recommender with frequency
        for poiRecommender in AverageDistanceUserGEO
        do
          outputRecfile=$recommendationFolder/"$recPrefix"_"complete_"$countryCode"_""$city"_Fold"$fold"_ranksysPOI_"$poiRecommender"FREQUENCY.txt
          $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile2 -tsf $testfile -cIndex true -rr $poiRecommender -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol -coordFile $coordFile -scoreFreq FREQUENCY

        done
        wait

		# KDE estimator recommender
        for poiRecommender in KDEstimatorRecommender
        do
: '
		  # KDE estimator recommender with no frequency
          outputRecfile=$recommendationFolder/"$recPrefix"_"complete_"$countryCode"_""$city"_Fold"$fold"_ranksysPOI_"$poiRecommender".txt
          $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex false -rr $poiRecommender -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol -coordFile $coordFile
'
		  # KDE estimator recommender with frequency
          outputRecfile=$recommendationFolder/"$recPrefix"_"complete_"$countryCode"_""$city"_Fold"$fold"_ranksysPOI_"$poiRecommender"_FREQUENCY.txt
          $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfileWithRep -tsf $testfile -cIndex false -rr $poiRecommender -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol -coordFile $coordFile

        done # End poiRecommender
        wait

	  # RankGeoFM
	  for repetition in 1 #2 3 4 5
      do
        for c in $cs_RANKGEOFM
        do
          for alphaF in $alphas_RANKGEOFM
          do
            for epsilon in $epsilons_RANKGEOFM
            do
              for kFactorizer in $kfactors_RANKGEOFM
              do
                for kNeighbour in $ns_RANKGEOFM
                do
                  for numIter in $iters_RANKGEOFM
                  do

                    for decay in $decays_RANKGEOFM
                    do

                      for isboldDriver in $isboldDriver_RANKGEOFM
                      do

                        for learnRate in $learnRates_RANKGEOFM
                        do

                          for maxRate in $maxRates_RANKGEOFM
                          do

                            outputRecfile=$recommendationFolder/"$recPrefix"_"complete"_"$countryCode"_"$city"_Fold"$fold"_ranksysPOI_"RankGeoFMRecommender""kFac"$kFactorizer"kNgh"$kNeighbour"dec"$decay"boldDriv"$isboldDriver"Iter"$numIter"lRate"$learnRate"mRate"$maxRate"alpha"$alphaF"c"$c"eps"$epsilon""_FREQUENCY_RESET_Rep"$repetition".txt
                            $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile2 -tsf $testfile -cIndex false -rr "RankGeoFMRecommender" -n $kNeighbour -nIFactorizer $numIter -kFactorizer $kFactorizer -aFactorizer $alphaF -epsilon $epsilon -c $c -svdDecay $decay -svdIsboldDriver $isboldDriver -svdLearnRate $learnRate -svdMaxLearnRate $maxRate -nI $itemsRecommended -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol -coordFile $coordFile



                          done # ENd max learn
                          wait

                        done # Learn rate
                        wait
                      done # End bold
                      wait

                    done # End decay
                    wait
                  done # End iter
                  wait

                done # ENd neigh
                wait

              done # End kFactors
              wait
            done # Epsilon
            wait
          done # Alpha
          wait
        done # End c
        wait
	done # End repetition
	wait

	   #Our GeoBPRMF from ranksys
	   for repetition in 1 #2 3 4 5
        do
          for factor in 10 50 100
          do
            for bias_reg in 0
            do
              for regU in 0.001 #Regularization for items and users is the same
              do
                for maxDist in 1 4
                do
                  outputRecfile=$recommendationFolder/"$recPrefix"_"complete_"$countryCode"_""$city"_Fold"$fold"_RankSys_GeoBPRMF_nFact"$factor"_nIter"$BPRNumIter"_LearnR"$BPRLearnRate"_BiasR"$bias_reg"_RegU"$regU"_RegI"$regU"_MaxDist"$maxDist"_Rep"$repetition".txt
                  $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex false -rr GeoBPRMF -nI $itemsRecommended -n 20 -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol -kFactorizer $factor -nIFactorizer $BPRNumIter -svdLearnRate $BPRLearnRate -svdRegBias $bias_reg -svdRegUser $regU -svdRegItem $regU -coordFile $coordFile -maxDist $maxDist
                done
                wait
              done # Reg U
              wait
            done # Bias reg
            wait
          done # Factors
          wait
        done # Repetition
        wait

		

	    #PopGeoNN: popularity, knn and minimum distance combined
        for poiRecommender in PopGeoNN
        do
          for UBsimilarity in SetJaccardUserSimilarity #SetCosineUserSimilarity #VectorJaccardUserSimilarity VectorCosineUserSimilarity
          do
            for neighbours in 100
            do
              outputRecfile=$recommendationFolder/"$recPrefix"_"complete_"$countryCode"_""$city"_Fold"$fold"_ranksysPOI_"$poiRecommender"_UBSim_"$UBsimilarity"_k$neighbours.txt
              $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex true -rr $poiRecommender -rs $UBsimilarity -nI $itemsRecommended -n $neighbours -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol -coordFile $coordFile
            done
            wait
          done
          wait
        done
        wait


	
		#GeoCFCA (based on GeoSoCa)
        UBsimilarity=SetJaccardUserSimilarity
        kNeighbour=100
        alpha=0.5
        modes="ORIGINAL EXPERIMENTAL_SURVEY"

        for mode in $modes
        do
          outputRecfile=$recommendationFolder/"$recPrefix"_complete_"$countryCode"_"$city""$fold"_ranksysPOI_"GeoKNNCa"_"$mode"_"$UBsimilarity"_"$kNeighbour"_alpha"$alpha"_FREQUENCY_Catlvl3.txt
          $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile2 -tsf $testfile -cIndex false -rr "GeoKNNca" -coordFile $coordFile -n $kNeighbour -rs $UBsimilarity -aFactorizer $alpha -ff2 $realFeatureFile -nI $itemsRecommended -orf $outputRecfile -mode $mode -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol
        done
        wait

		#FMFMGM recommender
        for repetition in 1 #2 3 4 5
        do
          for alpha_FMFMGM in $alphas_FMFMGM
          do
            for theta_FMFMGM in $thetas_FMFMGM
            do
              for distance_FMFMGM in $distances_FMFMGM #
              do
                for iter_FMFMGM in $iters_FMFMGM
                do
                  for kfactor_FMFMGM in $kfactors_FMFMGM
                  do
                    for alpha2_FMFMGM in $alphas2_FMFMGM
                    do
                      for beta_FMFMGM in $betas_FMFMGM
                      do
                        for learningRate_FMFMGM in $learningRates_FMFMGM
                        do
                          for sigmoid_FMFMGM in $sigmoids_FMFMGM
                          do

                            outputRecfile=$recommendationFolder/"$recPrefix"_complete_"$countryCode"_"$city""$fold"_ranksysPOI_"FMFMGM"_alpha"$alpha_FMFMGM"_theta"$theta_FMFMGM"_dist"$distance_FMFMGM"_iter"$iter_FMFMGM"_factor"$kfactor_FMFMGM"_alpha2"$alpha2_FMFMGM"_beta"$beta_FMFMGM"_lRate"$learningRate_FMFMGM"_sigmoid"$sigmoid_FMFMGM"_FREQUENCY_Rep"$repetition".txt
                            $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile2 -tsf $testfile -cIndex false -rr "FMFMGM" -coordFile $coordFile -aFactorizer $alpha_FMFMGM -thetaFactorizer $theta_FMFMGM -maxDist $distance_FMFMGM -nIFactorizer $iter_FMFMGM -kFactorizer $kfactor_FMFMGM -aFactorizer2 $alpha2_FMFMGM -svdBeta $beta_FMFMGM -svdLearnRate $learningRate_FMFMGM -useSigmoid $sigmoid_FMFMGM -n 20 -nI $itemsRecommended -orf $outputRecfile -ff $featureFile -matchCat $city -fic $featureItemCol -ffc $featureCatCol


                          done # sigmoid
                          wait
                        done # learningRate
                        wait
                      done # beta
                      wait
                     done # alphas2
                     wait
                    done # kFactor
                    wait
                  done # iters
                  wait
                done # distance
                wait
              done # Theta
              wait
            done # alphas
            wait
          done # Repetition
          wait




      fi #If of matching cities with the country code

    done
    wait #Cities

  done
  wait #CountryCode

done
wait #Fold


: '
Evaluation part
'

for fold in 0 #1
do
  directory=$PathOfSplits/Fold$fold/complete_CountryCode
  for countryCode in $lstCountryCodes
  do
    for city in $cities
    do
      if [[ $city == "$countryCode"* ]] ; then
        echo "Working with country code $countryCode"
        echo "Working with city $city"

        trainfile=$directory/complete_"$countryCode"/"split_"complete_"$countryCode""__trainingAggr""$aggregateStrategy""$fold".dat
        testfile=$PathOfSplits/Fold$fold/$city/"split_""$city""__test""$fold".dat
        trainfileSingle=$PathOfSplits/Fold$fold/$city/"split_""$city""__trainingAggr""$aggregateStrategy""$fold".dat



        resultsFolder=$directory/complete_"$countryCode"/ResultRankSysFolder
        recommendedFolderReal=$directory/complete_"$countryCode"/$recommendedFolder
        mkdir -p $resultsFolder


        find $recommendedFolderReal/ -name "$recPrefix"* | while read recFile; do
          recFileName=$(basename "$recFile" .txt) #extension removed

          #matching a specific recommender
          if [[ $recFileName == *_* ]] ; then
            for evthreshold in 1 #we work with implicit data
            do
              outputResultfile=$resultsFolder/"$nonaccresultsPrefix"_EvTh"$evthreshold"_"$recFileName"_VersionUI"$userMin""$itemMin"_CutOff"$cutoffs".txt
              if [ ! -f "$outputResultfile" ] ; then
                $javaCommand $jvmMemory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $trainfileSingle -tsf $testfile -rf $recFile -ff $realFeatureFile -thr $evthreshold -rc $cutoffs -orf $outputResultfile -onlyAcc false
              fi

			: '
			# Only for Locals
            user_feature=L
            outputResultfile=$resultsFolder/"$nonaccresultsPrefix"_EvTh"$evthreshold"_"$recFileName"_Cutoff5-10"AggrTrNewPopEvLocals.txt"
            if [ ! -f "$outputResultfile" ]; then
              echo 'train file is ' $train_file
              $javaCommand $jvmMemory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $trainfileSingle -tsf $testfile -rf $recFile -ff $realFeatureFile -thr $evthreshold -rc $cutoffs -orf $outputResultfile -onlyAcc false -lcsEv true -compUF true -uff $PathOfSplits/Fold"$fold"/"$city"/"RepTrainMaxDifTourist"$maxDiffTime"MinDifBot"$minDiffTime"MinNumPrefBot"$minClosePrefBot"LocTouBot"UserAttributeFile.txt -ufs $user_feature
            fi

            # Only for Tourists
            user_feature=T
            outputResultfile=$resultsFolder/"$nonaccresultsPrefix"_EvTh"$evthreshold"_"$recFileName"_Cutoff5-10"AggrTrNewPopEvTourists.txt"
            if [ ! -f "$outputResultfile" ]; then
              echo 'train file is ' $train_file
              $javaCommand $jvmMemory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $trainfileSingle -tsf $testfile -rf $recFile -ff $realFeatureFile -thr $evthreshold -rc $cutoffs -orf $outputResultfile -onlyAcc false -lcsEv true -compUF true -uff $PathOfSplits/Fold"$fold"/"$city"/"RepTrainMaxDifTourist"$maxDiffTime"MinDifBot"$minDiffTime"MinNumPrefBot"$minClosePrefBot"LocTouBot"UserAttributeFile.txt -ufs $user_feature
	    	fi
		'

            done
            wait
          fi # end of matching recommender
        done #end of find
        wait
      fi #If of matching cities with the country code
    done
    wait #Cities
  done
  wait #CountryCode
done
wait #Fold

: '
  It is the same script as the other of IRENMF but in this case we execute with the evaluation that allows to return previous visited POIs

  Script for the paper "Challenges on evaluating venue recommendation approaches"
'

# !/bin/bash
# Necessary to execute with ./

# Global variables for JVM
JAR=target/CrossDomainPOIRecommendation-0.1.1-SNAPSHOT.jar
jvmMemory=-Xmx15G
ttpathfiles=TrainTest
similaritiesFolder=Sim
javaCommand=java

recPrefix=rec

# Nomenclature for Datasets
FoursqrName=Foursqr


# Extension of Files
extensionMap=_Mapping.txt
extensionCoords=_Coords.txt
extensionFeature=_Features.txt
extensionAggregated=_Aggregated.txt

# Paths of Original Datasets
FoursqrCheckingsOriginal=$ttpathfiles/dataset_TIST2015_Checkins.txt
FoursqrCitiesOriginal=$ttpathfiles/dataset_TIST2015_Cities.txt
FoursqrPOISOriginal=$ttpathfiles/dataset_TIST2015_POIs.txt

# cities selected to perform the experiments
cities="MX_MexicoCity BR_SaoPaulo RU_Moscow MY_KualaLumpur CL_Santiago ID_Jakarta TR_Istanbul JP_Tokyo"

# In this case we need the full path for matlab
fullPath="$(pwd)"
PathOfSplits=$fullPath/$ttpathfiles/CitiesSplit
aggregateStrategy=AVERAGE

recommendedFolder=RecommendationFolder
allneighbours="5 10 20 30 40 50 60 70 80 90 100"
itemsRecommended=100


# Variables for recommendation
itemsRecommended=100
featureFile=$ttpathfiles/NewItemIdsCountryCityJoin.txt
featureItemCol=0
featureCatCol=1

realFeatureFile=$ttpathfiles/POIS_Features.txt
cutoffs="5,10"

allKFactorizerRankSys="10 50 100"
allLambdaFactorizerRankSys="0.1 1 10"
allAlphaFactorizerRankSys="0.1 1 10 100"

complete_cities=complete_cities
fullpathMatlab="$(locate matlab | grep bin/matlab | head -1)"

# CAUTION. This command is used to locate the instalation of Matlab in your computer. If there is an error launching the script, substitute the variable pathMatlab with the full path of your matlab executable
pathMatlab="$(dirname $fullpathMatlab)"
pathIrenMF=$fullPath/IRenMFTestWithTrain

# Variables for IRenMF (paper of ExperimentalEvaluation. Most of these variables are updated in the configureFile)
K=100
Alpha=0.4
lambda1=0.015
lambda2=0.015
lambda3=1
clusters=50
GeoNN=10

allKIRENMF="100 50"
allAlphaIRENMF="0.4 0.6"
allLambda3IRENMF="1 0.1"



function obtainConfigureFile() #The arguments are the k, the alpha and the lambda3
{
  configureFileSimple=""
  if [ "$1" == "100" ] && [ "$2" == "0.4" ] && [ "$3" == "1" ] ; then
    configureFileSimple="configure00" #Configure 0
  fi

  if [ "$1" == "100" ] && [ "$2" == "0.4" ] && [ "$3" == "0.1" ] ; then
    configureFileSimple="configure01" #Configure 1
  fi

  if [ "$1" == "100" ] && [ "$2" == "0.6" ] && [ "$3" == "1" ] ; then
    configureFileSimple="configure02" #Configure 2
  fi

  if [ "$1" == "100" ] && [ "$2" == "0.6" ] && [ "$3" == "0.1" ] ; then
    configureFileSimple="configure03" #Configure 3
  fi

  if [ "$1" == "50" ] && [ "$2" == "0.4" ] && [ "$3" == "1" ] ; then
    configureFileSimple="configure04" #Configure 4
  fi

  if [ "$1" == "50" ] && [ "$2" == "0.4" ] && [ "$3" == "0.1" ] ; then
    configureFileSimple="configure05" #Configure 5
  fi

  if [ "$1" == "50" ] && [ "$2" == "0.6" ] && [ "$3" == "1" ] ; then
    configureFileSimple="configure06" #Configure 6
  fi

  if [ "$1" == "50" ] && [ "$2" == "0.6" ] && [ "$3" == "0.1" ] ; then
    configureFileSimple="configure07" #Configure 7
  fi
  echo "$configureFileSimple"
}




# First, obtain the Pois coordinates and the NN of each POI for each city

totalPoiCoordsFile=$ttpathfiles/POIS_Coords.txt
nnCities=7
for fold in 0 # 1
do

  rm $PathOfSplits/Fold$fold/$complete_cities/POIS_"$complete_cities""_""$GeoNN""NN".txt
  rm $PathOfSplits/Fold$fold/$complete_cities/POIS_"$complete_cities""__"Coords.txt

  # For the single domain
  subcities="MX_MexicoCity BR_SaoPaulo RU_Moscow MY_KualaLumpur CL_Santiago JP_Tokyo ID_Jakarta TR_Istanbul"
  for city in $subcities
  do
    if [ $city != "complete_cities" ] && [ $city != "complete" ] ; then
      resultFileNNCityFile=$PathOfSplits/Fold$fold/$city/POIS_"$city"_"$GeoNN"NN.txt
      poisCoordsOfCityFile=$PathOfSplits/Fold$fold/$city/POIS_"$city"_"$extensionCoords"
      trainfile=$PathOfSplits/Fold$fold/$city/"split_""$city""__trainingAggr""$aggregateStrategy""$fold".dat

      if [ ! -f $resultFileNNCityFile ]; then
        $javaCommand $jvmMemory -jar $JAR -o printClosestPOIs -trf $trainfile $totalPoiCoordsFile $resultFileNNCityFile $poisCoordsOfCityFile $GeoNN
      fi

      # For the cross domain of the most popular cities
      cat $resultFileNNCityFile >> $PathOfSplits/Fold$fold/$complete_cities/POIS_"$complete_cities""_""$GeoNN""NN".txt
      cat $poisCoordsOfCityFile >> $PathOfSplits/Fold$fold/$complete_cities/POIS_"$complete_cities""__"Coords.txt
    fi
  done
  wait # End cities


  # For the cross domain of the nn cities -> obtain the NN closest pois for each poi and the coords of the training file

  for city in $subcities
  do
    if [ $city != "complete_cities" ] && [ $city != "complete" ] ; then
      directory=$PathOfSplits/Fold$fold/complete_NN"$nnCities"Cities/NCities"$nnCities"_$city
      resultFileNNCityFile=$directory/POIS_NCities"$nnCities"_"$city"_"$GeoNN"NN.txt
      poisCoordsOfCityFile=$directory/POIS_NCities"$nnCities"_"$city"_"$extensionCoords"
      trainfile=$directory/"split_complete_"NCities"$nnCities"_"$city""__trainingAggr""$aggregateStrategy""$fold".dat

      if [ ! -f $resultFileNNCityFile ]; then
        $javaCommand $jvmMemory -jar $JAR -o printClosestPOIs -trf $trainfile $totalPoiCoordsFile $resultFileNNCityFile $poisCoordsOfCityFile $GeoNN
      fi

    fi
  done
  wait # End cities

  #For the cross domain of the cities of the countries

  countryCodesToAnalize="MX CL JP"
  for countryCode in $countryCodesToAnalize
  do
    directory=$PathOfSplits/Fold$fold/complete_CountryCode/complete_"$countryCode"
    resultFileNNCountryCode=$directory/POIS_Country_"$countryCode"_"$GeoNN"NN.txt
    poisCoordsCountryCode=$directory/POIS_Country_"$countryCode"_"$extensionCoords"
    trainfile=$directory/"split_complete_"$countryCode"__trainingAggr""$aggregateStrategy""$fold".dat

    if [ ! -f $resultFileNNCountryCode ]; then
      $javaCommand $jvmMemory -jar $JAR -o printClosestPOIs -trf $trainfile $totalPoiCoordsFile $resultFileNNCountryCode $poisCoordsCountryCode $GeoNN
    fi

  done
  wait #end country codes


done # End fold
wait


# For Single domain

for fold in 0 # 1
do
  # For each fold
  for city in $cities
  do
    if [ $city != "complete_cities" ] && [ $city != "complete" ] ; then
      trainfile=$PathOfSplits/Fold$fold/$city/"split_""$city""__trainingAggr""$aggregateStrategy""$fold".dat
      trainfile2=$PathOfSplits/Fold$fold/$city/"split_""$city""__trainingAggr""SUM""$fold".dat
      testfile=$PathOfSplits/Fold$fold/$city/"split_""$city""__testWithTrain""$fold".dat


      totalPoiCoordsFile=$ttpathfiles/POIS"$extensionCoords"

      # Coordinates only for this city
      poisCoordsOfCityFile=$PathOfSplits/Fold$fold/$city/POIS_"$city""_""$extensionCoords"

      resultFileNNCityFile=$PathOfSplits/Fold$fold/$city/POIS_"$city""_""$GeoNN""NN".txt

      mkdir -p $PathOfSplits/Fold$fold/$city/RecommendationFolder

      for KIRENMF in $allKIRENMF
      do
        for alphaIRENMF in $allAlphaIRENMF
        do
          for lambda3IRENMF in $allLambda3IRENMF
          do
            configureFileSimple=$(obtainConfigureFile "$KIRENMF" "$alphaIRENMF" "$lambda3IRENMF")

            echo "Parameters K=$KIRENMF alpha=$alphaIRENMF lambda3=$lambda3IRENMF"
            echo "City " $city " working with configure " $configureFileSimple
            configureFileToSelect="configure"_"Fold""$fold"_"$city"_"K"$KIRENMF"_Alpha""$alphaIRENMF""_L1_"$lambda1"_L2_"$lambda2"_L3_"$lambda3IRENMF"_GeoNN"$GeoNN"_clusters"$clusters
            resultFile=$PathOfSplits/Fold$fold/$city/RecommendationFolder/"$recPrefix"_"$city""$fold"_IRENMF_$configureFileToSelect"TestWithRatingsTrain"
            clusterFile=$PathOfSplits/Fold$fold/$city/Clusters$clusters"_"
            rm $clusterFile"_clusters".mat


            if [ ! -f *"$resultFile"* ]; then

              $pathMatlab/./matlab -nodisplay -nodesktop -r "cd '$pathIrenMF/'; ItemGroupPOI('$configureFileSimple', '$trainfile', '$poisCoordsOfCityFile', '$resultFileNNCityFile', '$testfile', '$resultFileNNCityFile', '$resultFile', '$clusterFile'); quit"
            fi


            resultFile=$PathOfSplits/Fold$fold/$city/RecommendationFolder/"$recPrefix"_"$city""$fold"_IRENMF_FREQUENCY_$configureFileToSelect"TestWithRatingsTrain"
            clusterFile=$PathOfSplits/Fold$fold/$city/Clusters$clusters"_"
            rm $clusterFile"_clusters".mat

            if [ ! -f *"$resultFile"* ]; then

              echo $trainfile2
              $pathMatlab/./matlab -nodisplay -nodesktop -r "cd '$pathIrenMF/'; ItemGroupPOI('$configureFileSimple', '$trainfile2', '$poisCoordsOfCityFile', '$resultFileNNCityFile', '$testfile', '$resultFileNNCityFile', '$resultFile', '$clusterFile'); quit"
            fi

            echo "Finished $configureFileSimple"
          done
          wait # End lambda
        done
        wait # End alpha IrenMF
      done
      wait # End KIRENMF
    fi
  done # End Cities
  wait
done  # End Folds
wait




: '
Evaluation part
'

for city in $cities
do
  nonaccresultsPrefix=naeval
  if [ $city != "complete_cities" ] && [ $city != "complete" ] ; then
    # For each fold
    for fold in 0 # 1
    do

      trainfile=$PathOfSplits/Fold$fold/$city/"split_""$city""__trainingAggr""$aggregateStrategy""$fold".dat
      testfile=$PathOfSplits/Fold$fold/$city/"split_""$city""__testWithTrain""$fold".dat

      resultsFolder=$PathOfSplits/Fold$fold/$city/ResultRankSysFolder

      mkdir -p $resultsFolder

      # Find all recommenders and compute the ranksysEvaluation

      recommendedFolderReal=$PathOfSplits/Fold$fold/$city/$recommendedFolder

      find $recommendedFolderReal/ -name "$recPrefix"* | while read recFile; do
        recFileName=$(basename "$recFile" .txt) #extension removed
        if [[ $recFileName == *FREQUENCY* ]]; then
          trainfile=$PathOfSplits/Fold$fold/$city/"split_""$city""__trainingAggr""SUM""$fold".dat
        fi

        if [[ $recFileName == *TestWithRatingsTrain* ]]; then
          if [[ $recFileName == *IRENMF* ]]; then
            for evthreshold in 1 # We work with implicit data
            do
              outputResultfile=$resultsFolder/"$nonaccresultsPrefix"_EvTh"$evthreshold"_"$recFileName"_testWithTrainLCSEVCutOffs"$cutoffs".txt
              if [ ! -f "$outputResultfile" ]; then
                echo 'training file is ' $trainfile
                $javaCommand $jvmMemory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $trainfile -tsf $testfile -rf $recFile -ff $realFeatureFile -thr $evthreshold -rc $cutoffs -orf $outputResultfile -lcsEv true
              fi
            done # End evTh
            wait
          fi
        fi
      done # End find
      wait
    done # End Folds
    wait
  fi
done # End Cities
wait

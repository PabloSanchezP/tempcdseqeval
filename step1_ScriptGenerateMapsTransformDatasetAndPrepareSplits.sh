#!/bin/bash

: '
  First script that downloads and transforms the original dataset to long ids. It also splits the data and obtains the checkins per city
  and the temporal training-test split stated in the paper

  Necessary for paper "On the effects of aggregation strategies in different groups of users in venue recommendation"

'

# Global variables for indicating where we will store the destination files and about the JVM machine
JAR=target/CrossDomainPOIRecommendation-0.1.1-SNAPSHOT.jar
jvmMemory=-Xmx12G
ttpathfiles=TrainTest
similaritiesFolder=Sim
javaCommand=java

# Nomenclature for Dataset
FoursqrName=Foursqr


# Extension of Files
extensionMap=_Mapping.txt
extensionCoords=_Coords.txt
extensionFeature=_Features.txt
extensionAggregated=_Aggregated.txt

# Paths of Original Datasets
FoursqrCheckingsOriginal=$ttpathfiles/dataset_TIST2015_Checkins.txt
FoursqrCitiesOriginal=$ttpathfiles/dataset_TIST2015_Cities.txt
FoursqrPOISOriginal=$ttpathfiles/dataset_TIST2015_POIs.txt


# Parameters for the aggregate strategy
KCore=2
PathOfSplits=$ttpathfiles/CitiesSplit
aggregateStrategy=AVERAGE
aggregateStrategyTime=LAST

# Creation of the JAR file

mvn install:install-file -Dfile=libs/rankfusion.jar -DgroupId=rankfusion -DartifactId=rankfusion -Dversion=1.0 -Dpackaging=jar -DgeneratePom=true 
mvn install

# Download the full dataset and move the original files
wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=0BwrgZ-IdrTotZ0U0ZER2ejI3VVk' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=0BwrgZ-IdrTotZ0U0ZER2ejI3VVk" -O dataset_TIST2015.zip && rm -rf /tmp/cookies.txt
unzip dataset_TIST2015.zip

mkdir -p $ttpathfiles
mv dataset_TIST2015_Checkins.txt $ttpathfiles/
mv dataset_TIST2015_Cities.txt $ttpathfiles/
mv dataset_TIST2015_POIs.txt $ttpathfiles/




# Cities that we will use in the experiments. Complete_cities refers to all these 8 cities combined
cities="MX_MexicoCity BR_SaoPaulo RU_Moscow MY_KualaLumpur CL_Santiago JP_Tokyo ID_Jakarta TR_Istanbul complete_cities"
fileOfCities=cityFiles.txt


mkdir -p $ttpathfiles/Cities
mkdir -p $ttpathfiles/Countries

# If city and countries directories are empty, we need to separate the full dataset between the cities and the countries. All countries will contain all the checkins of the cities of that country
# First, the checkins of each city separately
if [ ! "$(ls -A $ttpathfiles/Cities)" ]; then
  $javaCommand $jvmMemory -jar $JAR -o FoursqrCheckinsPerCity -trf $FoursqrCheckingsOriginal $FoursqrPOISOriginal $FoursqrCitiesOriginal $ttpathfiles/Cities/ $ttpathfiles/ItemCountryCityJoin.txt
fi
echo "Created cities folder"

# Second, the checkins of the countries
if [ ! "$(ls -A $ttpathfiles/Countries)" ]; then
  $javaCommand $jvmMemory -jar $JAR -o FoursqrCheckinsPerCountry -trf $FoursqrCheckingsOriginal $FoursqrPOISOriginal $FoursqrCitiesOriginal $ttpathfiles/Countries/
fi
echo "Created countries folder"


# Create new map for users and items (map between old ids and new ids in order to reduce the space used by the datasets)
if [ ! -f $ttpathfiles/Users"$extensionMap" ]; then
  $javaCommand $jvmMemory -jar $JAR -o FoursqrObtainMapUsersItems -trf $FoursqrCheckingsOriginal -UMapping $ttpathfiles/Users"$extensionMap" -IMapping $ttpathfiles/POIS"$extensionMap"
fi
echo "Files of users and items mapping computation completed"

if [ ! -f $ttpathfiles/NewItemIdsCountryCityJoin.txt ]; then
  $javaCommand $jvmMemory -jar $JAR -o FoursqrNewPoisIdsPerCity -trf $ttpathfiles/ItemCountryCityJoin.txt -IMapping $ttpathfiles/POIS"$extensionMap" $ttpathfiles/NewItemIdsCountryCityJoin.txt
fi
echo "Created file of new id poi -> CountryCode and Id"

# Create mapping of categories (string of category to new id)
if [ ! -f $ttpathfiles/Categories"$extensionMap" ]; then
  $javaCommand $jvmMemory -jar $JAR -o FoursqrObtainMapCategories -trf $FoursqrPOISOriginal -CMapping $ttpathfiles/Categories"$extensionMap"
fi
echo "File categories mapping computation completed"


# Create coordinates files of the POIs
if [ ! -f $ttpathfiles/POIS"$extensionCoords" ]; then
  $javaCommand $jvmMemory -jar $JAR -o FoursqrObtainPoisCoords -trf $FoursqrPOISOriginal -IMapping $ttpathfiles/POIS"$extensionMap" -coordFile $ttpathfiles/POIS"$extensionCoords"
fi
echo "File of coordinates computation completed"


# Check if POIs features file exist
if [ ! -f $ttpathfiles/POIS"$extensionFeature" ]; then
  $javaCommand $jvmMemory -jar $JAR -o FoursqrObtainPoisFeatures -trf $FoursqrPOISOriginal -IMapping $ttpathfiles/POIS"$extensionMap" -CMapping $ttpathfiles/Categories"$extensionMap" -sfs $ttpathfiles/POIS"$extensionFeature"
fi
echo "File of POI features computation completed"


# Generate the Full dataset with ids changed and the new timestamps
if [ ! -f $ttpathfiles/"FullCheckingsTransformedWithTimeStamps".txt ]; then
  $javaCommand $jvmMemory -jar $JAR -o generateNewCheckingFileWithTimeStamps -trf $FoursqrCheckingsOriginal -IMapping $ttpathfiles/POIS"$extensionMap" -UMapping $ttpathfiles/Users"$extensionMap" -newDataset $ttpathfiles/"FullCheckingsTransformedWithTimeStamps".txt
fi
echo "Completed file transformation with timestamps"


# Create a file with all the cities considered
rm $ttpathfiles/$fileOfCities
touch $ttpathfiles/$fileOfCities
for cityFile in $cities
do
  if [ $cityFile != "complete_cities" ] && [ $cityFile != "complete" ] ; then
    echo $cityFile >> $ttpathfiles/$fileOfCities
  fi
done
echo "File of cities created"


# Create the splits
mkdir -p $PathOfSplits
if [ ! "$(ls -A $PathOfSplits/)" ]; then
  $javaCommand $jvmMemory -jar $JAR -o FoursqrGenerateSplits -trf $ttpathfiles/"FullCheckingsTransformedWithTimeStamps".txt $ttpathfiles/NewItemIdsCountryCityJoin.txt false $ttpathfiles/Users"$extensionMap" $ttpathfiles/POIS"$extensionMap" $ttpathfiles/$fileOfCities $KCore $PathOfSplits/
fi
echo "Finished split creation of constraints"



# Move the generated files
for fold in 0 # 1
do
  mkdir -p $PathOfSplits/Fold$fold
  mkdir -p $PathOfSplits/Fold$fold/complete/
  mkdir -p $PathOfSplits/Fold$fold/complete_cities/


  if [ ! -f $PathOfSplits/Fold$fold/complete_cities/"split_complete_cities__training""$fold".dat ]; then
    mv $PathOfSplits/"split_complete_cities__training"$fold.dat $PathOfSplits/Fold$fold/complete_cities
  fi

  if [ ! -f $PathOfSplits/Fold$fold/complete_cities/"split_complete_cities__testWithTrain""$fold".dat ]; then
    mv $PathOfSplits/"split_complete_cities__testWithTrain"$fold.dat $PathOfSplits/Fold$fold/complete_cities
  fi

  # Generate the testWithout repetitions
  $javaCommand $jvmMemory -jar $JAR -o GenerateTestWithoutTrain -trf $PathOfSplits/Fold$fold/complete_cities/"split_complete_cities__training""$fold".dat $PathOfSplits/Fold$fold/complete_cities/"split_complete_cities__testWithTrain""$fold".dat $PathOfSplits/Fold$fold/complete_cities/"split_complete_cities__test""$fold".dat


  if [ ! -f $PathOfSplits/Fold$fold/complete/"split_complete__training""$fold".dat ]; then
    mv $PathOfSplits/"split_complete__training"$fold.dat $PathOfSplits/Fold$fold/complete
  fi

  if [ ! -f $PathOfSplits/Fold$fold/complete/"split_complete__testWithTrain""$fold".dat ]; then
    mv $PathOfSplits/"split_complete__testWithTrain"$fold.dat $PathOfSplits/Fold$fold/complete
  fi

  # Generate the testWithout repetitions
  $javaCommand $jvmMemory -jar $JAR -o GenerateTestWithoutTrain -trf $PathOfSplits/Fold$fold/complete/"split_complete__training""$fold".dat $PathOfSplits/Fold$fold/complete/"split_complete__testWithTrain""$fold".dat $PathOfSplits/Fold$fold/complete/"split_complete__test""$fold".dat



  # Move and create all the splits and aggregate for the training
  for city in $cities
  do
    mkdir -p $PathOfSplits/Fold$fold/$city

    if [ ! -f $PathOfSplits/Fold$fold/$city/"split_"$city"__training""$fold".dat ]; then
      mv $PathOfSplits/"split_"$city"__training"$fold.dat $PathOfSplits/Fold$fold/$city
    fi

    if [ ! -f $PathOfSplits/Fold$fold/$city/"split_"$city"__testWithTrain""$fold".dat ]; then
      mv $PathOfSplits/"split_"$city"__testWithTrain"$fold.dat $PathOfSplits/Fold$fold/$city
    fi

    $javaCommand $jvmMemory -jar $JAR -o GenerateTestWithoutTrain -trf $PathOfSplits/Fold$fold/$city/"split_"$city"__training""$fold".dat $PathOfSplits/Fold$fold/$city/"split_"$city"__testWithTrain""$fold".dat $PathOfSplits/Fold$fold/$city/"split_"$city"__test""$fold".dat


    # Some recommenders as IRENMF and AvgDis may benefit for aggregating all the repetitions
    aggregateStrategy2=SUM
    if [ ! -f $PathOfSplits/Fold$fold/$city/"split_"$city"__trainingAggr"$aggregateStrategy2""$fold.dat ]; then
      $javaCommand $jvmMemory -jar $JAR -o AggregateWithWrapper -trf $PathOfSplits/Fold$fold/$city/"split_"$city"__training"$fold.dat -wStrat $aggregateStrategy2 -newDataset $PathOfSplits/Fold$fold/$city/"split_"$city"__trainingAggr"$aggregateStrategy2""$fold.dat
    fi

    if [ ! -f $PathOfSplits/Fold$fold/$city/"split_"$city"__trainingAggr"$aggregateStrategy""$fold.dat ]; then
      $javaCommand $jvmMemory -jar $JAR -o AggregateWithWrapper -trf $PathOfSplits/Fold$fold/$city/"split_"$city"__training"$fold.dat -wStrat $aggregateStrategy -newDataset $PathOfSplits/Fold$fold/$city/"split_"$city"__trainingAggr"$aggregateStrategy""$fold.dat
    fi

    if [ ! -f $PathOfSplits/Fold$fold/$city/"split_"$city"__trainingAggrTime"$aggregateStrategyTime""$fold.dat ]; then
      $javaCommand $jvmMemory -jar $JAR -o AggregateWithWrapperTimeStamps -trf $PathOfSplits/Fold$fold/$city/"split_"$city"__training"$fold.dat -wStratTime $aggregateStrategyTime -newDataset $PathOfSplits/Fold$fold/$city/"split_"$city"__trainingAggrTime"$aggregateStrategyTime""$fold.dat
    fi

  done # End cities
  wait

  # Country code
  countryCodesToAnalize="CL JP BR MX ID MY RU TR"
  # Obtain the cities per country


  mkdir -p $PathOfSplits/Fold$fold/complete_CountryCode
  for countryCode in $countryCodesToAnalize
  do
    directory=$PathOfSplits/Fold$fold/complete_CountryCode/complete_"$countryCode"
    mkdir -p $directory
    if [ ! "$(ls -A $directory)" ]; then
      # Take the complete partition and filter the train and the test.

      # Filter train
      $javaCommand $jvmMemory -jar $JAR -o FoursqrFilterFileByCountryCode -trf $PathOfSplits/Fold$fold/complete/"split_complete__training"$fold.dat $countryCode $ttpathfiles/NewItemIdsCountryCityJoin.txt $directory/split_complete_"$countryCode"__training"$fold".dat

      # Filter test
      $javaCommand $jvmMemory -jar $JAR -o FoursqrFilterFileByCountryCode -trf $PathOfSplits/Fold$fold/complete/"split_complete__test"$fold.dat $countryCode $ttpathfiles/NewItemIdsCountryCityJoin.txt $directory/split_complete_"$countryCode"__test"$fold".dat


      # Aggregate the train set
      if [ ! -f $directory/"split_complete_"$countryCode"__trainingAggr"$aggregateStrategy""$fold.dat ]; then
        $javaCommand $jvmMemory -jar $JAR -o AggregateWithWrapper -trf $directory/split_complete_"$countryCode"__training"$fold".dat -wStrat $aggregateStrategy -newDataset $directory/split_complete_"$countryCode"__trainingAggr$aggregateStrategy""$fold.dat
      fi

      mkdir -p $directory/RecommendationFolder

      mkdir -p $directory/ResultRankSysFolder


      aggregateStrategy2=SUM
      if [ ! -f $directory/"split_complete_"$countryCode"__trainingAggr"$aggregateStrategy2""$fold.dat ]; then
        $javaCommand $jvmMemory -jar $JAR -o AggregateWithWrapper -trf $directory/split_complete_"$countryCode"__training"$fold".dat -wStrat $aggregateStrategy2 -newDataset $directory/split_complete_"$countryCode"__trainingAggr$aggregateStrategy2""$fold.dat
      fi


    fi

  done
  wait #End countryCode



done # End fold
wait


# Obtain the nearest cities of each city (7 nearest cities)

# First, obtain a secondary file of the midpoint of each city
nnCities=7
if [ ! -f $ttpathfiles/MidPointCities.txt ] ; then
  echo "Computing the midpoint of each city"
  $javaCommand $jvmMemory -jar $JAR -o FoursqrObtainCityCenterByPOIs -trf $ttpathfiles/POIS_Coords.txt $ttpathfiles/NewItemIdsCountryCityJoin.txt $ttpathfiles/MidPointCities.txt
fi

# Obtain the nearest cities of each city
if [ ! -f $ttpathfiles/NN"$nnCities"Cities.txt ] ; then
  echo "Computing the nearest cities of each city"
  $javaCommand $jvmMemory -jar $JAR -o NearestCities -trf $ttpathfiles/MidPointCities.txt $nnCities $ttpathfiles/NN"$nnCities"Cities.txt
fi


# Obtain the training/test of the nearest cities
nnCities=7
subcities="MX_MexicoCity BR_SaoPaulo RU_Moscow MY_KualaLumpur CL_Santiago JP_Tokyo ID_Jakarta TR_Istanbul"
for fold in 0 # 1
do
  mkdir -p $PathOfSplits/Fold$fold/complete_NN"$nnCities"Cities

  for city in $subcities
  do
    directory=$PathOfSplits/Fold$fold/complete_NN"$nnCities"Cities/NCities"$nnCities"_"$city"
    mkdir -p $directory

    if [ ! "$(ls -A $directory)" ]; then

      # Filter training
      $javaCommand $jvmMemory -jar $JAR -o FoursqrFilterFileByNCitiesOfCity -trf $PathOfSplits/Fold$fold/complete/"split_complete__training"$fold.dat $city $ttpathfiles/NN"$nnCities"Cities.txt $nnCities $ttpathfiles/NewItemIdsCountryCityJoin.txt $directory/split_complete_NCities"$nnCities"_"$city"__training"$fold".dat

      # Filter test (the test will not be used)
      $javaCommand $jvmMemory -jar $JAR -o FoursqrFilterFileByNCitiesOfCity -trf $PathOfSplits/Fold$fold/complete/"split_complete__test"$fold.dat $city $ttpathfiles/NN"$nnCities"Cities.txt $nnCities $ttpathfiles/NewItemIdsCountryCityJoin.txt $directory/split_complete_NCities"$nnCities"_"$city"__test"$fold".dat

      # Aggregate the training set
      if [ ! -f $directory/"split_complete_"NCities"$nnCities"_"$city""__trainingAggr"$aggregateStrategy""$fold.dat ]; then
        $javaCommand $jvmMemory -jar $JAR -o AggregateWithWrapper -trf $directory/split_complete_NCities"$nnCities"_"$city"__training"$fold".dat -wStrat $aggregateStrategy -newDataset $directory/"split_complete_"NCities"$nnCities"_"$city""__trainingAggr"$aggregateStrategy""$fold.dat
      fi

      aggregateStrategy2=SUM
      if [ ! -f $directory/"split_complete_"NCities"$nnCities"_"$city""__trainingAggr"$aggregateStrategy2""$fold.dat ]; then
        $javaCommand $jvmMemory -jar $JAR -o AggregateWithWrapper -trf $directory/split_complete_NCities"$nnCities"_"$city"__training"$fold".dat -wStrat $aggregateStrategy2 -newDataset $directory/"split_complete_"NCities"$nnCities"_"$city""__trainingAggr"$aggregateStrategy2""$fold.dat
      fi

      mkdir -p $directory/RecommendationFolder

      mkdir -p $directory/ResultRankSysFolder

    fi

  done
  wait # End Cities
done
wait # End fold


if [ ! -d "MyMediaLite-3.11" ]; then
  wget "http://mymedialite.net/download/MyMediaLite-3.11.tar.gz"
  tar zxvf MyMediaLite-3.11.tar.gz
fi

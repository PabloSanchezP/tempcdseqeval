# !/bin/sh

: '
	This script allows us to obtain a summary file for each evaluation strategy for all the recommenders executed. The summary files will contain the results obtained by the recommenders in some evaluation metrics.

	The format of the output files are: fullPath \t name_rec_file \t fold \t evTh \t name of the metric \t result of the metric
'

# Cities
cities="MX_MexicoCity BR_SaoPaulo RU_Moscow MY_KualaLumpur CL_Santiago JP_Tokyo ID_Jakarta TR_Istanbul"

recommendedFolder=RecommendationFolder
allneighbours="5 10 20 30 40 50 60 70 80 90 100"
itemsRecommended=100


# Variables for recommendation
itemsRecommended=100
featureFile=$ttpathfiles/NewItemIdsCountryCityJoin.txt
featureItemCol=0
featureCatCol=1


complete_cities=complete_cities
nonaccresultsPrefix=naeval



# SingleDomain testWithTrain
for fold in 0 # 1
do
  summaryFileResultsRanksys=summaryFileResultsRanksysSingleDomaintestWithTrain"$fold".dat
  rm $summaryFileResultsRanksys
  for city in $cities
  do
    resultsFolder=TrainTest/CitiesSplit/Fold$fold/$city/ResultRankSysFolder
    for evthreshold in 1
    do
      find $resultsFolder/ -name "$nonaccresultsPrefix"_EvTh"$evthreshold"*"$city"*"$fold"*"testWithTrain"* | while read resultFile; do
        resultFileName=$(basename "$resultFile" .txt) #extension removed
        awk -v FILE=$summaryFileResultsRanksys -v TH=$evthreshold -v RNAME=$resultFileName -v FOLD="Fold"$fold 'BEGIN { RS = "\n" } ; { print FILENAME"\t"RNAME"\t"FOLD"\t"TH"\t"$0 >> FILE }' $resultFile
      done # End find
      wait
    done # End EvTh
    wait
  done # End City
  wait
done # End Fold
wait


# SingleDomain simple
for fold in 0 # 1
do
  summaryFileResultsRanksys=summaryFileResultsRanksysSingleDomain"$fold".dat
  rm $summaryFileResultsRanksys
  for city in $cities
  do
    resultsFolder=TrainTest/CitiesSplit/Fold$fold/$city/ResultRankSysFolder
    for evthreshold in 1
    do
      find $resultsFolder/ -name "$nonaccresultsPrefix"_EvTh"$evthreshold"*"$city"*"$fold"* | while read resultFile; do
        resultFileName=$(basename "$resultFile" .txt) #extension removed
        awk -v FILE=$summaryFileResultsRanksys -v TH=$evthreshold -v RNAME=$resultFileName -v FOLD="Fold"$fold 'BEGIN { RS = "\n" } ; { print FILENAME"\t"RNAME"\t"FOLD"\t"TH"\t"$0 >> FILE }' $resultFile
      done # End find
      wait
    done # End EvTh
    wait
  done # End City
  wait
done # End Fold
wait

#Cross domain cities of country
lstCountryCodes="MX CL JP"
for fold in 0 #1
do
	summaryFileResultsRanksys=summaryFileResultsRanksysCrossDomainCountryCitiesFold"$fold".dat
	rm $summaryFileResultsRanksys
	for countryCode in $lstCountryCodes
	do
		for city in $cities
		do
			if [[ $city == "$countryCode"* ]] ; then
				resultsFolder=TrainTest/CitiesSplit/Fold$fold/complete_CountryCode/complete_"$countryCode"/ResultRankSysFolder
				for evthreshold in 1
				do

						find $resultsFolder/ -name "$nonaccresultsPrefix"_EvTh"$evthreshold"*"$city"*"$fold"* | while read resultFile; do
							resultFileName=$(basename "$resultFile" .txt) #extension removed
							awk -v FILE=$summaryFileResultsRanksys -v TH=$evthreshold -v RNAME=$resultFileName -v FOLD="Fold"$fold 'BEGIN { RS = "\n" } ; { print FILENAME"\t"RNAME"\t"FOLD"\t"TH"\t"$0 >> FILE }' $resultFile
						done
				done
				wait #End evTh
			fi #End if country code
		done
		wait #End cities
	done
	wait #End countryCode
done
wait #End Fold


# Cross domain NN cities
nnCities=7
for fold in 0 # 1
do
  summaryFileResultsRanksys=summaryFileResultsRanksysCrossDomainNN"$nnCities"Fold"$fold".dat
  rm $summaryFileResultsRanksys
  for city in $cities
  do
    resultsFolder=TrainTest/CitiesSplit/Fold$fold/complete_NN"$nnCities"Cities/NCities"$nnCities"_"$city"/ResultRankSysFolder
    for evthreshold in 1
    do
      find $resultsFolder/ -name "$nonaccresultsPrefix"_EvTh"$evthreshold"*"$city"*_"$fold"* | while read resultFile; do
        resultFileName=$(basename "$resultFile" .txt) #extension removed
        awk -v FILE=$summaryFileResultsRanksys -v TH=$evthreshold -v RNAME=$resultFileName -v FOLD="Fold"$fold 'BEGIN { RS = "\n" } ; { print FILENAME"\t"RNAME"\t"FOLD"\t"TH"\t"$0 >> FILE }' $resultFile
      done # End find
      wait
    done # End EvTh
    wait
  done # End City
  wait
done # End Fold
wait




# Cross domain most popular cities
for fold in 0 # 1
do
  summaryFileResultsRanksys=summaryFileResultsRanksysCrossDomainIndependentCities"$fold".dat
  rm $summaryFileResultsRanksys
  resultsFolder=TrainTest/CitiesSplit/Fold$fold/$complete_cities/ResultRankSysFolder
  for evthreshold in 1
  do
    find $resultsFolder/ -name "$nonaccresultsPrefix"_EvTh"$evthreshold"*"$fold"* | while read resultFile; do
      resultFileName=$(basename "$resultFile" .txt) #extension removed
      awk -v FILE=$summaryFileResultsRanksys -v TH=$evthreshold -v RNAME=$resultFileName -v FOLD="Fold"$fold 'BEGIN { RS = "\n" } ; { print FILENAME"\t"RNAME"\t"FOLD"\t"TH"\t"$0 >> FILE }' $resultFile
    done # End find
    wait
  done # End EvTh
  wait
done # End Fold
wait
